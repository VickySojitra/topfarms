USE [Topfarms]
GO
/****** Object:  StoredProcedure [dbo].[VisitPurpose_SelectAll]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[VisitPurpose_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[Transactions_Upsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Transactions_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[Transactions_Delete]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Transactions_Delete]
GO
/****** Object:  StoredProcedure [dbo].[Transactions_ById]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Transactions_ById]
GO
/****** Object:  StoredProcedure [dbo].[Transactions_All]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Transactions_All]
GO
/****** Object:  StoredProcedure [dbo].[TradersInsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[TradersInsert]
GO
/****** Object:  StoredProcedure [dbo].[Traders_Upsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Traders_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[Traders_Logout]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Traders_Logout]
GO
/****** Object:  StoredProcedure [dbo].[Traders_Login]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Traders_Login]
GO
/****** Object:  StoredProcedure [dbo].[Traders_Delete]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Traders_Delete]
GO
/****** Object:  StoredProcedure [dbo].[Traders_ChangePassword]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Traders_ChangePassword]
GO
/****** Object:  StoredProcedure [dbo].[Traders_ById]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Traders_ById]
GO
/****** Object:  StoredProcedure [dbo].[Traders_All]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Traders_All]
GO
/****** Object:  StoredProcedure [dbo].[ShippingUpsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[ShippingUpsert]
GO
/****** Object:  StoredProcedure [dbo].[ShippingSelectAll]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[ShippingSelectAll]
GO
/****** Object:  StoredProcedure [dbo].[ShippingRateSelectByID]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[ShippingRateSelectByID]
GO
/****** Object:  StoredProcedure [dbo].[Products_Upsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Products_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[Products_FeaturedUnfeature]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Products_FeaturedUnfeature]
GO
/****** Object:  StoredProcedure [dbo].[Products_Delete]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Products_Delete]
GO
/****** Object:  StoredProcedure [dbo].[Products_ById]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Products_ById]
GO
/****** Object:  StoredProcedure [dbo].[Products_All]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Products_All]
GO
/****** Object:  StoredProcedure [dbo].[ProductCategoryUpdate]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductCategoryUpdate]
GO
/****** Object:  StoredProcedure [dbo].[ProductCategorySelectAll]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductCategorySelectAll]
GO
/****** Object:  StoredProcedure [dbo].[ProductCategorySelect]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductCategorySelect]
GO
/****** Object:  StoredProcedure [dbo].[ProductCategoryInsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[ProductCategoryInsert]
GO
/****** Object:  StoredProcedure [dbo].[OrderUpsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[OrderUpsert]
GO
/****** Object:  StoredProcedure [dbo].[OrdersSelectAll]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[OrdersSelectAll]
GO
/****** Object:  StoredProcedure [dbo].[OrderSelect]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[OrderSelect]
GO
/****** Object:  StoredProcedure [dbo].[OrderCommentUpsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[OrderCommentUpsert]
GO
/****** Object:  StoredProcedure [dbo].[OrderCommentSelectAll]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[OrderCommentSelectAll]
GO
/****** Object:  StoredProcedure [dbo].[LookupStatus_Upsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupStatus_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[LookupStatus_Delete]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupStatus_Delete]
GO
/****** Object:  StoredProcedure [dbo].[LookupStatus_ById]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupStatus_ById]
GO
/****** Object:  StoredProcedure [dbo].[LookupStatus_All]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupStatus_All]
GO
/****** Object:  StoredProcedure [dbo].[LookupState_Upsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupState_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[LookupState_Delete]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupState_Delete]
GO
/****** Object:  StoredProcedure [dbo].[LookupState_ById]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupState_ById]
GO
/****** Object:  StoredProcedure [dbo].[LookupState_All]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupState_All]
GO
/****** Object:  StoredProcedure [dbo].[LookupProductImages_Upsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupProductImages_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[LookupProductImages_Delete]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupProductImages_Delete]
GO
/****** Object:  StoredProcedure [dbo].[LookupProductImages_ById]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupProductImages_ById]
GO
/****** Object:  StoredProcedure [dbo].[LookupProductImages_All]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupProductImages_All]
GO
/****** Object:  StoredProcedure [dbo].[LookupProductCategoryAndSubCategory_Upsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupProductCategoryAndSubCategory_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[LookupProductCategoryAndSubCategory_Delete]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupProductCategoryAndSubCategory_Delete]
GO
/****** Object:  StoredProcedure [dbo].[LookupProductCategoryAndSubCategory_ById]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupProductCategoryAndSubCategory_ById]
GO
/****** Object:  StoredProcedure [dbo].[LookupProductCategoryAndSubCategory_All]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupProductCategoryAndSubCategory_All]
GO
/****** Object:  StoredProcedure [dbo].[LookupCountry_Upsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupCountry_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[LookupCountry_Delete]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupCountry_Delete]
GO
/****** Object:  StoredProcedure [dbo].[LookupCountry_ById]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupCountry_ById]
GO
/****** Object:  StoredProcedure [dbo].[LookupCountry_All]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupCountry_All]
GO
/****** Object:  StoredProcedure [dbo].[LookupCity_Upsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupCity_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[LookupCity_Delete]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupCity_Delete]
GO
/****** Object:  StoredProcedure [dbo].[LookupCity_ById]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupCity_ById]
GO
/****** Object:  StoredProcedure [dbo].[LookupCity_All]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[LookupCity_All]
GO
/****** Object:  StoredProcedure [dbo].[KnowledgeBaseCategory_Upsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[KnowledgeBaseCategory_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[KnowledgeBaseCategory_All]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[KnowledgeBaseCategory_All]
GO
/****** Object:  StoredProcedure [dbo].[KnowledgeBase_Upsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[KnowledgeBase_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[KnowledgeBase_Delete]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[KnowledgeBase_Delete]
GO
/****** Object:  StoredProcedure [dbo].[KnowledgeBase_ById]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[KnowledgeBase_ById]
GO
/****** Object:  StoredProcedure [dbo].[KnowledgeBase_All]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[KnowledgeBase_All]
GO
/****** Object:  StoredProcedure [dbo].[DailyReport_Upsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[DailyReport_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[DailyReport_SelectAll]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[DailyReport_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[DailyReport_Select]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[DailyReport_Select]
GO
/****** Object:  StoredProcedure [dbo].[Customers_Upsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Customers_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[Customers_Logout]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Customers_Logout]
GO
/****** Object:  StoredProcedure [dbo].[Customers_Login]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Customers_Login]
GO
/****** Object:  StoredProcedure [dbo].[Customers_Delete]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Customers_Delete]
GO
/****** Object:  StoredProcedure [dbo].[Customers_ChangePassword]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Customers_ChangePassword]
GO
/****** Object:  StoredProcedure [dbo].[Customers_ById]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Customers_ById]
GO
/****** Object:  StoredProcedure [dbo].[Customers_all]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Customers_all]
GO
/****** Object:  StoredProcedure [dbo].[BlogUpsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[BlogUpsert]
GO
/****** Object:  StoredProcedure [dbo].[BlogUpdateStatus]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[BlogUpdateStatus]
GO
/****** Object:  StoredProcedure [dbo].[BlogSelectByID]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[BlogSelectByID]
GO
/****** Object:  StoredProcedure [dbo].[BlogSelectAll]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[BlogSelectAll]
GO
/****** Object:  StoredProcedure [dbo].[BlogCategoryUpsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[BlogCategoryUpsert]
GO
/****** Object:  StoredProcedure [dbo].[BlogCategorySelectAll]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[BlogCategorySelectAll]
GO
/****** Object:  StoredProcedure [dbo].[Blog_Delete]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Blog_Delete]
GO
/****** Object:  StoredProcedure [dbo].[Admin_Upsert]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Admin_Upsert]
GO
/****** Object:  StoredProcedure [dbo].[Admin_Logout]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Admin_Logout]
GO
/****** Object:  StoredProcedure [dbo].[Admin_Login]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Admin_Login]
GO
/****** Object:  StoredProcedure [dbo].[Admin_Delete]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Admin_Delete]
GO
/****** Object:  StoredProcedure [dbo].[Admin_ChangePassword]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Admin_ChangePassword]
GO
/****** Object:  StoredProcedure [dbo].[Admin_ById]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Admin_ById]
GO
/****** Object:  StoredProcedure [dbo].[Admin_All]    Script Date: 21-08-2021 11:19:26 ******/
DROP PROCEDURE IF EXISTS [dbo].[Admin_All]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Product]') AND type in (N'U'))
ALTER TABLE [dbo].[Product] DROP CONSTRAINT IF EXISTS [FK_Product_Product]
GO
/****** Object:  Table [dbo].[VisitPurpose]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[VisitPurpose]
GO
/****** Object:  Table [dbo].[Transactions]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[Transactions]
GO
/****** Object:  Table [dbo].[TraderSales]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[TraderSales]
GO
/****** Object:  Table [dbo].[Traders]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[Traders]
GO
/****** Object:  Table [dbo].[TrackOrder]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[TrackOrder]
GO
/****** Object:  Table [dbo].[SupportTickets]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[SupportTickets]
GO
/****** Object:  Table [dbo].[SupportComments]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[SupportComments]
GO
/****** Object:  Table [dbo].[Shipping]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[Shipping]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[Role]
GO
/****** Object:  Table [dbo].[Rights]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[Rights]
GO
/****** Object:  Table [dbo].[Promotional]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[Promotional]
GO
/****** Object:  Table [dbo].[ProductReview]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[ProductReview]
GO
/****** Object:  Table [dbo].[ProductImages]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[ProductImages]
GO
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[ProductCategory]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[Product]
GO
/****** Object:  Table [dbo].[Permission]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[Permission]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[Orders]
GO
/****** Object:  Table [dbo].[OrderDetails]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[OrderDetails]
GO
/****** Object:  Table [dbo].[OrderComments]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[OrderComments]
GO
/****** Object:  Table [dbo].[Notifications]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[Notifications]
GO
/****** Object:  Table [dbo].[LookupStatus]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[LookupStatus]
GO
/****** Object:  Table [dbo].[LookupState]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[LookupState]
GO
/****** Object:  Table [dbo].[LookupProductImages]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[LookupProductImages]
GO
/****** Object:  Table [dbo].[LookupProductCategoryAndSubCategory]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[LookupProductCategoryAndSubCategory]
GO
/****** Object:  Table [dbo].[LookupKnowledgeBaseCategory]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[LookupKnowledgeBaseCategory]
GO
/****** Object:  Table [dbo].[LookupCountry]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[LookupCountry]
GO
/****** Object:  Table [dbo].[LookupCity]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[LookupCity]
GO
/****** Object:  Table [dbo].[KnowledgeBase]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[KnowledgeBase]
GO
/****** Object:  Table [dbo].[DailyReport]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[DailyReport]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[Customers]
GO
/****** Object:  Table [dbo].[BlogCategory]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[BlogCategory]
GO
/****** Object:  Table [dbo].[Blog]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[Blog]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[Admin]
GO
/****** Object:  Table [dbo].[Address]    Script Date: 21-08-2021 11:19:26 ******/
DROP TABLE IF EXISTS [dbo].[Address]
GO
/****** Object:  User [sa]    Script Date: 21-08-2021 11:19:26 ******/
DROP USER IF EXISTS [sa]
GO
USE [master]
GO
/****** Object:  Database [Topfarms]    Script Date: 21-08-2021 11:19:26 ******/
DROP DATABASE IF EXISTS [Topfarms]
GO
/****** Object:  Database [Topfarms]    Script Date: 21-08-2021 11:19:26 ******/
CREATE DATABASE [Topfarms]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Topfarms', FILENAME = N'D:\rdsdbdata\DATA\Topfarms.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'Topfarms_log', FILENAME = N'D:\rdsdbdata\DATA\Topfarms_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Topfarms].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Topfarms] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Topfarms] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Topfarms] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Topfarms] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Topfarms] SET ARITHABORT OFF 
GO
ALTER DATABASE [Topfarms] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Topfarms] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Topfarms] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Topfarms] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Topfarms] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Topfarms] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Topfarms] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Topfarms] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Topfarms] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Topfarms] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Topfarms] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Topfarms] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Topfarms] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Topfarms] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Topfarms] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Topfarms] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Topfarms] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Topfarms] SET RECOVERY FULL 
GO
ALTER DATABASE [Topfarms] SET  MULTI_USER 
GO
ALTER DATABASE [Topfarms] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Topfarms] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Topfarms] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Topfarms] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Topfarms] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Topfarms] SET QUERY_STORE = OFF
GO
USE [Topfarms]
GO
/****** Object:  User [sa]    Script Date: 21-08-2021 11:19:29 ******/
CREATE USER [sa] FOR LOGIN [sa] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [sa]
GO
/****** Object:  Table [dbo].[Address]    Script Date: 21-08-2021 11:19:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Address1] [nvarchar](max) NULL,
	[Address2] [nvarchar](max) NULL,
	[Pincode] [nvarchar](50) NULL,
	[Area] [nvarchar](max) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[CountryID] [bigint] NULL,
	[DelFlg] [bit] NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[Email] [varchar](500) NULL,
	[Password] [varchar](500) NULL,
	[Phone] [varchar](500) NULL,
	[AlternatePhone] [varchar](500) NULL,
	[LastLogin] [datetime] NULL,
	[AvtarURL] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Blog]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blog](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[BlogCategoryID] [bigint] NOT NULL,
	[ArticleName] [nvarchar](max) NOT NULL,
	[Articles] [nvarchar](max) NOT NULL,
	[DatePublished] [datetime] NOT NULL,
	[Status] [bit] NOT NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Blog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BlogCategory]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BlogCategory](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_BlogCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NOT NULL,
	[LastName] [nvarchar](max) NULL,
	[Email] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](256) NOT NULL,
	[Status] [int] NOT NULL,
	[Address] [bigint] NULL,
	[AvtarURL] [nvarchar](max) NULL,
	[Phone] [nchar](16) NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DailyReport]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DailyReport](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[TraderId] [bigint] NOT NULL,
	[IsSubmitted] [bit] NOT NULL,
	[CustomerName] [nvarchar](max) NOT NULL,
	[CustomerLocation] [nvarchar](max) NOT NULL,
	[NoOfCows] [int] NOT NULL,
	[VisitPurposeID] [tinyint] NOT NULL,
	[NewOrder] [bit] NOT NULL,
	[Summary] [nvarchar](max) NOT NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_DailyReport] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KnowledgeBase]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KnowledgeBase](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ArticleName] [varchar](500) NULL,
	[LookupKnowledgeBaseCategoryId] [bigint] NULL,
	[ArticleContant] [varchar](max) NULL,
	[DatePublished] [varchar](250) NULL,
	[LastChanged] [varchar](250) NULL,
	[LookupStatus] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_KnowledgeBase] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LookupCity]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LookupCity](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_LookupCity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LookupCountry]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LookupCountry](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_LookupCountry] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LookupKnowledgeBaseCategory]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LookupKnowledgeBaseCategory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_LookupKnowledgeBaseCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LookupProductCategoryAndSubCategory]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LookupProductCategoryAndSubCategory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[ParentId] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_LookupProductCategoryAndSubCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LookupProductImages]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LookupProductImages](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductImage1] [varchar](max) NULL,
	[ProductImage2] [varchar](max) NULL,
	[ProductImage3] [varchar](max) NULL,
	[ProductImage4] [varchar](max) NULL,
	[ProductImage5] [varchar](max) NULL,
	[ProductImage6] [varchar](max) NULL,
	[ProductImage7] [varchar](max) NULL,
	[ProductImage8] [varchar](max) NULL,
	[ProductImage9] [varchar](max) NULL,
	[ProductImage10] [varchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_LookupProductImages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LookupState]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LookupState](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_LookupState] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LookupStatus]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LookupStatus](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [varchar](250) NULL,
	[UpdatedBy] [bigint] NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedBy] [bigint] NULL,
 CONSTRAINT [PK_LookupStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notifications]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notifications](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NULL,
	[RoleID] [bigint] NULL,
	[Message] [nvarchar](max) NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Notifications] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderComments]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderComments](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderID] [bigint] NOT NULL,
	[RoleID] [bigint] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[SubmitDate] [datetime] NOT NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Commemts] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetails]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetails](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[ProductName] [nvarchar](max) NOT NULL,
	[Quantity] [decimal](18, 2) NOT NULL,
	[Rate] [decimal](18, 2) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_OrderDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerID] [bigint] NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[PaymentTypes] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[AssignTo] [bigint] NOT NULL,
	[AssignDate] [datetime] NULL,
	[ShipmentAddress] [bigint] NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permission]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permission](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CategoryID] [bigint] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[StandardPrice] [money] NOT NULL,
	[OfferPrice] [money] NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsFeatured] [bit] NOT NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductCategory](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Status] [bit] NOT NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductImages]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductImages](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[IsImage] [bit] NOT NULL,
	[URL] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductReview]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductReview](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductID] [bigint] NOT NULL,
	[Rating] [tinyint] NOT NULL,
	[Review] [nvarchar](100) NOT NULL,
	[Like] [bigint] NULL,
	[Dislike] [bigint] NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_ProductReview] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Promotional]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Promotional](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Type] [tinyint] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[PromoCode] [nvarchar](max) NOT NULL,
	[ProductID] [bigint] NOT NULL,
	[Discount] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ImageURL] [nvarchar](max) NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Promotional] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rights]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rights](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[RoleID] [bigint] NOT NULL,
	[PermissionID] [bigint] NOT NULL,
	[Read] [bit] NOT NULL,
	[Add] [bit] NOT NULL,
	[Edit] [bit] NOT NULL,
	[Delete] [bit] NOT NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Rights] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shipping]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shipping](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CategoryID] [bigint] NOT NULL,
	[FreeShipping] [money] NOT NULL,
	[FreeShippingAbove] [money] NOT NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Shipping] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SupportComments]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SupportComments](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SupportTicketID] [bigint] NOT NULL,
	[RoleID] [bigint] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[SubmitDate] [datetime] NOT NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_SupportComments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SupportTickets]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SupportTickets](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerID] [bigint] NOT NULL,
	[OrderID] [bigint] NULL,
	[SubmittedDate] [datetime] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Support Tickets] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TrackOrder]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrackOrder](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[Status] [int] NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_TrackOrder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Traders]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Traders](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[JoinedDate] [datetime] NULL,
	[EmailID] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](256) NOT NULL,
	[MobileNo] [nvarchar](15) NOT NULL,
	[AlternateMobileNo] [nvarchar](15) NULL,
	[Address] [bigint] NULL,
	[LastLogin] [datetime] NULL,
	[AvtarURL] [nvarchar](max) NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Traders] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TraderSales]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TraderSales](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TraderID] [bigint] NOT NULL,
	[OrderCompleted] [int] NOT NULL,
	[TotalSales] [money] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_TraderSales] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transactions]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transactions](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TotalDueAmount] [money] NOT NULL,
	[OrderID] [bigint] NULL,
	[CustomerID] [bigint] NULL,
	[Status] [tinyint] NOT NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [nvarchar](max) NULL,
 CONSTRAINT [PK_Transactions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VisitPurpose]    Script Date: 21-08-2021 11:19:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VisitPurpose](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[DelFlg] [bit] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_VisitPupose] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Address] ON 

INSERT [dbo].[Address] ([ID], [Address1], [Address2], [Pincode], [Area], [City], [State], [CountryID], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'A/15', N'Wall street', N'32623', N'Naroda', N'Ahmedabad', N'Gujarat', 0, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Address] OFF
GO
SET IDENTITY_INSERT [dbo].[Admin] ON 

INSERT [dbo].[Admin] ([Id], [Name], [Email], [Password], [Phone], [AlternatePhone], [LastLogin], [AvtarURL], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, N'RAM', N'RAM00@gmail.com', N'123123', N'02698965895', N'9869856985', NULL, NULL, CAST(N'2021-07-21T10:39:45.507' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[Admin] ([Id], [Name], [Email], [Password], [Phone], [AlternatePhone], [LastLogin], [AvtarURL], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, N'RAVAN', N'RAVAN00@gmail.com', N'123123', N'02698965895', N'9869856985', CAST(N'2021-07-21T12:16:10.480' AS DateTime), NULL, CAST(N'2021-07-21T10:41:01.273' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[Admin] ([Id], [Name], [Email], [Password], [Phone], [AlternatePhone], [LastLogin], [AvtarURL], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, N'BALI', N'RAVAN0110@gmail.com', N'123', N'026989658525', N'9856985698', NULL, NULL, CAST(N'2021-07-21T10:41:31.823' AS DateTime), 0, CAST(N'2021-07-29T05:57:29.120' AS DateTime), 0, NULL, 0)
INSERT [dbo].[Admin] ([Id], [Name], [Email], [Password], [Phone], [AlternatePhone], [LastLogin], [AvtarURL], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (4, N'kjjhj', N'RAVAN01nn10@gmail.com', N'4854', N'026529658525', N'9566985698', NULL, NULL, CAST(N'2021-07-21T11:34:05.937' AS DateTime), 0, NULL, NULL, CAST(N'2021-07-21T11:36:00.380' AS DateTime), 4)
INSERT [dbo].[Admin] ([Id], [Name], [Email], [Password], [Phone], [AlternatePhone], [LastLogin], [AvtarURL], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (5, N'5uh', N'jijj@gmail.com', N'jigar12', N'026529658525', N'9566985698', NULL, NULL, CAST(N'2021-07-21T11:34:21.530' AS DateTime), 0, NULL, NULL, CAST(N'2021-07-29T06:14:29.340' AS DateTime), 0)
INSERT [dbo].[Admin] ([Id], [Name], [Email], [Password], [Phone], [AlternatePhone], [LastLogin], [AvtarURL], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (6, N'Bhain', N'bhain01@gmail.com', N'123', N'026989658525', N'9856985698', NULL, NULL, CAST(N'2021-07-29T05:58:46.887' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[Admin] ([Id], [Name], [Email], [Password], [Phone], [AlternatePhone], [LastLogin], [AvtarURL], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (7, N'Bhavin', N'bhavin01@gmail.com', N'123', N'026989658525', N'9856985698', NULL, NULL, CAST(N'2021-07-29T06:04:46.250' AS DateTime), 0, CAST(N'2021-07-29T06:05:10.570' AS DateTime), 0, NULL, 0)
SET IDENTITY_INSERT [dbo].[Admin] OFF
GO
SET IDENTITY_INSERT [dbo].[Blog] ON 

INSERT [dbo].[Blog] ([ID], [BlogCategoryID], [ArticleName], [Articles], [DatePublished], [Status], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, 2, N'Available payment methods when checkout', N'<h3><em>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.</em></h3>

<p><strong>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.</strong></p>
', CAST(N'2021-08-21T10:09:01.263' AS DateTime), 1, 0, CAST(N'2021-08-21T10:09:01.267' AS DateTime), N'0', CAST(N'2021-08-21T10:09:01.270' AS DateTime), N'0')
SET IDENTITY_INSERT [dbo].[Blog] OFF
GO
SET IDENTITY_INSERT [dbo].[BlogCategory] ON 

INSERT [dbo].[BlogCategory] ([ID], [Name], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'Payment', 0, CAST(N'2021-08-19T22:56:40.300' AS DateTime), N'0', CAST(N'2021-08-19T22:56:40.303' AS DateTime), N'0')
INSERT [dbo].[BlogCategory] ([ID], [Name], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, N'Refund Policy', 0, CAST(N'2021-08-19T22:58:19.310' AS DateTime), N'0', CAST(N'2021-08-19T22:58:19.310' AS DateTime), N'0')
SET IDENTITY_INSERT [dbo].[BlogCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([ID], [FirstName], [LastName], [Email], [Password], [Status], [Address], [AvtarURL], [Phone], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'Vikas Sojitra', NULL, N'vikas@gmail.com', N'123', 2, 1, NULL, N'3232623523      ', 0, NULL, NULL, CAST(N'2021-08-20T05:15:39.107' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[Customers] OFF
GO
SET IDENTITY_INSERT [dbo].[DailyReport] ON 

INSERT [dbo].[DailyReport] ([ID], [Date], [TraderId], [IsSubmitted], [CustomerName], [CustomerLocation], [NoOfCows], [VisitPurposeID], [NewOrder], [Summary], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, CAST(N'2021-08-19T04:05:00.000' AS DateTime), 1, 1, N'Vikas', N'Ahmedabad', 5, 4, 1, N'Nice meeting', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[DailyReport] OFF
GO
SET IDENTITY_INSERT [dbo].[KnowledgeBase] ON 

INSERT [dbo].[KnowledgeBase] ([Id], [ArticleName], [LookupKnowledgeBaseCategoryId], [ArticleContant], [DatePublished], [LastChanged], [LookupStatus], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, N'M Parivahan', 1, N'mmm', N'gh', N'Tud', N'gh', CAST(N'2021-07-22T10:21:36.570' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[KnowledgeBase] ([Id], [ArticleName], [LookupKnowledgeBaseCategoryId], [ArticleContant], [DatePublished], [LastChanged], [LookupStatus], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, N'c Parivahan', 2, N'mmm', N'gh', N'Tud', N'gh', CAST(N'2021-07-22T10:21:52.270' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[KnowledgeBase] ([Id], [ArticleName], [LookupKnowledgeBaseCategoryId], [ArticleContant], [DatePublished], [LastChanged], [LookupStatus], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, N'K Parivahan', 3, N'asnd', N'gh', N'Tud', N'gh', CAST(N'2021-07-22T10:22:17.593' AS DateTime), 0, CAST(N'2021-07-29T06:35:52.620' AS DateTime), 0, CAST(N'2021-07-29T06:37:51.707' AS DateTime), 3)
INSERT [dbo].[KnowledgeBase] ([Id], [ArticleName], [LookupKnowledgeBaseCategoryId], [ArticleContant], [DatePublished], [LastChanged], [LookupStatus], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (4, N'K Parivahan', 3, N'asnd', N'gh', N'Tud', N'gh', CAST(N'2021-07-29T06:35:35.913' AS DateTime), 0, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[KnowledgeBase] OFF
GO
SET IDENTITY_INSERT [dbo].[LookupCity] ON 

INSERT [dbo].[LookupCity] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, N'ANAND', CAST(N'2021-07-21T13:03:19.160' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[LookupCity] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, N'NADIYAD', CAST(N'2021-07-21T13:03:32.293' AS DateTime), 0, NULL, NULL, CAST(N'2021-07-21T13:07:53.553' AS DateTime), 1)
INSERT [dbo].[LookupCity] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, N'SURAT', CAST(N'2021-07-21T13:04:01.133' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[LookupCity] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (4, N'KHAMBHAT', CAST(N'2021-07-21T13:04:11.880' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[LookupCity] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (5, N'PANMAHAL', CAST(N'2021-07-21T13:04:25.010' AS DateTime), 0, CAST(N'2021-07-21T13:05:05.693' AS DateTime), 0, NULL, 0)
INSERT [dbo].[LookupCity] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (6, N'BHARUCH', CAST(N'2021-07-21T13:04:40.897' AS DateTime), 0, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[LookupCity] OFF
GO
SET IDENTITY_INSERT [dbo].[LookupCountry] ON 

INSERT [dbo].[LookupCountry] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, N'ARYA VANSH', CAST(N'2021-07-21T12:34:32.677' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[LookupCountry] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, N'Gandhar', CAST(N'2021-07-21T12:35:25.540' AS DateTime), 0, NULL, NULL, CAST(N'2021-07-21T12:45:52.213' AS DateTime), 1)
INSERT [dbo].[LookupCountry] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, N'HINDUSTAN', CAST(N'2021-07-21T12:37:07.127' AS DateTime), 0, CAST(N'2021-07-21T12:39:51.780' AS DateTime), 0, NULL, 0)
INSERT [dbo].[LookupCountry] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (4, N'INDIA', CAST(N'2021-07-21T12:38:26.937' AS DateTime), 0, CAST(N'2021-07-21T12:39:29.813' AS DateTime), 0, NULL, 0)
INSERT [dbo].[LookupCountry] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (5, N'BHARAT', CAST(N'2021-07-21T12:38:45.233' AS DateTime), 0, CAST(N'2021-07-21T12:39:15.637' AS DateTime), 0, NULL, 0)
SET IDENTITY_INSERT [dbo].[LookupCountry] OFF
GO
SET IDENTITY_INSERT [dbo].[LookupKnowledgeBaseCategory] ON 

INSERT [dbo].[LookupKnowledgeBaseCategory] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N'sdgsd', CAST(N'2021-07-22T09:40:37.577' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[LookupKnowledgeBaseCategory] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, N'sagar', CAST(N'2021-07-22T09:40:53.250' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[LookupKnowledgeBaseCategory] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (3, N'tannu', CAST(N'2021-07-22T09:41:00.847' AS DateTime), 0, CAST(N'2021-07-22T09:41:30.007' AS DateTime), 0)
INSERT [dbo].[LookupKnowledgeBaseCategory] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (4, N'tannu', CAST(N'2021-07-22T09:41:21.070' AS DateTime), 0, NULL, NULL)
INSERT [dbo].[LookupKnowledgeBaseCategory] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (5, N'akash', CAST(N'2021-07-29T06:43:34.470' AS DateTime), 0, CAST(N'2021-07-29T06:43:42.733' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[LookupKnowledgeBaseCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[LookupProductCategoryAndSubCategory] ON 

INSERT [dbo].[LookupProductCategoryAndSubCategory] ([Id], [Name], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, N'ABC', 1, CAST(N'2021-07-21T10:04:23.143' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[LookupProductCategoryAndSubCategory] ([Id], [Name], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, N'DEF', 2, CAST(N'2021-07-21T10:05:03.430' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[LookupProductCategoryAndSubCategory] ([Id], [Name], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, N'GHI', 3, CAST(N'2021-07-21T10:05:12.113' AS DateTime), 0, CAST(N'2021-07-21T10:05:31.140' AS DateTime), 0, CAST(N'2021-07-21T10:33:31.200' AS DateTime), 0)
INSERT [dbo].[LookupProductCategoryAndSubCategory] ([Id], [Name], [ParentId], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (4, N'GHI', 3, CAST(N'2021-07-29T07:35:45.183' AS DateTime), 0, CAST(N'2021-07-29T07:35:53.217' AS DateTime), 0, CAST(N'2021-07-29T07:36:30.300' AS DateTime), 4)
SET IDENTITY_INSERT [dbo].[LookupProductCategoryAndSubCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[LookupProductImages] ON 

INSERT [dbo].[LookupProductImages] ([Id], [ProductImage1], [ProductImage2], [ProductImage3], [ProductImage4], [ProductImage5], [ProductImage6], [ProductImage7], [ProductImage8], [ProductImage9], [ProductImage10], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, N'1', N'1', N'1', N'1', N'1', N'1', N'1', N'1', N'1', N'1', CAST(N'2021-07-21T11:04:44.600' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[LookupProductImages] ([Id], [ProductImage1], [ProductImage2], [ProductImage3], [ProductImage4], [ProductImage5], [ProductImage6], [ProductImage7], [ProductImage8], [ProductImage9], [ProductImage10], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, N'2', N'2', N'2', N'2', N'2', N'2', N'2', N'2', N'2', N'0', CAST(N'2021-07-21T11:05:04.937' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[LookupProductImages] ([Id], [ProductImage1], [ProductImage2], [ProductImage3], [ProductImage4], [ProductImage5], [ProductImage6], [ProductImage7], [ProductImage8], [ProductImage9], [ProductImage10], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, N'2', N'2', N'2', N'2', N'2', N'2', N'2', N'2', N'2', N'2', CAST(N'2021-07-21T11:05:23.450' AS DateTime), 0, NULL, NULL, CAST(N'2021-07-21T11:18:00.903' AS DateTime), 0)
INSERT [dbo].[LookupProductImages] ([Id], [ProductImage1], [ProductImage2], [ProductImage3], [ProductImage4], [ProductImage5], [ProductImage6], [ProductImage7], [ProductImage8], [ProductImage9], [ProductImage10], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (4, N'2', N'2', N'2', N'2', N'4', N'4', N'4', N'2', N'2', N'2', CAST(N'2021-07-21T11:05:30.267' AS DateTime), 0, CAST(N'2021-07-21T11:06:33.027' AS DateTime), NULL, NULL, 0)
INSERT [dbo].[LookupProductImages] ([Id], [ProductImage1], [ProductImage2], [ProductImage3], [ProductImage4], [ProductImage5], [ProductImage6], [ProductImage7], [ProductImage8], [ProductImage9], [ProductImage10], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (5, N'2', N'2', N'2', N'2', N'4', N'4', N'4', N'2', N'2', N'2', CAST(N'2021-07-29T06:48:43.190' AS DateTime), 0, CAST(N'2021-07-29T06:48:56.003' AS DateTime), NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[LookupProductImages] OFF
GO
SET IDENTITY_INSERT [dbo].[LookupState] ON 

INSERT [dbo].[LookupState] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, N'AYODYA', CAST(N'2021-07-21T12:48:36.487' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[LookupState] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, N'HASTINAPUR', CAST(N'2021-07-21T12:49:07.583' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[LookupState] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, N'MATSHAYA', CAST(N'2021-07-21T12:49:32.183' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[LookupState] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (4, N'DHWARKA', CAST(N'2021-07-21T12:50:52.683' AS DateTime), 0, NULL, NULL, CAST(N'2021-07-29T07:15:17.420' AS DateTime), 4)
INSERT [dbo].[LookupState] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (5, N'PANCHAL', CAST(N'2021-07-21T12:51:06.793' AS DateTime), 0, CAST(N'2021-07-21T13:00:56.197' AS DateTime), 0, NULL, 0)
INSERT [dbo].[LookupState] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (6, N'GUJARAT', CAST(N'2021-07-21T12:51:28.090' AS DateTime), 0, CAST(N'2021-07-21T12:54:00.487' AS DateTime), 0, CAST(N'2021-07-21T12:57:14.630' AS DateTime), 1)
INSERT [dbo].[LookupState] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (7, N'AYODYAudh', CAST(N'2021-07-29T07:01:11.373' AS DateTime), 0, CAST(N'2021-07-29T07:01:23.833' AS DateTime), 0, CAST(N'2021-07-29T07:02:02.220' AS DateTime), 7)
SET IDENTITY_INSERT [dbo].[LookupState] OFF
GO
SET IDENTITY_INSERT [dbo].[LookupStatus] ON 

INSERT [dbo].[LookupStatus] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (1, N'GETUP', CAST(N'2021-07-21T13:12:19.020' AS DateTime), 0, NULL, NULL, CAST(N'2021-07-21T13:18:15.590' AS DateTime), 1)
INSERT [dbo].[LookupStatus] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (2, N'REVA', CAST(N'2021-07-21T13:12:52.793' AS DateTime), 0, N'Jul 21 2021  1:14PM', 0, NULL, 0)
INSERT [dbo].[LookupStatus] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (3, N'NO', CAST(N'2021-07-21T13:13:32.760' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[LookupStatus] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (4, N'BACK', CAST(N'2021-07-21T13:13:47.023' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[LookupStatus] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (5, N'RANI', CAST(N'2021-07-21T13:13:58.950' AS DateTime), 0, NULL, NULL, NULL, 0)
INSERT [dbo].[LookupStatus] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (6, N'SHOPING', CAST(N'2021-07-21T13:14:09.000' AS DateTime), 0, N'Jul 21 2021  1:14PM', 0, CAST(N'2021-07-21T13:18:24.803' AS DateTime), 1)
INSERT [dbo].[LookupStatus] ([Id], [Name], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [DeletedDate], [DeletedBy]) VALUES (7, N'PANMAHAL1', CAST(N'2021-07-29T07:04:53.420' AS DateTime), 0, N'Jul 29 2021  7:05AM', 0, CAST(N'2021-07-29T07:06:07.847' AS DateTime), 7)
SET IDENTITY_INSERT [dbo].[LookupStatus] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderComments] ON 

INSERT [dbo].[OrderComments] ([ID], [OrderID], [RoleID], [UserID], [Message], [SubmitDate], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, 1, 1, 1, N'Nice Product', CAST(N'2021-08-18T00:00:00.000' AS DateTime), 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[OrderComments] ([ID], [OrderID], [RoleID], [UserID], [Message], [SubmitDate], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, 1, 1, 1, N'Un assigned', CAST(N'2021-08-19T10:29:54.397' AS DateTime), 0, CAST(N'2021-08-19T10:29:54.397' AS DateTime), N'0', NULL, NULL)
SET IDENTITY_INSERT [dbo].[OrderComments] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([ID], [CustomerID], [OrderDate], [PaymentTypes], [Status], [AssignTo], [AssignDate], [ShipmentAddress], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, 1, CAST(N'2021-08-17T04:15:00.000' AS DateTime), 3, 1, 1, CAST(N'2021-08-19T10:29:12.033' AS DateTime), NULL, 0, NULL, NULL, CAST(N'2021-08-19T11:05:53.500' AS DateTime), N'0')
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductCategory] ON 

INSERT [dbo].[ProductCategory] ([ID], [Name], [Status], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'Semen', 1, 0, CAST(N'2021-08-14T14:16:20.383' AS DateTime), N'0', CAST(N'2021-08-15T00:17:41.870' AS DateTime), N'0')
INSERT [dbo].[ProductCategory] ([ID], [Name], [Status], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, N'Accessories', 0, 0, CAST(N'2021-08-14T14:17:49.060' AS DateTime), N'0', CAST(N'2021-08-14T21:27:36.250' AS DateTime), N'0')
INSERT [dbo].[ProductCategory] ([ID], [Name], [Status], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (3, N'Milk', 1, 0, CAST(N'2021-08-14T21:28:51.873' AS DateTime), N'0', CAST(N'2021-08-14T21:33:37.490' AS DateTime), N'0')
INSERT [dbo].[ProductCategory] ([ID], [Name], [Status], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (4, N'Seeds', 0, 0, CAST(N'2021-08-14T21:30:18.600' AS DateTime), N'0', CAST(N'2021-08-14T21:30:18.600' AS DateTime), N'0')
INSERT [dbo].[ProductCategory] ([ID], [Name], [Status], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (5, N'Veg', 0, 0, CAST(N'2021-08-14T21:31:32.380' AS DateTime), N'0', CAST(N'2021-08-14T21:31:32.380' AS DateTime), N'0')
INSERT [dbo].[ProductCategory] ([ID], [Name], [Status], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (6, N'Nonveg', 1, 0, CAST(N'2021-08-14T21:31:39.840' AS DateTime), N'0', CAST(N'2021-08-14T21:33:25.473' AS DateTime), N'0')
INSERT [dbo].[ProductCategory] ([ID], [Name], [Status], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (7, N'Organic', 1, 0, CAST(N'2021-08-14T21:31:50.540' AS DateTime), N'0', CAST(N'2021-08-14T21:33:28.233' AS DateTime), N'0')
INSERT [dbo].[ProductCategory] ([ID], [Name], [Status], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (8, N'Vegetables', 0, 0, CAST(N'2021-08-14T21:32:09.330' AS DateTime), N'0', CAST(N'2021-08-14T21:32:09.330' AS DateTime), N'0')
INSERT [dbo].[ProductCategory] ([ID], [Name], [Status], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (9, N'Balaji', 1, 0, CAST(N'2021-08-14T21:32:14.527' AS DateTime), N'0', CAST(N'2021-08-14T21:33:21.793' AS DateTime), N'0')
INSERT [dbo].[ProductCategory] ([ID], [Name], [Status], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (10, N'Real', 1, 0, CAST(N'2021-08-14T21:32:18.473' AS DateTime), N'0', CAST(N'2021-08-14T21:32:44.760' AS DateTime), N'0')
SET IDENTITY_INSERT [dbo].[ProductCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([ID], [Name], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'Customer', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Role] ([ID], [Name], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, N'Trader', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Role] ([ID], [Name], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (3, N'Admin', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
SET IDENTITY_INSERT [dbo].[Shipping] ON 

INSERT [dbo].[Shipping] ([ID], [CategoryID], [FreeShipping], [FreeShippingAbove], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, 2, 90.0000, 140.0000, 0, CAST(N'2021-08-18T23:10:17.683' AS DateTime), N'0', CAST(N'2021-08-18T23:10:17.683' AS DateTime), N'0')
INSERT [dbo].[Shipping] ([ID], [CategoryID], [FreeShipping], [FreeShippingAbove], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (2, 3, 10.0000, 900.0000, 0, CAST(N'2021-08-18T18:20:28.137' AS DateTime), N'0', CAST(N'2021-08-18T18:20:28.137' AS DateTime), N'0')
INSERT [dbo].[Shipping] ([ID], [CategoryID], [FreeShipping], [FreeShippingAbove], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (3, 4, 856.0000, 985.0000, 0, CAST(N'2021-08-18T22:48:29.667' AS DateTime), N'0', CAST(N'2021-08-18T22:48:29.667' AS DateTime), N'0')
INSERT [dbo].[Shipping] ([ID], [CategoryID], [FreeShipping], [FreeShippingAbove], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (4, 10, 850.0000, 1500.0000, 0, CAST(N'2021-08-18T23:16:36.837' AS DateTime), N'0', CAST(N'2021-08-18T23:16:36.837' AS DateTime), N'0')
SET IDENTITY_INSERT [dbo].[Shipping] OFF
GO
SET IDENTITY_INSERT [dbo].[Traders] ON 

INSERT [dbo].[Traders] ([ID], [FirstName], [LastName], [JoinedDate], [EmailID], [Password], [MobileNo], [AlternateMobileNo], [Address], [LastLogin], [AvtarURL], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'Archit', N'Soni', CAST(N'2021-05-20T00:00:00.000' AS DateTime), N'Archit@gmail.com', N'123', N'3235223236', N'2323623263', 1, NULL, NULL, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Traders] OFF
GO
SET IDENTITY_INSERT [dbo].[VisitPurpose] ON 

INSERT [dbo].[VisitPurpose] ([Id], [Name], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (1, N'Offer Presentation', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[VisitPurpose] ([Id], [Name], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (4, N'Nitrogen', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[VisitPurpose] ([Id], [Name], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (5, N'Delivery of Semens', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[VisitPurpose] ([Id], [Name], [DelFlg], [CreatedAt], [CreatedBy], [UpdatedAt], [UpdatedBy]) VALUES (6, N'Other', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[VisitPurpose] OFF
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Product] FOREIGN KEY([ID])
REFERENCES [dbo].[Product] ([ID])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Product]
GO
/****** Object:  StoredProcedure [dbo].[Admin_All]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from Admin 
--exec [Admin_All] '',0,0
CREATE Procedure [dbo].[Admin_All]
	@Search VARCHAR(MAX),
	@Offset BIGINT,
	@Limit BIGINT

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from Admin
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') OR
											(lower(Phone) like '%'+lower(@Search)+'%') OR
											(lower(Email) like '%'+lower(@Search)+'%')OR
											(lower(AlternatePhone) like '%'+lower(@Search)+'%')

										)	
									  )
									  And
									  DeletedBy = 0
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  [Id],
				[Name],
				[Email],
				[Password],
				[Phone],
				[AlternatePhone],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]

		from  [dbo].[Admin] 

		where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') OR
											(lower(Phone) like '%'+lower(@Search)+'%') OR
											(lower(Email) like '%'+lower(@Search)+'%')OR
											(lower(AlternatePhone) like '%'+lower(@Search)+'%')

										)	
									  
					
			  )
			  And
					DeletedBy = 0
		order by Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[Admin_ById]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[Admin_ById]

@Id BIGINT

AS
BEGIN 
	set nocount on;
	declare @Code int = 200,
		@Message varchar(50) = ''

	SET @Message = 'Admin Data retrive successfully'

	select @Code as Code,
		@Message as Message

	select 
				[Id],
				[Name],
				[Email],
				[Password],
				[Phone],
				[AlternatePhone],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
	from
		Admin
	where Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[Admin_ChangePassword]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[Admin_ChangePassword]
	   @Id BIGINT,
	   @OldPassword varchar(500) = NULL,
	   @NewPassword varchar(500) = NULL,
	   @ConfirmPassword varchar(500) = NULL

AS
BEGIN
	
	SET NOCOUNT ON;

  DECLARE @Code INT = 400,
	      @Message VARCHAR(255) = ''

	
		 IF @OldPassword IS NULL OR @OldPassword = ''
		 BEGIN
		      SET @Code = 403
			  SET @Message = 'Old password is required'
			  SET @Id = 0
		 END
		 ELSE IF @NewPassword IS NULL OR @NewPassword = ''
		 BEGIN
		      SET @Code = 403
			  SET @Message = 'New password is required'
			  SET @Id = 0
		 END
		 ELSE IF @ConfirmPassword IS NULL OR @ConfirmPassword = ''
		 BEGIN
		      SET @Code = 403
			  SET @Message = 'Confirm password is required'
			  SET @Id = 0
		 END
		 ELSE IF LOWER(@NewPassword) != LOWER(@ConfirmPassword) 
		 BEGIN
		      SET @Code = 403
			  SET @Message = 'New password does not match the confirm password'
			  SET @Id = 0
		 END
		 ELSE IF (SELECT COUNT(*) FROM [Admin] WHERE Id = @Id AND LOWER([Password]) = LOWER(@OldPassword)) = 0
		 BEGIN
		      SET @Code = 403
			  SET @Message = 'Old password does not match'
			  SET @Id = 0
		 END
		 ELSE
		 BEGIN
		     UPDATE 
			       [Admin]
			 SET 
			      [Password] = @NewPassword
			 WHERE 
			      Id = @Id

			 SET @Code = 200
			 SET @Message = 'Password changes successfully'
		 END
	

		SELECT @Code as Code,
			   @Message as [Message]

		SELECT 
				[Id],
				[Name],
				[Email],
				[Password],
				[Phone],
				[AlternatePhone],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
		FROM
				[dbo].[Admin]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Admin_Delete]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from Admin
--EXEC [Admin_Delete] 5,0

CREATE Procedure [dbo].[Admin_Delete]

@Id BIGINT,
@DeletedBy Bigint
	
AS
BEGIN
	SET nocount on;
	declare @Code int = 400,
	@Message varchar(500) = ''

	update Admin set DeletedDate = GETUTCDATE() , DeletedBy = @DeletedBy
	
	where Id = @Id

	SET @Code = 200
	SET @Message = 'Admin data Deleted Successfully'

	select @Code as Code,
	@Message as Message

	select 
				[Id],
				[Name],
				[Email],
				[Password],
				[Phone],
				[AlternatePhone],
				[LastLogin],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]

	from Admin
	where Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[Admin_Login]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[Admin_Login]

    @Email varchar(500) = null,
	@Password varchar(500) = null
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @Code Int = 400,
		@Message varchar(500) = '',
		@Id Int = 0

	IF @Email is null or @Email = ''
	BEGIN 
		SET @Id = 0
		SET @Code = 400
		SET @Message = 'Email is Required'
	END
	ELSE IF @Password is null or @Password = ''
	BEGIN
		SET @Id = 0
		SET @Code = 400
		SET @Message = 'Password is Required'
	END

	ELSE 
	BEGIN
		Select @Id = Id From Admin where Email = @Email

		IF @Id > 0
		BEGIN
			IF(select count(*) from Admin where Id = @Id and Password = @Password) > 0
			BEGIN
				
					SET @Code = 200
					SET @Message = 'Admin Login in Successfully'
				END
				
			ELSE 
			BEGIN
				SET @Code = 400
				SET @Message = 'Password Is Incorrect !'
			END
		END
		ELSE 
		BEGIN
			SET @Code = 400
			SET @Message = 'Email Is Not Registerd !'
		END
	END

	Select @Code as Code,
		@Message as Message

	SELECT 
				[Id],
				[Name],
				[Email],
				[Password],
				[Phone],
				[AlternatePhone],
				[LastLogin],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
		FROM
				[dbo].[Admin]
	WHERE Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[Admin_Logout]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from Admin
CREATE Procedure [dbo].[Admin_Logout]
@Id BIGINT

AS
BEGIN
	
	SET NOCOUNT ON;

	UPDATE Admin SET LastLogin = GETDATE()
	
	WHERE Id = @Id
	
	SELECT 1
   
END
GO
/****** Object:  StoredProcedure [dbo].[Admin_Upsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--SELECT* from Admin

--EXEC [Admin_Upsert] 7,'Bhavin','bhavin01@gmail.com','123','026989658525','9856985698'

CREATE procedure [dbo].[Admin_Upsert]

        @Id BIGINT = 0,
		@Name VARCHAR(500) = null,
		@Email VARCHAR(50) = null,
		@Password VARCHAR(500) = null,
		@Phone  VARCHAR(250) = null,
		@AlternatePhone  VARCHAR(250) = null,
		@CreatedBy BIGINT = 0,
		@UpdatedBy BIGINT = 0

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					IF @Name IS null or @Name = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Name Is required'
					END
					Else IF @Email IS null or @Email = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Email Is required'
					END
					ELSE IF @Email NOT LIKE '%@%._%'
					BEGIN
							SET @Id = 0
		  					SET @Code = 400
		  					SET @Message = 'Email is invalid !'
					END
					ELSE IF (select count(*) from [Admin] where lower(Email) = lower(@Email) ) > 0
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = ' Already Exsits Email Please Enter New Email  '
					END	
					ELSE IF @Password is null or @Password = ''
		                      BEGIN
			                  SET @Id = 0
			                  SET @Code = 400
			                  SET @Message = 'Password Is required'
		                 END
		              
					ELSE IF @Phone  IS null or @Phone = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Phone  Is required'
					END
					ELSE IF @AlternatePhone  is null or @AlternatePhone  = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'AlternatePhone  is required'
					END
					
					ELSE
					BEGIN
							INSERT INTO Admin(  Name,
												Email,
												Password,
												Phone ,
												AlternatePhone,
												CreatedDate,
												CreatedBy,
												DeletedBy )
									VALUES (
											@Name,
											lower(@Email),
											@Password,
											@Phone,
											@AlternatePhone,
											GETUTCDATE(),
											@CreatedBy,
											0 )

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = ' Admin Created Successfully'
					END
			END
			ELSE IF @Id > 0
			BEGIN

					IF @Name IS null or @Name = ''
					BEGIN
							
							SET @Code = 400
							SET @Message = '@Name Is required'
					END
					Else IF @Email IS null or @Email = ''
					BEGIN
							
							SET @Code = 400
							SET @Message = 'Email Is required'
					END
					ELSE IF @Email NOT LIKE '%@%._%'
					BEGIN
							
		  					SET @Code = 400
		  					SET @Message = 'Email is invalid !'
					END
					ELSE IF (select count(*) from [Admin] where lower(Email) = lower(@Email) AND Id != @ID ) > 0
					BEGIN
							
							SET @Code = 400
							SET @Message = ' Already Exsits Email Please Enter New Email  '
					END	
					ELSE IF @Password is null or @Password = ''
		                BEGIN
			                  
			                  SET @Code = 400
			                  SET @Message = 'Password Is required'
		                 END
		              
					ELSE IF @Phone  IS null or @Phone = ''
					BEGIN
							
							SET @Code = 400
							SET @Message = 'Phone  Is required'
					END
					ELSE IF @AlternatePhone  is null or @AlternatePhone  = ''
					BEGIN
							
							SET @Code = 400
							SET @Message = 'AlternatePhone  is required'
					END
					ELSE
					BEGIN
							UPDATE  Admin
							SET		
									Name  = ISNULL(@Name ,Name ),
									Email= ISNULL(lower(@Email),lower(Email)),
									Password = ISNULL(@Password,Password),
									Phone  = ISNULL(@Phone,Phone),
									AlternatePhone  = ISNULL(@AlternatePhone,AlternatePhone),				
									UpdatedDate = GETUTCDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'Admin Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[Email],
				[Password],
				[Phone],
				[AlternatePhone],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
		FROM
				[dbo].[Admin]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Blog_Delete]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from LookupCity
--EXEC [LookupCity_Delete]2,01

CREATE Procedure [dbo].[Blog_Delete]

@Id BIGINT,
@UpdatedBy nvarchar(max)

	
AS
BEGIN
	SET nocount on;
	declare @Code int = 400,
	@Message varchar(500) = ''

	update Blog set UpdatedAt = GETDATE() , UpdatedBy = @UpdatedBy, DelFlg = 1
	
	where Id = @Id

	SET @Code = 200
	SET @Message = 'Blog articles deleted Successfully'

	select @Code as Code,
	@Message as Message
END
GO
/****** Object:  StoredProcedure [dbo].[BlogCategorySelectAll]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BlogCategorySelectAll](
	@Search varchar(100),
	@Offset INT,
	@Limit INT
)	
AS

	SET NOCOUNT ON
	DECLARE @TotalRecords BIGINT;
SET @TotalRecords = (select count(*) FROM [BlogCategory])

   select 
		*
   FROM [BlogCategory]
	 WHERE ISNULL(@Search,'') = '' 
	 	  OR Name like '%'+@Search+'%' 
	 ORDER BY Id ASC
	 OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY

SELECT @TotalRecords AS TotalRecords
GO
/****** Object:  StoredProcedure [dbo].[BlogCategoryUpsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[BlogCategoryUpsert] (
	@ID int,
	@Name nvarchar(max),
	@DelFlg bit,
	@CreatedAt datetime,
	@CreatedBy nvarchar(max),
	@UpdatedAt datetime,
	@UpdatedBy nvarchar(max))
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

-- Update
IF(@Id > 0)
		BEGIN
			SELECT @Code AS Code, @Message AS [Message]
		
			UPDATE [dbo].BlogCategory
				SET Name = @Name
				   ,DelFlg = @DelFlg
				   ,CreatedAt = @CreatedAt
				   ,CreatedBy = @CreatedBy
				   ,UpdatedAt = @UpdatedAt
				   ,UpdatedBy = @UpdatedBy
				WHERE Id = @Id

				select S.* from BlogCategory S where ID = @ID
		END

-- Insert
ELSE	
	IF exists (select ID from Shipping where ID=@ID)
		BEGIN
			SET @Message='Already exists item.'
			SELECT @Code AS Code, @Message AS [Message]
		END
	ELSE
		BEGIN
			SELECT @Code AS Code, @Message AS [Message]
	
			INSERT INTO [dbo].BlogCategory
						(Name ,DelFlg,CreatedAt ,CreatedBy ,UpdatedAt ,UpdatedBy)
			VALUES
						(@Name, @DelFlg, @CreatedAt, @CreatedBy, @UpdatedAt, @UpdatedBy)
	
			select S.* from BlogCategory S where S.ID = SCOPE_IDENTITY()
		END
GO
/****** Object:  StoredProcedure [dbo].[BlogSelectAll]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BlogSelectAll](
	@Search varchar(100),
	@Offset INT,
	@Limit INT
)	
AS

	SET NOCOUNT ON
	DECLARE @TotalRecords BIGINT;
SET @TotalRecords = (select count(*) FROM [Blog])

   select 
		Blog.ID as ID, Blog.ArticleName As ArticleName, Blog.Articles as Articles, Blog.DatePublished As DatePublished, Blog.UpdatedAt As UpdatedAt,
		Blog.Status as Status, Blog.BlogCategoryID As BlogCategoryID, BlogCategory.Name As BlogCategoryName
   FROM [Blog] INNER join BlogCategory
   on BlogCategory.ID = Blog.BlogCategoryID
   
	 WHERE Blog.DelFlg=0 and ISNULL(@Search,'') = '' 
	 	  OR Blog.ArticleName like '%'+@Search+'%' 
	 ORDER BY Blog.Id ASC
	 OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY

SELECT @TotalRecords AS TotalRecords
GO
/****** Object:  StoredProcedure [dbo].[BlogSelectByID]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BlogSelectByID] (
	@ID bigint
)
AS

SET NOCOUNT ON

SELECT
	*
FROM
	Blog
WHERE
	[ID] = @ID
GO
/****** Object:  StoredProcedure [dbo].[BlogUpdateStatus]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from LookupCity
--EXEC [LookupCity_Delete]2,01

CREATE Procedure [dbo].[BlogUpdateStatus]

@Id BIGINT,
@Status bit,
@UpdatedBy nvarchar(max)

	
AS
BEGIN
	SET nocount on;
	declare @Code int = 400,
	@Message varchar(500) = ''

	update Blog set UpdatedAt = GETDATE() , UpdatedBy = @UpdatedBy, Status = 1
	
	where Id = @Id

	SET @Code = 200
	SET @Message = 'Blog articles status updated Successfully'

	select @Code as Code,
	@Message as Message
END
GO
/****** Object:  StoredProcedure [dbo].[BlogUpsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BlogUpsert] (
	@ID int,
	@BlogCategoryID bigint,
	@ArticleName nvarchar(max),
	@Articles nvarchar(max),
	@DatePublished datetime,
	@Status bit,
	@DelFlg bit,
	@CreatedAt datetime,
	@CreatedBy nvarchar(max),
	@UpdatedAt datetime,
	@UpdatedBy nvarchar(max))
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

-- Update
IF(@Id > 0)
		BEGIN
			SELECT @Code AS Code, @Message AS [Message]
		
			UPDATE [dbo].Blog
				SET BlogCategoryID = @BlogCategoryID,
					ArticleName=@ArticleName,
					Articles=@Articles,
					DatePublished=@DatePublished,
					Status=@Status
				   ,DelFlg = @DelFlg
				   ,CreatedAt = @CreatedAt
				   ,CreatedBy = @CreatedBy
				   ,UpdatedAt = @UpdatedAt
				   ,UpdatedBy = @UpdatedBy
				WHERE Id = @Id

				select S.* from Blog S where ID = @ID
		END

-- Insert
ELSE	
	IF exists (select ID from Blog where ID=@ID)
		BEGIN
			SET @Message='Already exists item.'
			SELECT @Code AS Code, @Message AS [Message]
		END
	ELSE
		BEGIN
			SELECT @Code AS Code, @Message AS [Message]
	
			INSERT INTO [dbo].Blog
						(BlogCategoryID ,ArticleName ,Articles ,DatePublished ,Status ,DelFlg,CreatedAt ,CreatedBy ,UpdatedAt ,UpdatedBy)
			VALUES
						(@BlogCategoryID,@ArticleName,@Articles,@DatePublished,@Status, @DelFlg, @CreatedAt, @CreatedBy, @UpdatedAt, @UpdatedBy)
	
			select S.* from Blog S where S.ID = SCOPE_IDENTITY()
		END
GO
/****** Object:  StoredProcedure [dbo].[Customers_all]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from Customers
--EXEC [Customers_all]'',0,0
CREATE Procedure  [dbo].[Customers_all]
		@Search varchar(MAX),
		@Offset BIGINT,
		@Limit BIGINT		
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select count(*) from [dbo].[Customers] C																						
								where
								(
									isnull(@Search,'') = '' or
									(										
										(lower(C.FirstName) like '%'+lower(@Search)+'%') OR
										(lower(C.Email) like '%'+lower(@Search)+'%') OR
										(lower(C.Phone) like '%'+lower(@Search)+'%') OR
										(lower(C.LastName) like '%'+lower(@Search)+'%')
									)
								)
								And
								C.DelFlg = 0
								
							)

		IF @Limit = 0 OR @Limit = -1
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select *,
		 C.FirstName + ' '+ ISNULL(C.LastName,'') as Name
		from [dbo].[Customers] C								
			where
			(
				isnull(@Search,'') = '' or
				(										
					(lower(C.FirstName) like '%'+lower(@Search)+'%') OR
					(lower(C.Email) like '%'+lower(@Search)+'%') OR
					(lower(C.Phone) like '%'+lower(@Search)+'%') OR
					(lower(C.LastName) like '%'+lower(@Search)+'%')
				)
			) And C.DelFlg = 0			
			order by C.Id desc
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
				
END
GO
/****** Object:  StoredProcedure [dbo].[Customers_ById]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[Customers_ById]
	
	@Id BIGINT

AS
BEGIN

	SET NOCOUNT ON;
	declare @Code Int = 200,
			@Message varchar(500) = ''

		SET @Message = 'Customer Data Retrived Successfully'

		SELECT @Code as Code,
			   @Message as [Message]

	select
				C.*,
				C.FirstName + ' ' + ISNULL(C.LastName, '') as 'Name',
				(A.Address1 + ' ' + isnull(A.Address2,'') +' '+ A.Area +' ' + A.City +' '+ A.State+' '+ isnull(L.Name,'') + ' '+ A.Pincode) as 'AddressString',
				(Select count(*) from Orders where CustomerID = C.ID) as 'TotalOrders',
				(Select SUM(Amount) from OrderDetails OD left join Orders O on OD.OrderID = O.ID where O.CustomerID = C.ID) as 'TotalSales',
				(Select TOP(1) OrderDate from Orders where CustomerID = C.ID order by OrderDate desc) as 'LastOrder'
			from [dbo].[Customers] C
			Left join Address A on A.ID = C.Address
			Left join LookupCountry L on L.Id = A.CountryID
		WHERE
				C.Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Customers_ChangePassword]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[Customers_ChangePassword]
	   @Id BIGINT,
	   @OldPassword varchar(500) = NULL,
	   @NewPassword varchar(500) = NULL,
	   @ConfirmPassword varchar(500) = NULL

AS
BEGIN
	
	SET NOCOUNT ON;

  DECLARE @Code INT = 400,
	      @Message VARCHAR(255) = ''

	
		 IF @OldPassword IS NULL OR @OldPassword = ''
		 BEGIN
		      SET @Code = 403
			  SET @Message = 'Old password is required'
			  SET @Id = 0
		 END
		 ELSE IF @NewPassword IS NULL OR @NewPassword = ''
		 BEGIN
		      SET @Code = 403
			  SET @Message = 'New password is required'
			  SET @Id = 0
		 END
		 ELSE IF @ConfirmPassword IS NULL OR @ConfirmPassword = ''
		 BEGIN
		      SET @Code = 403
			  SET @Message = 'Confirm password is required'
			  SET @Id = 0
		 END
		 ELSE IF LOWER(@NewPassword) != LOWER(@ConfirmPassword) 
		 BEGIN
		      SET @Code = 403
			  SET @Message = 'New password does not match the confirm password'
			  SET @Id = 0
		 END
		 ELSE IF (SELECT COUNT(*) FROM [Customers] WHERE Id = @Id AND LOWER([Password]) = LOWER(@OldPassword)) = 0
		 BEGIN
		      SET @Code = 403
			  SET @Message = 'Old password does not match'
			  SET @Id = 0
		 END
		 ELSE
		 BEGIN
		     UPDATE 
			       [Customers]
			 SET 
			      [Password] = @NewPassword
			 WHERE 
			      Id = @Id

			 SET @Code = 200
			 SET @Message = 'Password changes successfully'
		 END
	

		SELECT @Code as Code,
			   @Message as [Message]

		SELECT 
				[Id],
				[Name],
				[Email],
				[Password],
				[AddressLine1],
				[AddressLine2],
				[LookupCountryId],
				[LookupStateId],
				[LookupCityId],
				[Phone],
				[AlternatePhone],
				[LookupStatusId],
				[LastLogin],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
		FROM
				[dbo].[Customers]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Customers_Delete]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--select * from Customers
--EXEC [Customers_Delete] 5, 5

CREATE Procedure [dbo].[Customers_Delete]

@Id BIGINT,
@DeletedBy Bigint
	
AS
BEGIN
	SET nocount on;
	declare @Code int = 400,
	@Message varchar(500) = ''

	update Customers set DeletedDate = GETUTCDATE() , DeletedBy = @DeletedBy
	
	where Id = @Id

	SET @Code = 200
	SET @Message = 'Customers data Deleted Successfully'

	select @Code as Code,
	@Message as Message

	select 
				[Id],
				[Name],
				[Email],
				[Password],
				[AddressLine1],
				[AddressLine2],
				[LookupCountryId],
				[LookupStateId],
				[LookupCityId],
				[Phone],
				[AlternatePhone],
				[LookupStatusId],
				[LastLogin],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]

	from Customers
	where Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[Customers_Login]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[Customers_Login]

    @Email varchar(500) = null,
	@Password varchar(500) = null
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @Code Int = 400,
		@Message varchar(500) = '',
		@Id Int = 0

	IF @Email is null or @Email = ''
	BEGIN 
		SET @Id = 0
		SET @Code = 400
		SET @Message = 'Email is Required'
	END
	ELSE IF @Password is null or @Password = ''
	BEGIN
		SET @Id = 0
		SET @Code = 400
		SET @Message = 'Password is Required'
	END

	ELSE 
	BEGIN
		Select @Id = Id From Customers where Email = @Email

		IF @Id > 0
		BEGIN
			IF(select count(*) from Customers where Id = @Id and Password = @Password) > 0
			BEGIN
				
					SET @Code = 200
					SET @Message = 'Customers Login in Successfully'
				END
				
			ELSE 
			BEGIN
				SET @Code = 400
				SET @Message = 'Password Is Incorrect !'
			END
		END
		ELSE 
		BEGIN
			SET @Code = 400
			SET @Message = 'Email Is Not Registerd !'
		END
	END

	Select @Code as Code,
		@Message as Message

	SELECT 
				[Id],
				[Name],
				[Email],
				[Password],
				[AddressLine1],
				[AddressLine2],
				[LookupCountryId],
				[LookupStateId],
				[LookupCityId],
				[Phone],
				[AlternatePhone],
				[LookupStatusId],
				[LastLogin],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
		FROM
				[dbo].[Customers]
	WHERE Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[Customers_Logout]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--select * from Customers
CREATE Procedure [dbo].[Customers_Logout]

@Id BIGINT

AS
BEGIN
	
	SET NOCOUNT ON;

	UPDATE Customers SET LastLogin = GETDATE()
	
	WHERE Id = @Id
	
	SELECT 1
   
END
GO
/****** Object:  StoredProcedure [dbo].[Customers_Upsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--SELECT* from Customers

--EXEC [Customers_Upsert] 0,'arjun','adskijiik22@gmail.com','12113','HASTINAPUR','hastinapur',5,4,3,'9856985698','845789662',2

CREATE procedure [dbo].[Customers_Upsert]
	@Id bigint,
    @FirstName nvarchar(max),
	@LastName nvarchar(max),
	@Email nvarchar(255),
	@Password nvarchar(256),
	@Status int,
	@Address bigint,
	@AvtarURL nvarchar(max),
	@Phone nchar(16),
	@DelFlg bit,
	@CreatedBy int,	
	@UpdatedBy int

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					IF @FirstName IS null or @FirstName = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Name Is required'
					END
					Else IF @Email IS null or @Email = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Email Is required'
					END
					ELSE IF @Email NOT LIKE '%@%._%'
					BEGIN
							SET @Id = 0
		  					SET @Code = 400
		  					SET @Message = 'Email is invalid !'
					END
					ELSE IF (select count(*) from [Customers] where lower(Email) = lower(@Email) and Id != @Id) > 0
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = ' Already Exsits Email Please Enter New Email  '
					END	
					ELSE IF @Password is null or @Password = ''
		                      BEGIN
			                  SET @Id = 0
			                  SET @Code = 400
			                  SET @Message = 'Password Is required'
		                 END
					ELSE IF @Phone  IS null or @Phone = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Phone  Is required'
					END
					ELSE
					BEGIN
							INSERT INTO [Customers] (
													[FirstName],
													[LastName],
													[Email],
													[Password],
													[Status],
													[Address],
													[AvtarURL],
													[Phone],
													[DelFlg],
													[CreatedAt],
													[CreatedBy]													
												) VALUES (
													@FirstName,
													@LastName,
													@Email,
													@Password,
													@Status,
													@Address,
													@AvtarURL,
													@Phone,
													@DelFlg,
													GetDAte(),
													@CreatedBy													
												)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = ' Customers Created Successfully'
					END
			END
			ELSE IF @Id > 0
			BEGIN

					IF @FirstName IS null or @FirstName = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Name Is required'
					END
					Else IF @Email IS null or @Email = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Email Is required'
					END
					ELSE IF @Email NOT LIKE '%@%._%'
					BEGIN
							SET @Id = 0
		  					SET @Code = 400
		  					SET @Message = 'Email is invalid !'
					END
					ELSE IF (select count(*) from [Customers] where lower(Email) = lower(@Email) and Id != @Id) > 0
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = ' Already Exsits Email Please Enter New Email  '
					END	
					ELSE IF @Password is null or @Password = ''
		                      BEGIN
			                  
			                  SET @Code = 400
			                  SET @Message = 'Password Is required'
		                 END
						 
		              
					ELSE IF @Phone  IS null or @Phone = ''
					BEGIN							
							SET @Code = 400
							SET @Message = 'Phone  Is required'
					END					
					ELSE
					BEGIN
							UPDATE  Customers
								SET		
									[FirstName] = ISNULL(@FirstName, [FirstName]),
									[LastName] = @LastName,
									[Email] = @Email,
									[Password] = @Password,
									[Status] = @Status,
									[Address] = @Address,
									[AvtarURL] = @AvtarURL,
									[Phone] = Phone,
									[DelFlg] = @DelFlg,									
									[UpdatedAt] = GETDATE(),
									[UpdatedBy] = @UpdatedBy
							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'Customers Updated Successfully'
					END
			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT * FROM [dbo].[Customers] WHERE Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[DailyReport_Select]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DailyReport_Select]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'Daily Report Retrived Successfully.'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT D.*, 
		   T.FirstName + ' ' + ISNULL(T.LastName, '') as TraderName,
		   V.Name as VisitPurposeName
	FROM [dbo].DailyReport D
		left join Traders T on T.Id = D.TraderId
		left join VisitPurpose V on D.VisitPurposeID = V.Id
		WHERE D.Id = @Id and D.DelFlg = 0  and V.DelFlg = 0 and T.DelFlg=0
END
GO
/****** Object:  StoredProcedure [dbo].[DailyReport_SelectAll]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DailyReport_SelectAll]
	@Offset BIGINT,
	@Limit BIGINT,
    @Date date
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from DailyReport D
								left join Traders T on T.Id = D.TraderId
								left join VisitPurpose V on D.VisitPurposeID = V.Id
								where (isnull(@Date,'') = '' or (CAST(D.Date as DATE) = @Date)) AND D.DelFlg = 0 and V.DelFlg = 0 and T.DelFlg=0
							)

		IF @Limit = 0 OR @Limit = -1
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT D.*, 
		   T.FirstName + ' ' + ISNULL(T.LastName, '') as TraderName,
		   V.Name as VisitPurposeName
	FROM [dbo].DailyReport D
		left join Traders T on T.Id = D.TraderId
		left join VisitPurpose V on D.VisitPurposeID = V.Id
		where (isnull(@Date,'') = '' or (CAST(D.Date as DATE) = @Date)) AND D.DelFlg = 0 and V.DelFlg = 0 and T.DelFlg=0			 
		order by D.Id		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[DailyReport_Upsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DailyReport_Upsert] (
	@Id bigint,
	@TraderId bigint,	
	@IsSubmitted bit,
	@Date datetime,
	@CustomerName nvarchar(max),
	@CustomerLocation nvarchar(max),
	@NoOfCows int,
	@VisitPurposeID tinyint,
	@NewOrder bit,
	@Summary nvarchar(max),
	@DelFlg bit,
	@CreatedBy bigint,
	@UpdatedBy bigint
)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

		
SELECT @Code AS Code, @Message AS [Message]

-- Update
IF @Id > 0
	BEGIN
	UPDATE [DailyReport]
	SET
		[Date] = ISNULL(@Date,[Date]),
		[CustomerName] = ISNULL(@CustomerName,[CustomerName]),
		[CustomerLocation] = ISNULL(@CustomerLocation,[CustomerLocation]),
		[NoOfCows] = ISNULL(@NoOfCows,[NoOfCows]),
		[VisitPurposeID] = ISNULL(@VisitPurposeID,[VisitPurposeID]),
		[NewOrder] = ISNULL(@NewOrder,[NewOrder]),
		[Summary] = ISNULL(@Summary,[Summary]),
		[DelFlg] = @DelFlg,
		[UpdatedAt] = GETDATE(),
		[UpdatedBy] = @UpdatedBy,
		[TraderId] = IsNULL(@TraderId,TraderId),
		[IsSubmitted] = @IsSubmitted
	WHERE
		[Id] = @Id

	SELECT * from [DailyReport] where Id = @Id
	END

--INSERT
ELSE
	BEGIN
		INSERT INTO [DailyReport] (
									[Date],
									[CustomerName],
									[CustomerLocation],
									[NoOfCows],
									[VisitPurposeID],
									[NewOrder],
									[Summary],
									[DelFlg],
									[CreatedAt],
									[CreatedBy],
									[TraderId],
									[IsSubmitted]
								) VALUES (
									@Date,
									@CustomerName,
									@CustomerLocation,
									@NoOfCows,
									@VisitPurposeID,
									@NewOrder,
									@Summary,
									@DelFlg,
									GETDATE(),
									@CreatedBy,
									@TraderId,
									@IsSubmitted
								)
		
		SELECT * from [DailyReport] where Id = SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[KnowledgeBase_All]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec KnowledgeBase_All '',0,0,0
CREATE Proc [dbo].[KnowledgeBase_All]
@Search VARCHAR(MAX),
	@Offset Bigint,
	@Limit Bigint,
	@LookupKnowledgeBaseCategoryId Bigint

AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @TotalRecords BIGINT;

	SET @TotalRecords = (
	    SELECT 
		      COUNT(*) 
	    FROM 
		      [dbo].[KnowledgeBase] K
			  left join 
			  LookupKnowledgeBaseCategory LC  on K.LookupKnowledgeBaseCategoryId = LC.Id
			  
			  WHERE 
				(
					(  ISNULL(@Search,'') = '' 
					OR (LOwER(K.ArticleName) like '%'+LOWER(@Search)+'%')
					OR (LOwER(LC.Name) like '%'+LOWER(@Search)+'%')
				

				)
				and
					(@LookupKnowledgeBaseCategoryId = 0 or K.LookupKnowledgeBaseCategoryId = @LookupKnowledgeBaseCategoryId)
					
					)
					and 
				K.DeletedBy =0
				)
				

	IF @Limit = 0
	BEGIN
	     SET @Limit = @TotalRecords
		 IF @Limit = 0
		 BEGIN
		     SET @Limit = 10
		 END
	END
	 
	SELECT 

			    K.Id,
				K.ArticleName,
				K.LookupKnowledgeBaseCategoryId,
				LC.Name As LookupKnowledgeBaseCategoryName,
				K.ArticleContant,
				K.DatePublished,
				K.LastChanged,
				K.LookupStatus,
				K.CreatedDate,
				K.CreatedBy,
				K.UpdatedDate,
				K.UpdatedBy,
				K.DeletedDate,
				K.DeletedBy
	FROM 
	    [dbo].[KnowledgeBase] k
			  left join 
			  [LookupKnowledgeBaseCategory] LC on K.LookupKnowledgeBaseCategoryId = LC.Id
			  
	
	WHERE 
	(
		(  ISNULL(@Search,'') = '' 
		            OR (LOwER(K.ArticleName) like '%'+LOWER(@Search)+'%')
					OR (LOwER(LC.Name) like '%'+LOWER(@Search)+'%')
				
	)
	and
	(@LookupKnowledgeBaseCategoryId = 0 or K.LookupKnowledgeBaseCategoryId = @LookupKnowledgeBaseCategoryId)
	)
		
	ORDER BY 
	      K.Id
    DESC
		  OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY 


	select @TotalRecords as TotalRecords
	
END
GO
/****** Object:  StoredProcedure [dbo].[KnowledgeBase_ById]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec KnowledgeBase_ById 3
CREATE Proc [dbo].[KnowledgeBase_ById]

@Id BIGINT

AS
BEGIN 
	set nocount on;
	declare @Code int = 200,
		@Message varchar(50) = ''

	SET @Message = 'KnowledgeBase Data retrive successfully'

	select @Code as Code,
		@Message as Message

	SELECT 

			    K.Id,
				K.ArticleName,
				K.LookupKnowledgeBaseCategoryId,
				LC.Name As LookupKnowledgeBaseCategoryName,
				K.ArticleContant,
				K.DatePublished,
				K.LastChanged,
				K.LookupStatus,
				K.CreatedDate,
				K.CreatedBy,
				K.UpdatedDate,
				K.UpdatedBy,
				K.DeletedDate,
				K.DeletedBy
	FROM 
	    [dbo].[KnowledgeBase] k
			  left join 
			  [LookupKnowledgeBaseCategory] LC on K.LookupKnowledgeBaseCategoryId = LC.Id
			  
	where K.Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[KnowledgeBase_Delete]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec KnowledgeBase_Delete 3,3
CREATE Proc [dbo].[KnowledgeBase_Delete]

@Id BIGINT,
@DeletedBy BIGINT
	
AS
BEGIN
	SET nocount on;
	declare @Code int = 400,
	@Message varchar(500) = ''

	update KnowledgeBase set DeletedDate = GETUTCDATE() , DeletedBy = @DeletedBy
	
	where Id = @Id

	SET @Code = 200
	SET @Message = 'KnowledgeBase data Deleted Successfully'

	select @Code as Code,
	@Message as Message

	select 
						        Id,
				ArticleName,
				LookupKnowledgeBaseCategoryId,
				ArticleContant,
				DatePublished,
				LastChanged,
				LookupStatus,
				CreatedDate,
				CreatedBy,
				UpdatedDate,
				UpdatedBy,
				DeletedDate,
				DeletedBy

	from KnowledgeBase
	where Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[KnowledgeBase_Upsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec KnowledgeBase_Upsert 3,'K Parivahan',3,'asnd','gh','Tud','gh',0,0
CREATE Proc [dbo].[KnowledgeBase_Upsert]
     @Id BIGINT = 0,
		@ArticleName VARCHAR(500) ,
		@LookupKnowledgeBaseCategoryId Bigint,
		@ArticleContant varchar(max) ,
		@DatePublished VARCHAR(250) ,
		@LastChanged  VARCHAR(250) ,
		@LookupStatus varchar(50),
		@CreatedBy BIGINT ,
		@UpdatedBy BIGINT 

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					IF @ArticleName IS null or @ArticleName = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'ArticleName Is required'
					END
					Else IF @LookupKnowledgeBaseCategoryId =0
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'LookupKnowledgeBaseCategoryId Is required'
					END
					ELSE IF @ArticleContant is null or @ArticleContant	 = ''
		                      BEGIN
			                  SET @Id = 0
			                  SET @Code = 400
			                  SET @Message = 'ArticleContant Is required'
		                 END
		              
					ELSE IF @LastChanged  IS null or @LastChanged = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Phone  Is required'
					END
					ELSE IF @LookupStatus  is null or @LookupStatus  = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'LookupStatus  is required'
					END
					
					ELSE
					BEGIN
							INSERT INTO KnowledgeBase(  
												ArticleName,
												LookupKnowledgeBaseCategoryId,
												ArticleContant,
												DatePublished,
												LastChanged,
												LookupStatus,
												CreatedDate,
												CreatedBy,
												DeletedBy )
									VALUES (
											@ArticleName,
											@LookupKnowledgeBaseCategoryId,
											@ArticleContant,
											@DatePublished,
											@LastChanged,
											@LookupStatus,
											GETUTCDATE(),
											@CreatedBy,
											0)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = ' KnowledgeBase Created Successfully'
					END
			END
			ELSE IF @Id > 0
			BEGIN

					IF @ArticleName IS null or @ArticleName = ''
					BEGIN
							SET @Code = 400
							SET @Message = 'ArticleName Is required'
					END
					Else IF @LookupKnowledgeBaseCategoryId =0
					BEGIN
							SET @Code = 400
							SET @Message = 'LookupKnowledgeBaseCategoryId Is required'
					END
					ELSE IF @ArticleContant is null or @ArticleContant	 = ''
		                      BEGIN
			                  SET @Code = 400
			                  SET @Message = 'ArticleContant Is required'
		                 END
		              
					ELSE IF @LastChanged  IS null or @LastChanged = ''
					BEGIN
							SET @Code = 400
							SET @Message = 'Phone  Is required'
					END
					ELSE IF @LookupStatus  is null or @LookupStatus  = ''
					BEGIN
							SET @Code = 400
							SET @Message = 'LookupStatus  is required'
					END
					ELSE
					BEGIN
							UPDATE  KnowledgeBase
							SET		
									ArticleName = ISNULL(@ArticleName ,ArticleName ),
									LookupKnowledgeBaseCategoryId = @LookupKnowledgeBaseCategoryId,
									ArticleContant = ISNULL(@ArticleContant,ArticleContant),
									DatePublished  = ISNULL(@DatePublished,DatePublished),
									LastChanged  = ISNULL(@LastChanged,LastChanged),				
									LookupStatus  = ISNULL(@LookupStatus,LookupStatus),
									UpdatedDate = GETUTCDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'KnowledgeBase Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
		        Id,
				ArticleName,
				LookupKnowledgeBaseCategoryId,
				ArticleContant,
				DatePublished,
				LastChanged,
				LookupStatus,
				CreatedDate,
				CreatedBy,
				UpdatedDate,
				UpdatedBy,
				DeletedDate,
				DeletedBy
		FROM
				[dbo].[KnowledgeBase]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[KnowledgeBaseCategory_All]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec KnowledgeBaseCategory_All '',0,0
CREATE Proc [dbo].[KnowledgeBaseCategory_All]
    @Search VARCHAR(MAX),
	@Offset BIGINT,
	@Limit BIGINT

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from LookupKnowledgeBaseCategory
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') 
											
										)	
									  )
									  
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  [Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]

		from  [dbo].[LookupKnowledgeBaseCategory] 

		where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') 
											
										)	
									  
					
			  )

		order by Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[KnowledgeBaseCategory_Upsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--eXEC KnowledgeBaseCategory_Upsert 5,'akash',0,0 
CREATE Proc [dbo].[KnowledgeBaseCategory_Upsert]
   @Id  bigint,
   @Name  varchar(250),
   @CreatedBy BIGINT,
   @UpdatedBy Bigint

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					IF @Name IS null or @Name = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Name Is required'
					END
					ELSE
					BEGIN
							INSERT INTO LookupKnowledgeBaseCategory(  
							                    Name,
												CreatedDate,
												CreatedBy
										)
									VALUES (
											@Name,
											GETUTCDATE(),
											@CreatedBy)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = ' LookupKnowledgeBaseCategory Created Successfully'
					END
			END
			ELSE IF @Id > 0
			BEGIN

					IF @Name IS null or @Name = ''
					BEGIN
							
							SET @Code = 400
							SET @Message = '@Name Is required'
					END
					ELSE
					BEGIN
							UPDATE  LookupKnowledgeBaseCategory
							SET		
									Name  = ISNULL(@Name ,Name ),
									UpdatedDate = GETUTCDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'LookupKnowledgeBaseCategory Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy]
		FROM
				[dbo].[LookupKnowledgeBaseCategory]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[LookupCity_All]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from LookupCity 
--exec [LookupCity_All]'DHWARKA',0,0

CREATE Procedure [dbo].[LookupCity_All]
	@Search VARCHAR(MAX),
	@Offset BIGINT,
	@Limit BIGINT

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from LookupCity
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') 

										)	
									  )
									  And
									  DeletedBy = 0
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  [Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]

		from  [dbo].[LookupCity] 

		where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') 
											
										)	
									  
					And
					DeletedBy = 0
			  )

		order by Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[LookupCity_ById]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from LookupCity

--exec [LookupCity_ById]1
CREATE Procedure [dbo].[LookupCity_ById]

@Id BIGINT

AS
BEGIN 
	set nocount on;
	declare @Code int = 200,
		@Message varchar(50) = ''

	SET @Message = 'LookupCity Data retrive successfully'

	select @Code as Code,
		@Message as Message

	select 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
	from
		LookupCity
	where Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[LookupCity_Delete]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from LookupCity
--EXEC [LookupCity_Delete]2,01

CREATE Procedure [dbo].[LookupCity_Delete]

@Id BIGINT,
@DeletedBy BIGINT
	
AS
BEGIN
	SET nocount on;
	declare @Code int = 400,
	@Message varchar(500) = ''

	update LookupCity set DeletedDate = GETDATE() , DeletedBy = @DeletedBy
	
	where Id = @Id

	SET @Code = 200
	SET @Message = 'LookupCity data Deleted Successfully'

	select @Code as Code,
	@Message as Message

	select 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]

	from LookupCity
	where Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[LookupCity_Upsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--SELECT* from LookupCity

--EXEC [LookupCity_Upsert]0,'ANAND'

CREATE procedure [dbo].[LookupCity_Upsert]

        @Id BIGINT = 0,
		@Name VARCHAR(500) = null,
		@CreatedBy BIGINT = 0,
		@UpdatedBy BIGINT = 0

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					IF @Name IS null or @Name = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Name Is required'
					END
					
					ELSE IF (select count(*) from [LookupCity] where lower(Name) = lower(@Name) ) > 0
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = ' Already Exsits Name Please Enter New Name  '
					END	
					
					
					ELSE
					BEGIN
							INSERT INTO LookupCity(  Name,
														CreatedDate,
														CreatedBy,
														DeletedBy )
									VALUES (
														@Name,
														GETDATE(),
														@CreatedBy,
														0 )

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = ' LookupCity Created Successfully'
					END
			END
			ELSE IF @Id > 0
			BEGIN

					IF @Name IS null or @Name = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Name Is required'
					END
					
					ELSE IF (select count(*) from [LookupCity] where lower(Name) = lower(@Name) ) > 0
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = ' Already Exsits Name Please Enter New Name  '
					END	
					
					ELSE
					BEGIN
							UPDATE  LookupCity
							SET		
											Name  = ISNULL(@Name ,Name ),
												
											UpdatedDate = GETDATE(),
											UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'LookupCity Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
		FROM
				[dbo].[LookupCity]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[LookupCountry_All]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from LookupCountry 
--exec [LookupCountry_All]'026989658525',0,0
CREATE Procedure [dbo].[LookupCountry_All]
	@Search VARCHAR(MAX),
	@Offset BIGINT,
	@Limit BIGINT

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from LookupCountry
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') 

										)	
									  )
									  And
									  DeletedBy = 0
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  [Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]

		from  [dbo].[LookupCountry] 

		where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') 
											
										)	
									  
					And
					DeletedBy = 0
			  )

		order by Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[LookupCountry_ById]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from LookupCountry

--exec [LookupCountry_ById]3
CREATE Procedure [dbo].[LookupCountry_ById]

@Id BIGINT

AS
BEGIN 
	set nocount on;
	declare @Code int = 200,
		@Message varchar(50) = ''

	SET @Message = 'LookupCountry Data retrive successfully'

	select @Code as Code,
		@Message as Message

	select 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
	from
		LookupCountry
	where Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[LookupCountry_Delete]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from LookupCountry
--EXEC [LookupCountry_Delete]5,01

CREATE Procedure [dbo].[LookupCountry_Delete]

@Id BIGINT,
@DeletedBy BIGINT
	
AS
BEGIN
	SET nocount on;
	declare @Code int = 400,
	@Message varchar(500) = ''

	update LookupCountry set DeletedDate = GETDATE() , DeletedBy = @DeletedBy
	
	where Id = @Id

	SET @Code = 200
	SET @Message = 'LookupCountry data Deleted Successfully'

	select @Code as Code,
	@Message as Message

	select 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]

	from LookupCountry
	where Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[LookupCountry_Upsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--SELECT* from LookupCountry

--EXEC [LookupCountry_Upsert]0,'5uh','jijj@gmail.com','12j3','026529658525','9566985698'

CREATE procedure [dbo].[LookupCountry_Upsert]

        @Id BIGINT = 0,
		@Name VARCHAR(500) = null,
		@CreatedBy BIGINT = 0,
		@UpdatedBy BIGINT = 0

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					IF @Name IS null or @Name = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Name Is required'
					END
					
					ELSE IF (select count(*) from [LookupCountry] where lower(Name) = lower(@Name) ) > 0
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = ' Already Exsits Name Please Enter New Name  '
					END	
					
					
					ELSE
					BEGIN
							INSERT INTO LookupCountry(  Name,
														CreatedDate,
														CreatedBy,
														DeletedBy )
									VALUES (
														@Name,
														GETDATE(),
														@CreatedBy,
														0 )

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = ' LookupCountry Created Successfully'
					END
			END
			ELSE IF @Id > 0
			BEGIN

					IF @Name IS null or @Name = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Name Is required'
					END
					
					ELSE IF (select count(*) from [LookupCountry] where lower(Name) = lower(@Name) ) > 0
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = ' Already Exsits Name Please Enter New Name  '
					END	
					
					ELSE
					BEGIN
							UPDATE  LookupCountry
							SET		
											Name  = ISNULL(@Name ,Name ),
												
											UpdatedDate = GETDATE(),
											UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'LookupCountry Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
		FROM
				[dbo].[LookupCountry]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[LookupProductCategoryAndSubCategory_All]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec LookupProductCategoryAndSubCategory_All '',0,0,0
CREATE Proc [dbo].[LookupProductCategoryAndSubCategory_All]
@Search varchar(MAX),
	@Offset BIGINT,
	@Limit BIGINT,
	@ParentId BIGINT

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select count(*) from [dbo].[LookupProductCategoryAndSubCategory]LCC 
								
								left join [LookupProductCategoryAndSubCategory] LC
								on LCC.ParentId = LC.Id
			
								
			
								
								
								where (@ParentId = 0 or  LCC.ParentId = @ParentId) 
									  

								AND
								(
									isnull(@Search,'') = '' or
									(
									    
										
										(lower(LCC.Name) like '%'+lower(@Search)+'%')OR
										(lower(LC.ParentId) like '%'+lower(@Search)+'%')

									)
								)
								
							and
							LCC.DeletedBy=0
								
							)

		IF @Limit = 0
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select
				
				LCC.Id,
				LCC.Name,
				LCC.ParentId,
				LCC.CreatedDate,
				LCC.CreatedBy,
				LCC.UpdatedDate,
				LCC.UpdatedBy,
				LCC.DeletedDate,
				LCC.DeletedBy
			
				
		 from [dbo].[LookupProductCategoryAndSubCategory] LCC
								
								left join [LookupProductCategoryAndSubCategory] LC
								on LCC.ParentId = LC.Id
			
								
								
								where (@ParentId = 0 or  LCC.ParentId = @ParentId) 
									  

								AND
								(
									isnull(@Search,'') = '' or
									(
									    
										(lower(LCC.Name) like '%'+lower(@Search)+'%')OR
										(lower(LC.ParentId) like '%'+lower(@Search)+'%')
										


				)
			)
		
		and
							LCC.DeletedBy=0
			
			order by LCC.Id 
			
			desc
			
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
				
END
GO
/****** Object:  StoredProcedure [dbo].[LookupProductCategoryAndSubCategory_ById]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec LookupProductCategoryAndSubCategory_ById 2
CREATE Proc [dbo].[LookupProductCategoryAndSubCategory_ById]
	@Id BIGINT
AS
BEGIN

	SET NOCOUNT ON;

	Declare 
	        @Code Int = 200,
            @Message varchar(200) = ''

	SET @Message = 'Data Retrived Successfully'

	       Select @Code as Code,
			      @Message as [Message]


				Select 
					LCC.Id,
				LCC.Name,
				LCC.ParentId,
				LCC.CreatedDate,
				LCC.CreatedBy,
				LCC.UpdatedDate,
				LCC.UpdatedBy,
				LCC.DeletedDate,
				LCC.DeletedBy
			
				
		 from [dbo].[LookupProductCategoryAndSubCategory] LCC
								
								left join [LookupProductCategoryAndSubCategory] LC
								on LCC.ParentId = LC.Id
					  		       
				WHERE
					  LCC.Id = @Id 
END
GO
/****** Object:  StoredProcedure [dbo].[LookupProductCategoryAndSubCategory_Delete]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from LookupProductCategoryAndSubCategory
--Exec LookupProductCategoryAndSubCategory_Delete 4,4
CREATE Proc [dbo].[LookupProductCategoryAndSubCategory_Delete]
@Id Bigint,
	@DeletedBy Bigint
AS
BEGIN

SET NOCOUNT ON;

	DECLARE @Code INT = 400,
	        @Message VARCHAR(255) = ''
 
               
				UPDATE
					LookupProductCategoryAndSubCategory
				SET 
				    DeletedBy = @DeletedBy,
				    DeletedDate = getutcdate() 
					
				WHERE
					Id=@Id
				

				SET @Code = 200
			    SET @Message = 'Deleted Successfully'

				
			    SELECT @Code as Code,@Message as [Message]
			         
			  Select 
                  Id
				,Name
				,ParentId
				,CreatedDate
				,CreatedBy
				,UpdatedDate
				,UpdatedBy
				,DeletedDate
				 DeletedBy
	            FROM 
	                [dbo].[LookupProductCategoryAndSubCategory] 
			       
				WHERE
					  Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[LookupProductCategoryAndSubCategory_Upsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec LookupProductCategoryAndSubCategory_Upsert 4,'GHI','3',0,0
CREATE Proc [dbo].[LookupProductCategoryAndSubCategory_Upsert]
        @Id Bigint,
		@Name varchar(250),		
		@ParentId Bigint,
		@CreatedBy Bigint,
		@UpdatedBy Bigint
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @Code INT = 400,
	        @Message VARCHAR(255) = ''

	IF @Id = 0
	BEGIN
	      IF @Name IS NULL OR @Name = ''
		  BEGIN
			   SET @Id = 0
			   SET @Code = 400
			   SET @Message = ' Name Is Required'
		  END
		  
		  ELSE IF @ParentId = 0
		  BEGIN
		       SET @Id = 0
			   SET @Code = 400
			   SET @Message = 'ParentId Is Required'
		  END		
		  ELSE
		  BEGIN
		       INSERT INTO LookupProductCategoryAndSubCategory
			   (Name,ParentId,CreatedDate,CreatedBy,DeletedBy)
			   VALUES
			   (@Name,@ParentId,getutcdate(),@CreatedBy,0)
			   SET @Id = SCOPE_IDENTITY()
			   SET @Code = 200
			   SET @Message = 'ProductCategory Created successfully'
		  END 
    END
	ELSE IF @Id > 0
	     BEGIN
	      IF @Name IS NULL OR @Name = ''
		  BEGIN			   
			   SET @Code = 400
			   SET @Message = ' Name Is Required'
		  END
		  		  	
		  ELSE IF @ParentId = 0
		  BEGIN		       
			   SET @Code = 400
			   SET @Message = 'ParentId Is Required'
		  END		
			 ELSE
		     BEGIN
		          UPDATE 
			            LookupProductCategoryAndSubCategory
			      SET
					 Name = ISNULL(@Name,Name),
					 
					 ParentId = @ParentId,				
					 UpdatedBy = @UpdatedBy,
                     UpdatedDate = getutcdate() 
			   WHERE 
			         Id = @Id
			   
			   SET @Code = 200
			   SET @Message = 'ProductCategory updated successfully'
		  END
	END

	SELECT @Code as Code,@Message as [Message]
	

           	  Select 
				Id
				,Name
				,ParentId
				,CreatedDate
				,CreatedBy
				,UpdatedDate
				,UpdatedBy
				,DeletedDate
				DeletedBy
	            FROM 
	                [dbo].[LookupProductCategoryAndSubCategory]
	            WHERE
	                 Id = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[LookupProductImages_All]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec LookupProductImages_All '',0,0
CREATE Proc [dbo].[LookupProductImages_All]
@Search VARCHAR(MAX),
@Offset BIGINT,
	@Limit BIGINT
    
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from LookupProductImages
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(ProductImage1) like '%'+lower(@Search)+'%') 
											 
											

										)	
									  )AND
				                    Deletedby = 0
									  
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
			 ID,	
              ProductImage1 ,
              ProductImage2 ,
              ProductImage3 ,
              ProductImage4 ,
              ProductImage5 ,
              ProductImage6 ,
              ProductImage7 ,
              ProductImage8 ,
              ProductImage9 ,
              ProductImage10,
			  CreatedDate,
			  createdBy,
			  UpdatedDate,
              UpdatedBy,
			  DeletedDate,
			  DeletedBy
             
				
		FROM
				[dbo].[LookupProductImages]

		where ( isnull(@Search,'') = '' or
					(
						(lower(ProductImage1) like '%'+lower(@Search)+'%') 
						
						
					)
					
					)AND
				                    Deletedby = 0
			  

		order by   Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[LookupProductImages_ById]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec LookupProductImages_ById 2
CREATE Proc [dbo].[LookupProductImages_ById]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'LookupProductImages Data Retrived Successfully'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				 ID,	
              ProductImage1 ,
              ProductImage2 ,
              ProductImage3 ,
              ProductImage4 ,
              ProductImage5 ,
              ProductImage6 ,
              ProductImage7 ,
              ProductImage8 ,
              ProductImage9 ,
              ProductImage10,
			  CreatedDate,
			  createdBy,
			  UpdatedDate,
              UpdatedBy,
			  DeletedDate,
			  DeletedBy
             
				
		FROM
				[dbo].[LookupProductImages]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[LookupProductImages_Delete]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec LookupProductImages_Delete 2,2
CREATE Proc [dbo].[LookupProductImages_Delete]
@Id BIGINT = 0,
	@DeletedBy BIGINT = 0


AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code int = 400,
				@Message varchar(500) = ''

		update Products set DeletedDate = GETUTCDATE() , DeletedBy = @DeletedBy

		where Id = @Id

		SET @Code = 200
		SET @Message = 'Products Data Deleted Successfully'

		select @Code as Code,
			   @Message as Message

		
	SELECT 
                 ID,	
              ProductImage1 ,
              ProductImage2 ,
              ProductImage3 ,
              ProductImage4 ,
              ProductImage5 ,
              ProductImage6 ,
              ProductImage7 ,
              ProductImage8 ,
              ProductImage9 ,
              ProductImage10,
			  CreatedDate,
			  createdBy,
			  UpdatedDate,
              UpdatedBy,
			  DeletedDate,
			  DeletedBy
             
				
		FROM
				[dbo].[LookupProductImages]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[LookupProductImages_Upsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec LookupProductImages_Upsert 5,2,2,2,2,4,4,4,2,2,2,0,0
CREATE Proc [dbo].[LookupProductImages_Upsert]
		@Id bigint,	
        @ProductImage1 varchar(max),
        @ProductImage2 varchar(max),
        @ProductImage3 varchar(max),
        @ProductImage4 varchar(max),
        @ProductImage5 varchar(max),
        @ProductImage6 varchar(max),
        @ProductImage7 varchar(max),
        @ProductImage8 varchar(max),
        @ProductImage9 varchar(max),
        @ProductImage10 varchar(max),
		@CreatedBy BIGINT = 0,
	    @UpdatedBy BIGINT = 0
		
		
AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''
               
			
					IF @Id = 0
					BEGIN
							INSERT INTO  LookupProductImages
												
											 (
												
												ProductImage1 ,
												ProductImage2 ,
												ProductImage3 ,
												ProductImage4 ,
												ProductImage5 ,
												ProductImage6 ,
												ProductImage7 ,
												ProductImage8 ,
												ProductImage9 ,
												ProductImage10, 
												 CreatedDate,
												 createdBy,
												 DeletedBy
												 
												 

												)

									VALUES (
											
											@ProductImage1 ,
											@ProductImage2 ,
											@ProductImage3 ,
											@ProductImage4 ,
											@ProductImage5 ,
											@ProductImage6 ,
											@ProductImage7 ,
											@ProductImage8 ,
											@ProductImage9 ,
											@ProductImage10,
											GETUTCDATE(),
											@createdBy,
											0
											
											
											)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = 'LookupProductImages Data Created Successfully'
				END	
				

			
		

				   ELSE	IF @Id > 0
					BEGIN
							UPDATE  LookupProductImages

							SET		
							        
									ProductImage1 = @ProductImage1,
									ProductImage2 = @ProductImage2,
									ProductImage3 = @ProductImage3,
									ProductImage4 = @ProductImage4,
									ProductImage5 = @ProductImage5,
									ProductImage6 = @ProductImage6,
									ProductImage7 = @ProductImage7,
									ProductImage8 = @ProductImage8,
									ProductImage9 = @ProductImage9,
									ProductImage10 = @ProductImage10,
									UpdatedDate = GETUTCDATE(),
									UpdatedBy = UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'LookupProductImages Data Updated Successfully'
					END

			
			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
			  ID,	
              ProductImage1 ,
              ProductImage2 ,
              ProductImage3 ,
              ProductImage4 ,
              ProductImage5 ,
              ProductImage6 ,
              ProductImage7 ,
              ProductImage8 ,
              ProductImage9 ,
              ProductImage10,
			  CreatedDate,
			  createdBy,
			  UpdatedDate,
              UpdatedBy,
			  DeletedDate,
			  DeletedBy
             



FROM
				[dbo].[LookupProductImages]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[LookupState_All]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from LookupState 
--exec [LookupState_All]'',0,0

CREATE Procedure [dbo].[LookupState_All]
	@Search VARCHAR(MAX),
	@Offset BIGINT,
	@Limit BIGINT

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from LookupState
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') 

										)	
									  )
									  And
									  DeletedBy = 0
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  [Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]

		from  [dbo].[LookupState] 

		where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') 
											
										)	
									  
					And
					DeletedBy = 0
			  )

		order by Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[LookupState_ById]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from LookupState

--exec [LookupState_ById]5
CREATE Procedure [dbo].[LookupState_ById]

@Id BIGINT

AS
BEGIN 
	set nocount on;
	declare @Code int = 200,
		@Message varchar(50) = ''

	SET @Message = 'LookupState Data retrive successfully'

	select @Code as Code,
		@Message as Message

	select 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
	from
		LookupState
	where Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[LookupState_Delete]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from LookupState
--EXEC [LookupState_Delete] 7,7

CREATE Procedure [dbo].[LookupState_Delete]

@Id BIGINT,
@DeletedBy BIGINT
	
AS
BEGIN
	SET nocount on;
	declare @Code int = 400,
	@Message varchar(500) = ''

	update LookupState set DeletedDate = GETUTCDATE() , DeletedBy = @DeletedBy
	
	where Id = @Id

	SET @Code = 200
	SET @Message = 'LookupState data Deleted Successfully'

	select @Code as Code,
	@Message as Message

	select 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]

	from LookupState
	where Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[LookupState_Upsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--SELECT* from LookupState

--EXEC [LookupState_Upsert] 7,'AYODYAudh'

CREATE procedure [dbo].[LookupState_Upsert]

        @Id BIGINT = 0,
		@Name VARCHAR(500) = null,
		@CreatedBy BIGINT = 0,
		@UpdatedBy BIGINT = 0

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					IF @Name IS null or @Name = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Name Is required'
					END
					
					ELSE IF (select count(*) from [LookupState] where lower(Name) = lower(@Name) ) > 0
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = ' Already Exsits Name Please Enter New Name  '
					END	
					
					
					ELSE
					BEGIN
							INSERT INTO LookupState(  Name,
														CreatedDate,
														CreatedBy,
														DeletedBy )
									VALUES (
														@Name,
														GETUTCDATE(),
														@CreatedBy,
														0 )

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = ' LookupState Created Successfully'
					END
			END
			ELSE IF @Id > 0
			BEGIN

					IF @Name IS null or @Name = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Name Is required'
					END
					
					ELSE IF (select count(*) from [LookupState] where lower(Name) = lower(@Name) ) > 0
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = ' Already Exsits Name Please Enter New Name  '
					END	
					
					ELSE
					BEGIN
							UPDATE  LookupState
							SET		
											Name  = ISNULL(@Name ,Name ),
												
											UpdatedDate = GETUTCDATE(),
											UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'LookupState Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
		FROM
				[dbo].[LookupState]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[LookupStatus_All]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from LookupStatus
--exec [LookupStatus_All]'',0,0

CREATE Procedure [dbo].[LookupStatus_All]
	@Search VARCHAR(MAX),
	@Offset BIGINT,
	@Limit BIGINT

AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from LookupStatus
								
								where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') 

										)	
									  )
									  and
									  DeletedBy = 0
							)

		IF @Limit = 0
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

		select  [Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]

		from  [dbo].[LookupStatus] 

		where ( isnull(@Search,'') = '' or
										(
											(lower(Name) like '%'+lower(@Search)+'%') 
											
										)	
									  
					And
					DeletedBy = 0
			  )

		order by Id

		--desc 
		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[LookupStatus_ById]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from LookupStatus

--exec [LookupStatus_ById]1
CREATE Procedure [dbo].[LookupStatus_ById]

@Id BIGINT

AS
BEGIN 
	set nocount on;
	declare @Code int = 200,
		@Message varchar(50) = ''

	SET @Message = 'LookupStatus Data retrive successfully'

	select @Code as Code,
		@Message as Message

	select 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
	from
		LookupStatus
	where Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[LookupStatus_Delete]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from LookupStatus
--EXEC [LookupStatus_Delete] 7,7
CREATE Procedure [dbo].[LookupStatus_Delete]

@Id BIGINT,
@DeletedBy BIGINT
	
AS
BEGIN
	SET nocount on;
	declare @Code int = 400,
	@Message varchar(500) = ''

	update LookupStatus set DeletedDate = GETUTCDATE() , DeletedBy = @DeletedBy
	
	where Id = @Id

	SET @Code = 200
	SET @Message = 'LookupStatus data Deleted Successfully'

	select @Code as Code,
	@Message as Message

	select 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]

	from LookupStatus
	where Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[LookupStatus_Upsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--SELECT* from LookupStatus

--EXEC [LookupStatus_Upsert] 7,'PANMAHAL1'

CREATE procedure [dbo].[LookupStatus_Upsert]

        @Id BIGINT = 0,
		@Name VARCHAR(500) = null,
		@CreatedBy BIGINT = 0,
		@UpdatedBy BIGINT = 0

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					IF @Name IS null or @Name = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Name Is required'
					END
					
					ELSE IF (select count(*) from [LookupStatus] where lower(Name) = lower(@Name) ) > 0
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = ' Already Exsits Name Please Enter New Name  '
					END	
					
					
					ELSE
					BEGIN
							INSERT INTO LookupStatus(  Name,
														CreatedDate,
														CreatedBy,
														DeletedBy )
									VALUES (
														@Name,
														GETutcDATE(),
														@CreatedBy,
														0 )

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = ' LookupStatus Created Successfully'
					END
			END
			ELSE IF @Id > 0
			BEGIN

					IF @Name IS null or @Name = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Name Is required'
					END
					
					ELSE IF (select count(*) from [LookupStatus] where lower(Name) = lower(@Name) ) > 0
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = ' Already Exsits Name Please Enter New Name  '
					END	
					
					ELSE
					BEGIN
							UPDATE  LookupStatus
							SET		
											Name  = ISNULL(@Name ,Name ),
												
											UpdatedDate = GETUTCDATE(),
											UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'LookupStatus Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
				[Id],
				[Name],
				[CreatedDate],
				[CreatedBy],
				[UpdatedDate],
				[UpdatedBy],
				[DeletedDate],
				[DeletedBy]
		FROM
				[dbo].[LookupStatus]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[OrderCommentSelectAll]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderCommentSelectAll]
	@Offset BIGINT,
	@Limit BIGINT,
    @OrderId BIGINT	
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from OrderComments O 
									left join Role R on O.RoleId = R.Id
									where (O.OrderId= @OrderId) AND O.DelFlg = 0
							)

		IF @Limit = 0 OR @Limit = -1
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	select 
			O.*,
			CASE
			 WHEN lower(R.Name) like '%admin%' THEN (Select Name from Admin where Id = O.UserId)
			 WHEN lower(R.Name) like '%trader%' THEN (Select FirstName +' '+ ISNULL(LastName,'') from Traders where Id = O.UserId)
			 WHEN lower(R.Name) like '%customer%' THEN (Select FirstName +' '+ ISNULL(LastName,'') from Customers where Id = O.UserId)
			END as 'UserName',
			CASE
			 WHEN lower(R.Name) like '%admin%' THEN (Select AvtarURL from Admin where Id = O.UserId)
			 WHEN lower(R.Name) like '%trader%' THEN (Select AvtarURL from Traders where Id = O.UserId)
			 WHEN lower(R.Name) like '%customer%' THEN (Select AvtarURL from Customers where Id = O.UserId)
			END as'UserAvtarURL'
		from OrderComments O 
			left join Role R on O.RoleId = R.Id
		where (O.OrderId= @OrderId) AND O.DelFlg = 0				 
		order by O.SubmitDate desc		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[OrderCommentUpsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[OrderCommentUpsert] (
	@Id bigint,
	@OrderID bigint,
	@RoleID bigint,
	@UserID bigint,
	@Comment nvarchar(max),
	@DelFlg bit,	
	@CreatedBy int,	
	@UpdatedBy int
)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

		
SELECT @Code AS Code, @Message AS [Message]

-- Update
IF @Id > 0
	BEGIN
	UPDATE OrderComments
	SET
		OrderID = @OrderID,
		[RoleID] = @RoleID,
		[UserID] = @UserID,
		[Message] = @Comment,		
		[DelFlg] = @DelFlg,
		[UpdatedAt] = GETDATE(),
		[UpdatedBy] = @UpdatedBy
	WHERE
		[Id] = @Id

	SELECT * from OrderComments where Id = @Id
	END

--INSERT
ELSE
	BEGIN
		INSERT INTO OrderComments (
			[OrderID],
			[RoleID],
			[UserID],
			[Message],			
			[SubmitDate],
			[DelFlg],
			[CreatedAt],
			[CreatedBy]
		) VALUES (
			@OrderID,
			@RoleID,
			@UserID,
			@Comment,
			GETDATE(),
			@DelFlg,
			GETDATE(),
			@CreatedBy			
		)
		
		SELECT * from OrderComments where Id = SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[OrderSelect]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrderSelect]
@Id BIGINT
AS
BEGIN
	set nocount on;
	declare @Code Int = 200,
			@Message varchar(500) = ''

			SET @Message = 'Order Data Retrived Successfully.'

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT * FROM [dbo].Orders WHERE Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[OrdersSelectAll]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OrdersSelectAll]
	@Offset BIGINT,
	@Limit BIGINT,
    @Status TINYINT = 0,
	@CustomerName nvarchar(max) = null,
	@TraderName nvarchar(max) = null
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from Orders O
									left join Customers C on C.Id  = O.CustomerID and C.DelFlg = 0
									left join Traders T on T.Id  = O.AssignTo and T.DelFlg = 0									
								where (isnull(@CustomerName,'') = '' or ((lower(C.FirstName+ISNULL(C.LastName, ''))) like '%'+lower(@CustomerName)+'%')) AND
									  (isnull(@TraderName,'') = '' or ((lower(T.FirstName + ISNULL(T.LastName, ''))) like '%'+lower(@TraderName)+'%')) AND
									  (@Status = 0 or (O.Status = @Status)) AND O.DelFlg = 0
							)

		IF @Limit = 0 OR @Limit = -1
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT 
		O.*,
		C.FirstName + ' ' + ISNULL(C.LastName, '') as 'CustomerName',
		C.Id as 'CustomerID',
		T.Id as 'AssignTo',
		T.FirstName + ' ' + ISNULL(T.LastName, '') as 'AssignToName',
		(select count(*) from OrderComments where OrderID = O.ID) as 'NoOfComment'
		FROM [dbo].Orders O
		left join Customers C on C.Id  = O.CustomerID and C.DelFlg = 0
		left join Traders T on T.Id  = O.AssignTo and T.DelFlg = 0									
		where (isnull(@CustomerName,'') = '' or ((lower(C.FirstName+ISNULL(C.LastName, ''))) like '%'+lower(@CustomerName)+'%')) AND
			  (isnull(@TraderName,'') = '' or ((lower(T.FirstName + ISNULL(T.LastName, ''))) like '%'+lower(@TraderName)+'%')) AND
			  (@Status = 0 or (O.Status = @Status)) AND O.DelFlg = 0				 
		order by O.Id		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords

END
GO
/****** Object:  StoredProcedure [dbo].[OrderUpsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[OrderUpsert] (
	@Id bigint,
	@CustomerID bigint,
	@PaymentTypes tinyint,
	@Status tinyint,
	@AssignTo bigint,
	@DelFlg bit,	
	@CreatedBy int,	
	@UpdatedBy int
)
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

		
SELECT @Code AS Code, @Message AS [Message]

-- Update
IF @Id > 0
	BEGIN
	UPDATE Orders
	SET
		[CustomerID] = ISNULL(@CustomerID,[CustomerID]),
		[PaymentTypes] = ISNULL(@PaymentTypes,[PaymentTypes]),
		[Status] = ISNULL(@Status,[Status]),
		[AssignTo] = ISNULL(@AssignTo,[AssignTo]),
		[AssignDate] = CASE WHEN @AssignTo is not null THEN ISNULL([AssignDate],GETDATE()) END,
		[DelFlg] = @DelFlg,
		[UpdatedAt] = GETDATE(),
		[UpdatedBy] = @UpdatedBy
	WHERE
		[Id] = @Id

	SELECT * from Orders where Id = @Id
	END

--INSERT
ELSE
	BEGIN
		INSERT INTO [Orders] (
			[CustomerID],
			[OrderDate],
			[PaymentTypes],
			[Status],
			[AssignTo],
			[DelFlg],
			[CreatedAt],
			[CreatedBy]
		) VALUES (
			@CustomerID,
			GETDATE(),
			@PaymentTypes,
			@Status,
			@AssignTo,
			@DelFlg,
			GETDATE(),
			@CreatedBy			
		)
		
		SELECT * from [Orders] where Id = SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[ProductCategoryInsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProductCategoryInsert] (
	@Name nvarchar(max),
	@Status bit,
	@DelFlg bit,
	@CreatedAt datetime,
	@CreatedBy nvarchar(max),
	@UpdatedAt datetime,
	@UpdatedBy nvarchar(max)
)
AS

SET NOCOUNT ON

INSERT INTO [ProductCategory] (
	[Name],
	[Status],
	[DelFlg],
	[CreatedAt],
	[CreatedBy],
	[UpdatedAt],
	[UpdatedBy]
) VALUES (
	@Name,
	@Status,
	@DelFlg,
	@CreatedAt,
	@CreatedBy,
	@UpdatedAt,
	@UpdatedBy
)

SELECT SCOPE_IDENTITY() AS ID
GO
/****** Object:  StoredProcedure [dbo].[ProductCategorySelect]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProductCategorySelect] (
	@ID bigint
)
AS

SET NOCOUNT ON

SELECT
	[ID],
	[Name],
	[Status],
	[DelFlg],
	[CreatedAt],
	[CreatedBy],
	[UpdatedAt],
	[UpdatedBy]
FROM
	[ProductCategory]
WHERE
	[ID] = @ID
GO
/****** Object:  StoredProcedure [dbo].[ProductCategorySelectAll]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProductCategorySelectAll](
	@Search varchar(100),
	@Offset INT,
	@Limit INT
)	
AS

	SET NOCOUNT ON
	DECLARE @TotalRecords BIGINT;
SET @TotalRecords = (select count(*) FROM [ProductCategory])

   select 
		*
   FROM [ProductCategory]
	 WHERE ISNULL(@Search,'') = '' 
	 	  OR Name like '%'+@Search+'%' 
	 	  OR [Status] like '%'+@Search+'%' 
	 ORDER BY Id ASC
	 OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY

SELECT @TotalRecords AS TotalRecords
GO
/****** Object:  StoredProcedure [dbo].[ProductCategoryUpdate]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ProductCategoryUpdate] (
	@ID bigint,
	@Name nvarchar(MAX),
	@Status bit,
	@UpdatedAt datetime
)
AS

SET NOCOUNT ON

UPDATE
	[ProductCategory]
SET
	[Name] = @Name,
	[Status] = @Status,
	[UpdatedAt] = @UpdatedAt
WHERE
	 [ID] = @ID
GO
/****** Object:  StoredProcedure [dbo].[Products_All]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec Products_All '',0,0,0
CREATE Proc [dbo].[Products_All]

	@Search varchar(MAX),
	@Offset bigint,
	@Limit bigint	

AS
BEGIN
		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
			select count(*) from [dbo].[Product] P
				where (isnull(@Search,'') = '' or ((lower(P.Name) like '%'+lower(@Search)+'%'))) ANd P.DelFlg = 0								
			)

		IF @Limit = 0 or @Limit = -1
		BEGIN

			SET @Limit = @TotalRecords
			IF @Limit = 0
			BEGIN
					SET @Limit = 10
			END
			
		END

		select * FROM [dbo].[Product] P					
			where (isnull(@Search,'') = '' or ((lower(P.Name) like '%'+lower(@Search)+'%'))) ANd P.DelFlg= 0			
			order by P.Id desc			
		    OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
END
GO
/****** Object:  StoredProcedure [dbo].[Products_ById]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec Products_ById 2
CREATE Proc [dbo].[Products_ById]
@Id BIGINT

AS
BEGIN 
	set nocount on;
	declare @Code int = 200,
		@Message varchar(50) = ''

	SET @Message = 'Product Data retrive successfully.'

	select @Code as Code,
		@Message as Message

	select * FROM [dbo].[Product] P where P.Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[Products_Delete]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec Products_Delete 4,4
CREATE Proc [dbo].[Products_Delete]
@Id BIGINT,
@DeletedBy BIGINT
	
AS
BEGIN
	SET nocount on;
	declare @Code int = 400,
	@Message varchar(500) = ''

	update Product 
		set UpdatedAt = GETDATE(), 
			UpdatedBy = @DeletedBy, 
			DelFlg = 1	
		where Id = @Id

	SET @Code = 200
	SET @Message = 'Products data Deleted Successfully'

	select @Code as Code,
	@Message as Message

	select * from Product where Id = @Id and DelFlg = 0
END
GO
/****** Object:  StoredProcedure [dbo].[Products_FeaturedUnfeature]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec Products_FeaturedUnfeature 2
CREATE Proc [dbo].[Products_FeaturedUnfeature]
	@Id INT
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @Code INT = 400,
	        @Message VARCHAR(200) = '',	
			@IsFeatured int = 0

	SELECT @IsFeatured = IsFeatured FROM Products WHERE Id = @Id

	IF @IsFeatured = 1
	BEGIN
	     UPDATE Products SET IsFeatured = 0 WHERE Id = @Id

		 SET @Code = 200
		 SET @Message = 'Products Unfeature Successfully'
	END
	ELSE
	BEGIN
	    UPDATE Products SET IsFeatured = 1 WHERE Id = @Id

		 SET @Code = 200
		 SET @Message = 'Products Featured Successfully'
	END
    
	SELECT @Code as Code,
			@Message as [Message]

	SELECT 
				 Id ,
                LookupProductCategoryAndSubCategoryId ,
                Name ,
                StandardPrice ,
                OfferPrice ,
                ProductDescription ,
                IsFeatured ,
                LookupStatusId ,
				CreatedDate,
				CreatedBy,
				UpdatedDate,
				UpdatedBy,
				DeletedDate,
				DeletedBy
		FROM
				[dbo].[Products]
		WHERE
				Id = @Id
	
END
GO
/****** Object:  StoredProcedure [dbo].[Products_Upsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--eXEC Products_Upsert 4,'111',cfg,'1.8','1.6',mihir,'n15',0,0
CREATE Proc [dbo].[Products_Upsert]
	@Id bigint,
    @CategoryID bigint,
	@Name nvarchar(max),
	@StandardPrice money,
	@OfferPrice money,
	@Description nvarchar(max),
	@IsActive bit,
	@IsFeatured bit,
	@DelFlg bit,
	@CreatedBy bigint,	
	@UpdatedBy bigint
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @Code INT = 400,
	        @Message VARCHAR(255) = ''

	IF @Id = 0
	BEGIN
	      
		 
		  IF @Name IS NULL OR @Name = ''
		  BEGIN
			   SET @Id = 0
			   SET @Code = 400
			   SET @Message = 'Name is Required'
		  END
		  ELSE IF (Select count(name) from Product where Name = @Name) > 0
		  BEGIN
			   SET @Id = 0
			   SET @Code = 400
			   SET @Message = 'Name is already used for another product.'
		  END

		  ELSE
		  BEGIN
		       INSERT INTO [Product] (
										[CategoryID],
										[Name],
										[StandardPrice],
										[OfferPrice],
										[Description],
										[IsActive],
										[IsFeatured],
										[DelFlg],
										[CreatedAt],
										[CreatedBy]
									) VALUES (
										@CategoryID,
										@Name,
										@StandardPrice,
										@OfferPrice,
										@Description,
										@IsActive,
										@IsFeatured,
										@DelFlg,
										GetDATE(),
										@CreatedBy
									)

			   SET @Id = SCOPE_IDENTITY()
			   SET @Code = 200
			   SET @Message = 'Products Created successfully'
		  END 
    END
	ELSE IF @Id > 0
	     BEGIN
		  IF @Name IS NULL OR @Name = ''
		  BEGIN
			   SET @Code = 400
			   SET @Message = ' Name Is Required'
		  END
		  ELSE IF (Select count(name) from Product where Name = @Name and Id != @Id) > 0
		  BEGIN
			   SET @Id = 0
			   SET @Code = 400
			   SET @Message = 'Name is already used for another product.'
		  END
			 ELSE
		     BEGIN
		          UPDATE
						[Product]
					SET
						[CategoryID] = @CategoryID,
						[Name] = @Name,
						[StandardPrice] = @StandardPrice,
						[OfferPrice] = @OfferPrice,
						[Description] = @Description,
						[IsActive] = @IsActive,
						[IsFeatured] = @IsFeatured,
						[DelFlg] = @DelFlg,						
						[UpdatedAt] = GETDATE(),
						[UpdatedBy] = @UpdatedBy
					WHERE
						 [ID] = @ID
			   
			   SET @Code = 200
			   SET @Message = 'Products updated successfully'
		  END
	END

	SELECT @Code as Code,@Message as [Message]	
    Select * FROM [Product] WHERE Id = @Id
END



GO
/****** Object:  StoredProcedure [dbo].[ShippingRateSelectByID]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ShippingRateSelectByID] (
	@ID bigint
)
AS

SET NOCOUNT ON

SELECT
	ID,
	CategoryID,
	FreeShipping,
	FreeShippingAbove
FROM
	Shipping
WHERE
	[ID] = @ID
GO
/****** Object:  StoredProcedure [dbo].[ShippingSelectAll]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ShippingSelectAll](
	@Search varchar(100),
	@Offset INT,
	@Limit INT
)	
AS

	SET NOCOUNT ON
	DECLARE @TotalRecords BIGINT;
SET @TotalRecords = (select count(*) FROM Shipping)

   select 
		Shipping.ID as ID, 
		ProductCategory.Name As CategoryName,
		Shipping.CategoryID As CategoryID,
		Shipping.FreeShippingAbove As FreeShippingAbove, 
		Shipping.FreeShipping As FreeShipping
   FROM Shipping
   left join ProductCategory
   on Shipping.CategoryID = ProductCategory.ID
	 WHERE ISNULL(@Search,'') = '' 
	 	  OR Shipping.FreeShippingAbove like '%'+@Search+'%' 
	 	  OR Shipping.FreeShipping like '%'+@Search+'%' 
	 ORDER BY Id ASC
	 OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY

SELECT @TotalRecords AS TotalRecords
GO
/****** Object:  StoredProcedure [dbo].[ShippingUpsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ShippingUpsert] (
	@ID int,
	@CategoryID bigint,
	@FreeShipping money,
	@FreeShippingAbove money,
	@DelFlg bit,
	@CreatedAt datetime,
	@CreatedBy varchar(max),
	@UpdatedAt datetime,
	@UpdatedBy varchar(max))
AS

SET NOCOUNT ON
DECLARE @Code INT = 200,
	    @Message VARCHAR(255) = ''

-- Update
IF(@Id > 0)
		BEGIN
			SELECT @Code AS Code, @Message AS [Message]
		
			UPDATE [dbo].Shipping
				SET CategoryID = @CategoryID
				   ,FreeShipping = @FreeShipping
				   ,FreeShippingAbove = @FreeShippingAbove
				   ,DelFlg = @DelFlg
				   ,CreatedAt = @CreatedAt
				   ,CreatedBy = @CreatedBy
				   ,UpdatedAt = @UpdatedAt
				   ,UpdatedBy = @UpdatedBy
				WHERE Id = @Id

				select S.* from Shipping S where ID = @ID
		END

-- Insert
ELSE	
	IF exists (select CategoryID from Shipping where CategoryID=@CategoryID)
		BEGIN
			SET @Message='CategoryID already exists.'
			SELECT @Code AS Code, @Message AS [Message]
		END
	ELSE
		BEGIN
			SELECT @Code AS Code, @Message AS [Message]
	
			INSERT INTO [dbo].Shipping
						(CategoryID ,FreeShipping ,FreeShippingAbove ,DelFlg ,CreatedAt ,CreatedBy ,UpdatedAt ,UpdatedBy)
			VALUES
						(@CategoryID, @FreeShipping, @FreeShippingAbove, @DelFlg, @CreatedAt, @CreatedBy, @UpdatedAt, @UpdatedBy)
	
			select S.* from Shipping S where S.ID = SCOPE_IDENTITY()
		END
GO
/****** Object:  StoredProcedure [dbo].[Traders_All]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Traders_All '',0,0,0,0,0,0
CREATE proc [dbo].[Traders_All]
	@Search VARCHAR(MAX),
	@Offset Bigint,
	@Limit Bigint	

AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @TotalRecords BIGINT;

	SET @TotalRecords = (
	    SELECT 
		      COUNT(*) 
	    FROM 
		      [dbo].[Traders] T
			  where
				(
					(ISNULL(@Search,'') = '' 
					OR (LOwER(T.FirstName) like '%'+LOWER(@Search)+'%')
					OR (LOwER(T.LastName) like '%'+LOWER(@Search)+'%'))
				) And T.DelFlg=0
				)

	IF @Limit = 0
	BEGIN
	     SET @Limit = @TotalRecords
		 IF @Limit = 0
		 BEGIN
		     SET @Limit = 10
		 END
	END
	 
	SELECT *
	FROM 
	    [dbo].[Traders] T			  	
	where
				(
					(ISNULL(@Search,'') = '' 
					OR (LOwER(T.FirstName) like '%'+LOWER(@Search)+'%')
					OR (LOwER(T.LastName) like '%'+LOWER(@Search)+'%'))
				) And T.DelFlg=0
	ORDER BY 
	      T.Id
    DESC
		  OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY 


	select @TotalRecords as TotalRecords
	
END
GO
/****** Object:  StoredProcedure [dbo].[Traders_ById]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec Traders_ById 2
CREATE proc [dbo].[Traders_ById]

@ID BIGINT

AS
BEGIN 
	set nocount on;
	declare @Code int = 200,
		@Message varchar(50) = ''

	SET @Message = 'Trader Data retrive successfully'

	select @Code as Code,
		@Message as Message

	select 
				C.*,
				C.FirstName + ' ' + ISNULL(C.LastName, '') as 'Name',
				(A.Address1 + ' ' + isnull(A.Address2,'') +' '+ A.Area +' ' + A.City +' '+ A.State+' '+ isnull(L.Name,'') + ' '+ A.Pincode) as 'AddressString',
				(Select count(*) from Orders where AssignTo = C.ID) as 'TotalOrders',
				(Select SUM(TotalAmount) from Orders where AssignTo = C.ID) as 'TotalSales',
				(Select TOP(1) OrderDate from Orders where AssignTo = C.ID order by OrderDate desc) as 'LastOrder'
	FROM 
	    [dbo].[Traders] C
		Left join Address A on A.ID = C.Address
		Left join LookupCountry L on L.Id = A.CountryID
	where C.Id = @ID
END
GO
/****** Object:  StoredProcedure [dbo].[Traders_ChangePassword]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from Traders
--Exec Traders_ChangePassword 2,'12345678','12312312','12312312'
CREATE proc [dbo].[Traders_ChangePassword]
      @Id BIGINT,
	   @OldPassword varchar(500),
	   @NewPassword varchar(500),
	   @ConfirmPassword varchar(500)

AS
BEGIN
	
	SET NOCOUNT ON;

  DECLARE @Code INT = 400,
	      @Message VARCHAR(255) = ''

	
		 IF @OldPassword IS NULL OR @OldPassword = ''
		 BEGIN
		      SET @Code = 403
			  SET @Message = 'Old password is required'
			  SET @Id = 0
		 END
		 ELSE IF @NewPassword IS NULL OR @NewPassword = ''
		 BEGIN
		      SET @Code = 403
			  SET @Message = 'New password is required'
			  SET @Id = 0
		 END
		 ELSE IF @ConfirmPassword IS NULL OR @ConfirmPassword = ''
		 BEGIN
		      SET @Code = 403
			  SET @Message = 'Confirm password is required'
			  SET @Id = 0
		 END
		 ELSE IF LOWER(@NewPassword) != LOWER(@ConfirmPassword) 
		 BEGIN
		      SET @Code = 403
			  SET @Message = 'New password does not match the confirm password'
			  SET @Id = 0
		 END
		 ELSE IF (SELECT COUNT(*) FROM [Traders] WHERE Id = @Id AND LOWER([Password]) = LOWER(@OldPassword)) = 0
		 BEGIN
		      SET @Code = 403
			  SET @Message = 'Old password does not match'
			  SET @Id = 0
		 END
		 ELSE
		 BEGIN
		     UPDATE 
			       [Traders]
			 SET 
			      [Password] = @NewPassword
			 WHERE 
			      Id = @Id

			 SET @Code = 200
			 SET @Message = 'Password changes successfully'
		 END
	

		SELECT @Code as Code,
			   @Message as [Message]

		SELECT 
				Id,
			Name,
			Email,
			Password,
			AddressLine1,
			AddressLine2,
			LookupCountryId,
			LookupStateId,
			LookupCityId,
			Phone,
			AlternatePhone,
			TraderSince,
			LastOrder,
			Rating,
			JoinedDate,
			LastLogin,
			OrderCompleted,
			Points,
			LookupStatusId,
			LastLogin,
			CreatedDate,
			CreatedBy,
			UpdatedDate,
			UpdatedBy,
			DeletedDate,
		    DeletedBy
		FROM
				[dbo].[Traders]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[Traders_Delete]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Traders_Delete]
@Id BIGINT,
@DeletedBy BIGINT
	
AS
BEGIN
	SET nocount on;
	declare @Code int = 400,
	@Message varchar(500) = ''

	update Traders set DeletedDate = GETUTCDATE() , DeletedBy = @DeletedBy
	
	where Id = @Id

	SET @Code = 200
	SET @Message = 'Traders data Deleted Successfully'

	select @Code as Code,
	@Message as Message

	select 
				Id,
			Name,
			Email,
			Password,
			AddressLine1,
			AddressLine2,
			LookupCountryId,
			LookupStateId,
			LookupCityId,
			Phone,
			AlternatePhone,
			TraderSince,
			LastOrder,
			Rating,
			JoinedDate,
			LastLogin,
			OrderCompleted,
			Points,
			LookupStatusId,
			LastLogin,
			CreatedDate,
			CreatedBy,
			UpdatedDate,
			UpdatedBy,
			DeletedDate,
		    DeletedBy

	from Traders
	where Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[Traders_Login]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from Traders
--Exec Traders_Login 'akram@gmail.com','12345678'
CREATE proc [dbo].[Traders_Login]
  @Email varchar(500) = null,
	@Password varchar(500) = null
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @Code Int = 400,
		@Message varchar(500) = '',
		@Id Int = 0

	IF @Email is null or @Email = ''
	BEGIN 
		SET @Id = 0
		SET @Code = 400
		SET @Message = 'Email is Required'
	END
	ELSE IF @Password is null or @Password = ''
	BEGIN
		SET @Id = 0
		SET @Code = 400
		SET @Message = 'Password is Required'
	END

	ELSE 
	BEGIN
		Select @Id = Id From Traders where Email = @Email

		IF @Id > 0
		BEGIN
			IF(select count(*) from Traders where Id = @Id and Password = @Password) > 0
			BEGIN
				
					SET @Code = 200
					SET @Message = 'Traders Login in Successfully'
				END
				
			ELSE 
			BEGIN
				SET @Code = 400
				SET @Message = 'Password Is Incorrect !'
			END
		END
		ELSE 
		BEGIN
			SET @Code = 400
			SET @Message = 'Email Is Not Registerd !'
		END
	END

	Select @Code as Code,
		@Message as Message

	select 
				Id,
			Name,
			Email,
			Password,
			AddressLine1,
			AddressLine2,
			LookupCountryId,
			LookupStateId,
			LookupCityId,
			Phone,
			AlternatePhone,
			TraderSince,
			LastOrder,
			Rating,
			JoinedDate,
			LastLogin,
			OrderCompleted,
			Points,
			LookupStatusId,
			LastLogin,
			CreatedDate,
			CreatedBy,
			UpdatedDate,
			UpdatedBy,
			DeletedDate,
		    DeletedBy

	from Traders
	WHERE Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[Traders_Logout]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Select * from Traders
--Exec Traders_Logout 2
CREATE proc [dbo].[Traders_Logout]
@Id BIGINT

AS
BEGIN
	
	SET NOCOUNT ON;

	UPDATE Traders SET LastLogin = GETUTCDATE()
	
	WHERE Id = @Id
	
	SELECT 1
   
END
GO
/****** Object:  StoredProcedure [dbo].[Traders_Upsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from  Traders
--Exec Traders_Upsert 3,'chakaram','chakram@gmail.com',12345678,'gfvjyfgs','dbtgfg',1,1,1,9824454873,9913535833,'fvgjh','assd',9,'17/05/1998',1,1,1,0,0
CREATE proc [dbo].[Traders_Upsert]

        @Id   bigint ,
        @Name   varchar(500) ,
        @Email   varchar(500) ,
        @Password   varchar(500), 
        @AddressLine1   varchar(max),  
        @AddressLine2   varchar(max),
        @LookupCountryId  bigint ,
        @LookupStateId   bigint ,
        @LookupCityId   bigint ,
        @Phone   varchar(50),
        @AlternatePhone   varchar(50),
        @TraderSince   varchar(150),
        @LastOrder   varchar(150),
        @Rating   bigint,
        @JoinedDate   varchar(250),
        @OrderCompleted   bigint,
        @Points   bigint,
        @LookupStatusId   bigint,
        
		@CreatedBy Bigint,
		@UpdatedBy Bigint
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @Code INT = 400,
				@Messages VARCHAR(500) = ''

		    IF @Id = 0
			BEGIN
			
			    IF @Name IS null or @Name = ''
				BEGIN
						SET @Id = 0
						SET @Code = 400
						SET @Messages = 'Name Is required'
				END
				ELSE IF @Email IS null or @Email = ''
				BEGIN
						SET @Id = 0
						SET @Code = 400
						SET @Messages = 'Email Is required'
				END
				ELSE IF @Password IS null or @Password = ''
				BEGIN
						SET @Id = 0
						SET @Code = 400
						SET @Messages = 'Password Is required'
				END
				ELSE IF @AddressLine1 IS null or @AddressLine1 = ''
				BEGIN
						SET @Id = 0
						SET @Code = 400
						SET @Messages = 'AddressLine1 Is required'
				END
				 ELSE IF (SELECT count(*) FROM Traders WHERE LOWER(AddressLine1) = Lower(@AddressLine1) AND DeletedBy = 0) > 0
		  BEGIN
		  	   SET @Id = 0
		  	   SET @Code = 403
		  	   SET @Messages = 'AddressLine1 already exist !'
		  END
				ELSE IF @AddressLine2 IS null or @AddressLine2 = ''
				BEGIN
						SET @Id = 0
						SET @Code = 400
						SET @Messages = 'AddressLine2 Is required'
				END
				ELSE IF @LookupCountryId = 0
				BEGIN
						SET @Id = 0
						SET @Code = 400
						SET @Messages = 'LookupCountryId Is required'
				END
				ELSE IF @LookupStateId = 0
				BEGIN
						SET @Id = 0
						SET @Code = 400
						SET @Messages = 'LookupStateId Is required'
				END
				ELSE IF @LookupCityId = 0
				BEGIN
						SET @Id = 0
						SET @Code = 400
						SET @Messages = 'LookupCityId Is required'
				END

				ELSE IF @Phone IS null or @Phone = ''
				BEGIN
						SET @Id = 0
						SET @Code = 400
						SET @Messages = 'PhoneNumber Is required'
				END
				ELSE IF @AlternatePhone IS null or @AlternatePhone = ''
				BEGIN
						SET @Id = 0
						SET @Code = 400
						SET @Messages = 'AlternatePhone Is required'
				END
				
			ELSE
			BEGIN
					insert into Traders
					(	
					    Name,
						Email,
						Password,
						AddressLine1,
						AddressLine2,
						LookupCountryId,
						LookupStateId,
						LookupCityId,
						Phone,
						AlternatePhone,
						TraderSince,
						LastOrder,
						Rating,
						JoinedDate,
						OrderCompleted,
						Points,
						LookupStatusId,
						LastLogin,
						CreatedDate,
						CreatedBy,
						DeletedBy
						
					)
					values
					( 
						@Name,
						@Email,
						@Password,
						@AddressLine1,
						@AddressLine2,
						@LookupCountryId,
						@LookupStateId,
						@LookupCityId,
						@Phone,
						@AlternatePhone,
						@TraderSince,
						@LastOrder,
						@Rating,
						@JoinedDate,
						@OrderCompleted,
						@Points,
						@LookupStatusId,
						1,
						GETDATE(),
						@CreatedBy,
						0
					)

					SET @Id = SCOPE_IDENTITY()
					SET @Code = 200
					SET @Messages = 'Traders Data Created'
			END
			END

		ELSE 
			IF @Id > 0
				BEGIN
					IF @Name IS null or @Name = ''
				BEGIN
						SET @Code = 400
						SET @Messages = 'Name Is required'
				END
				ELSE IF @Email IS null or @Email = ''
				BEGIN
						SET @Code = 400
						SET @Messages = 'Email Is required'
				END
				ELSE IF @Password IS null or @Password = ''
				BEGIN
						SET @Code = 400
						SET @Messages = 'Password Is required'
				END
				ELSE IF @AddressLine1 IS null or @AddressLine1 = ''
				BEGIN
						SET @Code = 400
						SET @Messages = 'AddressLine1 Is required'
				END
				 ELSE IF (SELECT count(*) FROM Traders WHERE LOWER(AddressLine1) = Lower(@AddressLine1) and Id != @Id  AND DeletedBy = 0) > 0
		  BEGIN
		  	   SET @Code = 403
		  	   SET @Messages = 'AddressLine1 already exist !'
		  END
				ELSE IF @AddressLine2 IS null or @AddressLine2 = ''
				BEGIN
						SET @Code = 400
						SET @Messages = 'AddressLine2 Is required'
				END
				ELSE IF @LookupCountryId = 0
				BEGIN
						SET @Code = 400
						SET @Messages = 'LookupCountryId Is required'
				END
				ELSE IF @LookupStateId = 0
				BEGIN
						SET @Code = 400
						SET @Messages = 'LookupStateId Is required'
				END
				ELSE IF @LookupCityId = 0
				BEGIN
						SET @Code = 400
						SET @Messages = 'LookupCityId Is required'
				END

				ELSE IF @Phone IS null or @Phone = ''
				BEGIN
						SET @Code = 400
						SET @Messages = 'PhoneNumber Is required'
				END
				ELSE IF @AlternatePhone IS null or @AlternatePhone = ''
				BEGIN
						SET @Code = 400
						SET @Messages = 'AlternatePhone Is required'
				END
				ELSE
				BEGIN
						update [Traders] SET
								Name = ISNULL(@Name, Name),
								Email = ISNULL(@Email, Email),
								Password = ISNULL(@Password, Password),
								AddressLine1 = ISNULL(@AddressLine1, AddressLine1),
								AddressLine2 = ISNULL(@AddressLine2, AddressLine2),
								LookupCountryId = @LookupCountryId,
								LookupStateId = @LookupStateId,
								LookupCityId = @LookupCityId,
								Phone = ISNULL(@Phone, Phone),
								AlternatePhone = ISNULL(@AlternatePhone, AlternatePhone),
								TraderSince = ISNULL(@TraderSince, TraderSince),
								LastOrder = ISNULL(@LastOrder, LastOrder),
								Rating = @Rating,
								JoinedDate = ISNULL(@JoinedDate, JoinedDate),
								OrderCompleted = @OrderCompleted,
								Points = @Points,
								LookupStatusId = @LookupStatusId,
								UpdatedBy = @UpdatedBy,
								UpdatedDate = getdate() 
						WHERE
								Id = @Id

						SET @Code = 200
						SET @Messages = 'Traders Updated Successfully'
				END
			END

		SELECT @Code as Code,
			   @Messages as [Message]

		SELECT 
			Id,
			Name,
			Email,
			Password,
			AddressLine1,
			AddressLine2,
			LookupCountryId,
			LookupStateId,
			LookupCityId,
			Phone,
			AlternatePhone,
			TraderSince,
			LastOrder,
			Rating,
			JoinedDate,
			LastLogin,
			OrderCompleted,
			Points,
			LookupStatusId,
			LastLogin,
			CreatedDate,
			CreatedBy,
			UpdatedDate,
			UpdatedBy,
			DeletedDate,
				DeletedBy
		FROM
			[dbo].[Traders]
		WHERE
			Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[TradersInsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TradersInsert] (
	@FirstName nvarchar(MAX),
	@LastName nvarchar(MAX),
	@JoinedDate datetime,
	@EmailID nvarchar(255),
	@Password nvarchar(256),
	@MobileNo nvarchar(15),
	@AlternateMobileNo nvarchar(15),
	@Address nvarchar(MAX),
	@DelFlg bit,
	@CreatedAt datetime,
	@CreatedBy nvarchar(MAX),
	@UpdatedAt datetime,
	@UpdatedBy nvarchar(MAX)
)
AS

SET NOCOUNT ON
IF EXISTS (SELECT 1 FROM Traders
WHERE EmailID = @EmailID)
BEGIN
 SELECT -1
END
ELSE
BEGIN
INSERT INTO [Traders] (
	[FirstName],
	[LastName],
	[JoinedDate],
	[EmailID],
	[Password],
	[MobileNo],
	[AlternateMobileNo],
	[Address],
	[DelFlg],
	[CreatedAt],
	[CreatedBy],
	[UpdatedAt],
	[UpdatedBy]
) VALUES (
	@FirstName,
	@LastName,
	@JoinedDate,
	@EmailID,
	@Password,
	@MobileNo,
	@AlternateMobileNo,
	@Address,
	@DelFlg,
	@CreatedAt,
	@CreatedBy,
	@UpdatedAt,
	@UpdatedBy
)

SELECT SCOPE_IDENTITY() AS ID
END
GO
/****** Object:  StoredProcedure [dbo].[Transactions_All]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from Transactions
--Exec Transactions_All '',0,0,0
CREATE Proc [dbo].[Transactions_All]
@Search VARCHAR(MAX),
	@Offset Bigint,
	@Limit Bigint,
	@CustomerId Bigint

AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @TotalRecords BIGINT;

	SET @TotalRecords = (
	    SELECT 
		      COUNT(*) 
	    FROM 
		      [dbo].[Transactions] T
			  left join 
			  Customers C  on T.CustomerId = C.Id
			  
			  WHERE 
				(
					(  ISNULL(@Search,'') = '' 
					OR (LOwER(T.Date) like '%'+LOWER(@Search)+'%')
					OR (LOwER(T.CustomerId) like '%'+LOWER(@Search)+'%')
					OR (LOwER(C.Name) like '%'+LOWER(@Search)+'%')
				

				)
				and
					(@CustomerId = 0 or T.CustomerId = @CustomerId)
					
					)
					and
					T.DeletedBy=0
				)

	IF @Limit = 0
	BEGIN
	     SET @Limit = @TotalRecords
		 IF @Limit = 0
		 BEGIN
		     SET @Limit = 10
		 END
	END
	 
	SELECT 

		        T.Id,
				T.Amount,
				T.CustomerId,
				C.Name As CustomerName,
				T.Date,
				T.LookupStatus,
				T.CreatedDate,
				T.CreatedBy,
				T.UpdatedDate,
				T.UpdatedBy,
				T.DeletedDate,
				T.DeletedBy
	FROM 
	    [dbo].[Transactions] T
			  left join 
			  [Customers] C on T.CustomerId = C.Id
			  
	
	WHERE 
	(
		(  ISNULL(@Search,'') = '' 
		            OR (LOwER(T.Date) like '%'+LOWER(@Search)+'%')
					OR (LOwER(T.CustomerId) like '%'+LOWER(@Search)+'%')
					OR (LOwER(C.Name) like '%'+LOWER(@Search)+'%')
				
	)
	and
	(@CustomerId = 0 or T.CustomerId = @CustomerId)
	)
		
	ORDER BY 
	      T.Id
    DESC
		  OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY 


	select @TotalRecords as TotalRecords
	
END
GO
/****** Object:  StoredProcedure [dbo].[Transactions_ById]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec Transactions_ById 3
CREATE Proc [dbo].[Transactions_ById]

@Id BIGINT

AS
BEGIN 
	set nocount on;
	declare @Code int = 200,
		@Message varchar(50) = ''

	SET @Message = 'Transactions Data retrive successfully'

	select @Code as Code,
		@Message as Message

	SELECT 

		        T.Id,
				T.Amount,
				T.CustomerId,
				C.Name As CustomerName,
				T.Date,
				T.LookupStatus,
				T.CreatedDate,
				T.CreatedBy,
				T.UpdatedDate,
				T.UpdatedBy,
				T.DeletedDate,
				T.DeletedBy
	FROM 
	    [dbo].[Transactions] T
			  left join 
			  [Customers] C on T.CustomerId = C.Id
	where T.Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[Transactions_Delete]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec Transactions_Delete 4,4
CREATE Proc [dbo].[Transactions_Delete]

@Id BIGINT,
@DeletedBy BIGINT
	
AS
BEGIN
	SET nocount on;
	declare @Code int = 400,
	@Message varchar(500) = ''

	update Transactions set DeletedDate = GETUTCDATE() , DeletedBy = @DeletedBy
	
	where Id = @Id

	SET @Code = 200
	SET @Message = 'Transactions data Deleted Successfully'

	select @Code as Code,
	@Message as Message

	select 
						        Id,
				Amount,
				CustomerId,
				Date,
				LookupStatus,
				CreatedDate,
				CreatedBy,
				UpdatedDate,
				UpdatedBy,
				DeletedDate,
				DeletedBy


	from Transactions
	where Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[Transactions_Upsert]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Transactions_Upsert]
        @Id BIGINT = 0,
		@Amount varchar(500),
		@CustomerId Bigint,
		@Date varchar(max) ,
		@LookupStatus VARCHAR(250) ,
		@CreatedBy BIGINT ,
		@UpdatedBy BIGINT 

AS
BEGIN

			SET NOCOUNT ON;

			DECLARE @Code int = 400,
					@Message varchar(500) = ''

			IF @Id = 0
			BEGIN
					IF @Amount IS null or @Amount = ''
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'Amount Is required'
					END
					Else IF @CustomerId =0
					BEGIN
							SET @Id = 0
							SET @Code = 400
							SET @Message = 'CustomerId Is required'
					END
					ELSE IF @Date is null or @Date	 = ''
		                      BEGIN
			                  SET @Id = 0
			                  SET @Code = 400
			                  SET @Message = 'Date Is required'
		                 END
		              
					ELSE
					BEGIN
							INSERT INTO Transactions(  
												Amount,
												CustomerId,
												Date,
												LookupStatus,
												CreatedDate,
												CreatedBy,
												DeletedBy )
									VALUES (
											@Amount,
											@CustomerId,
											@Date,
											@LookupStatus,
											GETUTCDATE(),
											@CreatedBy,
											0)

							SET @Id = SCOPE_IDENTITY()
							SET @Code = 200
							SET @Message = ' Transactions Created Successfully'
					END
			END
			ELSE IF @Id > 0
			BEGIN

					IF @Amount IS null or @Amount = ''
					BEGIN
							SET @Code = 400
							SET @Message = 'Amount Is required'
					END
					Else IF @CustomerId =0
					BEGIN
							SET @Code = 400
							SET @Message = 'CustomerId Is required'
					END
					ELSE IF @Date is null or @Date	 = ''
		                      BEGIN
			                  SET @Code = 400
			                  SET @Message = 'Date Is required'
		                 END
					ELSE
					BEGIN
							UPDATE  Transactions
							SET		
									Amount = ISNULL(@Amount,Amount),
									CustomerId = @CustomerId,
									Date = ISNULL(@Date,Date),
									LookupStatus  = ISNULL(@LookupStatus,LookupStatus),
									UpdatedDate = GETUTCDATE(),
									UpdatedBy = @UpdatedBy

							WHERE   Id = @Id

							SET @Code = 200
							SET @Message = 'Transactions Updated Successfully'
					END

			END

			SELECT @Code as Code,
				   @Message as [Message]

	SELECT 
		        Id,
				Amount,
				CustomerId,
				Date,
				LookupStatus,
				CreatedDate,
				CreatedBy,
				UpdatedDate,
				UpdatedBy,
				DeletedDate,
				DeletedBy
		FROM
				[dbo].[Transactions]
		WHERE
				Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[VisitPurpose_SelectAll]    Script Date: 21-08-2021 11:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VisitPurpose_SelectAll]
	@Offset BIGINT,
	@Limit BIGINT    
AS
BEGIN

		SET NOCOUNT ON;

		DECLARE @TotalRecords bigint;

		SET @TotalRecords = (
								select COUNT(*) from VisitPurpose D
								where D.DelFlg = 0
							)

		IF @Limit = 0 OR @Limit = -1
		BEGIN
				SET @Limit = @TotalRecords
				IF @Limit = 0
				BEGIN
					SET @Limit = 10
				END
		END

	SELECT D.* FROM [dbo].VisitPurpose D
		where D.DelFlg = 0				 
		order by D.Id		
		OFFSET @offset rows fetch next @Limit rows only

		select @TotalRecords as TotalRecords
END
GO
USE [master]
GO
ALTER DATABASE [Topfarms] SET  READ_WRITE 
GO
