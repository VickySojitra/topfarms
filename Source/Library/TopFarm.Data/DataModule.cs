﻿//-----------------------------------------------------------------------
// <copyright file="DataModule.cs" company="Dexoc Solutions">
//     Copyright Dexoc Solutions. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TopFarm.Data
{
    using Autofac;
    using TopFarm.Data.Contract;
    using TopFarm.Data.V1;

    /// <summary>
    /// Contract Class for DataModule.
    /// </summary>
    public class DataModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TradersDao>().As<AbstractTradersDao>().InstancePerDependency();
            builder.RegisterType<OrdersDao>().As<AbstractOrdersDao>().InstancePerDependency();
            builder.RegisterType<ProductCategoryDao>().As<AbstractProductCategoryDao>().InstancePerDependency();
            builder.RegisterType<CustomerDao>().As<AbstractCustomerDao>().InstancePerDependency();
            builder.RegisterType<TransactionDao>().As<AbstractTransactionDao>().InstancePerDependency();
            builder.RegisterType<NotificationDao>().As<AbstractNotificationDao>().InstancePerDependency();
            builder.RegisterType<SupportTicketDao>().As<AbstractSupportTicketDao>().InstancePerDependency();
            builder.RegisterType<ShippingRateDao>().As<AbstractShippingRateDao>().InstancePerDependency();
            builder.RegisterType<BlogCategoryDao>().As<AbstractBlogCategoryDao>().InstancePerDependency();
            builder.RegisterType<BlogDao>().As<AbstractBlogDao>().InstancePerDependency();
            builder.RegisterType<DailyReportDao>().As<AbstractDailyReportDao>().InstancePerDependency();
            builder.RegisterType<ProductDao>().As<AbstractProductDao>().InstancePerDependency();
            builder.RegisterType<PromotionalDao>().As<AbstractPromotionalDao>().InstancePerDependency();
            builder.RegisterType<ProductReviewDao>().As<AbstractProductReviewDao>().InstancePerDependency();
            builder.RegisterType<CountryDao>().As<AbstractCountryDao>().InstancePerDependency();
            builder.RegisterType<PromotionTypeDao>().As<AbstractPromotionTypeDao>().InstancePerDependency();
            builder.RegisterType<AddressDao>().As<AbstractAddressDao>().InstancePerDependency();
            builder.RegisterType<CartDao>().As<AbstractCartDao>().InstancePerDependency();
            builder.RegisterType<WishlistDao>().As<AbstractWishlistDao>().InstancePerDependency();
            builder.RegisterType<AdminDao>().As<AbstractAdminDao>().InstancePerDependency();
            builder.RegisterType<CommonDao>().As<AbstractCommonDao>().InstancePerDependency();

            base.Load(builder);
        }
    }
}
