﻿using System;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Data.Contract
{
    public abstract class AbstractDailyReportDao
    {
        public abstract PagedList<AbstractDailyReport> SelectAll(PageParam pageParam, DateTime? date = null, long traderId = 0);
        public abstract PagedList<AbstractCommonDD> VisitPurposeSelectAll(PageParam pageParam);
        public abstract SuccessResult<AbstractDailyReport> Select(int id);
        public abstract SuccessResult<AbstractDailyReport> InsertUpdate(AbstractDailyReport abstractDailyReport);
    }
}
