﻿using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Data.Contract
{
    public abstract class AbstractProductCategoryDao
    {
        public abstract int Insert(AbstractProductCategory abstractCategory);

        public abstract PagedList<AbstractProductCategory> SelectAll(PageParam pageParam, string search = "");

        public abstract AbstractProductCategory SelectByID(long ID);

        public abstract int Update(AbstractProductCategory abstractCategory);
    }
}
