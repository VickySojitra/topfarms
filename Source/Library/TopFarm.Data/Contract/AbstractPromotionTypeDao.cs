﻿using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Data.Contract
{
    public abstract class AbstractPromotionTypeDao
    {
        public abstract PagedList<AbstractPromotionType> SelectAll(PageParam pageParam, string search = "");
    }
}
