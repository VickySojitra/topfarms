﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopFarm.Entities.Contract;

namespace TopFarm.Data.Contract
{
   public abstract class AbstractCommonDao
    {
        public abstract AbstractCommonCount GetcDashboardCountList(long ID);

        public abstract AbstractCommonCount GettDashboardCountList(long ID, int RoleId);

    }
}
