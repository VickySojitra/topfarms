﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Data.Contract
{
    public abstract class AbstractShippingRateDao
    {
        public abstract SuccessResult<AbstractShippingRate> Upsert(AbstractShippingRate abstractCategory);

        public abstract PagedList<AbstractShippingRate> SelectAll(PageParam pageParam, string search = "");

        public abstract AbstractShippingRate SelectByID(long ID);
    }
}
