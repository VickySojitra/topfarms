﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Data.Contract
{
    public abstract class AbstractAdminDao
    {
        public abstract PagedList<AbstractAdmin> SelectAll(PageParam pageParam, string search = "");

        public abstract SuccessResult<AbstractAdmin> Select(int id);

        public abstract SuccessResult<AbstractAdmin> Login(string Email, string WebPassword);
        public abstract SuccessResult<AbstractAdmin> ChangePassword(int Id, string OldPassword, string NewPassword);

        public abstract SuccessResult<AbstractAdmin> InsertUpdate(AbstractAdmin abstractCustomer);
        public abstract SuccessResult<AbstractCustomer> TInsertUpdate(AbstractCustomer abstractCustomer);

        public abstract bool Delete(int id);
        public abstract SuccessResult<AbstractCustomer> TSelect(int id);

    }
}
