﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Data.Contract
{
    public abstract class AbstractCartDao
    {
        public abstract SuccessResult<AbstractCart> Upsert(AbstractCart abstractBlog);
        public abstract PagedList<AbstractCart> SelectAllByCustomerID(long CustomrtId);
        public abstract bool DeleteByID(long id);
        public abstract SuccessResult<AbstractCheckout> UpsertCheckOut(AbstractCheckout abstractBlog);
        public abstract SuccessResult<AbstractCheckout> SelectCheckoutByCustID(long CustomrtId);
        public abstract bool DeleteCheckoutByCustID(long id);
        public abstract SuccessResult<AbstractCheckout> UpsertAdrsCheckOut(AbstractCheckout abstractBlog);
        public abstract PagedList<AbstractCart> GroupCategorybyId(long CustomrtId);
    }
}
