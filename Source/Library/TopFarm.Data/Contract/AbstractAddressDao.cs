﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Data.Contract
{
    public abstract class AbstractAddressDao
    {
        public abstract SuccessResult<AbstractAddress> Upsert(AbstractAddress abstractPromotional);

        public abstract AbstractAddress SelectByID(long ID);
        
        public abstract AbstractAddress AddressSelectByID(long ID);

        public abstract PagedList<AbstractAddress> SelectAll(PageParam pageParam, string search = "");
        public abstract bool Delete(int id);
    }
}
