﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Data.Contract
{
    public abstract class AbstractBlogCategoryDao
    {
        public abstract SuccessResult<AbstractBlogCategory> Upsert(AbstractBlogCategory abstractBlogCategory);

        public abstract PagedList<AbstractBlogCategory> SelectAll(string search = "");
    }
}
