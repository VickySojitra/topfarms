﻿using System;
using System.Collections.Generic;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Data.Contract
{
    public abstract class AbstractBlogDao
    {
        public abstract SuccessResult<AbstractBlog> Upsert(AbstractBlog abstractBlog);

        public abstract PagedList<AbstractBlog> SelectAll(PageParam pageParam, string search = "");

        public abstract AbstractBlog SelectByID(long ID);
        public abstract List<AbstractBlog> Blog_BlogCategoryId(long BlogCategoryId);

        public abstract bool UpdateStatus(long ID, bool Status);

        public abstract bool DeleteByID(long id);
    }
}
