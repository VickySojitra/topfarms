﻿using System;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Data.Contract
{
    public abstract class AbstractCustomerDao
    {
        public abstract PagedList<AbstractCustomer> SelectAll(PageParam pageParam, string search = "");
        public abstract PagedList<AbstractCustomer> CustromerReportSelectAll(DateTime startDate, DateTime endDate);
        public abstract SuccessResult<AbstractCustomer> Select(int id);
        public abstract SuccessResult<AbstractCustomer> GetCustomerById(int id);

        public abstract SuccessResult<AbstractCustomer> Customer_Login(string Email, string WebPassword);
        public abstract SuccessResult<AbstractCustomer> Customer_ChangePassword(int Id, string OldPassword, string NewPassword);

        public abstract SuccessResult<AbstractCustomer> InsertUpdate(AbstractCustomer abstractCustomer);

        public abstract bool Delete(int id);

        public abstract bool IsEmailIdExist(string Email);

        public abstract bool Updatetoken(string Email,string Token);

        public abstract bool ResetPassword(string token, string changePassword);
    }
}
