﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Data.Contract
{
    public abstract class AbstractSupportTicketDao
    {
        public abstract PagedList<AbstractSupportTicket> SelectAll(PageParam pageParam, string search = "");

        public abstract SuccessResult<AbstractSupportTicket> SelectbyID(int id);

        public abstract SuccessResult<AbstractSupportTicket> SelectbyOrderID(int id);

        public abstract PagedList<AbstractSupportTicket> SelectbyCustomerID(PageParam pageParam, long id, int status = 0);

        public abstract SuccessResult<AbstractSupportTicket> InsertUpdate(AbstractSupportTicket AbstractSupportTicket);
        public abstract SuccessResult<AbstractSupportComment> InsertUpdateComment(AbstractSupportComment SupportComment);
        public abstract bool Delete(int id);

        public abstract bool UpdateStatus(long ID, bool Status);

        public abstract int[] TicketCount();
        public abstract PagedList<AbstractSupportComment> SelectCommentsByTicketId(PageParam pageParam, long ticketId);
    }
}
