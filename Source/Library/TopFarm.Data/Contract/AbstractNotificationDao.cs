﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Data.Contract
{
    public abstract class AbstractNotificationDao
    {
        public abstract SuccessResult<AbstractNotification> InsertUpdate(AbstractNotification abstractNotification);

        public abstract PagedList<AbstractNotification> Notifications_ByRoleID(string search = "",int RoleID = 0,int UserID = 0);

        public abstract SuccessResult<AbstractNotification> SelectByID(long ID);

        public abstract int Update(AbstractNotification abstractNotification);
    }
}
