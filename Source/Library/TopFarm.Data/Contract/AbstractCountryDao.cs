﻿using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Data.Contract
{
    public abstract class AbstractCountryDao
    {
        public abstract PagedList<AbstractCountry> SelectAll(PageParam pageParam, string search = "");
    }
}
