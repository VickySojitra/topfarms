﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class DailyReportDao : AbstractDailyReportDao
    {
        public override PagedList<AbstractDailyReport> SelectAll(PageParam pageParam, DateTime? date, long traderId = 0)
        {
            PagedList<AbstractDailyReport> Outcome = new PagedList<AbstractDailyReport>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Date", date, dbType: DbType.Date, direction: ParameterDirection.Input);
            param.Add("@TraderID", traderId, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("DailyReport_SelectAll", param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<DailyReport>());
                Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Outcome;
        }

        public override SuccessResult<AbstractDailyReport> Select(int Id)
        {
            SuccessResult<AbstractDailyReport> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("DailyReport_Select", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractDailyReport>>().SingleOrDefault();
                Outcome.Item = task.Read<DailyReport>().SingleOrDefault();
                Outcome.Item.DailyVisits = new List<AbstractDailyVisit>();
                Outcome.Item.DailyVisits.AddRange(task.Read<DailyVisit>());
            }

            return Outcome;
        }

        public override SuccessResult<AbstractDailyReport> InsertUpdate(AbstractDailyReport abstractOrders)
        {
            SuccessResult<AbstractDailyReport> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractOrders.ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@TraderId", abstractOrders.TraderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsSubmitted", abstractOrders.IsSubmitted, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@Date", abstractOrders.Date, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@CustomerName", abstractOrders.Visit.CustomerName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CustomerLocation", abstractOrders.Visit.CustomerLocation, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NoOfCows", abstractOrders.Visit.NoOfCows, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@VisitPurposeID", abstractOrders.Visit.VisitPurposeID, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@NewOrder", abstractOrders.Visit.NewOrder, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@Summary", abstractOrders.Visit.Summary, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DelFlg", abstractOrders.DelFlg, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractOrders.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractOrders.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("DailyReport_Upsert", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractDailyReport>>().SingleOrDefault();
                Outcome.Item = task.Read<DailyReport>().SingleOrDefault();

                Outcome.Item.DailyVisits = new List<AbstractDailyVisit>();
                Outcome.Item.DailyVisits.AddRange(task.Read<DailyVisit>());
            }

            return Outcome;
        }

        public override PagedList<AbstractCommonDD> VisitPurposeSelectAll(PageParam pageParam)
        {
            PagedList<AbstractCommonDD> Outcome = new PagedList<AbstractCommonDD>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("VisitPurpose_SelectAll", param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<CommonDD>());
                Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Outcome;
        }
    }
}
