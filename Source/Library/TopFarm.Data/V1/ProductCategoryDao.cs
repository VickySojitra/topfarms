﻿using Dapper;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class ProductCategoryDao: AbstractProductCategoryDao
	{
		/// <summary>
		/// Inserts a record into the ProductCategory table.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="status"></param>
		/// <param name="delFlg"></param>
		/// <param name="createdAt"></param>
		/// <param name="createdBy"></param>
		/// <param name="updatedAt"></param>
		/// <param name="updatedBy"></param>
		/// <returns></returns>
		public override int Insert(AbstractProductCategory abstractCategory)
		{
			Database db = null;
			DbCommand dbCommand = null;
			try
			{
				db = new SqlDatabase(Configurations.ConnectionString);
				dbCommand = db.GetStoredProcCommand("ProductCategoryInsert");

				db.AddInParameter(dbCommand, "Name", DbType.String, abstractCategory.Name);
				db.AddInParameter(dbCommand, "Tax", DbType.Double, abstractCategory.Tax);

				db.AddInParameter(dbCommand, "Status", DbType.Boolean, abstractCategory.Status);
				db.AddInParameter(dbCommand, "DelFlg", DbType.Boolean, abstractCategory.DelFlg);
				db.AddInParameter(dbCommand, "CreatedAt", DbType.DateTime, abstractCategory.CreatedAt);
				db.AddInParameter(dbCommand, "CreatedBy", DbType.String, abstractCategory.CreatedBy);
				db.AddInParameter(dbCommand, "UpdatedAt", DbType.DateTime, abstractCategory.UpdatedAt);
				db.AddInParameter(dbCommand, "UpdatedBy", DbType.String, abstractCategory.UpdatedBy);

				// Execute the query and return the new identity value
				int returnValue = Convert.ToInt32(db.ExecuteScalar(dbCommand));

				return returnValue;
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (dbCommand != null)
				{
					dbCommand.Dispose();
					dbCommand = null;
				}
				if (db != null)
					db = null;
			}
		}

        public override PagedList<AbstractProductCategory> SelectAll(PageParam pageParam, string search = "")
        {
            try
            {
				PagedList<AbstractProductCategory> classes = new PagedList<AbstractProductCategory>();

				var param = new DynamicParameters();
				param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
				param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
				param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

				using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
				{
					var task = con.QueryMultiple("ProductCategorySelectAll", param, commandType: CommandType.StoredProcedure);
					classes.Values.AddRange(task.Read<ProductCategory>());
					classes.TotalRecords = task.Read<long>().SingleOrDefault();
				}
				return classes;
			}
            catch(Exception)
            {
				throw;
            }
		}

        public override AbstractProductCategory SelectByID(long ID)
        {
			Database db = null;
			DbCommand dbCommand = null;
			try
			{
				db = new SqlDatabase(Configurations.ConnectionString);
				dbCommand = db.GetStoredProcCommand("ProductCategorySelect");

				db.AddInParameter(dbCommand, "ID", DbType.Int64, ID);
				DataSet dataSet = db.ExecuteDataSet(dbCommand);

				ProductCategory productCategory = null;
				if (dataSet.Tables.Count > 0)
                {
					productCategory = new ProductCategory()
					{
						ID= Convert.ToInt64(dataSet.Tables[0].Rows[0].ItemArray[0]),
						Name = dataSet.Tables[0].Rows[0].ItemArray[1].ToString(),
						Tax = Convert.ToDouble(dataSet.Tables[0].Rows[0].ItemArray[2]),
						Status = Convert.ToBoolean(dataSet.Tables[0].Rows[0].ItemArray[3])
					};
				}
				return productCategory;
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (dbCommand != null)
				{
					dbCommand.Dispose();
					dbCommand = null;
				}
				if (db != null)
					db = null;
			}
		}

        public override int Update(AbstractProductCategory abstractCategory)
        {
			Database db = null;
			DbCommand dbCommand = null;
			try
			{
				db = new SqlDatabase(Configurations.ConnectionString);
				dbCommand = db.GetStoredProcCommand("ProductCategoryUpdate");

				db.AddInParameter(dbCommand, "ID", DbType.Int64, abstractCategory.ID);
				db.AddInParameter(dbCommand, "Name", DbType.String, abstractCategory.Name);
				db.AddInParameter(dbCommand, "Status", DbType.Boolean, abstractCategory.Status);
				db.AddInParameter(dbCommand, "UpdatedAt", DbType.DateTime, abstractCategory.UpdatedAt);
				db.AddInParameter(dbCommand, "Tax", DbType.Double, abstractCategory.Tax);


				return db.ExecuteNonQuery(dbCommand);
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (dbCommand != null)
				{
					dbCommand.Dispose();
					dbCommand = null;
				}
				if (db != null)
					db = null;
			}
		}
    }
}
