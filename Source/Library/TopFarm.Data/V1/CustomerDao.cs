﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class CustomerDao : AbstractCustomerDao
    {
        public override bool Delete(int id)
        {
            try
            {
                bool isDelete = false;
                var param = new DynamicParameters();
                param.Add("@Id", id, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.Query<bool>("Customers_Delete", param, commandType: CommandType.StoredProcedure);
                    isDelete = task.SingleOrDefault<bool>();
                }

                return isDelete;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractCustomer> Customer_ChangePassword(int Id, string OldPassword, string NewPassword)
        {
            try
            {
                SuccessResult<AbstractCustomer> result = null;
                var param = new DynamicParameters();
                param.Add("@Id", Id, DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@OldPassword", OldPassword, DbType.String, direction: ParameterDirection.Input);
                param.Add("@NewPassword", NewPassword, DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Customers_ChangePassword", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractCustomer>>().SingleOrDefault();
                    result.Item = task.Read<Customer>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractCustomer> Customer_Login(string Email, string WebPassword)
        {
            SuccessResult<AbstractCustomer> result = null;
            var param = new DynamicParameters();
            param.Add("@Email", Email, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", WebPassword, DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Customers_Login", param, commandType: CommandType.StoredProcedure);
                result = task.Read<SuccessResult<AbstractCustomer>>().SingleOrDefault();
                result.Item = task.Read<Customer>().SingleOrDefault();
            }

            return result;
        }

        public override SuccessResult<AbstractCustomer> InsertUpdate(AbstractCustomer abstractCustomer)
        {
            try
            {
                SuccessResult<AbstractCustomer> result = null;
                var param = new DynamicParameters();
                param.Add("@ID", abstractCustomer.ID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@FirstName", abstractCustomer.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@LastName", abstractCustomer.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@Email", abstractCustomer.Email, DbType.String, direction: ParameterDirection.Input);
                param.Add("@Password", abstractCustomer.Password, DbType.String, direction: ParameterDirection.Input);                
                param.Add("@Address", abstractCustomer.Address, DbType.Int16, direction: ParameterDirection.Input);
                param.Add("@Status", abstractCustomer.Status, DbType.Int16, direction: ParameterDirection.Input);
                param.Add("@AvtarURL", abstractCustomer.AvtarURL, DbType.String, direction: ParameterDirection.Input);
                param.Add("@Phone", abstractCustomer.Phone, DbType.String, direction: ParameterDirection.Input);
                param.Add("@DelFlg", abstractCustomer.DelFlg, DbType.Boolean, direction: ParameterDirection.Input);                
                param.Add("@CreatedBy", abstractCustomer.CreatedBy, DbType.String, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", abstractCustomer.UpdatedBy, DbType.String, direction: ParameterDirection.Input);
                param.Add("@NIP", abstractCustomer.NIP, DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Customers_Upsert", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractCustomer>>().SingleOrDefault();
                    result.Item = task.Read<Customer>().SingleOrDefault();
                }

                return result;
            }
            catch
            {
                throw;
            }

        }

        public override SuccessResult<AbstractCustomer> Select(int id)
        {
            try
            {
                SuccessResult<AbstractCustomer> result = null;
                var param = new DynamicParameters();

                param.Add("@Id", id, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Customers_ById", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractCustomer>>().SingleOrDefault();
                    result.Item = task.Read<Customer>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractCustomer> SelectAll(PageParam pageParam, string search = "")
        {
            try
            {
                PagedList<AbstractCustomer> classes = new PagedList<AbstractCustomer>();

                var param = new DynamicParameters();
                param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Customers_all", param, commandType: CommandType.StoredProcedure);
                    classes.Values.AddRange(task.Read<Customer>());
                    classes.TotalRecords = task.Read<long>().SingleOrDefault();
                }
                return classes;
            }
            catch
            {
                throw;
            }
        }
        public override PagedList<AbstractCustomer> CustromerReportSelectAll(DateTime startDate, DateTime endDate)
        {
            try
            {
                PagedList<AbstractCustomer> classes = new PagedList<AbstractCustomer>();

                var param = new DynamicParameters();
                param.Add("@StartDate", startDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("@EndDate", endDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("CustomersReport_all", param, commandType: CommandType.StoredProcedure);
                    classes.Values.AddRange(task.Read<Customer>());
                }
                return classes;
            }
            catch
            {
                throw;
            }
        }
        public override bool IsEmailIdExist(string Email)
        {
            try
            {
                bool isDelete = false;
                var param = new DynamicParameters();
                param.Add("@Email", Email, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.Query<bool>("IsEmailId_Exist", param, commandType: CommandType.StoredProcedure);
                    isDelete = task.SingleOrDefault<bool>();
                }

                return isDelete;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override bool Updatetoken(string Email, string Token)
        {
            string query = "UPDATE Customers SET Token = @Token WHERE Email = @Email";
            //query += " VALUES (@Token) where Email=@Email";
            SqlConnection con = new SqlConnection(Configurations.ConnectionString);
            SqlCommand myCommand = new SqlCommand(query, con);
            myCommand.Parameters.AddWithValue("@Token", Token);
            myCommand.Parameters.AddWithValue("@Email", Email);
            // ... other parameters
            con.Open();
            myCommand.ExecuteNonQuery();
            con.Close();
            return true;
        }

        public override bool ResetPassword(string token, string changePassword)
        {
            try
            {
                bool resetPassword = false;
                var param = new DynamicParameters();
                param.Add("@token", token, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@changePassword", changePassword, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.Query<bool>("customer_reset_password", param, commandType: CommandType.StoredProcedure);
                    resetPassword = task.SingleOrDefault<bool>();
                }

                return resetPassword;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractCustomer> GetCustomerById(int id)
        {
            try
            {
                SuccessResult<AbstractCustomer> result = null;
                var param = new DynamicParameters();

                param.Add("@Id", id, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("getCustomers_ById", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractCustomer>>().SingleOrDefault();
                    result.Item = task.Read<Customer>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
