﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class AdminDao : AbstractAdminDao
    {
        public override bool Delete(int id)
        {
            try
            {
                bool isDelete = false;
                var param = new DynamicParameters();
                param.Add("@Id", id, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.Query<bool>("Admin_Delete", param, commandType: CommandType.StoredProcedure);
                    isDelete = task.SingleOrDefault<bool>();
                }

                return isDelete;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractAdmin> ChangePassword(int Id, string OldPassword, string NewPassword)
        {
            try
            {
                SuccessResult<AbstractAdmin> result = null;
                var param = new DynamicParameters();
                param.Add("@Id", Id, DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@OldPassword", OldPassword, DbType.String, direction: ParameterDirection.Input);
                param.Add("@NewPassword", NewPassword, DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Admin_ChangePassword", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                    result.Item = task.Read<Admin>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractAdmin> Login(string Email, string WebPassword)
        {
            SuccessResult<AbstractAdmin> result = null;
            var param = new DynamicParameters();
            param.Add("@Email", Email, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", WebPassword, DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Admin_Login", param, commandType: CommandType.StoredProcedure);
                result = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                result.Item = task.Read<Admin>().SingleOrDefault();
            }

            return result;
        }

        public override SuccessResult<AbstractAdmin> InsertUpdate(AbstractAdmin abstractCustomer)
        {
            try
            {
                SuccessResult<AbstractAdmin> result = null;
                var param = new DynamicParameters();
                param.Add("@Id", abstractCustomer.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Name", abstractCustomer.Name, dbType: DbType.String, direction: ParameterDirection.Input);                
                param.Add("@Email", abstractCustomer.Email, DbType.String, direction: ParameterDirection.Input);
                param.Add("@Password", abstractCustomer.Password, DbType.String, direction: ParameterDirection.Input);
                param.Add("@AvtarURL", abstractCustomer.AvtarURL, DbType.String, direction: ParameterDirection.Input);
                param.Add("@Phone", abstractCustomer.Phone, DbType.String, direction: ParameterDirection.Input);
                param.Add("@DelFlg", abstractCustomer.DelFlg, DbType.Boolean, direction: ParameterDirection.Input);                
                param.Add("@CreatedBy", abstractCustomer.CreatedBy, DbType.String, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", abstractCustomer.UpdatedBy, DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Admin_Upsert", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                    result.Item = task.Read<Admin>().SingleOrDefault();
                }

                return result;
            }
            catch
            {
                throw;
            }

        }

        public override SuccessResult<AbstractAdmin> Select(int id)
        {
            try
            {
                SuccessResult<AbstractAdmin> result = null;
                var param = new DynamicParameters();

                param.Add("@Id", id, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Admin_ById", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractAdmin>>().SingleOrDefault();
                    result.Item = task.Read<Admin>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractAdmin> SelectAll(PageParam pageParam, string search = "")
        {
            try
            {
                PagedList<AbstractAdmin> classes = new PagedList<AbstractAdmin>();

                var param = new DynamicParameters();
                param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Admin_All", param, commandType: CommandType.StoredProcedure);
                    classes.Values.AddRange(task.Read<Admin>());
                    classes.TotalRecords = task.Read<long>().SingleOrDefault();
                }
                return classes;
            }
            catch
            {
                throw;
            }

        }

        public override SuccessResult<AbstractCustomer> TInsertUpdate(AbstractCustomer abstractCustomer)
        {
            try
            {
                SuccessResult<AbstractCustomer> result = null;
                var param = new DynamicParameters();
                param.Add("@ID", abstractCustomer.ID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@FirstName", abstractCustomer.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@LastName", abstractCustomer.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@JoinedDate", null, dbType: DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("@EmailID", abstractCustomer.Email, DbType.String, direction: ParameterDirection.Input);
               // param.Add("@Password", abstractCustomer.Password, DbType.String, direction: ParameterDirection.Input);
                param.Add("@MobileNo", abstractCustomer.MobileNo, DbType.String, direction: ParameterDirection.Input);
                param.Add("@AlternateMobileNo", abstractCustomer.AlternateMobileNo, DbType.String, direction: ParameterDirection.Input);
                param.Add("@Address", abstractCustomer.Address, DbType.Int16, direction: ParameterDirection.Input);
                param.Add("@LastLogin",null, DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("@AvtarURL", abstractCustomer.AvtarURL, DbType.String, direction: ParameterDirection.Input);
                param.Add("@Status", true, DbType.Int16, direction: ParameterDirection.Input);
                param.Add("@DelFlg", abstractCustomer.DelFlg, DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("@CreatedAt", null, DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("@CreatedBy", abstractCustomer.CreatedBy, DbType.Int16, direction: ParameterDirection.Input);
                param.Add("@UpdatedAt", System.DateTime.Now, DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", abstractCustomer.UpdatedBy, DbType.Int16, direction: ParameterDirection.Input);

                //param.Add("@Phone", abstractCustomer.Phone, DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Traders_Upsert", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractCustomer>>().SingleOrDefault();
                    result.Item = task.Read<Customer>().SingleOrDefault();
                }

                return result;
            }
            catch
            {
                throw;
            }
        }

        public override SuccessResult<AbstractCustomer> TSelect(int id)
        {
            try
            {
                SuccessResult<AbstractCustomer> result = null;
                var param = new DynamicParameters();

                param.Add("@Id", id, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Traders_ById", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractCustomer>>().SingleOrDefault();
                    result.Item = task.Read<Customer>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
