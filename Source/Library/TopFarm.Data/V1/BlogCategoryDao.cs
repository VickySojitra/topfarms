﻿using Dapper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class BlogCategoryDao : AbstractBlogCategoryDao
    {
        public override PagedList<AbstractBlogCategory> SelectAll( string search = "")
        {
            try
            {
                PagedList<AbstractBlogCategory> classes = new PagedList<AbstractBlogCategory>();

                var param = new DynamicParameters();
                param.Add("@Offset",0, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Limit", 0, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("BlogCategorySelectAll", param, commandType: CommandType.StoredProcedure);
                    classes.Values.AddRange(task.Read<BlogCategory>());
                    classes.TotalRecords = task.Read<long>().SingleOrDefault();
                }
                return classes;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractBlogCategory> Upsert(AbstractBlogCategory abstractBlogCategory)
        {
            try
            {
                SuccessResult<AbstractBlogCategory> result = null;
                var param = new DynamicParameters();

                param.Add("@ID", abstractBlogCategory.ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Name", abstractBlogCategory.Name, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("DelFlg", abstractBlogCategory.DelFlg, DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("CreatedAt", abstractBlogCategory.CreatedAt, DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("CreatedBy", abstractBlogCategory.CreatedBy, DbType.String, direction: ParameterDirection.Input);
                param.Add("UpdatedAt", abstractBlogCategory.UpdatedAt, DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("UpdatedBy", abstractBlogCategory.UpdatedBy, DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("BlogCategoryUpsert", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractBlogCategory>>().SingleOrDefault();
                    result.Item = task.Read<BlogCategory>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
