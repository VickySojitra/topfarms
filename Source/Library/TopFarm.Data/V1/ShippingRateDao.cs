﻿using Dapper;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class ShippingRateDao : AbstractShippingRateDao
    {
        public override PagedList<AbstractShippingRate> SelectAll(PageParam pageParam, string search = "")
        {
            try
            {
                PagedList<AbstractShippingRate> classes = new PagedList<AbstractShippingRate>();

                var param = new DynamicParameters();
                param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("ShippingSelectAll", param, commandType: CommandType.StoredProcedure);
                    classes.Values.AddRange(task.Read<ShippingRate>());
                    classes.TotalRecords = task.Read<long>().SingleOrDefault();
                }
                return classes;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override AbstractShippingRate SelectByID(long ID)
        {
            Database db = null;
            DbCommand dbCommand = null;
            try
            {
                db = new SqlDatabase(Configurations.ConnectionString);
                dbCommand = db.GetStoredProcCommand("ShippingRateSelectByID");

                db.AddInParameter(dbCommand, "ID", DbType.Int64, ID);
                DataSet dataSet = db.ExecuteDataSet(dbCommand);

                ShippingRate productCategory = null;
                if (dataSet.Tables.Count > 0)
                {
                    productCategory = new ShippingRate()
                    {
                        ID = Convert.ToInt64(dataSet.Tables[0].Rows[0].ItemArray[0]),
                        CategoryID = Convert.ToInt64(dataSet.Tables[0].Rows[0].ItemArray[1]),
                        FreeShipping = dataSet.Tables[0].Rows[0].ItemArray[2].ToString(),
                        FreeShippingAbove = dataSet.Tables[0].Rows[0].ItemArray[3].ToString()
                    };
                }



                return productCategory;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (dbCommand != null)
                {
                    dbCommand.Dispose();
                    dbCommand = null;
                }
                if (db != null)
                    db = null;
            }
        }

        public override SuccessResult<AbstractShippingRate> Upsert(AbstractShippingRate abstractCategory)
        {
            try
            {
                SuccessResult<AbstractShippingRate> result = null;
                var param = new DynamicParameters();

                param.Add("@ID", abstractCategory.ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@CategoryID", abstractCategory.CategoryID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@FreeShipping", Convert.ToDouble(abstractCategory.FreeShipping), dbType: DbType.Double, direction: ParameterDirection.Input);
                param.Add("@FreeShippingAbove", Convert.ToDouble(abstractCategory.FreeShippingAbove), DbType.Double, direction: ParameterDirection.Input);
                param.Add("DelFlg", abstractCategory.DelFlg, DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("CreatedAt", abstractCategory.CreatedAt, DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("CreatedBy", abstractCategory.CreatedBy, DbType.String, direction: ParameterDirection.Input);
                param.Add("UpdatedAt", abstractCategory.UpdatedAt, DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("UpdatedBy", abstractCategory.UpdatedBy, DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("ShippingUpsert", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractShippingRate>>().SingleOrDefault();
                    result.Item = task.Read<ShippingRate>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
