﻿using Dapper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class TransactionDao : AbstractTransactionDao
    {
        public override bool Delete(int id)
        {
			try
			{
				bool isDelete = false;
				var param = new DynamicParameters();
				param.Add("@ID", id, dbType: DbType.Int64, direction: ParameterDirection.Input);

				using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
				{
					var task = con.Query<bool>("Transactions_Delete", param, commandType: CommandType.StoredProcedure);
					isDelete = task.SingleOrDefault<bool>();
				}

				return isDelete;
			}
			catch (Exception)
			{
				throw;
			}
		}

        public override SuccessResult<AbstractTransaction> InsertUpdate(AbstractTransaction AbstractTransaction)
        {
			try
			{

				SuccessResult<AbstractTransaction> result = null;
				var param = new DynamicParameters();
				param.Add("ID", AbstractTransaction.ID, DbType.Int64, direction: ParameterDirection.Input);
				param.Add("TotalDueAmount", AbstractTransaction.TotalDueAmount, DbType.Double, direction: ParameterDirection.Input);
				param.Add("OrderID", AbstractTransaction.OrderID, DbType.Int64, direction: ParameterDirection.Input);
				param.Add("CustomerID", AbstractTransaction.CustomerID, DbType.Int64, direction: ParameterDirection.Input);
				param.Add("Status", AbstractTransaction.status, DbType.Int64, direction: ParameterDirection.Input);
				param.Add("DelFlg", AbstractTransaction.DelFlg, DbType.Boolean, direction: ParameterDirection.Input);
				param.Add("CreatedAt", AbstractTransaction.CreatedAt, DbType.DateTime, direction: ParameterDirection.Input);
				param.Add("CreatedBy", AbstractTransaction.CreatedBy, DbType.String, direction: ParameterDirection.Input);
				param.Add("UpdatedAt", AbstractTransaction.UpdatedAt, DbType.DateTime, direction: ParameterDirection.Input);
				param.Add("UpdatedBy", AbstractTransaction.UpdatedBy, DbType.String, direction: ParameterDirection.Input);

				using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
				{
					var task = con.QueryMultiple("Transactions_Upsert", param, commandType: CommandType.StoredProcedure);
					result = task.Read<SuccessResult<AbstractTransaction>>().SingleOrDefault();
					result.Item = task.Read<Transaction>().SingleOrDefault();
				}
				return result;

			}
			catch (Exception)
			{
				throw;
			}
		}

        public override SuccessResult<AbstractTransaction> SelectbyCustomerID(int id)
        {
			try
			{
				SuccessResult<AbstractTransaction> result = null;
				var param = new DynamicParameters();

				param.Add("@CustomerID", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

				using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
				{
					var task = con.QueryMultiple("Transactions_ByCustomerId", param, commandType: CommandType.StoredProcedure);
					result = task.Read<SuccessResult<AbstractTransaction>>().SingleOrDefault();
					result.Item = task.Read<Transaction>().SingleOrDefault();
				}

				return result;
			}
			catch (Exception)
			{
				throw;
			}
		}

		public override SuccessResult<AbstractTransaction> SelectbyOrderID(int id)
		{
			try
			{
				SuccessResult<AbstractTransaction> result = null;
				var param = new DynamicParameters();

				param.Add("@OrderID", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

				using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
				{
					var task = con.QueryMultiple("Transactions_ByOrderId", param, commandType: CommandType.StoredProcedure);
					result = task.Read<SuccessResult<AbstractTransaction>>().SingleOrDefault();
					result.Item = task.Read<Transaction>().SingleOrDefault();
				}

				return result;
			}
			catch (Exception)
			{
				throw;
			}
		}

		public override SuccessResult<AbstractTransaction> SelectbyID(int id)
		{
			try
			{
				SuccessResult<AbstractTransaction> result = null;
				var param = new DynamicParameters();

				param.Add("@ID", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

				using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
				{
					var task = con.QueryMultiple("Transactions_ById", param, commandType: CommandType.StoredProcedure);
					result = task.Read<SuccessResult<AbstractTransaction>>().SingleOrDefault();
					result.Item = task.Read<Transaction>().SingleOrDefault();
				}

				return result;
			}
			catch (Exception)
			{
				throw;
			}
		}

		public override PagedList<AbstractTransaction> SelectAll(PageParam pageParam, string search = "")
        {
			try
			{
				PagedList<AbstractTransaction> classes = new PagedList<AbstractTransaction>();

				var param = new DynamicParameters();
				param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
				param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
				param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

				using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
				{
					var task = con.QueryMultiple("Transactions_All", param, commandType: CommandType.StoredProcedure);
					classes.Values.AddRange(task.Read<Transaction>());
					classes.TotalRecords = task.Read<long>().SingleOrDefault();
				}
				return classes;
			}
			catch (Exception)
			{
				throw;
			}
		}
    }
}
