﻿using Dapper;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class ProductDao : AbstractProductDao
    {
        public override PagedList<AbstractProduct> SelectAll(PageParam pageParam, string search = "")
        {
            PagedList<AbstractProduct> Outcome = new PagedList<AbstractProduct>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Products_All", param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<Product>());
                Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Outcome;
        }

        public override SuccessResult<AbstractProduct> Select(long Id)
        {
            SuccessResult<AbstractProduct> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Products_ById", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractProduct>>().SingleOrDefault();
                Outcome.Item = task.Read<Product>().SingleOrDefault();
            }

            return Outcome;
        }

        public override SuccessResult<AbstractProduct> InsertUpdate(AbstractProduct abstractOrders)
        {
            SuccessResult<AbstractProduct> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractOrders.ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CategoryID", abstractOrders.CategoryID, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Name", abstractOrders.Name, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@StandardPrice", abstractOrders.StandardPrice, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@OfferPrice", abstractOrders.OfferPrice, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@Description", abstractOrders.Description, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsActive", abstractOrders.IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@IsFeatured", abstractOrders.IsFeatured, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@DelFlg", abstractOrders.DelFlg, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractOrders.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractOrders.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            param.Add("@PF", abstractOrders.PF, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Milk_PF", abstractOrders.Milk_PF, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Protein_PF", abstractOrders.Protein_PF, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Fat_PF", abstractOrders.Fat_PF, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Longevity_PF", abstractOrders.Longevity_PF, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Fertility_PF", abstractOrders.Fertility_PF, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Habit_PF", abstractOrders.Habit_PF, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@TPI", abstractOrders.TPI, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@NetMerit_TPI", abstractOrders.NetMerit_TPI, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Milk_TPI", abstractOrders.Milk_TPI, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Protein_TPI", abstractOrders.Protein_TPI, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Fat_TPI", abstractOrders.Fat_TPI, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Longevity_TPI", abstractOrders.Longevity_TPI, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CalvingEase_TPI", abstractOrders.CalvingEase_TPI, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Habit_TPI", abstractOrders.Habit_TPI, dbType: DbType.Int64, direction: ParameterDirection.Input);



            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Product_Upsert", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractProduct>>().SingleOrDefault();
                Outcome.Item = task.Read<Product>().SingleOrDefault();
            }

            return Outcome;
        }

        public override SuccessResult<AbstractProduct> Delete(long id, long deletedBy)
        {
            SuccessResult<AbstractProduct> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", deletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Products_Delete", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractProduct>>().SingleOrDefault();
                Outcome.Item = task.Read<Product>().SingleOrDefault();
            }

            return Outcome;
        }

        public override PagedList<AbstractProductImages> ProductImagesByProductId(long productId)
        {
            PagedList<AbstractProductImages> Outcome = new PagedList<AbstractProductImages>();

            var param = new DynamicParameters();
            param.Add("@ProductId", productId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("ProductImagesByProductId", param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<ProductImages>());
                Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Outcome;
        }

        public override SuccessResult<AbstractProductImages> InsertUpdateProductImage(AbstractProductImages product)
        {
            SuccessResult<AbstractProductImages> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", product.ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductId", product.ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@IsImage", product.IsImage, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@URL", product.URL, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsActive", product.IsActive, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@DelFlg", product.DelFlg, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", product.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", product.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("ProductImagesUpsert", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractProductImages>>().SingleOrDefault();
                Outcome.Item = task.Read<ProductImages>().SingleOrDefault();
            }

            return Outcome;
        }

        public override SuccessResult<AbstractProductImages> DeleteProductImage(long id, long deletedBy)
        {
            SuccessResult<AbstractProductImages> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", deletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("ProductImagesDelete", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractProductImages>>().SingleOrDefault();
                Outcome.Item = task.Read<ProductImages>().SingleOrDefault();
            }

            return Outcome;
        }

        public override PagedList<AbstractProduct> GetProductsByFilter(PageParam pageParam, string search = "", long categoryId = 0, ProductSortTypes SortBy = ProductSortTypes.New, decimal minPrice = 0, decimal maxPrice = 0)
        {
            PagedList<AbstractProduct> Outcome = new PagedList<AbstractProduct>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CategoryId", categoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@SortBy", (int)SortBy, dbType: DbType.Int16, direction: ParameterDirection.Input);
            param.Add("@MinPrice", minPrice, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@MaxPrice", maxPrice, dbType: DbType.Decimal, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("GetProductsByFilter", param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<Product>());
                Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Outcome;
        }

        public override PagedList<AbstractProduct> GetProductpriceByFilter(PageParam pageParam, Product search, long categoryId = 0, ProductSortTypes SortBy = ProductSortTypes.New, decimal minPrice = 0, decimal maxPrice = 0)
        {
            PagedList<AbstractProduct> Outcome = new PagedList<AbstractProduct>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CategoryId", categoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", "", dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@SortBy", (int)SortBy, dbType: DbType.Int16, direction: ParameterDirection.Input);
            param.Add("@MinPrice", minPrice, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@MaxPrice", maxPrice, dbType: DbType.Decimal, direction: ParameterDirection.Input);


            param.Add("@PF             ", search.PF, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Milk_PF        ", search.Milk_PF, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Protein_PF     ", search.Protein_PF, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Fat_PF         ", search.Fat_PF, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Longevity_PF   ", search.Longevity_PF, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Fertility_PF   ", search.Fertility_PF, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Habit_PF       ", search.Habit_PF, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@TPI            ", search.TPI, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@NetMerit_TPI   ", search.NetMerit_TPI, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Milk_TPI       ", search.Milk_TPI, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Protein_TPI    ", search.Protein_TPI, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Fat_TPI        ", search.Fat_TPI, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Longevity_TPI  ", search.Longevity_TPI, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CalvingEase_TPI", search.CalvingEase_TPI, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Habit_TPI      ", search.Habit_TPI, dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("GetProductpriceByFilter", param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<Product>());
                Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Outcome;
        }

        public override SuccessResult<AbstractProductMetaData> GetMetaData()
        {
            SuccessResult<AbstractProductMetaData> Outcome = null;
            var param = new DynamicParameters();

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Product_MetaData", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractProductMetaData>>().SingleOrDefault();
                Outcome.Item = task.Read<ProductMetaData>().SingleOrDefault();
            }

            return Outcome;
        }

        public override List<AbstractProduct> GetProductByCategory(long Id)
        {
            Database db = null;
            DbCommand dbCommand = null;
            List<AbstractProduct> listProduct = new List<AbstractProduct>();
            try
            {
                db = new SqlDatabase(Configurations.ConnectionString);
                dbCommand = db.GetStoredProcCommand("Products_BypCategoryId");

                db.AddInParameter(dbCommand, "Id", DbType.Int64, Id);
                DataSet dataSet = db.ExecuteDataSet(dbCommand);

                for (int i = 0; i < dataSet.Tables[1].Rows.Count; i++)
                {
                    Product pr = new Product
                    {
                        ID = Convert.ToInt64(dataSet.Tables[1].Rows[i].ItemArray[0]),
                        Name = (dataSet.Tables[1].Rows[i].ItemArray[1]).ToString(),
                        CategoryID = Convert.ToInt64(dataSet.Tables[1].Rows[i].ItemArray[2])
                    };
                    listProduct.Add(pr);
                }

                return listProduct;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (dbCommand != null)
                {
                    dbCommand.Dispose();
                    dbCommand = null;
                }
                if (db != null)
                    db = null;
            }
        }

        public override PagedList<AbstractProduct> GetProductPriceList()
        {
            SuccessResult<AbstractProduct> Outcome = null;
            var param = new DynamicParameters();
            PagedList<AbstractProduct> classes = new PagedList<AbstractProduct>();

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Product_PriceData", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractProduct>>().SingleOrDefault();
                classes.Values.AddRange(task.Read<Product>());
            }
            return classes;
        }

    }
}
