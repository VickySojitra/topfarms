﻿using Dapper;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class PromotionalDao : AbstractPromotionalDao
    {
        public override AbstractPromotional DiscountByPromocode(string Prmocode)
        {
            Database db = null;
            DbCommand dbCommand = null;
            try
            {
                db = new SqlDatabase(Configurations.ConnectionString);
                dbCommand = db.GetStoredProcCommand("DiscountByPrmocode");

                db.AddInParameter(dbCommand, "Promocode", DbType.String, Prmocode);
                DataSet dataSet = db.ExecuteDataSet(dbCommand);

                Promotional productCategory = null;
                if (dataSet.Tables.Count > 0)
                {
                    productCategory = new Promotional()
                    {
                        ID = Convert.ToInt64(dataSet.Tables[1].Rows.Count) == 0 ? 0 : Convert.ToInt64(dataSet.Tables[1].Rows[0].ItemArray[0]),
                        Discount = Convert.ToInt64(dataSet.Tables[1].Rows.Count) == 0 ? "" : dataSet.Tables[1].Rows[0].ItemArray[1].ToString(),
                    };
                }

                return productCategory;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (dbCommand != null)
                {
                    dbCommand.Dispose();
                    dbCommand = null;
                }
                if (db != null)
                    db = null;
            }
        }

        public override PagedList<AbstractPromotional> SelectAll(PageParam pageParam, string search = "")
        {
            try
            {
                PagedList<AbstractPromotional> classes = new PagedList<AbstractPromotional>();

                var param = new DynamicParameters();
                param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("PromotionalSelectAll", param, commandType: CommandType.StoredProcedure);
                    classes.Values.AddRange(task.Read<Promotional>());
                    classes.TotalRecords = task.Read<long>().SingleOrDefault();
                }
                return classes;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override AbstractPromotional SelectByID(long ID)
        {
            Database db = null;
            DbCommand dbCommand = null;
            try
            {
                db = new SqlDatabase(Configurations.ConnectionString);
                dbCommand = db.GetStoredProcCommand("PromotionalSelectByID");

                db.AddInParameter(dbCommand, "ID", DbType.Int64, ID);
                DataSet dataSet = db.ExecuteDataSet(dbCommand);

                Promotional productCategory = null;
                if (dataSet.Tables.Count > 0)
                {
                    productCategory = new Promotional()
                    {
                        ID = Convert.ToInt64(dataSet.Tables[0].Rows[0].ItemArray[0]),
                        Type = Convert.ToInt64(dataSet.Tables[0].Rows[0].ItemArray[1]),
                        Name = dataSet.Tables[0].Rows[0].ItemArray[2].ToString(),
                        ProductName = dataSet.Tables[0].Rows[0].ItemArray[3].ToString(),
                        PromoCode = dataSet.Tables[0].Rows[0].ItemArray[4].ToString(),
                        ProductID = Convert.ToInt64(dataSet.Tables[0].Rows[0].ItemArray[5]),
                        Discount = dataSet.Tables[0].Rows[0].ItemArray[6].ToString(),
                        Status = Convert.ToBoolean(dataSet.Tables[0].Rows[0].ItemArray[7]),
                        ImageURL = dataSet.Tables[0].Rows[0].ItemArray[8].ToString(),
                    };
                }

                return productCategory;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (dbCommand != null)
                {
                    dbCommand.Dispose();
                    dbCommand = null;
                }
                if (db != null)
                    db = null;
            }
        }

        public override SuccessResult<AbstractPromotional> Upsert(AbstractPromotional abstractPromotional)
        {
            try
            {
                SuccessResult<AbstractPromotional> result = null;
                var param = new DynamicParameters();

                param.Add("@ID", abstractPromotional.ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Type", abstractPromotional.Type, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Name", abstractPromotional.Name, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@PromoCode", abstractPromotional.PromoCode, DbType.String, direction: ParameterDirection.Input);
                param.Add("@ProductID", abstractPromotional.ProductID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Discount", string.IsNullOrWhiteSpace(abstractPromotional.Discount) == true ? string.Empty : abstractPromotional.Discount, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@Status", abstractPromotional.Status, DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("@ImageURL", abstractPromotional.ImageURL, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@DelFlg", abstractPromotional.DelFlg, DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("@CreatedAt", abstractPromotional.CreatedAt, DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("@CreatedBy", abstractPromotional.CreatedBy, DbType.String, direction: ParameterDirection.Input);
                param.Add("@UpdatedAt", abstractPromotional.UpdatedAt, DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", abstractPromotional.UpdatedBy, DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("PromotionalUpsert", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractPromotional>>().SingleOrDefault();
                    result.Item = task.Read<Promotional>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
