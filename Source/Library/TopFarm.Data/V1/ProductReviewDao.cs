﻿using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class ProductReviewDao : AbstractProductReviewDao
    {
        #region Product review
        public override SuccessResult<AbstractProductReview> Select(long Id)
        {
            SuccessResult<AbstractProductReview> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("ProductReviewSelect", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractProductReview>>().SingleOrDefault();
                Outcome.Item = task.Read<ProductReview>().SingleOrDefault();
            }

            return Outcome;
        }

        public override PagedList<AbstractProductReview> SelectAll(PageParam pageParam, string search = "", long productId = 0, int SortBy = 0)
        {
            PagedList<AbstractProductReview> Outcome = new PagedList<AbstractProductReview>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProductId", productId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@SortBy", SortBy, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("ProductReviewSelectAll", param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<ProductReview>());
                Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Outcome;
        }

        public override SuccessResult<AbstractProductReview> InsertUpdate(AbstractProductReview productReview)
        {
            SuccessResult<AbstractProductReview> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", productReview.ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductID", productReview.ProductID, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Rating", productReview.Rating, dbType: DbType.Int16, direction: ParameterDirection.Input);
            param.Add("@Review", productReview.Review, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DelFlg", productReview.DelFlg, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", productReview.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", productReview.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedRole", productReview.CreatedRole, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedRole", productReview.UpdatedRole, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("ProductReviewUpsert", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractProductReview>>().SingleOrDefault();
                Outcome.Item = task.Read<ProductReview>().SingleOrDefault();
            }

            return Outcome;
        }

        public override SuccessResult<AbstractProductReview> Delete(long id, long deletedBy, int deletedRole)
        {
            SuccessResult<AbstractProductReview> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", deletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedRole", deletedRole, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("ProductReviewDelete", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractProductReview>>().SingleOrDefault();
                Outcome.Item = task.Read<ProductReview>().SingleOrDefault();
            }

            return Outcome;
        }
        #endregion

        #region Review Likes Dislike
        public override PagedList<AbstractProductReviewLike> ProdLikeSelectAll(PageParam pageParam, string search = "", long productReviewId = 0)
        {
            PagedList<AbstractProductReviewLike> Outcome = new PagedList<AbstractProductReviewLike>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProductReviewId", productReviewId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("ProductReviewLikesSelectAll", param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<ProductReviewLike>());
                Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Outcome;
        }

        public override SuccessResult<AbstractProductReviewLike> ProdLikeSelect(long Id)
        {
            SuccessResult<AbstractProductReviewLike> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("ProductReviewLikesSelect", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractProductReviewLike>>().SingleOrDefault();
                Outcome.Item = task.Read<ProductReviewLike>().SingleOrDefault();
            }

            return Outcome;
        }

        public override SuccessResult<AbstractProductReviewLike> ProdLikeInsertUpdate(AbstractProductReviewLike productReviewLike)
        {
            SuccessResult<AbstractProductReviewLike> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", productReviewLike.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProdReviewId", productReviewLike.ProdReviewId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Like", productReviewLike.Like, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@Dislike", productReviewLike.Dislike, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@DelFlg", productReviewLike.DelFlg, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", productReviewLike.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", productReviewLike.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CreatedRole", productReviewLike.CreatedRole, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UpdatedRole", productReviewLike.UpdatedRole, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("ProductReviewLikesUpsert", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractProductReviewLike>>().SingleOrDefault();
                Outcome.Item = task.Read<ProductReviewLike>().SingleOrDefault();
            }

            return Outcome;
        }

        public override SuccessResult<AbstractProductReviewLike> ProdLikeDelete(long id, long deletedBy, int deletedRole)
        {
            SuccessResult<AbstractProductReviewLike> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedBy", deletedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DeletedRole", deletedRole, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("ProductReviewLikesDelete", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractProductReviewLike>>().SingleOrDefault();
                Outcome.Item = task.Read<ProductReviewLike>().SingleOrDefault();
            }

            return Outcome;
        }
        #endregion
    }
}