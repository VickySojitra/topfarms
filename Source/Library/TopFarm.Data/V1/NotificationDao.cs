﻿using Dapper;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class NotificationDao : AbstractNotificationDao
    {
        public override SuccessResult<AbstractNotification> InsertUpdate(AbstractNotification abstractNotification)
        {
            try
            {

                SuccessResult<AbstractNotification> resultnotification = null;
                var param = new DynamicParameters();
                param.Add("ID", abstractNotification.ID, DbType.Int64, direction: ParameterDirection.Input);
                param.Add("UserID", abstractNotification.UserID, DbType.Int64, direction: ParameterDirection.Input);
                param.Add("RoleID", abstractNotification.RoleID, DbType.Int64, direction: ParameterDirection.Input);
                param.Add("Message", abstractNotification.Message, DbType.String, direction: ParameterDirection.Input);
                param.Add("CreatedBy", abstractNotification.CreatedBy, DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("NotificationsInsert", param, commandType: CommandType.StoredProcedure);
                    resultnotification = task.Read<SuccessResult<AbstractNotification>>().SingleOrDefault();
                    resultnotification.Item = task.Read<Notification>().SingleOrDefault();
                }
                return resultnotification;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractNotification> Notifications_ByRoleID( string search = "",int RoleID = 0, int UserID = 0)
        {

            try
            {
                PagedList<AbstractNotification> classes = new PagedList<AbstractNotification>();

                var param = new DynamicParameters();
                param.Add("@Offset", 0, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Limit", 0, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@RoleID", RoleID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@UserID", UserID, dbType: DbType.Int32, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Notifications_ByRoleID", param, commandType: CommandType.StoredProcedure);
                    classes.Values.AddRange(task.Read<Notification>());
                    classes.TotalRecords = task.Read<long>().SingleOrDefault();
                }
                return classes;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractNotification> SelectByID(long ID)
        {
            try
            {
                SuccessResult<AbstractNotification> resultNotification = null;
                var param = new DynamicParameters();

                param.Add("@UserID", ID, dbType: DbType.Int32, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("NotificationsSelect", param, commandType: CommandType.StoredProcedure);
                    resultNotification = task.Read<SuccessResult<AbstractNotification>>().SingleOrDefault();
                    resultNotification.Item = task.Read<Notification>().SingleOrDefault();
                }

                return resultNotification;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override int Update(AbstractNotification abstractNotification)
        {
            throw new NotImplementedException();
        }
    }
}
