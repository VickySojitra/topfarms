﻿using Dapper;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class OrdersDao : AbstractOrdersDao
    {
        public override PagedList<AbstractOrders> SelectAll(PageParam pageParam, string customerName = "", string traderName = "", int Status = 0, long CustomerId = 0, long traderId = 0)
        {
            PagedList<AbstractOrders> Outcome = new PagedList<AbstractOrders>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CustomerName", customerName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@TraderName", traderName, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Status", Status, dbType: DbType.Int16, direction: ParameterDirection.Input);
            param.Add("@CustomerId", CustomerId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@TraderId", traderId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("OrdersSelectAll", param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<Orders>());
                Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Outcome;
        }

        public override PagedList<AbstractOrders> SalesReportSelectAll(DateTime startDate, DateTime endDate)
        {
            PagedList<AbstractOrders> Outcome = new PagedList<AbstractOrders>();

            var param = new DynamicParameters();
            param.Add("@StartDate", startDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@EndDate", endDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("SalesReportSelectAll", param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<Orders>());
            }
            return Outcome;
        }

        public override SuccessResult<AbstractOrders> Select(int Id)
        {
            SuccessResult<AbstractOrders> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", Id, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("OrderSelect", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractOrders>>().SingleOrDefault();
                Outcome.Item = task.Read<Orders>().SingleOrDefault();
                if (task.IsConsumed == false)
                {
                    Outcome.Item.OrderDetails.AddRange(task.Read<OrderDetails>());
                }
                if (task.IsConsumed == false)
                {
                    Outcome.Item.OrderTimeline.AddRange(task.Read<OrderTimeline>());
                }
            }

            return Outcome;
        }

        public override SuccessResult<AbstractOrders> InsertUpdate(AbstractOrders abstractOrders)
        {
            SuccessResult<AbstractOrders> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractOrders.ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", abstractOrders.OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CustomerID", abstractOrders.CustomerID, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PaymentTypes", abstractOrders.PaymentTypes, dbType: DbType.Byte, direction: ParameterDirection.Input);
            param.Add("@Status", abstractOrders.Status, dbType: DbType.Byte, direction: ParameterDirection.Input);
            param.Add("@AssignTo", abstractOrders.AssignTo, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DelFlg", abstractOrders.DelFlg, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractOrders.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractOrders.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShipmentAddress", abstractOrders.ShipmentAddress, dbType: DbType.Int64, direction: ParameterDirection.Input);

            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("OrderUpsert", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractOrders>>().SingleOrDefault();
                Outcome.Item = task.Read<Orders>().SingleOrDefault();
            }

            return Outcome;
        }

        public override SuccessResult<AbstractOrders> InsertUpdateOrder(AbstractOrders abstractOrders)
        {
            SuccessResult<AbstractOrders> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractOrders.ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", abstractOrders.OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@CustomerID", abstractOrders.CustomerID, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PaymentTypes", abstractOrders.PaymentTypes, dbType: DbType.Byte, direction: ParameterDirection.Input);
            param.Add("@Status", abstractOrders.Status, dbType: DbType.Byte, direction: ParameterDirection.Input);
            param.Add("@AssignTo", abstractOrders.AssignTo, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@DelFlg", abstractOrders.DelFlg, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractOrders.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractOrders.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ShipmentAddress", abstractOrders.ShipmentAddress, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@PromoCodeId", abstractOrders.PromoCodeId, dbType: DbType.Int64, direction: ParameterDirection.Input);



            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("OrdersUpsert", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractOrders>>().SingleOrDefault();
                Outcome.Item = task.Read<Orders>().SingleOrDefault();
            }

            return Outcome;
        }

        public override PagedList<AbstractOrderComment> SelectAllOrderComment(PageParam pageParam, int orderId)
        {
            PagedList<AbstractOrderComment> Outcome = new PagedList<AbstractOrderComment>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", orderId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("OrderCommentSelectAll", param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<OrderComment>());
                Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Outcome;
        }

        public override SuccessResult<AbstractOrderComment> InsertUpdateOrderComment(AbstractOrderComment abstractOrderComment)
        {
            SuccessResult<AbstractOrderComment> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractOrderComment.ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderID", abstractOrderComment.OrderID, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@RoleID", abstractOrderComment.RoleID, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UserID", abstractOrderComment.UserID, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Comment", abstractOrderComment.Message, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DelFlg", abstractOrderComment.DelFlg, dbType: DbType.Boolean, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractOrderComment.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", abstractOrderComment.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("OrderCommentUpsert", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractOrderComment>>().SingleOrDefault();
                Outcome.Item = task.Read<OrderComment>().SingleOrDefault();
            }

            return Outcome;
        }

        public override SuccessResult<AbstractOrderMetaData> GetOrderMetadata()
        {
            SuccessResult<AbstractOrderMetaData> Outcome = null;
            var param = new DynamicParameters();

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Order_MetaData", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractOrderMetaData>>().SingleOrDefault();
                Outcome.Item = task.Read<OrderMetaData>().SingleOrDefault();
            }

            return Outcome;
        }

        public override PagedList<AbstractOrderTimeline> SelectOrderTimeline(int orderId)
        {
            PagedList<AbstractOrderTimeline> Outcome = new PagedList<AbstractOrderTimeline>();

            var param = new DynamicParameters();
            param.Add("@OrderId", orderId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("OrderTimelineSelectAll", param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<OrderTimeline>());
                Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return Outcome;
        }

        public override SuccessResult<AbstractOrderTimeline> InsertUpdateOrderTimeline(AbstractOrderTimeline orderTimeline)
        {
            SuccessResult<AbstractOrderTimeline> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", orderTimeline.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", orderTimeline.OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Status", orderTimeline.Status, dbType: DbType.Byte, direction: ParameterDirection.Input);
            param.Add("@Note", orderTimeline.Note, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedRole", orderTimeline.CreatedRole, dbType: DbType.Int16, direction: ParameterDirection.Input);
            param.Add("@UpdatedRole", orderTimeline.UpdatedRole, dbType: DbType.Int16, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", orderTimeline.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", orderTimeline.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("OrderTimelineUpsert", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractOrderTimeline>>().SingleOrDefault();
                Outcome.Item = task.Read<OrderTimeline>().SingleOrDefault();
            }

            return Outcome;
        }

        public override PagedList<AbstractOrderDetails> SelectOrderDetail(int orderId)
        {
            PagedList<AbstractOrderDetails> Outcome = new PagedList<AbstractOrderDetails>();

            var param = new DynamicParameters();
            param.Add("@OrderId", orderId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("OrderDetailsSelectAll", param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<OrderDetails>());
                Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return Outcome;
        }

        public override SuccessResult<AbstractOrderDetails> InsertUpdateOrderDetail(AbstractOrderDetails orderDetails)
        {
            SuccessResult<AbstractOrderDetails> Outcome = null;
            var param = new DynamicParameters();

            param.Add("@Id", orderDetails.Id, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@OrderId", orderDetails.OrderId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@ProductId", orderDetails.ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Quantity", orderDetails.Quantity, dbType: DbType.Int16, direction: ParameterDirection.Input);
            param.Add("@Rate", orderDetails.Rate, dbType: DbType.Decimal, direction: ParameterDirection.Input);
            param.Add("@CreatedRole", orderDetails.CreatedRole, dbType: DbType.Int16, direction: ParameterDirection.Input);
            param.Add("@UpdatedRole", orderDetails.UpdatedRole, dbType: DbType.Int16, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", orderDetails.CreatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@UpdatedBy", orderDetails.UpdatedBy, dbType: DbType.Int64, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("OrderDetailsUpsert", param, commandType: CommandType.StoredProcedure);
                Outcome = task.Read<SuccessResult<AbstractOrderDetails>>().SingleOrDefault();
                Outcome.Item = task.Read<OrderDetails>().SingleOrDefault();
            }

            return Outcome;
        }

        public override AbstractOrderData OrderDetailsById(int id)
        {
            Database db = null;
            DbCommand dbCommand = null;
            try
            {
                db = new SqlDatabase(Configurations.ConnectionString);
                dbCommand = db.GetStoredProcCommand("getOrderDetails_ById");

                db.AddInParameter(dbCommand, "ID", DbType.Int64, id);
                DataSet dataSet = db.ExecuteDataSet(dbCommand);

                OrderDetailsData orderDetailsData = new OrderDetailsData();
                AbstractOrderDetails objOrder = new AbstractOrderDetails();
                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        var orderList = dataSet.Tables[2].Rows;
                        orderDetailsData = new OrderDetailsData()
                        {
                            ID = Convert.ToInt64(dataSet.Tables[1].Rows[0].ItemArray[0]),
                            FirstName = dataSet.Tables[1].Rows[0].ItemArray[1].ToString(),
                            OrderDate = Convert.ToDateTime(dataSet.Tables[1].Rows[0].ItemArray[2]).ToString("MM/dd/yyyy"),
                            Status = Convert.ToInt16(dataSet.Tables[1].Rows[0].ItemArray[3]),
                            PaymentTypes = Convert.ToInt16(dataSet.Tables[1].Rows[0].ItemArray[4]),
                            Address1 = Convert.ToString(dataSet.Tables[1].Rows[0].ItemArray[5]),
                            Email = Convert.ToString(dataSet.Tables[1].Rows[0].ItemArray[6]),
                            listProduct = new System.Collections.Generic.List<AbstractOrderDetails>(),
                            TotalAmount = Convert.ToInt64(dataSet.Tables[3].Rows[0].ItemArray[1])
                        };
                      
                        System.Collections.Generic.List<AbstractOrderDetails> orderData = new System.Collections.Generic.List<AbstractOrderDetails>();
                        for (int i = 0; i < orderList.Count; i++)
                        {
                            orderData.AddRange(new System.Collections.Generic.List<AbstractOrderDetails>
                             {
                                 new AbstractOrderDetails {
                                     Id = Convert.ToInt64(dataSet.Tables[2].Rows[i].ItemArray[0]),
                                     OrderId = Convert.ToInt64(dataSet.Tables[2].Rows[i].ItemArray[1]),
                                     ProductName = dataSet.Tables[2].Rows[i].ItemArray[2].ToString(),
                                     Quantity = Convert.ToInt16(dataSet.Tables[2].Rows[i].ItemArray[3]),
                                     Rate = Convert.ToInt64(dataSet.Tables[2].Rows[i].ItemArray[4]),
                                     Amount = Convert.ToInt64(dataSet.Tables[2].Rows[i].ItemArray[5]),
                                     URL= Convert.ToString(dataSet.Tables[2].Rows[i].ItemArray[6])
                                 }
                             });
                        }
                        orderDetailsData.listProduct.AddRange(orderData);
                    }
                }

                return orderDetailsData;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (dbCommand != null)
                {
                    dbCommand.Dispose();
                    dbCommand = null;
                }
                if (db != null)
                    db = null;
            }
        }

        public override AbstractOrders TrackOrderDetail(int id)
        {
            Database db = null;
            DbCommand dbCommand = null;
            try
            {
                db = new SqlDatabase(Configurations.ConnectionString);
                dbCommand = db.GetStoredProcCommand("TrackOrderDetailById");

                db.AddInParameter(dbCommand, "ID", DbType.Int64, id);
                DataSet dataSet = db.ExecuteDataSet(dbCommand);

                Orders orderDetailsData = new Orders();

                if (dataSet.Tables.Count > 0)
                {
                    orderDetailsData = new Orders()
                    {
                        ID = Convert.ToInt64(dataSet.Tables[1].Rows[0].ItemArray[0]),
                        OrderId = Convert.ToInt64(dataSet.Tables[1].Rows[0].ItemArray[1]),
                        ShipMethod = Convert.ToInt16(dataSet.Tables[1].Rows[0].ItemArray[2]),
                        intStatus = Convert.ToInt16(dataSet.Tables[1].Rows[0].ItemArray[3])
                    };
                }
                    return orderDetailsData;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (dbCommand != null)
                {
                    dbCommand.Dispose();
                    dbCommand = null;
                }
                if (db != null)
                    db = null;
            }
        }

        public override PagedList<AbstractOrderData> OrderIdListById(int userId)
        {
            PagedList<AbstractOrderData> Outcome = new PagedList<AbstractOrderData>();

            var param = new DynamicParameters();
            param.Add("@Id", userId, dbType: DbType.Int64, direction: ParameterDirection.Input);

            //using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            //{
            //    var task = con.QueryMultiple("OrderIdListById", param, commandType: CommandType.StoredProcedure);
            //    Outcome.Values.AddRange(task.Read<AbstractOrderData>());
            //    Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            //}

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("OrderIdListById", param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<OrderDetailsData>());
        //        Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Outcome;
        }
    }
}