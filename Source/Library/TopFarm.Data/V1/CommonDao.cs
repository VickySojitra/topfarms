﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopFarm.Common;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class CommonDao : AbstractCommonDao
    {
        public override AbstractCommonCount GetcDashboardCountList(long ID)
        {
            Database db = null;
            DbCommand dbCommand = null;
            try
            {
                db = new SqlDatabase(Configurations.ConnectionString);
                dbCommand = db.GetStoredProcCommand("[DashboardCountByID]");

                db.AddInParameter(dbCommand, "ID", DbType.Int64, ID);
                db.AddInParameter(dbCommand, "RoleId", DbType.Int64, ProjectSession.LoginRoleID);

                DataSet dataSet = db.ExecuteDataSet(dbCommand);

                CommonCount commonCount = null;
                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        commonCount = new CommonCount()
                        {
                            OrdersCount = Convert.ToInt32(dataSet.Tables[0].Rows[0].ItemArray[0]),
                            WishlistCount = Convert.ToInt32(dataSet.Tables[1].Rows[0].ItemArray[0]),
                            SupportTicketsCount = Convert.ToInt32(dataSet.Tables[2].Rows[0].ItemArray[0]),
                            NotificationCount = Convert.ToInt32(dataSet.Tables[3].Rows[0].ItemArray[0]),
                            CartCount = Convert.ToInt32(dataSet.Tables[4].Rows[0].ItemArray[0])
                        };
                    }
                }

                return commonCount;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (dbCommand != null)
                {
                    dbCommand.Dispose();
                    dbCommand = null;
                }
                if (db != null)
                    db = null;
            }
        }

        public override AbstractCommonCount GettDashboardCountList(long ID, int RoleId)
        {
            Database db = null;
            DbCommand dbCommand = null;
            try
            {
                db = new SqlDatabase(Configurations.ConnectionString);
                dbCommand = db.GetStoredProcCommand("TraderDashboardCountByID");

                db.AddInParameter(dbCommand, "ID", DbType.Int64, ID);
                db.AddInParameter(dbCommand, "RoleId", DbType.Int64, RoleId);

                DataSet dataSet = db.ExecuteDataSet(dbCommand);

                CommonCount commonCount = null;
                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        commonCount = new CommonCount()
                        {
                           // TotalSalesCount = Convert.ToInt32(dataSet.Tables[0].Rows[0].ItemArray[0]),
                            trNotificationCount = Convert.ToInt32(dataSet.Tables[0].Rows[0].ItemArray[0]),
                            TotalSalesCount= Convert.ToInt32(dataSet.Tables[1].Rows[0].ItemArray[0]),
                            TotalTradersCount = Convert.ToInt32(dataSet.Tables[2].Rows[0].ItemArray[0]),
                            MemberSince= dataSet.Tables[3].Rows[0].ItemArray[0].ToString()
                        };
                    }
                }

                return commonCount;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (dbCommand != null)
                {
                    dbCommand.Dispose();
                    dbCommand = null;
                }
                if (db != null)
                    db = null;
            }
        }
    }
}
