﻿using Dapper;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class SupportTicketDao : AbstractSupportTicketDao
    {
        public override int[] TicketCount()
        {
            Database db = null;
            DbCommand dbCommand = null;
            try
            {
                db = new SqlDatabase(Configurations.ConnectionString);
                dbCommand = db.GetStoredProcCommand("TicketCounts");
                DataSet dataSet = db.ExecuteDataSet(dbCommand);


                int[] ticketCounts = new int[3];
                if (dataSet.Tables.Count > 0)
                {
                    ticketCounts[0] = Convert.ToInt32(dataSet.Tables[0].Rows[0].ItemArray[0]);
                    ticketCounts[1] = Convert.ToInt32(dataSet.Tables[1].Rows[0].ItemArray[0]);
                    ticketCounts[2] = Convert.ToInt32(dataSet.Tables[2].Rows[0].ItemArray[0]);
                }

                return ticketCounts;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (dbCommand != null)
                {
                    dbCommand.Dispose();
                    dbCommand = null;
                }
                if (db != null)
                    db = null;
            }
        }

        public override bool Delete(int id)
        {
            try
            {
                bool isDelete = false;
                var param = new DynamicParameters();
                param.Add("@ID", id, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.Query<bool>("SupportTicketsDelete", param, commandType: CommandType.StoredProcedure);
                    isDelete = task.SingleOrDefault<bool>();
                }

                return isDelete;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractSupportTicket> InsertUpdate(AbstractSupportTicket abstractSupportTicket)
        {
            try
            {
                SuccessResult<AbstractSupportTicket> result = null;
                var param = new DynamicParameters();
                param.Add("@Id", abstractSupportTicket.ID, DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@CustomerId", abstractSupportTicket.CustomerID, DbType.Int64, direction: ParameterDirection.Input);                
                param.Add("@Status", abstractSupportTicket.Status, DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("@DelFlg", abstractSupportTicket.DelFlg, DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("@CreatedBy", abstractSupportTicket.CreatedBy, DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", abstractSupportTicket.UpdatedBy, DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("SupportTicket_Upsert", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractSupportTicket>>().SingleOrDefault();
                    result.Item = task.Read<SupportTicket>().SingleOrDefault();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractSupportComment> InsertUpdateComment(AbstractSupportComment supportComment)
        {
            try
            {
                SuccessResult<AbstractSupportComment> result = null;
                var param = new DynamicParameters();
                param.Add("@ID", supportComment.ID, DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@SupportTicketID", supportComment.SupportTicketID, DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@RoleID", supportComment.RoleID, DbType.Int16, direction: ParameterDirection.Input);
                param.Add("@UserID", supportComment.UserID, DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Comment", supportComment.Comment, DbType.String, direction: ParameterDirection.Input);
                param.Add("@DelFlg", supportComment.DelFlg, DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("@CreatedBy", supportComment.CreatedBy, DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", supportComment.UpdatedBy, DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("SupportComment_Upsert", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractSupportComment>>().SingleOrDefault();
                    result.Item = task.Read<SupportComment>().SingleOrDefault();
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractSupportTicket> SelectAll(PageParam pageParam, string search = "")
        {
            try
            {
                PagedList<AbstractSupportTicket> classes = new PagedList<AbstractSupportTicket>();

                var param = new DynamicParameters();
                param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("SupportTicketsSelectAll", param, commandType: CommandType.StoredProcedure);
                    classes.Values.AddRange(task.Read<SupportTicket>());
                    classes.TotalRecords = task.Read<long>().SingleOrDefault();
                }
                return classes;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractSupportTicket> SelectbyCustomerID(PageParam pageParam, long id, int status = 0)
        {
            try
            {
                PagedList<AbstractSupportTicket> classes = new PagedList<AbstractSupportTicket>();
                var param = new DynamicParameters();
                param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@CustomerID", id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Status", status, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("SupportTicket_ByCustomerId", param, commandType: CommandType.StoredProcedure);
                    classes.Values.AddRange(task.Read<SupportTicket>());
                    classes.TotalRecords = task.Read<long>().SingleOrDefault();
                }

                return classes;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractSupportTicket> SelectbyID(int id)
        {
            try
            {
                SuccessResult<AbstractSupportTicket> result = null;
                var param = new DynamicParameters();

                param.Add("@ID", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("SupportTicketsSelect", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractSupportTicket>>().SingleOrDefault();
                    result.Item = task.Read<SupportTicket>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractSupportTicket> SelectbyOrderID(int id)
        {
            try
            {
                SuccessResult<AbstractSupportTicket> result = null;
                var param = new DynamicParameters();

                param.Add("@OrderID", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("SupportTicketsSelect_ByOrderId", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractSupportTicket>>().SingleOrDefault();
                    result.Item = task.Read<SupportTicket>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override bool UpdateStatus(long ID, bool Status)
        {
            try
            {
                bool isDelete = false;
                var param = new DynamicParameters();
                param.Add("@ID", ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Status", Status, dbType: DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", string.Empty, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.Query<bool>("SupportTicketUpdateStatus", param, commandType: CommandType.StoredProcedure);
                    isDelete = task.SingleOrDefault<bool>();
                }

                return isDelete;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractSupportComment> SelectCommentsByTicketId(PageParam pageParam, long ticketId)
        {
            try
            {
                PagedList<AbstractSupportComment> classes = new PagedList<AbstractSupportComment>();

                var param = new DynamicParameters();
                param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@SupportTicketId", ticketId, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("SupportCommentsByTicketId", param, commandType: CommandType.StoredProcedure);
                    classes.Values.AddRange(task.Read<SupportComment>());
                    classes.TotalRecords = task.Read<long>().SingleOrDefault();
                }
                return classes;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
