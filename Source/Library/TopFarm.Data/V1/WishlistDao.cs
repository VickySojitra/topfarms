﻿using Dapper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class WishlistDao : AbstractWishlistDao
    {
        public override bool DeleteByID(long id)
        {
            try
            {
                bool isDelete = false;
                var param = new DynamicParameters();
                param.Add("@Id", id, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.Query<bool>("Wishlist_Delete", param, commandType: CommandType.StoredProcedure);
                    isDelete = task.SingleOrDefault<bool>();
                }

                return isDelete;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractWishlist> SelectAllByCustomerID(long CustomrtId)
        {
            try
            {
                PagedList<AbstractWishlist> classes = new PagedList<AbstractWishlist>();

                var param = new DynamicParameters();
                param.Add("@CustomerId", CustomrtId, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Wishlist_ByCustomerId", param, commandType: CommandType.StoredProcedure);
                    classes.Values.AddRange(task.Read<Wishlist>());
                    classes.TotalRecords = task.Read<long>().SingleOrDefault();
                }
                return classes;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractWishlist> Upsert(AbstractWishlist abstractBlog)
        {
            try
            {
                SuccessResult<AbstractWishlist> result = null;
                var param = new DynamicParameters();

                param.Add("@Id", abstractBlog.ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@ProductId", abstractBlog.ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@CustomerId", abstractBlog.CustomerId, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Wishlist_Upsert", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractWishlist>>().SingleOrDefault();
                    result.Item = task.Read<Wishlist>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
