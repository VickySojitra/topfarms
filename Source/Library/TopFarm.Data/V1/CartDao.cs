﻿using Dapper;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class CartDao : AbstractCartDao
    {
        public override bool DeleteByID(long id)
        {
            try
            {
                bool isDelete = false;
                var param = new DynamicParameters();
                param.Add("@Id", id, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.Query<bool>("Cart_Delete", param, commandType: CommandType.StoredProcedure);
                    isDelete = task.SingleOrDefault<bool>();
                }

                return isDelete;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override bool DeleteCheckoutByCustID(long id)
        {
            try
            {
                bool isDelete = false;
                var param = new DynamicParameters();
                param.Add("@CustomerId", id, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.Query<bool>("Checkout_Delete_ByCustomerId", param, commandType: CommandType.StoredProcedure);
                    isDelete = task.SingleOrDefault<bool>();
                }

                return isDelete;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractCart> SelectAllByCustomerID(long CustomrtId)
        {
            try
            {
                PagedList<AbstractCart> classes = new PagedList<AbstractCart>();
                PagedList<AbstractCart> classes1 = new PagedList<AbstractCart>();

                var param = new DynamicParameters();
                param.Add("@CustomerId", CustomrtId, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("CartData_ByCustomerId", param, commandType: CommandType.StoredProcedure);
                    classes.Values.AddRange(task.Read<Cart>());
                    classes.TotalRecords = task.Read<long>().SingleOrDefault();

                }
                return classes;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractCart> GroupCategorybyId(long CustomrtId)
        {
            try
            {
                PagedList<AbstractCart> classes = new PagedList<AbstractCart>();

                var param = new DynamicParameters();
                param.Add("@CustomerId", CustomrtId, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    

                    var task = con.QueryMultiple("CartCount_ByCustomerId", param, commandType: CommandType.StoredProcedure);
                    classes.Values.AddRange(task.Read<countCart>());

                }
                return classes;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractCheckout> SelectCheckoutByCustID(long CustomrtId)
        {
            try
            {
                Database db = null;
                DbCommand dbCommand = null;
                SuccessResult<AbstractCheckout> result = new SuccessResult<AbstractCheckout>();

                //var param = new DynamicParameters();
                //param.Add("@CustomerId", CustomrtId, dbType: DbType.Int64, direction: ParameterDirection.Input);

                //using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                //{
                //    var task = con.QueryMultiple("Checkout_ByCId", param, commandType: CommandType.StoredProcedure);
                //    result = task.Read<SuccessResult<AbstractCheckout>>().SingleOrDefault();
                //    result.Item = task.Read<Checkout>().SingleOrDefault();
                //}
                db = new SqlDatabase(Configurations.ConnectionString);
                dbCommand = db.GetStoredProcCommand("Checkout_ByCId");

                db.AddInParameter(dbCommand, "CustomerId", DbType.Int64, CustomrtId);
                DataSet dataSet = db.ExecuteDataSet(dbCommand);

                if (dataSet.Tables[1].Rows.Count > 0)
                {
                    Checkout checkout = new Checkout()
                    {
                        ID = Convert.ToInt64(dataSet.Tables[1].Rows[0].ItemArray[0]),
                        CustomerID = Convert.ToInt64(dataSet.Tables[1].Rows[0].ItemArray[1]),
                        Note = dataSet.Tables[1].Rows[0].ItemArray[2].ToString(),
                        ShippingAddress = Convert.ToInt16(dataSet.Tables[1].Rows[0].ItemArray[3]),
                        BillingAddress = Convert.ToInt16(dataSet.Tables[1].Rows[0].ItemArray[4]),
                        PromoCode = dataSet.Tables[1].Rows[0].ItemArray[5].ToString(),
                        ShippingMethod = Convert.ToInt16(dataSet.Tables[1].Rows[0].ItemArray[6]),
                        PaymentMethod = Convert.ToInt16(dataSet.Tables[1].Rows[0].ItemArray[7]),
                        ShipAmount = Convert.ToDouble(dataSet.Tables[1].Rows[0].ItemArray[8])
                    };
                    result.Item = checkout;
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractCart> Upsert(AbstractCart abstractBlog)
        {
            try
            {
                SuccessResult<AbstractCart> result = null;
                var param = new DynamicParameters();

                param.Add("@Id", abstractBlog.ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@ProductId", abstractBlog.ProductId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@CustomerId", abstractBlog.CustomerId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Quantity", abstractBlog.Quantity, dbType: DbType.Int16, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Cart_Upsert", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractCart>>().SingleOrDefault();
                    result.Item = task.Read<Cart>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractCheckout> UpsertAdrsCheckOut(AbstractCheckout abstractBlog)
        {
            throw new NotImplementedException();
        }

        public override SuccessResult<AbstractCheckout> UpsertCheckOut(AbstractCheckout abstractBlog)
        {
            try
            {
                SuccessResult<AbstractCheckout> result = null;
                var param = new DynamicParameters();

                param.Add("@Id", abstractBlog.ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@CustomerId", abstractBlog.CustomerID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Note", abstractBlog.Note, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@ShippingAddress", abstractBlog.ShippingAddress, dbType: DbType.Int16, direction: ParameterDirection.Input);
                param.Add("@BillingAddress", abstractBlog.BillingAddress, dbType: DbType.Int16, direction: ParameterDirection.Input);
                param.Add("@PromoCode", abstractBlog.PromoCode, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@ShippingMethod", abstractBlog.ShippingMethod, dbType: DbType.Int16, direction: ParameterDirection.Input);
                param.Add("@PaymentMethod", abstractBlog.PaymentMethod, dbType: DbType.Int16, direction: ParameterDirection.Input);
                param.Add("@ShipAmount", abstractBlog.ShipAmount, dbType: DbType.Double, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Checkout_Upsert", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractCheckout>>().SingleOrDefault();
                    result.Item = task.Read<Checkout>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
