﻿using Dapper;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class TradersDao : AbstractTradersDao
    {
        public override SuccessResult<AbstractTraders> Upsert(AbstractTraders abstractTraders)
        {
            try
            {
                SuccessResult<AbstractTraders> result = null;
                var param = new DynamicParameters();

                param.Add("@ID", abstractTraders.ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@FirstName", abstractTraders.FirstName, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@LastName", abstractTraders.LastName, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@JoinedDate", DateTime.Now, dbType: DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("@EmailID", abstractTraders.EmailID, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@Password", abstractTraders.Password, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@MobileNo", abstractTraders.MobileNo, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@AlternateMobileNo", abstractTraders.AlternateMobileNo, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@Address", abstractTraders.Address, DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@LastLogin", DateTime.Now, dbType: DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("@AvtarURL", abstractTraders.AvtarURL, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@Status", abstractTraders.Status, DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("@DelFlg", abstractTraders.DelFlg, DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("@CreatedAt", abstractTraders.CreatedAt, DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("@CreatedBy", abstractTraders.CreatedBy, DbType.String, direction: ParameterDirection.Input);
                param.Add("@UpdatedAt", abstractTraders.UpdatedAt, DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", abstractTraders.UpdatedBy, DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("TradersUpsert", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractTraders>>().SingleOrDefault();
                    result.Item = task.Read<Traders>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractTraders> SelectAll(PageParam pageParam, string search = "")
        {
            PagedList<AbstractTraders> Outcome = new PagedList<AbstractTraders>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int64, direction: ParameterDirection.Input);
            param.Add("@search", search, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Traders_All", param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<Traders>());
                Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Outcome;
        }

        public override PagedList<AbstractTraders> TraderReportSelectAll(DateTime startDate, DateTime endDate)
        {
            PagedList<AbstractTraders> Outcome = new PagedList<AbstractTraders>();

            var param = new DynamicParameters();
            param.Add("@StartDate", startDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);
            param.Add("@EndDate", endDate, dbType: DbType.DateTime, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("TradersReport_All", param, commandType: CommandType.StoredProcedure);
                Outcome.Values.AddRange(task.Read<Traders>());
                Outcome.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return Outcome;
        }

        public override SuccessResult<AbstractTraders> SelectByID(long id)
        {
            try
            {
                SuccessResult<AbstractTraders> result = null;
                var param = new DynamicParameters();

                param.Add("@ID", id, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Traders_ById", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractTraders>>().SingleOrDefault();
                    result.Item = task.Read<Traders>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public override bool UpdateStatus(long ID, bool Status)
        {
            try
            {
                bool isDelete = false;
                var param = new DynamicParameters();
                param.Add("@ID", ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Status", Status, dbType: DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", string.Empty, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.Query<bool>("TraderUpdateStatus", param, commandType: CommandType.StoredProcedure);
                    isDelete = task.SingleOrDefault<bool>();
                }

                return isDelete;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override bool DeleteByID(long id)
        {
            try
            {
                bool isDelete = false;
                var param = new DynamicParameters();
                param.Add("@ID", id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", string.Empty, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.Query<bool>("TradersDelete", param, commandType: CommandType.StoredProcedure);
                    isDelete = task.SingleOrDefault<bool>();
                }

                return isDelete;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractTraderMetaData> GetMetaData()
        {
            try
            {
                SuccessResult<AbstractTraderMetaData> Outcome = null;
                var param = new DynamicParameters();

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("Trader_MetaData", param, commandType: CommandType.StoredProcedure);
                    Outcome = task.Read<SuccessResult<AbstractTraderMetaData>>().SingleOrDefault();
                    Outcome.Item = task.Read<TraderMetaData>().SingleOrDefault();
                }

                return Outcome;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractTraders> Login(string Email, string WebPassword)
        {
            SuccessResult<AbstractTraders> result = null;
            var param = new DynamicParameters();
            param.Add("@Email", Email, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", WebPassword, DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple("Traders_Login", param, commandType: CommandType.StoredProcedure);
                result = task.Read<SuccessResult<AbstractTraders>>().SingleOrDefault();
                result.Item = task.Read<Traders>().SingleOrDefault();
            }

            return result;
        }
    }
}
