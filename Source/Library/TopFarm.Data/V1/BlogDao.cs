﻿using Dapper;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class BlogDao : AbstractBlogDao
    {
        public override bool DeleteByID(long id)
        {
            try
            {
                bool isDelete = false;
                var param = new DynamicParameters();
                param.Add("@ID", id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", string.Empty, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.Query<bool>("Blog_Delete", param, commandType: CommandType.StoredProcedure);
                    isDelete = task.SingleOrDefault<bool>();
                }

                return isDelete;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractBlog> SelectAll(PageParam pageParam, string search = "")
        {
            try
            {
                PagedList<AbstractBlog> classes = new PagedList<AbstractBlog>();

                var param = new DynamicParameters();
                param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("BlogSelectAll", param, commandType: CommandType.StoredProcedure);
                    classes.Values.AddRange(task.Read<Blog>());
                    classes.TotalRecords = task.Read<long>().SingleOrDefault();
                }
                return classes;
            }
            catch (Exception)
            {
                throw;
            }
        }
        //public override SuccessResult<AbstractBlog> Blog_BlogCategoryId(long BlogCategoryId)
        //{
        //    SuccessResult<AbstractBlog> Blog = null;
        //    var param = new DynamicParameters();

        //    param.Add("@BlogCategoryId", BlogCategoryId, dbType: DbType.Int64, direction: ParameterDirection.Input);

        //    using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
        //    {
        //        var task = con.QueryMultiple("Blog_BlogCategoryId", param, commandType: CommandType.StoredProcedure);
        //        Blog = task.Read<SuccessResult<AbstractBlog>>().SingleOrDefault();
        //        Blog.Item = task.Read<Blog>().SingleOrDefault();
        //    }
        //    return Blog;
        //}
        public override List<AbstractBlog> Blog_BlogCategoryId(long BlogCategoryId)
        {
            Database db = null;
            DbCommand dbCommand = null;
            List<AbstractBlog> listBlog = new List<AbstractBlog>();
            try
            {
                db = new SqlDatabase(Configurations.ConnectionString);
                dbCommand = db.GetStoredProcCommand("Blog_BlogCategoryId");

                db.AddInParameter(dbCommand, "BlogCategoryId", DbType.Int64, BlogCategoryId);
                DataSet dataSet = db.ExecuteDataSet(dbCommand);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    Blog pr = new Blog
                    {
                        ID = Convert.ToInt64(dataSet.Tables[0].Rows[i].ItemArray[0]),
                        BlogCategoryID = Convert.ToInt64(dataSet.Tables[0].Rows[i].ItemArray[1]),
                        ArticleName = (dataSet.Tables[0].Rows[i].ItemArray[2]).ToString(),
                        Articles = (dataSet.Tables[0].Rows[i].ItemArray[3]).ToString(),
                    };
                    listBlog.Add(pr);
                }

                return listBlog;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (dbCommand != null)
                {
                    dbCommand.Dispose();
                    dbCommand = null;
                }
                if (db != null)
                    db = null;
            }
        }
        public override AbstractBlog SelectByID(long ID)
        {
            Database db = null;
            DbCommand dbCommand = null;
            try
            {
                db = new SqlDatabase(Configurations.ConnectionString);
                dbCommand = db.GetStoredProcCommand("BlogSelectByID");

                db.AddInParameter(dbCommand, "ID", DbType.Int64, ID);
                DataSet dataSet = db.ExecuteDataSet(dbCommand);

                Blog productCategory = null;
                if (dataSet.Tables.Count > 0)
                {
                    productCategory = new Blog()
                    {
                        ID = Convert.ToInt64(dataSet.Tables[0].Rows[0].ItemArray[0]),
                        BlogCategoryID = Convert.ToInt64(dataSet.Tables[0].Rows[0].ItemArray[1]),
                        ArticleName = dataSet.Tables[0].Rows[0].ItemArray[2].ToString(),
                        Articles = dataSet.Tables[0].Rows[0].ItemArray[3].ToString(),
                    };
                }



                return productCategory;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (dbCommand != null)
                {
                    dbCommand.Dispose();
                    dbCommand = null;
                }
                if (db != null)
                    db = null;
            }
        }

        public override bool UpdateStatus(long ID, bool Status)
        {
            try
            {
                bool isDelete = false;
                var param = new DynamicParameters();
                param.Add("@ID", ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Status", Status, dbType: DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", string.Empty, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.Query<bool>("BlogUpdateStatus", param, commandType: CommandType.StoredProcedure);
                    isDelete = task.SingleOrDefault<bool>();
                }

                return isDelete;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractBlog> Upsert(AbstractBlog abstractBlog)
        {
            try
            {
                SuccessResult<AbstractBlog> result = null;
                var param = new DynamicParameters();

                param.Add("@ID", abstractBlog.ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@BlogCategoryID", abstractBlog.BlogCategoryID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@ArticleName", abstractBlog.ArticleName, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@Articles", abstractBlog.Articles, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@DatePublished", abstractBlog.DatePublished, dbType: DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("@Status", abstractBlog.Status, dbType: DbType.Int16, direction: ParameterDirection.Input);
                param.Add("DelFlg", abstractBlog.DelFlg, DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("CreatedAt", abstractBlog.CreatedAt, DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("CreatedBy", abstractBlog.CreatedBy, DbType.String, direction: ParameterDirection.Input);
                param.Add("UpdatedAt", abstractBlog.UpdatedAt, DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("UpdatedBy", abstractBlog.UpdatedBy, DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("BlogUpsert", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractBlog>>().SingleOrDefault();
                    result.Item = task.Read<Blog>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
