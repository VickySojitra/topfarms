﻿using Dapper;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Data.V1
{
    public class AddressDao : AbstractAddressDao
    {
        public override AbstractAddress SelectByID(long ID)
        {
            Database db = null;
            DbCommand dbCommand = null;
            try
            {
                db = new SqlDatabase(Configurations.ConnectionString);
                dbCommand = db.GetStoredProcCommand("AddressSelectByID");

                db.AddInParameter(dbCommand, "ID", DbType.Int64, ID);
                DataSet dataSet = db.ExecuteDataSet(dbCommand);

                Address productCategory = null;
                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        productCategory = new Address()
                        {
                            ID = Convert.ToInt64(dataSet.Tables[0].Rows[0].ItemArray[0]),
                            Address1 = dataSet.Tables[0].Rows[0].ItemArray[1].ToString(),
                            Address2 = dataSet.Tables[0].Rows[0].ItemArray[2].ToString(),
                            Pincode = dataSet.Tables[0].Rows[0].ItemArray[3].ToString(),
                            Area = dataSet.Tables[0].Rows[0].ItemArray[4].ToString(),
                            City = dataSet.Tables[0].Rows[0].ItemArray[5].ToString(),
                            State = dataSet.Tables[0].Rows[0].ItemArray[6].ToString(),
                            CountryID = Convert.ToInt64(dataSet.Tables[0].Rows[0].ItemArray[7]),
                            CustomerId = Convert.ToInt64(dataSet.Tables[0].Rows[0].ItemArray[13]),
                            IsPrimaryAddress = Convert.ToBoolean(dataSet.Tables[0].Rows[0].ItemArray[14])
                        };
                    }
                }

                return productCategory;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (dbCommand != null)
                {
                    dbCommand.Dispose();
                    dbCommand = null;
                }
                if (db != null)
                    db = null;
            }
        }

        public override AbstractAddress AddressSelectByID(long ID)
        {
            Database db = null;
            DbCommand dbCommand = null;
            try
            {
                db = new SqlDatabase(Configurations.ConnectionString);
                dbCommand = db.GetStoredProcCommand("CustomerAddressSelectByID");

                db.AddInParameter(dbCommand, "ID", DbType.Int64, ID);
                DataSet dataSet = db.ExecuteDataSet(dbCommand);

                Address productCategory = null;
                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        productCategory = new Address()
                        {
                            ID = Convert.ToInt64(dataSet.Tables[0].Rows[0].ItemArray[0]),
                            Address1 = dataSet.Tables[0].Rows[0].ItemArray[1].ToString(),
                            Address2 = dataSet.Tables[0].Rows[0].ItemArray[2].ToString(),
                            Pincode = dataSet.Tables[0].Rows[0].ItemArray[3].ToString(),
                            Area = dataSet.Tables[0].Rows[0].ItemArray[4].ToString(),
                            City = dataSet.Tables[0].Rows[0].ItemArray[5].ToString(),
                            State = dataSet.Tables[0].Rows[0].ItemArray[6].ToString(),
                            CountryID = Convert.ToInt64(dataSet.Tables[0].Rows[0].ItemArray[7]),
                            CustomerId= Convert.ToInt64(dataSet.Tables[0].Rows[0].ItemArray[13])
                        };
                    }
                }

                return productCategory;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (dbCommand != null)
                {
                    dbCommand.Dispose();
                    dbCommand = null;
                }
                if (db != null)
                    db = null;
            }
        }

        public override SuccessResult<AbstractAddress> Upsert(AbstractAddress abstractPromotional)
        {
            try
            {
                SuccessResult<AbstractAddress> result = null;
                var param = new DynamicParameters();

                param.Add("@ID", abstractPromotional.ID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@CustomerId", abstractPromotional.CustomerId, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@Address1", abstractPromotional.Address1, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@Address2", abstractPromotional.Address2, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@Pincode", abstractPromotional.Pincode, DbType.String, direction: ParameterDirection.Input);
                param.Add("@Area", abstractPromotional.Area, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@City", abstractPromotional.City, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@State", abstractPromotional.State, DbType.String, direction: ParameterDirection.Input);
                param.Add("@CountryID", abstractPromotional.CountryID, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@DelFlg", abstractPromotional.DelFlg, DbType.Boolean, direction: ParameterDirection.Input);
                param.Add("@CreatedAt", abstractPromotional.CreatedAt, DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("@CreatedBy", abstractPromotional.CreatedBy, DbType.String, direction: ParameterDirection.Input);
                param.Add("@UpdatedAt", abstractPromotional.UpdatedAt, DbType.DateTime, direction: ParameterDirection.Input);
                param.Add("@UpdatedBy", abstractPromotional.UpdatedBy, DbType.String, direction: ParameterDirection.Input);
                param.Add("@IsPrimaryAddress", abstractPromotional.IsPrimaryAddress, DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("AddressUpsert", param, commandType: CommandType.StoredProcedure);
                    result = task.Read<SuccessResult<AbstractAddress>>().SingleOrDefault();
                    result.Item = task.Read<Address>().SingleOrDefault();
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractAddress> SelectAll(PageParam pageParam, string search = "")
        {
            try
            {
                var customerId = (int)ProjectSession.LoginUserID;
                PagedList<AbstractAddress> classes = new PagedList<AbstractAddress>();

                var param = new DynamicParameters();
                param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
                param.Add("@Search", search, dbType: DbType.String, direction: ParameterDirection.Input);
                param.Add("@CustomerId", customerId, dbType: DbType.String, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.QueryMultiple("CustomerAddress_all", param, commandType: CommandType.StoredProcedure);
                    classes.Values.AddRange(task.Read<Address>());
                    classes.TotalRecords = task.Read<long>().SingleOrDefault();
                }
                return classes;
            }
            catch
            {
                throw;
            }

        }
        public override bool Delete(int id)
        {
            try
            {
                bool isDelete = false;
                var param = new DynamicParameters();
                param.Add("@Id", id, dbType: DbType.Int64, direction: ParameterDirection.Input);
                param.Add("@DeletedBy", id, dbType: DbType.Int64, direction: ParameterDirection.Input);

                using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
                {
                    var task = con.Query<bool>("CustomerAddress_Delete", param, commandType: CommandType.StoredProcedure);
                    isDelete = task.SingleOrDefault<bool>();
                }

                return isDelete;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
