﻿using TopFarm.Entities.Contract;

namespace TopFarm.Entities.V1
{
    public class Admin : AbstractAdmin { }
    public class Traders : AbstractTraders { }
    public class Orders : AbstractOrders { }
    public class OrderMetaData : AbstractOrderMetaData { }
    public class OrderTimeline : AbstractOrderTimeline { }
    public class OrderDetails : AbstractOrderDetails { }
    public class OrderDetailsData : AbstractOrderData { }

    public class ShippingRate : AbstractShippingRate { }
    public class ProductCategory : AbstractProductCategory { }
    public class Notification : AbstractNotification { }
    public class Transaction : AbstractTransaction { }
    public class Customer : AbstractCustomer { }
    public class SupportTicket : AbstractSupportTicket { }
    public class SupportComment : AbstractSupportComment { }
    public class OrderComment : AbstractOrderComment { }
    public class BlogCategory : AbstractBlogCategory { }
    public class Blog : AbstractBlog { }
    public class DailyReport : AbstractDailyReport { }
    public class DailyVisit : AbstractDailyVisit { }

    public class CommonDD : AbstractCommonDD { }
    public class Product : AbstractProduct { }
    public class ProductMetaData : AbstractProductMetaData { }
    public class ProductImages : AbstractProductImages { }
    public class ProductReview : AbstractProductReview { }
    public class ProductReviewLike : AbstractProductReviewLike { }
    public class Promotional : AbstractPromotional { }
    public class PromotionTypes : AbstractPromotionType { }
    public class Country : AbstractCountry { }
    public class Address : AbstractAddress { }
    public class TraderMetaData : AbstractTraderMetaData { }
    public class Cart : AbstractCart { }
    public class countCart : AbstractCart { }

    public class Wishlist : AbstractWishlist { }
    public class CommonCount : AbstractCommonCount { }
    public class Checkout : AbstractCheckout {}
}
