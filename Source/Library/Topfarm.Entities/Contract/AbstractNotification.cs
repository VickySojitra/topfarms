﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TopFarm.Entities.Contract
{
    public abstract class AbstractNotification :AbstractBaseClass
    {
        public long ID { get; set; }

        public long UserID { get; set; }

        public long RoleID { get; set; }
        public long CustomerNotificationCount { get; set; }
        public long AdminNotificationCount { get; set; }

        public string Message { get; set; }

    }
}
