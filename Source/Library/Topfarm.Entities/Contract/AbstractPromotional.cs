﻿using System;
using System.IO;
using TopFarm.Common;

namespace TopFarm.Entities.Contract
{
    public class AbstractPromotional : AbstractBaseClass
    {
        public long ID { get; set; }

        public long Type { get; set; }

        public string Name { get; set; }

        public string PromoCode { get; set; }

        public long ProductID { get; set; }

        public string Discount { get; set; }

        public bool Status { get; set; }

        public string ImageURL { get; set; }
        public string AvtarFullURL => string.IsNullOrWhiteSpace(ImageURL) == false ? Path.Combine(Configurations.ClientURL, ImageURL) : "img/shop/account/avatar.jpg";


        public string ProductName { get; set; }

    }
}
