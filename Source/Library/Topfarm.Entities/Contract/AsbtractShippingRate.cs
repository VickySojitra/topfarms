﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TopFarm.Entities.Contract
{
    public abstract class AbstractShippingRate : AbstractBaseClass
    {
        public long ID { get; set; }

        public long CategoryID { get; set; }

        public string CategoryName{get;set;}

        public string FreeShipping { get; set; }

        public string FreeShippingAbove { get; set; }
    }
}
