﻿using System.IO;
using TopFarm.Common;

namespace TopFarm.Entities.Contract
{
    public abstract class AbstractProductReview : AbstractBaseClass
    {
        public long ID { get; set; }
        public long ProductID { get; set; }
        public string Review { get; set; }
        public int Rating { get; set; }
        public int TotalLike { get; set; }
        public int TotalDislike { get; set; }
        public string ReviewerName { get; set; }
        public string ReviewerImgURL { get; set; }
        /// <summary>
        /// Format : Comma sepatared by (RoleId_UserId)
        /// Example : 1_1,2_5,3_5
        /// </summary>
        public string LikedUsers { get; set; }
        /// <summary>
        /// Format : Comma sepatared by (RoleId_UserId)
        /// Example : 1_1,2_5,3_5
        /// </summary>
        public string DisLikedUsers { get; set; }

        public bool IsLikeByLoginUser => LikedUsers == null ? false : LikedUsers.Contains(ProjectSession.LoginRoleID + "_" + ProjectSession.LoginUserID);
        public bool IsDisLikeByLoginUser => DisLikedUsers == null ? false : DisLikedUsers.Contains(ProjectSession.LoginRoleID + "_" + ProjectSession.LoginUserID);
        public double HelpfulPercentage => (TotalLike + TotalDislike > 0) ? ((double)TotalLike / (TotalLike + TotalDislike)) * 100 : 0;
        public string ReviewerImgFullURL => ReviewerImgURL != null ? Path.Combine(Configurations.ClientURL, ReviewerImgURL) : "";
        public string ReviewDate => CreatedAt?.ToString("MMM dd,yyyy");
    }
}
