﻿namespace TopFarm.Entities.Contract
{
    public abstract class AbstractBlogCategory : AbstractBaseClass
    {
        public long ID { get; set; }

        public string Name { get; set; }
    }
}
