﻿using System;

namespace TopFarm.Entities.Contract
{
    public class AbstractOrderComment : AbstractBaseClass
    {
        public long ID { get; set; }
        public long OrderID { get; set; }
        public long RoleID { get; set; }
        public long UserID { get; set; }
        public string UserName { get; set; }
        public string UserAvtarURL { get; set; }

        public string Message { get; set; }
        public DateTime SubmitDate { get; set; }
        public string SubmitDateTimeStr => SubmitDate != null ? SubmitDate.ToString("dd/MM/yyyy hh:mm tt") : "-";
        public string SubmitDateStr => SubmitDate != null ? SubmitDate.ToString("dd/MM/yyyy") : "";
        public string SubmitTimeStr => SubmitDate != null ? SubmitDate.ToString("hh:mm tt") : "";
    }
}
