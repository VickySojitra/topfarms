﻿namespace TopFarm.Entities.Contract
{
    public abstract class AbstractProductReviewLike : AbstractBaseClass
    {
        public long Id { get; set; }
        public long ProdReviewId { get; set; }
        public bool Like { get; set; }
        public bool Dislike { get; set; }
    }
}
