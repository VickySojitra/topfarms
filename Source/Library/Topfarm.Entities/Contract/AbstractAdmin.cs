﻿using System;
using System.IO;
using TopFarm.Common;

namespace TopFarm.Entities.Contract
{
    public abstract class AbstractAdmin : AbstractBaseClass
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string AvtarURL { get; set; }
        public string AvtarFullURL => string.IsNullOrWhiteSpace(AvtarURL) == false ? Path.Combine(Configurations.ClientURL, AvtarURL) : "img/shop/account/avatar.jpg";
        public string Phone { get; set; }
        public DateTime LastLogin { get; set; }
        public string LastLoginDateStr => LastLogin != null ? LastLogin.ToString("dd/MM/yyyy hh:mm tt") : "-";
    }
}
