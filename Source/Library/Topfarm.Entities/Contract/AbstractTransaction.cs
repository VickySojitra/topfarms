﻿namespace TopFarm.Entities.Contract
{
    public abstract class AbstractTransaction : AbstractBaseClass
    {
        public long ID { get; set; }

        public double TotalDueAmount { get; set; }

        public long OrderID { get; set; }

        public long CustomerID { get; set; }

        public int status { get; set; }

    }
}
