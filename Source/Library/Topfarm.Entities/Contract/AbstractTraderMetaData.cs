﻿namespace TopFarm.Entities.Contract
{
    public abstract class AbstractTraderMetaData
    {
        public long TotalTrader { get; set; }
        public long ActiveTrader { get; set; }
        public long InActiveTrader { get; set; }
        public long TotalSales { get; set; }
    }
}
