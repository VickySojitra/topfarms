﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TopFarm.Entities.Contract
{
    public abstract class AbstractBaseClass
    {
        public bool DelFlg { get; set; } = false;
        public long CreatedBy { get; set; }
        public long CreatedRole { get; set; }
        public DateTime? CreatedAt { get; set; }
        public long UpdatedBy { get; set; }
        public long UpdatedRole { get; set; }
        public DateTime? UpdatedAt { get; set; }

        [NotMapped]
        public string CreatedDateStr => CreatedAt != null ? CreatedAt?.ToString("dd/MM/yyyy hh:mm tt") : "-";
        [NotMapped]
        public string UpdatedDateStr => UpdatedAt != null ? UpdatedAt?.ToString("dd/MM/yyyy hh:mm tt") : "-";
    }
}
