﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TopFarm.Entities.Contract
{
    public abstract class AbstractSupportTicket : AbstractBaseClass
    {
        public long ID { get; set; }

        public DateTime SubmittedDate { get; set; }

        public long CustomerID { get; set; }

        public string CustomerName {get;set;}

        public bool Status { get; set; }

        [NotMapped]
        public string SubmittedDateStr => SubmittedDate != null ? SubmittedDate.ToString("dd/MM/yyyy hh:mm tt") : "-";
    }

    public abstract class AbstractSupportComment : AbstractBaseClass
    {
        public long ID { get; set; }
        public long SupportTicketID { get; set; }
        public DateTime SubmitDate { get; set; }
        public int RoleID { get; set; }
        public long UserID { get; set; }
        public string Comment { get; set; }
        public string UserName { get; set; }
        public string UserAvtarURL { get; set; }
        [NotMapped]
        public string SubmitDateTimeStr => SubmitDate != null ? SubmitDate.ToString("dd/MM/yyyy hh:mm tt") : "-";
        public string SubmitDateStr => SubmitDate != null ? SubmitDate.ToString("dd/MM/yyyy") : "";
        public string SubmitTimeStr => SubmitDate != null ? SubmitDate.ToString("hh:mm tt") : "";
    }
}
