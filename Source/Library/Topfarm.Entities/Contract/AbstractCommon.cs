﻿namespace TopFarm.Entities.Contract
{
    public abstract class AbstractCommonDD : AbstractBaseClass
    {
        public long ID { get; set; }

        public string Name { get; set; }
    }
}
