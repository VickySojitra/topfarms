using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TopFarm.Entities.Contract
{
    public abstract class AbstractCommonCount: AbstractBaseClass
    {
        public int OrdersCount { get; set; }
        public int WishlistCount { get; set; }
        public int SupportTicketsCount { get; set; }
        public int NotificationCount { get; set; }
        public int trNotificationCount { get; set; }

        public int CartCount { get; set; }
        public int TotalSalesCount { get; set; }
        public int TotalTradersCount { get; set; }
        public string MemberSince { get; set; }
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                              