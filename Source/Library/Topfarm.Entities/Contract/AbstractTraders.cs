﻿using System;

namespace TopFarm.Entities.Contract
{
    public class AbstractTraders : AbstractBaseClass
    {
        #region Property
        public long ID { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime JoinedDate { get; set; }

        public string EmailID { get; set; }

        public string Password { get; set; }

        public string MobileNo { get; set; }

        public string AlternateMobileNo { get; set; }

        public long Address { get; set; }

        public DateTime LastLogin { get; set; }

        public string AvtarURL { get; set; }
        public bool Status { get; set; } = false;

        public long CountryID { get; set; }

        public string City { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Pincode { get; set; }
        public double OrderCompleted { get; set; }
        public DateTime LastOrder { get; set; }
        public string LastOrderDateStr => LastOrder != null ? LastOrder.ToString("dd/MM/yyyy hh:mm tt") : "-";
        public long TotalOrders { get; set; }
        public long TotalSales { get; set; }
        #endregion
    }
}
