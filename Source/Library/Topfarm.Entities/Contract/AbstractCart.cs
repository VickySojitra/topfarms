﻿using System.IO;
using TopFarm.Common;

namespace TopFarm.Entities.Contract
{
    public abstract class AbstractCart : AbstractBaseClass
    {
        public long ID { get; set; }
        public long ProductId { get; set; }
        public long CustomerId { get; set; }
        public int Quantity { get; set; }
        public string ProductName { get; set; }
        public string ProductImg { get; set; }
        public decimal ProductPrice { get; set; }
        public string ProdImgFullURL => string.IsNullOrEmpty(ProductImg) == false ? Path.Combine(Configurations.ClientURL, ProductImg) : Path.Combine(Configurations.ClientURL, "/img/No-prod-images.png");
        public decimal Total => ProductPrice * Quantity;
        public decimal TaxPercentage { get; set; }
        public decimal Tax => ((ProductPrice * TaxPercentage) / 100) * Quantity;
        public int CategoryId { get; set; }
        public decimal FreeShipping { get; set; }
        public decimal FreeShippingAbove { get; set; }
        public decimal Charge { get; set; }
        public string Name { get; set; }
        public decimal TotalShipping { get; set; }

    }

    public abstract class AbstractCheckout
    {
        public long ID { get; set; }
        public long CustomerID { get; set; }        
        public string Note { get; set; }
        public string PromoCode { get; set; }
        public int ShippingAddress { get; set; }
        public int BillingAddress { get; set; }
        public int ShippingMethod { get; set; }
        public int PaymentMethod { get; set; }
        public double ShipAmount { get; set; }
        public double TaxAmount { get; set; }

    }
}
