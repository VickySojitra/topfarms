﻿namespace TopFarm.Entities.Contract
{
    public abstract class AbstractProductImages : AbstractBaseClass
    {
        public long ID { get; set; }
        public long ProductId { get; set; }
        public string URL { get; set; }
        public bool IsActive { get; set; }
        public bool IsImage { get; set; }
    }
}
