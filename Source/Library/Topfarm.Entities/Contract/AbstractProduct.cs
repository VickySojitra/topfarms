﻿using System.Collections.Generic;
using System.IO;
using TopFarm.Common;

namespace TopFarm.Entities.Contract
{
    public abstract class AbstractProduct : AbstractBaseClass
    {
        public long ID { get; set; }
        public long CategoryID { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsFeatured { get; set; }
        public decimal StandardPrice { get; set;}
        public decimal OfferPrice { get; set; }
        public string Description { get; set; }
        public string CategoryName { get; set; }
        public long Sales { get; set; }
        public string ImageURL { get; set; }
        public string ImageFullURL => string.IsNullOrEmpty(ImageURL) == false ? Path.Combine(Configurations.ClientURL, ImageURL) : Path.Combine(Configurations.ClientURL, "/img/No-prod-images.png");
        public List<AbstractProductImages> ProductImages { get; set; }
        public int TotalReview { get; set; }
        public int FiveStarReview { get; set; }
        public int FourStarReview { get; set; }
        public int ThreeStarReview { get; set; }
        public int TwoStarReview { get; set; }
        public int OneStarReview { get; set; }
        public decimal Rating { get; set; }
        public bool IsWishlisted { get; set; }
        public bool IsInCart { get; set; }
        public double Tax { get; set; }

        // PF
        public long PF { get; set; }
        public long Milk_PF { get; set; }
        public long Protein_PF { get; set; }
        public long Fat_PF { get; set; }
        public long Longevity_PF { get; set; }
        public long Fertility_PF { get; set; }
        public long Habit_PF { get; set; }


        //TPI
        public long TPI { get; set; }
        public long NetMerit_TPI { get; set; }
        public long Milk_TPI { get; set; }
        public long Protein_TPI { get; set; }
        public long Fat_TPI { get; set; }
        public long Longevity_TPI { get; set; }
        public long CalvingEase_TPI { get; set; }
        public long Habit_TPI { get; set; }

    }
    public abstract class AbstractProductMetaData : AbstractBaseClass
    {
        public long TotalProducts { get; set; }
        public long ActiveProducts { get; set; }
        public long InActiveProducts { get; set; }
        public decimal MaxProductPrice { get; set; }
    }
}
