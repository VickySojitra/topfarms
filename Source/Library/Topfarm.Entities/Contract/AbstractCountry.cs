﻿namespace TopFarm.Entities.Contract
{
    public abstract class AbstractCountry : AbstractBaseClass
    {
        public long ID { get; set; }

        public string Name { get; set; }
    }
}
