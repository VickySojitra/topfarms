﻿using System;
using System.Collections.Generic;

namespace TopFarm.Entities.Contract
{
    public class AbstractOrders : AbstractBaseClass
    {
        public long strOrderId { get; set; }
        public long OrderId { get; set; }
        public long ID { get; set; }
        public long CustomerID { get; set; }
        public DateTime OrderDate { get; set; }
        public byte PaymentTypes { get; set; }
        public byte Status { get; set; }
        public int intStatus { get; set; }
        public string strStatus { get; set; }

        public long AssignTo { get; set; }
        public DateTime AssignDate { get; set; }
        public int ShipmentAddress { get; set; }
        public int ShipMethod { get; set; }
        public string strShipMethod { get; set; }

        public int PromoCodeId { get; set; }

        public string ShipmentAddressStr { get; set; }
        public string CustomerName { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public string AssignToName { get; set; }
        public int NoOfComment { get; set; }
        public string OrderDateTimeStr => OrderDate != null ? OrderDate.ToString("dd/MM/yyyy hh:mm tt") : "-";
        public string OrderDateStr => OrderDate != null ? OrderDate.ToString("dd/MM/yyyy") : "";
        public string OrderTimeStr => OrderDate != null ? OrderDate.ToString("hh:mm tt") : "";
        public string AssignDateTimeStr => AssignDate != null ? AssignDate.ToString("dd/MM/yyyy hh:mm tt") : "-";
        public string AssignDateStr => AssignDate != null ? AssignDate.ToString("dd/MM/yyyy") : "";
        public string AssignTimeStr => AssignDate != null ? AssignDate.ToString("hh:mm tt") : "";
        public decimal FinalAmount => TaxAmount + TotalAmount;

        public List<AbstractOrderDetails> OrderDetails { get; set; } = new List<AbstractOrderDetails>();
        public List<AbstractOrderTimeline> OrderTimeline { get; set; } = new List<AbstractOrderTimeline>();
    }

    public class AbstractOrderDetails : AbstractBaseClass
    {
        public long Id { get; set; }
        public long OrderId { get; set; }
        public long ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal Rate { get; set; }

        public decimal Amount { get; set; }
        public string ProductName { get; set; }
        public string URL { get; set; }
    }
    public class AbstractOrderData : AbstractBaseClass
    {
        public long ID { get; set; }
        public string FirstName { get; set; }
        public string OrderDate { get; set; }
        public int Status { get; set; }
        public int PaymentTypes { get; set; }
        public int OrderId { get; set; }
        public string ProductName { get; set; }
        public string Address1 { get; set; }
        public string Email { get; set; }

        public int Quantity { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }
        public decimal TotalAmount { get; set; }

        public List<AbstractOrderDetails> listProduct { get; set; }
    }

    public class AbstractOrderTimeline : AbstractBaseClass
    {
        public long Id { get; set; }
        public long OrderId { get; set; }
        public int Status { get; set; }
        public string Note { get; set; }
        public DateTime SubmitDateTime { get; set; }
        public string SubmitDateTimeStr => SubmitDateTime != null ? SubmitDateTime.ToString("dd/MM/yyyy hh:mm tt") : "-";
        public string SubmitDateStr => SubmitDateTime != null ? SubmitDateTime.ToString("dd/MM/yyyy") : "";
        public string SubmitTimeStr => SubmitDateTime != null ? SubmitDateTime.ToString("hh:mm tt") : "";
    }

    public class AbstractOrderMetaData : AbstractBaseClass
    {
        public int TotalUnassignedOrder { get; set; }
        public int TotalInProgressOrder { get; set; }
        public int TotalDeliveredOrderPast7Day { get; set; }
    }
}
