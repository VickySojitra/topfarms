﻿using System;

namespace TopFarm.Entities.Contract
{
    public abstract class AbstractCustomer : AbstractBaseClass
    {
        public long ID { get; set; }
        public long CustomerId { get; set; }
        public long AddressId { get; set; }


        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string EmailID { get; set; }

        public string FileURL { get; set; }
        public string Password { get; set; }
        public string MobileNo { get; set; }
        public string AlternateMobileNo { get; set; }
        public string NIP { get; set; }


        public string token { get; set; }

        public string confirmPassword { get; set; }
        public string confirmchangePassword { get; set; }

        public double DueAmount { get; set; }
        public int Status { get; set; }
        public int Address { get; set; }
        public string AddressString { get; set; }
        public string AvtarURL { get; set; }
        public string Phone { get; set; }
        public DateTime LastLogin { get; set; }

        public DateTime JoinedDate { get; set; }
        public DateTime LastOrder { get; set; }

        public string LastOrderDateStr => LastOrder != null ? LastOrder.ToString("dd/MM/yyyy hh:mm tt") : "-";
        public long TotalOrders { get; set; }
        public long TotalSales { get; set; }
        public long TotalDueAmount { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Pincode { get; set; }
        public string Area { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public long CountryID { get; set; }
        public string showImage { get; set; }
        public bool IsPrimaryAddress { get; set; }
        public string strIsPrimaryAddress { get; set; }

    }
}
