﻿using System;
using System.Collections.Generic;

namespace TopFarm.Entities.Contract
{
    public abstract class AbstractDailyReport : AbstractBaseClass
    {
        public long ID { get; set; }
        public long TraderId { get; set; }
        public string TraderName { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
        public bool IsSubmitted { get; set; }
        public string DateTimeStr => Date != null ? Date.ToString("hh:mm tt") : "-";
        public string DateStr => Date != null ? Date.ToString("dd-MM-yyyy") : "-";
        public List<AbstractDailyVisit> DailyVisits { get; set; }
        public AbstractDailyVisit Visit { get; set; }
    }

    public abstract class AbstractDailyVisit
    {
        public long ID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerLocation { get; set; }
        public int NoOfCows { get; set; }
        public int VisitPurposeID { get; set; }
        public string VisitPurposeName { get; set; }
        public bool NewOrder { get; set; }
        public string Summary { get; set; }
    }
}
