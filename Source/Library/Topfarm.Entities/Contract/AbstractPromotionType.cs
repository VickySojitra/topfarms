﻿namespace TopFarm.Entities.Contract
{
    public abstract class AbstractPromotionType : AbstractBaseClass
    {
        public long ID { get; set; }

        public string Name { get; set; }
    }
}
