﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TopFarm.Entities.Contract
{
    public abstract class AbstractBlog : AbstractBaseClass
    {
        public long ID { get; set; }

        public long BlogCategoryID { get; set; }

        public string BlogCategoryName { get; set; }

        public string ArticleName { get; set; }

        public string Articles { get; set; }

        public DateTime? DatePublished { get; set; } = null;

        public bool Status { get; set; } = true;
        [NotMapped]
        public string DatePublishedStr => DatePublished != null ? DatePublished?.ToString("dd/MM/yyyy hh:mm tt") : "-";
    }
}
