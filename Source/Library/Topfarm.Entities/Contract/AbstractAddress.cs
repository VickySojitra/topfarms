﻿namespace TopFarm.Entities.Contract
{
    public abstract class AbstractAddress :AbstractBaseClass
    {
        public long ID { get; set; }
        public long CustomerId { get; set; }
        public string Address1 { get; set; }
        public string Phone { get; set; }
        public string Address2 { get; set; }
        public string Pincode { get; set; }
        public string Area { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public long CountryID { get; set; }
        public bool IsPrimaryAddress { get; set; }
        public bool IsAllowPrimary { get; set; }
        public string AddressString { get; set; }
    }
}
