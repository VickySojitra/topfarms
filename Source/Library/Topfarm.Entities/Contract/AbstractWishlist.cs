﻿using System.IO;
using TopFarm.Common;

namespace TopFarm.Entities.Contract
{
    public abstract class AbstractWishlist : AbstractBaseClass
    {
        public long ID { get; set; }
        public long ProductId { get; set; }
        public long CustomerId { get; set; }
        public string ProductName { get; set; }
        public string ProductImg { get; set; }
        public decimal ProductPrice { get; set; }
        public string ProdImgFullURL => string.IsNullOrEmpty(ProductImg) == false ? Path.Combine(Configurations.ClientURL, ProductImg) : Path.Combine(Configurations.ClientURL, "/img/No-prod-images.png");
    }
}
