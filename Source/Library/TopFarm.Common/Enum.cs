﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TopFarm.Common
{
    public enum Gender
	{
        Male,
        Female
	}

    public enum RatingEnum
    {        
        A = 1,
        
        B = 2,

        C = 3,
        
        D = 4,

        E = 5

    }
    public enum RoleEnum
    {
        SuperAdmin = 1,

        Admin = 2,

    }
    public enum SupportStatusEnum
    {
        Open = 1,

        Inprocess = 2,

        Resolved = 3
    }
    public enum CreditDebitEnum
    {
       Credit = 1,

       Debit = 2
    }

    public enum PaymentMode
    {
        CashOnDelivery = 1,

        Online = 2
    }

    public enum NotificationType
    {
        Offer = 1,

        Product = 2,

        Category = 3,

        Order = 4
    }
    public enum BankDetailStatus
    {
        Success = 1,

        InProcess = 2,

        Pending = 3

    }
    public enum Month
    {
        January = 1,

        February = 2,

        March = 3,

        April = 4,

        May = 5,

        June = 6,

        July = 7,

        August = 8,

        September = 9,

        October = 10,

        November = 11,

        December = 12,
    }

    public enum OrderStatus
    {
        UnAssigned = 1,
        Assigned = 2,
        Fulfilled = 3,
        Delivered = 4,
        Delay = 5,
        Cancelled = 6,
        OutForDelivery = 7,
        InProcess = 8
    }

    public enum PaymentStatus
    {
        COD = 1,
        Paid = 2
    }

    public enum CustomerStatus
    {
        Creditor = 1,
        Debtors = 2
    }

    public enum MessageType
    {
        /// <summary>
        /// for Success message Class
        /// </summary>
        success,

        /// <summary>
        /// for error message Class
        /// </summary>
        danger,

        /// <summary>
        /// for Warning message Class
        /// </summary>
        warning,

        /// <summary>
        /// for info message Class
        /// </summary>
        info
    }

    public enum ProductSortTypes
    {
        Popularity,
        LowToHighPrice,
        HighToLowPrice,
        AverageRating,
        AtoZ,
        ZtoA,
        New
    }

    public enum ShippingMethod
    {
        Courier = 1,
        LocalShipping = 2,
        SelfPickup = 3,
        PolishPost = 4
    }
}
