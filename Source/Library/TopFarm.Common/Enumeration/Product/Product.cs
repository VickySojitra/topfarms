﻿namespace TopFarm.Common.Enumeration.Product
{
    public enum ProductCategoryStatus : int
    {
        Active=1,
        NonActive=2,
    }
}
