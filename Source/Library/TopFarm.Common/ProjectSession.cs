﻿using System.Web;
using System;

namespace TopFarm.Common
{
    public class ProjectSession
    {
        /// <summary>
        /// Gets or sets the client connection string.
        /// </summary>
        /// <value>The client connection string.</value>
        public static string ClientConnectionString
        {
            get
            {
                if (HttpContext.Current.Session["ConnectionString"] == null)
                {
                    return Configurations.ConnectionString;
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["ConnectionString"]);
                }
            }

            set
            {
                HttpContext.Current.Session["ConnectionString"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the admin user identifier.
        /// </summary>
        /// <value>The admin user identifier.</value>
        public static long LoginUserID
        {
            get
            {
                if (HttpContext.Current.Session["LoginUserID"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["LoginUserID"]);
                }
            }

            set
            {
                HttpContext.Current.Session["LoginUserID"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the admin user.
        /// </summary>
        /// <value>The name of the admin user.</value>
        public static string LoginUserName
        {
            get
            {
                if (HttpContext.Current.Session["LoginUserName"] == null)
                {
                    return "Admin";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["LoginUserName"]);
                }
            }

            set
            {
                HttpContext.Current.Session["LoginUserName"] = value;
            }
        }

        public static double TotalTax
        {
            get
            {
                if (HttpContext.Current.Session["TotalTax"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Double(HttpContext.Current.Session["TotalTax"]);
                }
            }

            set
            {
                HttpContext.Current.Session["TotalTax"] = value;
            }
        }
        public static double ShipAmount
        {
            get
            {
                if (HttpContext.Current.Session["ShipAmount"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Double(HttpContext.Current.Session["ShipAmount"]);
                }
            }

            set
            {
                HttpContext.Current.Session["ShipAmount"] = value;
            }
        }

        public static double PaymentMethod
        {
            get
            {
                if (HttpContext.Current.Session["PaymentMethod"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Double(HttpContext.Current.Session["PaymentMethod"]);
                }
            }

            set
            {
                HttpContext.Current.Session["PaymentMethod"] = value;
            }
        }

        public static string Mobile
        {
            get
            {
                if (HttpContext.Current.Session["Mobile"] == null)
                {
                    return "Mobile";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["Mobile"]);
                }
            }

            set
            {
                HttpContext.Current.Session["Mobile"] = value;
            }
        }

        public static string Email
        {
            get
            {
                if (HttpContext.Current.Session["Email"] == null)
                {
                    return "";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["Email"]);
                }
            }

            set
            {
                HttpContext.Current.Session["Email"] = value;
            }
        }

        public static string Menu
        {
            get
            {
                if (HttpContext.Current.Session["Menu"] == null)
                {
                    return "Menu";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["Menu"]);
                }
            }

            set
            {
                HttpContext.Current.Session["Menu"] = value;
            }
        }

        public static int SubscriptionId
        {
            get
            {
                if (HttpContext.Current.Session["SubscriptionId"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["SubscriptionId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["SubscriptionId"] = value;
            }
        }

        public static int InstituteId
        {
            get
            {
                if (HttpContext.Current.Session["InstituteId"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["InstituteId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["InstituteId"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the access token of the facebook user.
        /// </summary>
        /// <value>The value of the facebook user access token.</value>
        public static string AccessToken
        {
            get
            {
                if (HttpContext.Current.Session["AccessToken"] == null)
                {
                    return "AccessToken";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["AccessToken"]);
                }
            }

            set
            {
                HttpContext.Current.Session["AccessToken"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the plan.
        /// </summary>
        /// <value>
        /// The plan.
        /// </value>        


        /// <summary>
        /// Gets or sets the facebook business ads Id.
        /// </summary>
        /// <value>The value of the facebook business ads Id.</value>
        public static string BusinessAdAccountID
        {
            get
            {
                if (HttpContext.Current.Session["BusinessAdAccountID"] == null)
                {
                    return "BusinessAdAccountID";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["BusinessAdAccountID"]);
                }
            }

            set
            {
                HttpContext.Current.Session["BusinessAdAccountID"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the facebook business campaign Id.
        /// </summary>
        /// <value>The value of the facebook business campaign Id.</value>
        public static string BusinessCampaignId
        {
            get
            {
                if (HttpContext.Current.Session["BusinessCampaignId"] == null)
                {
                    return "BusinessCampaignId";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["BusinessCampaignId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["BusinessCampaignId"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the facebook business adset Id.
        /// </summary>
        /// <value>The value of the facebook business adset Id.</value>
        public static string BusinessAdsetId
        {
            get
            {
                if (HttpContext.Current.Session["BusinessAdsetId"] == null)
                {
                    return "BusinessAdsetId";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["BusinessAdsetId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["BusinessAdsetId"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the facebook business ads Id.
        /// </summary>
        /// <value>The value of the facebook business ads Id.</value>
        public static string BusinessAdsID
        {
            get
            {
                if (HttpContext.Current.Session["BusinessAdsID"] == null)
                {
                    return "BusinessAdsID";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["BusinessAdsID"]);
                }
            }

            set
            {
                HttpContext.Current.Session["BusinessAdsID"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public static int PageSize
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session["PageSize"] != null)
                {
                    if (ConvertTo.Integer(HttpContext.Current.Session["PageSize"]) == 0)
                    {
                        HttpContext.Current.Session["PageSize"] = Configurations.PageSize;
                    }

                    return ConvertTo.Integer(HttpContext.Current.Session["PageSize"]);

                }
                else
                {
                    return 10;
                }
            }

            set
            {
                HttpContext.Current.Session["PageSize"] = value;
            }
        }
        public static int TestID
        {
            get
            {
                if (HttpContext.Current.Session["TestID"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["TestID"]);
                }
            }

            set
            {
                HttpContext.Current.Session["TestID"] = value;
            }
        }

        public static DateTime StartTime
        {
            get
            {
                if (HttpContext.Current.Session["StartTime"] == null)
                {
                    return DateTime.Now;
                }
                else
                {
                    return (DateTime)(HttpContext.Current.Session["StartTime"]);
                }
            }

            set
            {
                HttpContext.Current.Session["StartTime"] = value;
            }
        }

        public static int Result
        {
            get
            {
                if (HttpContext.Current.Session["Result"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["Result"]);
                }
            }

            set
            {
                HttpContext.Current.Session["Result"] = value;
            }
        }

        public static bool IsLogin
        {
            get
            {
                if (HttpContext.Current.Session["IsLogin"] == null)
                {
                    return false;
                }
                else
                {
                    return ConvertTo.Boolean(HttpContext.Current.Session["IsLogin"]);
                }
            }

            set
            {
                HttpContext.Current.Session["IsLogin"] = value;
            }
        }

        public static bool IsAdmin
        {
            get
            {
                if (HttpContext.Current.Session["IsAdmin"] == null)
                {
                    return false;
                }
                else
                {
                    return ConvertTo.Boolean(HttpContext.Current.Session["IsAdmin"]);
                }
            }

            set
            {
                HttpContext.Current.Session["IsAdmin"] = value;
            }
        }

        public static string Otp
        {
            get
            {
                if (HttpContext.Current.Session["Otp"] == null)
                {
                    return "Otp";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["Otp"]);
                }
            }

            set
            {
                HttpContext.Current.Session["Otp"] = value;
            }
        }


        public static string OtpType
        {
            get
            {
                if (HttpContext.Current.Session["OtpType"] == null)
                {
                    return "OtpType";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["OtpType"]);
                }
            }

            set
            {
                HttpContext.Current.Session["OtpType"] = value;
            }
        }

        public static string Password
        {
            get
            {
                if (HttpContext.Current.Session["Password"] == null)
                {
                    return "Password";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["Password"]);
                }
            }

            set
            {
                HttpContext.Current.Session["Password"] = value;
            }
        }

        public static string IsVerified
        {
            get
            {
                if (HttpContext.Current.Session["IsVerified"] == null)
                {
                    return "Not Verified";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["IsVerified"]);
                }
            }

            set
            {
                HttpContext.Current.Session["IsVerified"] = value;
            }
        }

        public static string IsExists
        {
            get
            {
                if (HttpContext.Current.Session["IsExists"] == null)
                {
                    return "No";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["IsExists"]);
                }
            }

            set
            {
                HttpContext.Current.Session["IsExists"] = value;
            }
        }
        /// <summary>
        /// Gets or sets the admin user identifier.
        /// </summary>
        /// <value>The admin user identifier.</value>
        public static long LoginRoleID
        {
            get
            {
                if (HttpContext.Current.Session["LoginRoleID"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["LoginRoleID"]);
                }
            }

            set
            {
                HttpContext.Current.Session["LoginRoleID"] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static int TempId
        {
            get
            {
                if (HttpContext.Current.Session["TempId"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["TempId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["TempId"] = value;
            }
        }
        public static string ReferenceCode
        {
            get
            {
                if (HttpContext.Current.Session["ReferenceCode"] == null)
                {
                    return "";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["ReferenceCode"]);
                }
            }

            set
            {
                HttpContext.Current.Session["ReferenceCode"] = value;
            }
        }

        public static long OrderId
        {
            get
            {
                if (HttpContext.Current.Session["OrderId"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Long(HttpContext.Current.Session["OrderId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["OrderId"] = value;
            }
        }

        public static string strOrderId
        {
            get
            {
                if (HttpContext.Current.Session["strOrderId"] == null)
                {
                    return "";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["strOrderId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["strOrderId"] = value;
            }
        }
        public static int TempCouponId
        {
            get
            {
                if (HttpContext.Current.Session["TempCouponId"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["TempCouponId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["TempCouponId"] = value;
            }
        }
        public static bool IsSpin
        {
            get
            {
                if (HttpContext.Current.Session["IsSpin"] == null)
                {
                    return false;
                }
                else
                {
                    return ConvertTo.Boolean(HttpContext.Current.Session["IsSpin"]);
                }
            }

            set
            {
                HttpContext.Current.Session["IsSpin"] = value;
            }
        }
    }
}