﻿//-----------------------------------------------------------------------
// <copyright file="Utility.cs" company="">
//     Copyright . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TopFarm.Common
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Net.Http;
    using System.Reflection;    
    using System.Net;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using System.Configuration;

    /// <summary>
    /// Sealed Class Utility
    /// </summary>
    public static class Utility
    {
        #region Public Methods

        private static readonly string Key = "TOP#FARMS@2021";

        /// <summary>
        /// Creates the data table.
        /// </summary>
        /// <typeparam name="T">The class</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="ECommercequeCon">The ECommerceque con.</param>
        /// <param name="startWith">The start with.</param>
        /// <returns>
        /// The data table
        /// </returns>
        public static DataTable CreateDataTable<T>(IEnumerable<T> list, List<string> ECommercequeCon = null, string startWith = "")
        {
            try
            {
                Type type = typeof(T);
                PropertyInfo[] properties = type.GetProperties().Where(p => p.CanRead).ToArray();

                DataTable dataTable = new DataTable();
                foreach (PropertyInfo info in properties)
                {
                    dataTable.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
                }

                if (ECommercequeCon != null)
                {
                    DataColumn[] arrayDC = new DataColumn[ECommercequeCon.Count];
                    for (int i = 0; i < ECommercequeCon.Count; i++)
                    {
                        arrayDC[i] = dataTable.Columns[ECommercequeCon[i]];
                    }

                   UniqueConstraint custECommerceque = new UniqueConstraint(arrayDC);
                    dataTable.Constraints.Add(custECommerceque);
                }

                foreach (T entity in list)
                {
                    object[] values = new object[properties.Count()];

                    for (int i = 0; i < properties.Count(); i++)
                    {
                        values[i] = properties[i].GetValue(entity);
                    }

                    try
                    {
                        dataTable.Rows.Add(values);
                    }
                    catch (Exception)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            if (Convert.ToString(values[i]).StartsWith(startWith))
                            {
                                //values[i] = GenerateHashIds(startWith);
                                break;
                            }
                        }

                        dataTable.Rows.Add(values);
                    }
                }

                return dataTable;
            }
            catch (Exception)
            {
                // todo Error Logging
                throw;
            }
        }

        public static string HtmlDecode(string htmlString)
        {
            return WebUtility.HtmlDecode(htmlString);
        }

        /// <summary>
        /// Gets the base path.
        /// </summary>
        /// <returns>the base path.</returns>
        public static string GetBasePath()
        {
            string root = string.Empty;

            if (string.IsNullOrEmpty(AppDomain.CurrentDomain.RelativeSearchPath))
            {
                root = AppDomain.CurrentDomain.BaseDirectory; //// exe folder for WinForms, Consoles, Windows Services
            }
            else
            {
                root = AppDomain.CurrentDomain.RelativeSearchPath + "\\"; //// bin folder for Web Apps
            }

            return root;
        }

        /// <summary>
        /// Returns an individual HTTP Header value
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="key">The key.</param>
        /// <returns>returns header value</returns>
        public static string GetHeader(HttpRequestMessage request, string key)
        {
            IEnumerable<string> keys = null;
            if (!request.Headers.TryGetValues(key, out keys))
            {
                return null;
            }

            return keys.First();
        }

        /// <summary>
        /// Gets the property value.
        /// </summary>
        /// <param name="entity">The source.</param>
        /// <param name="propName">Name of the property.</param>
        /// <returns>Returns the object.</returns>
        public static object GetPropValue(object entity, string propName)
        {
            return entity.GetType().GetProperty(propName).GetValue(entity, null);
        }

        public static string Encrypt(string data)
        {
            string encData = null;
            byte[][] keys = GetHashKeys(Key);

            try
            {
                encData = EncryptStringToBytes_Aes(data, keys[0], keys[1]);
            }
            catch (CryptographicException) { }
            catch (ArgumentNullException) { }

            return encData;
        }

        public static string Decrypt(string data)
        {
            string decData = null;
            byte[][] keys = GetHashKeys(Key);

            try
            {
                decData = DecryptStringFromBytes_Aes(data, keys[0], keys[1]);
            }
            catch (CryptographicException) { }
            catch (ArgumentNullException) { }

            return decData;
        }

        private static byte[][] GetHashKeys(string key)
        {
            byte[][] result = new byte[2][];
            Encoding enc = Encoding.UTF8;

            SHA256 sha2 = new SHA256CryptoServiceProvider();

            byte[] rawKey = enc.GetBytes(key);
            byte[] rawIV = enc.GetBytes(key);

            byte[] hashKey = sha2.ComputeHash(rawKey);
            byte[] hashIV = sha2.ComputeHash(rawIV);

            Array.Resize(ref hashIV, 16);

            result[0] = hashKey;
            result[1] = hashIV;

            return result;
        }

        //source: https://msdn.microsoft.com/de-de/library/system.security.cryptography.aes(v=vs.110).aspx
        private static string EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            byte[] encrypted;

            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt =
                            new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(encrypted);
        }

        //source: https://msdn.microsoft.com/de-de/library/system.security.cryptography.aes(v=vs.110).aspx
        private static string DecryptStringFromBytes_Aes(string cipherTextString, byte[] Key, byte[] IV)
        {
            byte[] cipherText = Convert.FromBase64String(cipherTextString);

            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            string plaintext = null;

            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt =
                            new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }
        #endregion
    }
}