﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractWishlistService
    {
        public abstract SuccessResult<AbstractWishlist> Upsert(AbstractWishlist abstractBlog);

        public abstract PagedList<AbstractWishlist> SelectAllByCustomerID(long CustomrtId);

        public abstract bool DeleteByID(long id);
    }
}