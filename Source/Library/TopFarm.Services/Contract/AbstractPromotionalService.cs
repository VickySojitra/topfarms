﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractPromotionalService
    {
        public abstract SuccessResult<AbstractPromotional> Upsert(AbstractPromotional abstractPromotional);

        public abstract PagedList<AbstractPromotional> SelectAll(PageParam pageParam, string search = "");

        public abstract AbstractPromotional SelectByID(long ID);
        public abstract AbstractPromotional DiscountByPromocode(string Prmocode);

    }
}
