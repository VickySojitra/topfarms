﻿using System;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractOrdersService
    {
        public abstract PagedList<AbstractOrders> SelectAll(PageParam pageParam, string customerName = "", string traderName = "", int Status = 0, long CustomerId =0, long traderId = 0);
        public abstract PagedList<AbstractOrders> SalesReportSelectAll(DateTime startDate, DateTime endDate);
        public abstract SuccessResult<AbstractOrders> Select(int id);
        public abstract SuccessResult<AbstractOrders> InsertUpdate(AbstractOrders abstractOrders);

        public abstract PagedList<AbstractOrderComment> SelectAllOrderComment(PageParam pageParam, int orderId);
        public abstract SuccessResult<AbstractOrderComment> InsertUpdateOrderComment(AbstractOrderComment abstractOrderComment);

        public abstract SuccessResult<AbstractOrderMetaData> GetOrderMetadata();

        public abstract PagedList<AbstractOrderTimeline> SelectOrderTimeline(int orderId);
        public abstract SuccessResult<AbstractOrderTimeline> InsertUpdateOrderTimeline(AbstractOrderTimeline orderTimeline);

        public abstract PagedList<AbstractOrderDetails> SelectOrderDetail(int orderId);
        public abstract SuccessResult<AbstractOrderDetails> InsertUpdateOrderDetail(AbstractOrderDetails orderDetails);
        public abstract AbstractOrderData OrderDetailsById(int id);
        public abstract AbstractOrders TrackOrderDetail(int id);
        public abstract PagedList<AbstractOrderData> OrderIdListById(int userId);
    }
}
