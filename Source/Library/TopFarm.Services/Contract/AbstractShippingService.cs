﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Services.Contract.Shipping
{
    public abstract class AbstractShippingService
    {
        public abstract SuccessResult<AbstractShippingRate> Upsert(AbstractShippingRate abstractCategory);

        public abstract PagedList<AbstractShippingRate> SelectAll(PageParam pageParam, string search = "");

        public abstract AbstractShippingRate SelectByID(long ID);
    }
}
