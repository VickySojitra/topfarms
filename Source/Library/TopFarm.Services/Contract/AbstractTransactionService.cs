﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractTransactionService
    {
        public abstract PagedList<AbstractTransaction> SelectAll(PageParam pageParam, string search = "");

        public abstract SuccessResult<AbstractTransaction> SelectbyID(int id);

        public abstract SuccessResult<AbstractTransaction> SelectbyOrderID(int id);

        public abstract SuccessResult<AbstractTransaction> SelectbyCustomerID(int id);

        public abstract SuccessResult<AbstractTransaction> InsertUpdate(AbstractTransaction AbstractTransaction);

        public abstract bool Delete(int id);
    }
}
