﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractNotificationService
    {
        public abstract SuccessResult<AbstractNotification> InsertUpdate(AbstractNotification abstractNotification);

        public abstract PagedList<AbstractNotification> Notifications_ByRoleID( string search = "",int RoleID = 0,int UserID = 0);

        public abstract SuccessResult<AbstractNotification> SelectByID(long ID);

        public abstract int Update(AbstractNotification abstractNotification);
    }
}
