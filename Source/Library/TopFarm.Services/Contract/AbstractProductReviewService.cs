﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractProductReviewService
    {
        public abstract PagedList<AbstractProductReview> SelectAll(PageParam pageParam, string search = "", long productId = 0, int SortBy = 0);
        public abstract SuccessResult<AbstractProductReview> Select(long id);
        public abstract SuccessResult<AbstractProductReview> InsertUpdate(AbstractProductReview productReview);
        public abstract SuccessResult<AbstractProductReview> Delete(long id, long deletedBy, int deletedRole);

        public abstract PagedList<AbstractProductReviewLike> ProdLikeSelectAll(PageParam pageParam, string search = "", long productReviewId = 0);
        public abstract SuccessResult<AbstractProductReviewLike> ProdLikeSelect(long id);
        public abstract SuccessResult<AbstractProductReviewLike> ProdLikeInsertUpdate(AbstractProductReviewLike productReviewLike);
        public abstract SuccessResult<AbstractProductReviewLike> ProdLikeDelete(long id, long deletedBy, int deletedRole);
    }
}
