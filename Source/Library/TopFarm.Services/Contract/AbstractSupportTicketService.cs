﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractSupportTicketService
    {
        public abstract PagedList<AbstractSupportTicket> SelectAll(PageParam pageParam, string search = "");
        public abstract PagedList<AbstractSupportComment> SelectCommentsByTicketId(PageParam pageParam, long ticketId);

        public abstract SuccessResult<AbstractSupportTicket> SelectbyID(int id);

        public abstract PagedList<AbstractSupportTicket> SelectbyCustomerID(PageParam pageParam, long id, int status = 0);

        public abstract SuccessResult<AbstractSupportTicket> InsertUpdate(AbstractSupportTicket AbstractSupportTicket);

        public abstract SuccessResult<AbstractSupportComment> InsertUpdateComment(AbstractSupportComment SupportComment);

        public abstract bool Delete(int id);

        public abstract bool UpdateStatus(long ID, bool Status);

        public abstract int[] TicketCount();
    }
}
