﻿using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractProductCategoryService
    {
        public abstract int Insert(AbstractProductCategory abstractProductCategory);

        public abstract PagedList<AbstractProductCategory> SelectAll(PageParam pageParam, string search = "");

        public abstract AbstractProductCategory SelectByID(long ID);

        public abstract int Update(AbstractProductCategory abstractProductCategory);
    }
}
