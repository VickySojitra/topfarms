﻿using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractPromotionTypeService
    {
        public abstract PagedList<AbstractPromotionType> SelectAll(PageParam pageParam, string search = "");
    }
}
