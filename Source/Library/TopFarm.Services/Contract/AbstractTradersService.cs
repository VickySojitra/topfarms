﻿using System;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractTradersService
    {
        public abstract SuccessResult<AbstractTraders> Upsert(AbstractTraders abstractAddress);

        public abstract SuccessResult<AbstractTraders> SelectByID(long id);
        public abstract PagedList<AbstractTraders> SelectAll(PageParam pageParam, string search = "");
        public abstract PagedList<AbstractTraders> TraderReportSelectAll(DateTime startDate, DateTime endDate);

        public abstract bool UpdateStatus(long ID, bool Status);

        public abstract bool DeleteByID(long id);

        public abstract SuccessResult<AbstractTraderMetaData> GetMetaData();
        public abstract SuccessResult<AbstractTraders> Login(string Email, string WebPassword);
    }
}
