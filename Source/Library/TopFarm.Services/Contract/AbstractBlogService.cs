﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using System.Collections.Generic;


namespace TopFarm.Services.Contract
{
    public abstract class AbstractBlogService
    {
        public abstract SuccessResult<AbstractBlog> Upsert(AbstractBlog abstractBlog);

        public abstract PagedList<AbstractBlog> SelectAll(PageParam pageParam, string search = "");

        public abstract AbstractBlog SelectByID(long ID);

        public abstract List<AbstractBlog> Blog_BlogCategoryId(long BlogCategoryId);

        public abstract bool UpdateStatus(long ID, bool Status);

        public abstract bool DeleteByID(long id);
    }
}
