﻿using System;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractDailyReportService
    {
        public abstract PagedList<AbstractDailyReport> SelectAll(PageParam pageParam, DateTime? date = null, long traderId = 0);

        public abstract SuccessResult<AbstractDailyReport> Select(int id);

        public abstract SuccessResult<AbstractDailyReport> InsertUpdate(AbstractDailyReport abstractDailyReport);
        public abstract PagedList<AbstractCommonDD> VisitPurposeSelectAll(PageParam pageParam);
    }
}
