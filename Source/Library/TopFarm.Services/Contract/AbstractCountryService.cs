﻿using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractCountryService
    {
        public abstract PagedList<AbstractCountry> SelectAll(PageParam pageParam, string search = "");
    }
}
