﻿using System.Collections.Generic;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractProductService
    {
        public abstract PagedList<AbstractProduct> SelectAll(PageParam pageParam, string search = "");
        public abstract SuccessResult<AbstractProduct> Select(long id);
        public abstract SuccessResult<AbstractProduct> InsertUpdate(AbstractProduct product);
        public abstract SuccessResult<AbstractProduct> Delete(long id, long deletedBy);
        public abstract PagedList<AbstractProductImages> ProductImagesByProductId(long productId);
        public abstract SuccessResult<AbstractProductImages> InsertUpdateProductImage(AbstractProductImages product);
        public abstract SuccessResult<AbstractProductImages> DeleteProductImage(long id, long deletedBy);
        public abstract PagedList<AbstractProduct> GetProductsByFilter(PageParam pageParam, string search = "", long categoryId = 0, ProductSortTypes SortBy = ProductSortTypes.New, decimal minPrice = 0, decimal maxPrice= 0);
        public abstract SuccessResult<AbstractProductMetaData> GetMetaData();
        public abstract List<AbstractProduct> GetProductByCategory(long id);
        public abstract PagedList<AbstractProduct> GetProductPriceList();
        public abstract PagedList<AbstractProduct> GetProductpriceByFilter(PageParam pageParam, Product search, long categoryId = 0, ProductSortTypes SortBy = ProductSortTypes.New, decimal minPrice = 0, decimal maxPrice = 0);
    }
}
;