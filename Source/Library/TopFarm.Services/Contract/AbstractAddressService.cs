﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractAddressService
    {
        public abstract SuccessResult<AbstractAddress> Upsert(AbstractAddress abstractPromotional);

        public abstract AbstractAddress SelectByID(long ID);
        public abstract PagedList<AbstractAddress> SelectAll(PageParam pageParam, string search = "");
        public abstract bool Delete(int id);
        public abstract AbstractAddress AddressByID(long ID);

    }
}
