﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractAdminService
    {
        public abstract PagedList<AbstractAdmin> SelectAll(PageParam pageParam, string search = "");

        public abstract SuccessResult<AbstractAdmin> Select(int id);

        public abstract SuccessResult<AbstractAdmin> Login(string Email, string WebPassword);
        public abstract SuccessResult<AbstractAdmin> ChangePassword(int Id, string OldPassword, string NewPassword);

        public abstract SuccessResult<AbstractAdmin> InsertUpdate(AbstractAdmin abstractCustomer);
        public abstract SuccessResult<AbstractCustomer> TInsertUpdate(AbstractCustomer abstractCustomer);
        public abstract SuccessResult<AbstractCustomer> TSelect(int id);

        public abstract bool Delete(int id);
    }
}
