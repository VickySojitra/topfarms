﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopFarm.Entities.Contract;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractCommonService
    {
        public abstract AbstractCommonCount GetcDashboardCountList(long ID);
        public abstract AbstractCommonCount GettDashboardCountList(long ID, int RoleId);
    }
}
    