﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;

namespace TopFarm.Services.Contract
{
    public abstract class AbstractBlogCategoryService
    {
        public abstract SuccessResult<AbstractBlogCategory> Upsert(AbstractBlogCategory abstractBlogCategory);

        public abstract PagedList<AbstractBlogCategory> SelectAll(string search = "");
    }
}
