﻿//-----------------------------------------------------------------------
// <copyright file="ServiceModule.cs" company="Premiere Digital Services">
//     Copyright Premiere Digital Services. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TopFarm.Services
{
    using Autofac;
    using Data;
    using TopFarm.Services.Contract;
    using TopFarm.Services.Contract.Shipping;

    /// <summary>
    /// The Service module for dependency injection.
    /// </summary>
    public class ServiceModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {           
            builder.RegisterModule<DataModule>();
            builder.RegisterType<V1.TradersServices>().As<AbstractTradersService>().InstancePerDependency();
            builder.RegisterType<V1.OrdersServices>().As<AbstractOrdersService>().InstancePerDependency();
            builder.RegisterType<V1.NotificationService>().As<AbstractNotificationService>().InstancePerDependency();
            builder.RegisterType<V1.SupportTicketService>().As<AbstractSupportTicketService>().InstancePerDependency();
            builder.RegisterType<V1.TransactionService>().As<AbstractTransactionService>().InstancePerDependency();
            builder.RegisterType<V1.CustomerService>().As<AbstractCustomerService>().InstancePerDependency();
            builder.RegisterType<V1.ProductCategoryServices>().As<AbstractProductCategoryService>().InstancePerDependency();
            builder.RegisterType<V1.ShippingServices>().As<AbstractShippingService>().InstancePerDependency();
            builder.RegisterType<V1.BlogCategoryService>().As<AbstractBlogCategoryService>().InstancePerDependency();
            builder.RegisterType<V1.BlogService>().As<AbstractBlogService>().InstancePerDependency();
            builder.RegisterType<V1.DailyReportService>().As<AbstractDailyReportService>().InstancePerDependency();
            builder.RegisterType<V1.ProductServices>().As<AbstractProductService>().InstancePerDependency();
            builder.RegisterType<V1.PromotionalService>().As<AbstractPromotionalService>().InstancePerDependency();
            builder.RegisterType<V1.ProductReviewServices>().As<AbstractProductReviewService>().InstancePerDependency();
            builder.RegisterType<V1.PromotionTypeService>().As<AbstractPromotionTypeService>().InstancePerDependency();
            builder.RegisterType<V1.CountryService>().As<AbstractCountryService>().InstancePerDependency();
            builder.RegisterType<V1.AddressService>().As<AbstractAddressService>().InstancePerDependency();
            builder.RegisterType<V1.CartService>().As<AbstractCartService>().InstancePerDependency();
            builder.RegisterType<V1.WishlistService>().As<AbstractWishlistService>().InstancePerDependency();
            builder.RegisterType<V1.AdminService>().As<AbstractAdminService>().InstancePerDependency();
            builder.RegisterType<V1.CommonService>().As<AbstractCommonService>().InstancePerDependency();

            base.Load(builder);
        }
    }
}