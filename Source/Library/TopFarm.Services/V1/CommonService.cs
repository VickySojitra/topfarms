﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;

namespace TopFarm.Services.V1
{
    public class CommonService : AbstractCommonService
    {
        private AbstractCommonDao _abstractCommonDao;

        public CommonService(AbstractCommonDao abstractCommonDao)
        {
            this._abstractCommonDao = abstractCommonDao;
        }

        public override AbstractCommonCount GetcDashboardCountList(long ID)
        {
            try
            {
                return _abstractCommonDao.GetcDashboardCountList(ID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override AbstractCommonCount GettDashboardCountList(long ID, int RoleId)
        {
            try
            {
                return _abstractCommonDao.GettDashboardCountList(ID,RoleId);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
