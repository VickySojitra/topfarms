﻿using System;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;

namespace TopFarm.Services.V1
{
    public class PromotionTypeService: AbstractPromotionTypeService
    {
        private AbstractPromotionTypeDao _AbstractPromotionTypeDao;

        public PromotionTypeService(AbstractPromotionTypeDao AbstractPromotionalDao)
        {
            this._AbstractPromotionTypeDao = AbstractPromotionalDao;
        }

        public override PagedList<AbstractPromotionType> SelectAll(PageParam pageParam, string search = "")
        {
            try
            {
                return _AbstractPromotionTypeDao.SelectAll(pageParam, search);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
