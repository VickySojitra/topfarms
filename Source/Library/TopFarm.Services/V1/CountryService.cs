﻿using System;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;

namespace TopFarm.Services.V1
{
    public class CountryService: AbstractCountryService
    {
        private AbstractCountryDao _abstractShippingRateDao;

        public CountryService(AbstractCountryDao AbstractShippingRateDao)
        {
            this._abstractShippingRateDao = AbstractShippingRateDao;
        }

        public override PagedList<AbstractCountry> SelectAll(PageParam pageParam, string search = "")
        {
            try
            {
                return _abstractShippingRateDao.SelectAll(pageParam, search);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
