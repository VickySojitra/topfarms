﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;

namespace TopFarm.Services.V1
{
    public class WishlistService : AbstractWishlistService
    {
        private AbstractWishlistDao _abstractWishlistDao;

        public WishlistService(AbstractWishlistDao abstractBlogDao)
        {
            this._abstractWishlistDao = abstractBlogDao;
        }

        public override bool DeleteByID(long id)
        {
            return _abstractWishlistDao.DeleteByID(id);
        }

        public override PagedList<AbstractWishlist> SelectAllByCustomerID(long CustomerId)
        {
            return _abstractWishlistDao.SelectAllByCustomerID(CustomerId);
        }

        public override SuccessResult<AbstractWishlist> Upsert(AbstractWishlist abstractBlog)
        {
            return _abstractWishlistDao.Upsert(abstractBlog);
        }
    }
}
