﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;

namespace TopFarm.Services.V1
{
    public class AdminService : AbstractAdminService
    {
        private AbstractAdminDao _abstractAdminDao;

        public AdminService(AbstractAdminDao abstractCustomerDao)
        {
            this._abstractAdminDao = abstractCustomerDao;
        }

        public override SuccessResult<AbstractAdmin> ChangePassword(int Id, string OldPassword, string NewPassword)
        {
            return _abstractAdminDao.ChangePassword(Id, OldPassword, NewPassword);
        }

        public override SuccessResult<AbstractAdmin> Login(string Email, string WebPassword)
        {
            return _abstractAdminDao.Login(Email, WebPassword);
        }

        public override bool Delete(int id)
        {
            return _abstractAdminDao.Delete(id);
        }

        public override SuccessResult<AbstractAdmin> InsertUpdate(AbstractAdmin abstractCustomer)
        {
            return _abstractAdminDao.InsertUpdate(abstractCustomer);
        }

        public override SuccessResult<AbstractAdmin> Select(int id)
        {
            return _abstractAdminDao.Select(id);
        }

        public override PagedList<AbstractAdmin> SelectAll(PageParam pageParam, string search = "")
        {
            return _abstractAdminDao.SelectAll(pageParam, search);
        }

        public override SuccessResult<AbstractCustomer> TInsertUpdate(AbstractCustomer abstractCustomer)
        {
            return _abstractAdminDao.TInsertUpdate(abstractCustomer);
        }

        public override SuccessResult<AbstractCustomer> TSelect(int id)
        {
            return _abstractAdminDao.TSelect(id);
        }
    }
}
