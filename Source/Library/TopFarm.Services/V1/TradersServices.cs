﻿using System;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;
using TopFarm.Entities.V1;


namespace TopFarm.Services.V1
{
    public class TradersServices : AbstractTradersService
    {
        private AbstractTradersDao _abstractTradersDao;
        private AbstractNotificationDao _abstractNotificationDao;


        public TradersServices(AbstractTradersDao abstractTradersDao, AbstractNotificationDao abstractNotificationDao)
        {
            this._abstractTradersDao = abstractTradersDao;
            this._abstractNotificationDao = abstractNotificationDao;
        }

        public override SuccessResult<AbstractTraders> Upsert(AbstractTraders abstractTraders)
        {
            try
            {
                var quote = _abstractTradersDao.Upsert(abstractTraders);

                if (quote.Item != null && quote.Code == 200)
                {
                    AbstractNotification abstractNotification = new Notification();
                    abstractNotification.UserID = ProjectSession.LoginUserID;
                    abstractNotification.RoleID = ProjectSession.LoginRoleID;
                    abstractNotification.Message = "You Active The<a href= 'javascript:void(0)'http://localhost:7531/TDailyReport > </ a > Employee " + " In  </ a > Salon";
                    abstractNotification.CreatedBy = ProjectSession.LoginUserID;

                    _abstractNotificationDao.InsertUpdate(abstractNotification);
                }
                return quote;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractTraders> SelectAll(PageParam pageParam, string search = "")
        {
            return this._abstractTradersDao.SelectAll(pageParam, search);
        }

        public override PagedList<AbstractTraders> TraderReportSelectAll(DateTime startDate, DateTime endDate)
        {
            return this._abstractTradersDao.TraderReportSelectAll(startDate, endDate);
        }

        public override SuccessResult<AbstractTraders> SelectByID(long id)
        {
            try
            {
                return this._abstractTradersDao.SelectByID(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override bool UpdateStatus(long ID, bool Status)
        {
            try
            {
                return this._abstractTradersDao.UpdateStatus(ID, Status);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override bool DeleteByID(long id)
        {
            try
            {
                return this._abstractTradersDao.DeleteByID(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractTraderMetaData> GetMetaData()
        {
            try
            {
                return this._abstractTradersDao.GetMetaData();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractTraders> Login(string Email, string WebPassword)
        {
            return this._abstractTradersDao.Login(Email, WebPassword);
        }
    }
}
