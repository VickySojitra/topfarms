﻿using System;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;
using System.Collections.Generic;
using TopFarm.Entities.V1;


namespace TopFarm.Services.V1
{
    public class BlogService : AbstractBlogService
    {
        private AbstractBlogDao _abstractBlogDao;
        private AbstractNotificationDao _abstractNotificationDao;

        public BlogService(AbstractBlogDao abstractBlogDao, AbstractNotificationDao abstractNotificationDao)
        {
            this._abstractBlogDao = abstractBlogDao;
            this._abstractNotificationDao = abstractNotificationDao;

        }

        public override bool DeleteByID(long id)
        {
            try
            {
                return _abstractBlogDao.DeleteByID(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractBlog> SelectAll(PageParam pageParam, string search = "")
        {
            try
            {
                return _abstractBlogDao.SelectAll(pageParam, search);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override AbstractBlog SelectByID(long ID)
        {
            try
            {
                return _abstractBlogDao.SelectByID(ID);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public override List<AbstractBlog> Blog_BlogCategoryId(long BlogCategoryId)
        {
            try
            {
                return _abstractBlogDao.Blog_BlogCategoryId(BlogCategoryId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override bool UpdateStatus(long ID, bool Status)
        {
            try
            {
                return _abstractBlogDao.UpdateStatus(ID, Status);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractBlog> Upsert(AbstractBlog abstractBlog)
        {
            try
            {
                var quote = _abstractBlogDao.Upsert(abstractBlog);

                if (quote.Item != null && quote.Code == 200)
                {
                    AbstractNotification abstractNotification = new Notification();
                    abstractNotification.UserID = ProjectSession.LoginUserID;
                    abstractNotification.RoleID = ProjectSession.LoginRoleID;
                    abstractNotification.Message = "Blog Add";
                    abstractNotification.CreatedBy = ProjectSession.LoginUserID;

                    _abstractNotificationDao.InsertUpdate(abstractNotification);
                }
                return _abstractBlogDao.Upsert(abstractBlog);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
