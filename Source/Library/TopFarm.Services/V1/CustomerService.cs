﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;
using TopFarm.Entities.V1;
using System;

namespace TopFarm.Services.V1
{
    public class CustomerService : AbstractCustomerService
    {
        private AbstractCustomerDao _abstractCustomerDao;
        private AbstractNotificationDao _abstractNotificationDao;

        public CustomerService(AbstractCustomerDao abstractCustomerDao,AbstractNotificationDao abstractNotificationDao)
        {
            this._abstractCustomerDao = abstractCustomerDao;
            this._abstractNotificationDao= abstractNotificationDao;
        }

        public override SuccessResult<AbstractCustomer> Customer_ChangePassword(int Id, string OldPassword, string NewPassword)
        {
            return _abstractCustomerDao.Customer_ChangePassword(Id, OldPassword, NewPassword);
        }

        public override SuccessResult<AbstractCustomer> Customer_Login(string Email, string WebPassword)
        {
            return _abstractCustomerDao.Customer_Login(Email, WebPassword);
        }

        public override bool Delete(int id)
        {
            return _abstractCustomerDao.Delete(id);
        }

        public override SuccessResult<AbstractCustomer> GetCustomerById(int id)
        {
            return _abstractCustomerDao.GetCustomerById(id);

        }

        public override SuccessResult<AbstractCustomer> InsertUpdate(AbstractCustomer abstractCustomer)
        {
            var quote = _abstractCustomerDao.InsertUpdate(abstractCustomer);

            if (quote.Item != null  && quote.Code == 200)
            {
                AbstractNotification abstractNotification= new Notification();
                abstractNotification.UserID = ProjectSession.LoginUserID;
                abstractNotification.RoleID = ProjectSession.LoginRoleID;
                abstractNotification.Message = "Customer Added";
                abstractNotification.CreatedBy = ProjectSession.LoginUserID;

                _abstractNotificationDao.InsertUpdate(abstractNotification);
            }
            return _abstractCustomerDao.InsertUpdate(abstractCustomer);
        }

        public override bool IsEmailIdExist(string Email)
        {
            return _abstractCustomerDao.IsEmailIdExist(Email);
        }

        public override bool ResetPassword(string token, string changePassword)
        {
            return _abstractCustomerDao.ResetPassword(token, changePassword);
        }

        public override SuccessResult<AbstractCustomer> Select(int id)
        {
            return _abstractCustomerDao.Select(id);
        }

        public override PagedList<AbstractCustomer> SelectAll(PageParam pageParam, string search = "")
        {
            return _abstractCustomerDao.SelectAll(pageParam, search);
        }
        public override PagedList<AbstractCustomer> CustromerReportSelectAll(DateTime startDate, DateTime endDate)
        {
            return _abstractCustomerDao.CustromerReportSelectAll(startDate, endDate);
        }

        public override bool Updatetoken(string Email, string Token)
        {
            return _abstractCustomerDao.Updatetoken(Email, Token);
        }
    }
}
