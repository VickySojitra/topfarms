﻿using System;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;

namespace TopFarm.Services.V1
{
    public class ProductCategoryServices:AbstractProductCategoryService
    {
        private AbstractProductCategoryDao _abstractCategoryDao;

        public ProductCategoryServices(AbstractProductCategoryDao abstractCategoryDao)
        {
            this._abstractCategoryDao = abstractCategoryDao;
        }


        public override int Insert(AbstractProductCategory abstractCategory)
        {
            try
            {
                return this._abstractCategoryDao.Insert(abstractCategory);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractProductCategory> SelectAll(PageParam pageParam, string search = "")
        {
            try
            {
                return this._abstractCategoryDao.SelectAll(pageParam, search);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override AbstractProductCategory SelectByID(long ID)
        {
            try
            {
                return this._abstractCategoryDao.SelectByID(ID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override int Update(AbstractProductCategory abstractCategory)
        {
            try
            {
                return this._abstractCategoryDao.Update(abstractCategory);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
