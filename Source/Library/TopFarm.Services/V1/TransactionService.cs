﻿using System;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;

namespace TopFarm.Services.V1
{
    public class TransactionService : AbstractTransactionService
    {
        private AbstractTransactionDao _abstractTransactionDao;

        public TransactionService(AbstractTransactionDao AbstractTransactionDao)
        {
            this._abstractTransactionDao = AbstractTransactionDao;
        }

        public override bool Delete(int id)
        {
            try
            {
                return _abstractTransactionDao.Delete(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractTransaction> InsertUpdate(AbstractTransaction AbstractTransaction)
        {
            try
            {
                return _abstractTransactionDao.InsertUpdate(AbstractTransaction);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractTransaction> SelectAll(PageParam pageParam, string search = "")
        {
            try
            {
                return _abstractTransactionDao.SelectAll(pageParam, search);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractTransaction> SelectbyCustomerID(int id)
        {
            try
            {
                return _abstractTransactionDao.SelectbyCustomerID(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractTransaction> SelectbyID(int id)
        {
            try
            {
                return _abstractTransactionDao.SelectbyID(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractTransaction> SelectbyOrderID(int id)
        {
            try
            {
                return _abstractTransactionDao.SelectbyOrderID(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
