﻿using System;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;
using TopFarm.Entities.V1;


namespace TopFarm.Services.V1
{
    public class DailyReportService : AbstractDailyReportService
    {
        private AbstractDailyReportDao _abstractOrdersDao;
        private AbstractNotificationDao _abstractNotificationDao;


        public DailyReportService(AbstractDailyReportDao abstractTradersDao, AbstractNotificationDao abstractNotificationDao)
        {
            this._abstractOrdersDao = abstractTradersDao;
            this._abstractNotificationDao = abstractNotificationDao;
        }

        public override SuccessResult<AbstractDailyReport> InsertUpdate(AbstractDailyReport abstractOrders)
        {
            var quote = _abstractOrdersDao.InsertUpdate(abstractOrders);

            if (quote.Item != null && quote.Code == 200)
            {
                AbstractNotification abstractNotification = new Notification();
                abstractNotification.UserID = ProjectSession.LoginUserID;
                abstractNotification.RoleID = ProjectSession.LoginRoleID;
                abstractNotification.Message = "<a href='http://localhost:7531/TDailyReport'>http://localhost:7531/TDailyReport</a>";
                abstractNotification.CreatedBy = ProjectSession.LoginUserID;

                _abstractNotificationDao.InsertUpdate(abstractNotification);
            }
            return this._abstractOrdersDao.InsertUpdate(abstractOrders);
        }

        public override SuccessResult<AbstractDailyReport> Select(int id)
        {
            return this._abstractOrdersDao.Select(id);
        }

        public override PagedList<AbstractDailyReport> SelectAll(PageParam pageParam, DateTime? date = null, long traderId = 0)
        {
            return this._abstractOrdersDao.SelectAll(pageParam, date, traderId);
        }

        public override PagedList<AbstractCommonDD> VisitPurposeSelectAll(PageParam pageParam)
        {
            return this._abstractOrdersDao.VisitPurposeSelectAll(pageParam);
        }
    }
}
