﻿using System;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;

namespace TopFarm.Services.V1
{
    public class SupportTicketService : AbstractSupportTicketService
    {
        private AbstractSupportTicketDao _abstractSupportTicketDao;

        public SupportTicketService(AbstractSupportTicketDao AbstractSupportTicketDao)
        {
            this._abstractSupportTicketDao = AbstractSupportTicketDao;
        }

        public override bool Delete(int id)
        {
            try
            {
                return _abstractSupportTicketDao.Delete(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractSupportTicket> InsertUpdate(AbstractSupportTicket AbstractSupportTicket)
        {
            try
            {
                return _abstractSupportTicketDao.InsertUpdate(AbstractSupportTicket);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractSupportComment> InsertUpdateComment(AbstractSupportComment SupportComment)
        {
            return _abstractSupportTicketDao.InsertUpdateComment(SupportComment);
        }

        public override PagedList<AbstractSupportTicket> SelectAll(PageParam pageParam, string search = "")
        {
            try
            {
                return _abstractSupportTicketDao.SelectAll(pageParam, search);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractSupportTicket> SelectbyCustomerID(PageParam pageParam, long id, int status = 0)
        {
            try
            {
                return _abstractSupportTicketDao.SelectbyCustomerID(pageParam, id, status);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractSupportTicket> SelectbyID(int id)
        {
            try
            {
                return _abstractSupportTicketDao.SelectbyID(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractSupportComment> SelectCommentsByTicketId(PageParam pageParam, long ticketId)
        {
            return _abstractSupportTicketDao.SelectCommentsByTicketId(pageParam, ticketId);
        }

        public override int[] TicketCount()
        {
            try
            {
                return _abstractSupportTicketDao.TicketCount();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override bool UpdateStatus(long ID, bool Status)
        {
            try
            {
                return _abstractSupportTicketDao.UpdateStatus(ID, Status);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
