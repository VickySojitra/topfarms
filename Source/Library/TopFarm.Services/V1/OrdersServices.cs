﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;
using TopFarm.Entities.V1;
using System;

namespace TopFarm.Services.V1
{
    public class OrdersServices : AbstractOrdersService
    {
        private AbstractOrdersDao _abstractOrdersDao;
        private AbstractNotificationDao _abstractNotificationDao;


        public OrdersServices(AbstractOrdersDao abstractTradersDao, AbstractNotificationDao abstractNotificationDao)
        {
            this._abstractOrdersDao = abstractTradersDao;
            this._abstractNotificationDao = abstractNotificationDao;

        }

        public override SuccessResult<AbstractOrderMetaData> GetOrderMetadata()
        {
            return _abstractOrdersDao.GetOrderMetadata();
        }

        public override SuccessResult<AbstractOrders> InsertUpdate(AbstractOrders abstractOrders)
        {
            return this._abstractOrdersDao.InsertUpdateOrder(abstractOrders);
        }

        public override SuccessResult<AbstractOrderComment> InsertUpdateOrderComment(AbstractOrderComment abstractOrderComment)
        {
            var quote = _abstractOrdersDao.InsertUpdateOrderComment(abstractOrderComment);

            if (quote.Item != null && quote.Code == 200)
            {
                AbstractNotification abstractNotification = new Notification();
                abstractNotification.UserID = ProjectSession.LoginUserID;
                abstractNotification.RoleID = ProjectSession.LoginRoleID;
                abstractNotification.Message = "Customer Added";
                abstractNotification.CreatedBy = ProjectSession.LoginUserID;

                _abstractNotificationDao.InsertUpdate(abstractNotification);
            }
            return quote;
        }

        public override SuccessResult<AbstractOrderDetails> InsertUpdateOrderDetail(AbstractOrderDetails orderDetails)
        {
            var quote = _abstractOrdersDao.InsertUpdateOrderDetail(orderDetails);

            if (quote.Item != null && quote.Code == 200)
            {
                AbstractNotification abstractNotification = new Notification();
                abstractNotification.UserID = ProjectSession.LoginUserID;
                abstractNotification.RoleID = ProjectSession.LoginRoleID;
                abstractNotification.Message = "<a onclick=showOrderDetails("+quote.Item.OrderId+")>Your Order Details For "+quote.Item.ProductName+" </a>";
                abstractNotification.CreatedBy = ProjectSession.LoginUserID;

                _abstractNotificationDao.InsertUpdate(abstractNotification);
            }
            return quote;
        }

        public override SuccessResult<AbstractOrderTimeline> InsertUpdateOrderTimeline(AbstractOrderTimeline orderTimeline)
        {
            return this._abstractOrdersDao.InsertUpdateOrderTimeline(orderTimeline);
        }

        public override AbstractOrderData OrderDetailsById(int id)
        {
            return this._abstractOrdersDao.OrderDetailsById(id);
        }

        public override SuccessResult<AbstractOrders> Select(int id)
        {
            return this._abstractOrdersDao.Select(id);
        }

        public override PagedList<AbstractOrders> SelectAll(PageParam pageParam, string customerName = "", string traderName = "", int Status = 0, long CustomerId = 0, long traderId = 0)
        {
            return this._abstractOrdersDao.SelectAll(pageParam, customerName, traderName, Status, CustomerId, traderId);
        }

        public override PagedList<AbstractOrders> SalesReportSelectAll(DateTime startDate, DateTime endDate)
        {
            return this._abstractOrdersDao.SalesReportSelectAll(startDate,endDate);
        }

        public override PagedList<AbstractOrderComment> SelectAllOrderComment(PageParam pageParam, int orderId)
        {
            return this._abstractOrdersDao.SelectAllOrderComment(pageParam, orderId);
        }

        public override PagedList<AbstractOrderDetails> SelectOrderDetail(int orderId)
        {
            return this._abstractOrdersDao.SelectOrderDetail(orderId);
        }

        public override PagedList<AbstractOrderTimeline> SelectOrderTimeline(int orderId)
        {
            return this._abstractOrdersDao.SelectOrderTimeline(orderId);
        }

        public override AbstractOrders TrackOrderDetail(int id)
        {
            return this._abstractOrdersDao.TrackOrderDetail(id);
        }

        public override PagedList<AbstractOrderData> OrderIdListById(int userId)
        { 
            return this._abstractOrdersDao.OrderIdListById(userId);
        }

    }
}
