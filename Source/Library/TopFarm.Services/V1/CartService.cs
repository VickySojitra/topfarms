﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;

namespace TopFarm.Services.V1
{
    public class CartService : AbstractCartService
    {
        private AbstractCartDao _abstractcartDao;

        public CartService(AbstractCartDao abstractBlogDao)
        {
            this._abstractcartDao = abstractBlogDao;
        }

        public override bool DeleteByID(long id)
        {
            return _abstractcartDao.DeleteByID(id);
        }

        public override PagedList<AbstractCart> SelectAllByCustomerID(long CustomerId)
        {
            return _abstractcartDao.SelectAllByCustomerID(CustomerId);
        }

        public override SuccessResult<AbstractCart> Upsert(AbstractCart abstractBlog)
        {
            return _abstractcartDao.Upsert(abstractBlog);
        }

        public override SuccessResult<AbstractCheckout> UpsertCheckOut(AbstractCheckout abstractBlog)
        {
            return _abstractcartDao.UpsertCheckOut(abstractBlog);
        }

        public override SuccessResult<AbstractCheckout> SelectCheckoutByCustID(long CustomrtId)
        {
            return _abstractcartDao.SelectCheckoutByCustID(CustomrtId);
        }
        public override bool DeleteCheckoutByCustID(long id)
        {
            return _abstractcartDao.DeleteCheckoutByCustID(id);
        }

        public override SuccessResult<AbstractCheckout> UpsertAdrsCheckOut(AbstractCheckout abstractBlog)
        {
            return _abstractcartDao.UpsertAdrsCheckOut(abstractBlog);
        }
        public override PagedList<AbstractCart> GroupCategorybyId(long CustomerId)
        {
            return _abstractcartDao.GroupCategorybyId(CustomerId);
        }
    }
}
