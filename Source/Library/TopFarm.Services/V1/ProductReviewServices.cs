﻿using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;

namespace TopFarm.Services.V1
{
    public class ProductReviewServices : AbstractProductReviewService
    {
        private AbstractProductReviewDao _productReviewDao;

        public ProductReviewServices(AbstractProductReviewDao abstractCategoryDao)
        {
            this._productReviewDao = abstractCategoryDao;
        }

        public override SuccessResult<AbstractProductReview> Delete(long id, long deletedBy, int deletedRole)
        {
            return _productReviewDao.Delete(id, deletedBy, deletedRole);
        }

        public override SuccessResult<AbstractProductReview> InsertUpdate(AbstractProductReview productReview)
        {
            return _productReviewDao.InsertUpdate(productReview);
        }

        public override SuccessResult<AbstractProductReviewLike> ProdLikeDelete(long id, long deletedBy, int deletedRole)
        {
            return _productReviewDao.ProdLikeDelete(id, deletedBy, deletedRole);
        }

        public override SuccessResult<AbstractProductReviewLike> ProdLikeInsertUpdate(AbstractProductReviewLike productReviewLike)
        {
            return _productReviewDao.ProdLikeInsertUpdate(productReviewLike);
        }

        public override SuccessResult<AbstractProductReviewLike> ProdLikeSelect(long id)
        {
            return _productReviewDao.ProdLikeSelect(id);
        }

        public override PagedList<AbstractProductReviewLike> ProdLikeSelectAll(PageParam pageParam, string search = "", long productReviewId = 0)
        {
            return _productReviewDao.ProdLikeSelectAll(pageParam, search, productReviewId);
        }

        public override SuccessResult<AbstractProductReview> Select(long id)
        {
            return _productReviewDao.Select(id);
        }

        public override PagedList<AbstractProductReview> SelectAll(PageParam pageParam, string search = "", long productId = 0, int SortBy = 0)
        {
            return _productReviewDao.SelectAll(pageParam, search, productId, SortBy);
        }
    }
}
