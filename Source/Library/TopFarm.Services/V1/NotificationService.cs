﻿using System;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;

namespace TopFarm.Services.V1
{
    public class NotificationService : AbstractNotificationService
    {
        private AbstractNotificationDao _abstractNotificationDao;

        public NotificationService(AbstractNotificationDao AbstractNotificationDao)
        {
            this._abstractNotificationDao = AbstractNotificationDao;
        }

        public override SuccessResult<AbstractNotification> InsertUpdate(AbstractNotification abstractNotification)
        {
            try
            {
                return _abstractNotificationDao.InsertUpdate(abstractNotification);
            }
            catch
            {
                throw;
            }
        }

        public override PagedList<AbstractNotification> Notifications_ByRoleID( string search = "",int RoleID = 0,int UserID = 0)
        {
            try
            {
                return _abstractNotificationDao.Notifications_ByRoleID(search, RoleID, UserID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractNotification> SelectByID(long ID)
        {
            try
            {
                return _abstractNotificationDao.SelectByID(ID);
            }
            catch
            {
                throw;
            }
        }

        public override int Update(AbstractNotification abstractNotification)
        {
            throw new System.NotImplementedException();
        }
    }
}
