﻿using System.Collections.Generic;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Services.Contract;

namespace TopFarm.Services.V1
{
    public class ProductServices: AbstractProductService
    {
        private AbstractProductDao _productDao;

        public ProductServices(AbstractProductDao abstractCategoryDao)
        {
            this._productDao = abstractCategoryDao;
        }

        public override SuccessResult<AbstractProduct> Delete(long id, long deletedBy)
        {
            return _productDao.Delete(id, deletedBy);
        }

        public override SuccessResult<AbstractProductImages> DeleteProductImage(long id, long deletedBy)
        {
            return _productDao.DeleteProductImage(id, deletedBy);
        }

        public override SuccessResult<AbstractProductMetaData> GetMetaData()
        {
            return _productDao.GetMetaData();
        }

        public override List<AbstractProduct> GetProductByCategory(long id)
        {
            return _productDao.GetProductByCategory(id);
        }

        public override PagedList<AbstractProduct> GetProductsByFilter(PageParam pageParam, string search = "", long categoryId = 0, ProductSortTypes SortBy = ProductSortTypes.New, decimal minPrice = 0, decimal maxPrice = 0)
        {
            return _productDao.GetProductsByFilter(pageParam, search, categoryId, SortBy, minPrice,maxPrice);
        }

        public override PagedList<AbstractProduct> GetProductpriceByFilter(PageParam pageParam, Product search, long categoryId = 0, ProductSortTypes SortBy = ProductSortTypes.New, decimal minPrice = 0, decimal maxPrice = 0)
        {
            return _productDao.GetProductpriceByFilter(pageParam, search, categoryId, SortBy, minPrice, maxPrice);
        }

        public override SuccessResult<AbstractProduct> InsertUpdate(AbstractProduct product)
        {
            return _productDao.InsertUpdate(product);
        }

        public override SuccessResult<AbstractProductImages> InsertUpdateProductImage(AbstractProductImages product)
        {
            return _productDao.InsertUpdateProductImage(product);
        }

        public override PagedList<AbstractProductImages> ProductImagesByProductId(long productId)
        {
            return _productDao.ProductImagesByProductId(productId);
        }

        public override SuccessResult<AbstractProduct> Select(long id)
        {
            return _productDao.Select(id);
        }

        public override PagedList<AbstractProduct> SelectAll(PageParam pageParam, string search = "")
        {
            return _productDao.SelectAll(pageParam, search);
        }

        public override PagedList<AbstractProduct> GetProductPriceList()
        {
            return _productDao.GetProductPriceList();
        }
    }
}
