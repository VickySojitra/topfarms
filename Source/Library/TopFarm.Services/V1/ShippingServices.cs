﻿using System;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract.Shipping;

namespace TopFarm.Services.V1
{
    public class ShippingServices : AbstractShippingService
    {

        private AbstractShippingRateDao _abstractShippingRateDao;

        public ShippingServices(AbstractShippingRateDao AbstractShippingRateDao)
        {
            this._abstractShippingRateDao = AbstractShippingRateDao;
        }

        public override PagedList<AbstractShippingRate> SelectAll(PageParam pageParam, string search = "")
        {
            try
            {
                return _abstractShippingRateDao.SelectAll(pageParam, search);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override AbstractShippingRate SelectByID(long ID)
        {
            try
            {
                return _abstractShippingRateDao.SelectByID(ID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractShippingRate> Upsert(AbstractShippingRate abstractCategory)
        {
            try
            {
                return _abstractShippingRateDao.Upsert(abstractCategory);
            }
            catch(Exception)
            {
                throw;
            }
        }
    }
}
