﻿using System;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;

namespace TopFarm.Services.V1
{
    public class BlogCategoryService : AbstractBlogCategoryService
    {
        private AbstractBlogCategoryDao _abstractBlogCategoryDao;

        public BlogCategoryService(AbstractBlogCategoryDao abstractBlogCategoryDao)
        {
            this._abstractBlogCategoryDao = abstractBlogCategoryDao;
        }

        public override PagedList<AbstractBlogCategory> SelectAll( string search = "")
        {
            try
            {
                return _abstractBlogCategoryDao.SelectAll(search);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractBlogCategory> Upsert(AbstractBlogCategory abstractBlogCategory)
        {
            try
            {
                return _abstractBlogCategoryDao.Upsert(abstractBlogCategory);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
