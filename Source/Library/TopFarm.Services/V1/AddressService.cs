﻿using System;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;

namespace TopFarm.Services.V1
{
    public class AddressService : AbstractAddressService
    {
        private AbstractAddressDao _AbstractAddressDao;

        public AddressService(AbstractAddressDao AbstractShippingRateDao)
        {
            this._AbstractAddressDao = AbstractShippingRateDao;
        }

        public override AbstractAddress SelectByID(long ID)
        {
            try
            {
                return _AbstractAddressDao.AddressSelectByID(ID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractAddress> Upsert(AbstractAddress abstractPromotional)
        {
            try
            {
                return _AbstractAddressDao.Upsert(abstractPromotional);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public override PagedList<AbstractAddress> SelectAll(PageParam pageParam, string search = "")
        {
            return _AbstractAddressDao.SelectAll(pageParam, search);
        }
        public override bool Delete(int id)
        {
            return _AbstractAddressDao.Delete(id);
        }

        public override AbstractAddress AddressByID(long ID)
        {
            try
            {
                return _AbstractAddressDao.SelectByID(ID);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
