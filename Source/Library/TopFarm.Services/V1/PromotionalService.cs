﻿using System;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;
using TopFarm.Entities.V1;

namespace TopFarm.Services.V1
{
    public class PromotionalService : AbstractPromotionalService
    {
        private AbstractPromotionalDao _abstractPromotionalDao;
        private AbstractNotificationDao _abstractNotificationDao;


        public PromotionalService(AbstractPromotionalDao AbstractPromotionalDao, AbstractNotificationDao abstractNotificationDao)
        {
            this._abstractPromotionalDao = AbstractPromotionalDao;
            this._abstractNotificationDao = abstractNotificationDao;

        }

        public override AbstractPromotional DiscountByPromocode(string Prmocode)
        {
            try
            {
                return _abstractPromotionalDao.DiscountByPromocode(Prmocode);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PagedList<AbstractPromotional> SelectAll(PageParam pageParam, string search = "")
        {
            try
            {
                return _abstractPromotionalDao.SelectAll(pageParam, search);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override AbstractPromotional SelectByID(long ID)
        {
            try
            {
                return _abstractPromotionalDao.SelectByID(ID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SuccessResult<AbstractPromotional> Upsert(AbstractPromotional abstractPromotional)
        {
            try
            {

                var quote = _abstractPromotionalDao.Upsert(abstractPromotional);

                if (quote.Item != null && quote.Code == 200)
                {
                    AbstractNotification abstractNotification = new Notification();
                    abstractNotification.UserID = ProjectSession.LoginUserID;
                    abstractNotification.RoleID = ProjectSession.LoginRoleID;
                    abstractNotification.Message = "Promotional Add";
                    abstractNotification.CreatedBy = ProjectSession.LoginUserID;

                    _abstractNotificationDao.InsertUpdate(abstractNotification);
                }
                return quote;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
