﻿//-----------------------------------------------------------------------
// <copyright file="AbstractBaseService.cs" company="Premiere Digital Services">
//     Copyright Premiere Digital Services. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TopFarm.Services
{
	using Common;
	using Common.Paging;
	using System;
	using System.Collections.Generic;
	using System.Threading.Tasks;

	/// <summary>
	/// Class Base Business Access Layer
	/// </summary>
	public abstract class AbstractBaseService
    {
        /// <summary>
        /// Executes the dynamic query.
        /// </summary>
        /// <typeparam name="A">The abstract class.</typeparam>
        /// <typeparam name="I">The base model class.</typeparam>
        /// <param name="query">The query.</param>
        /// <param name="param">The parameter.</param>
        /// <returns>
        /// returns the paged list result.
        /// </returns>
        public virtual Task<SuccessResult<PagedList<A>>> ExecuteDynamicQuery<A, I>(string query, PageParam param)
            where A : BaseModel
            where I : BaseModel
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Executes the dynamic query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="param">The parameter.</param>
        /// <returns>
        /// returns the paged list result.
        /// </returns>
        public virtual Task<SuccessResult<PagedList<dynamic>>> ExecuteDynamicQuery(string query, PageParam param)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Executes the dynamic query Elastic.
        /// </summary>
        /// <typeparam name="A">The abstract class.</typeparam>
        /// <typeparam name="I">The base model class.</typeparam>
        /// <param name="query">The query.</param>
        /// <param name="type">The type.</param>
        /// <returns>
        /// returns the paged list result.
        /// </returns>
        public virtual Task<SuccessResult<PagedList<A>>> ExecuteDynamicQueryElastic<A, I>(string type)
            where A : BaseModel
            where I : BaseModel
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Executes the dynamic query Elastic.
        /// </summary>
        /// <typeparam name="A">The abstract class.</typeparam>
        /// <typeparam name="I">The base model class.</typeparam>
        /// <param name="query">The query.</param>
        /// <param name="sortDescriptors">The sort descriptors.</param>
        /// <param name="param">The parameter.</param>
        /// <param name="type">The type.</param>
        /// <returns>
        /// returns the paged list result.
        /// </returns>
        public virtual Task<SuccessResult<PagedList<A>>> ExecuteDynamicQuery<A, I>(PageParam param, string type)
            where A : BaseModel
            where I : BaseModel
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Executes the dynamic query Elastic.
        /// </summary>
        /// <typeparam name="I">The base model class.</typeparam>
        /// <param name="query">The query.</param>
        /// <param name="aggregation">The aggregation.</param>
        /// <param name="sortDescriptors">The sort descriptors.</param>
        /// <param name="param">The parameter.</param>
        /// <param name="type">The type.</param>
        /// <returns>
        /// returns the paged list result.
        /// </returns>
        public virtual Task<SuccessResult<PagedList<dynamic>>> ExecuteDynamicQuery<I>(Dictionary<string, string> sortDescriptors, PageParam param, string type)
            where I : BaseModel
        {
            throw new NotImplementedException();
        }
    }
}
