/**
 * Off-canvas toggler
*/

const offcanvas = (() => {

  const offcanvasTogglers = document.querySelectorAll('[data-bs-toggle="offcanvas"]'),
        offcanvasDismissers = document.querySelectorAll('[data-bs-dismiss="offcanvas"]'),
        offcanvas = document.querySelectorAll('.offcanvas'),
        docBody = document.body,
        fixedElements = document.querySelectorAll('[data-fixed-element]'),
        hasScrollbar = window.innerWidth > docBody.clientWidth;
  

  // Open off-canvas function
  const offcanvasOpen = (offcanvasID, toggler) => {

    document.querySelector(offcanvasID).classList.add('show');
    if (hasScrollbar) {
      if (fixedElements.length) {
        for (let i = 0; i < fixedElements.length; i++) {
          fixedElements[i].classList.add('right-15');
        }
      }
    }
    docBody.classList.add('offcanvas-open');
  };

  // Close off-canvas function
  const offcanvasClose = () => {
    for (let i = 0; i < offcanvas.length; i++) {
      offcanvas[i].classList.remove('show');
    }
    
    docBody.classList.remove('offcanvas-open');
  }

  // Open off-canvas event handler
  for (let i = 0; i < offcanvasTogglers.length; i++) {
    offcanvasTogglers[i].addEventListener('click', (e) => {
      e.preventDefault();
      offcanvasOpen(e.currentTarget.dataset.bsTarget, e.currentTarget);
    });
  }

  // Close off-canvas event handler
  for (let i = 0; i < offcanvasDismissers.length; i++) {
    offcanvasDismissers[i].addEventListener('click', (e) => {
      e.preventDefault();
      offcanvasClose();
    });
  }
})();

export default offcanvas;
