﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TopFarm.Enum1
{
    public static class ShippingCal
    {
        public const string
            Courier = "25.5",
            LocalShipping = "10.5",
            SelfPickup = "30",
        PolishPost = "10.0";
    }
}