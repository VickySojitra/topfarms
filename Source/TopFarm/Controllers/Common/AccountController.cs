﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Pages;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers
{
    public class AccountController : Controller
    {
        #region Fields
        private readonly AbstractCustomerService _customerService;
        private readonly AbstractAdminService _adminService;
        private readonly AbstractTradersService _traderService;
        private readonly AbstractProductCategoryService _productCategoryService;
        #endregion

        #region Ctor
        public AccountController(AbstractCustomerService customerService,
            AbstractAdminService adminService,
            AbstractProductCategoryService productCategoryService,
            AbstractTradersService traderService)
        {
            this._customerService = customerService;
            _adminService = adminService;
            _traderService = traderService;
            _productCategoryService = productCategoryService;
        }
        #endregion

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.LogIn)]
        public ActionResult LogIn(string email, string password, string remember)
        {
            SuccessResult<AbstractCustomer> userData = _customerService.Customer_Login(email, password);
            if (userData != null && userData.Code == 200 && userData.Item != null)
            {
                if (userData.Item.Status == 2)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Your Account is not active. Please contact to administrator.");
                }
                Session.Clear();
                ProjectSession.LoginUserID = userData.Item.ID;
                ProjectSession.LoginUserName = userData.Item.FirstName + " " + userData.Item.LastName;

#warning Change code to dynamic (RoleID)
                ProjectSession.LoginRoleID = 1; // Customer
                                                //ProjectSession.Menu = userData.Item.Permission;
                                                //ProjectSession.IsAdmin = (userData.Item.RoleName == "Admin");

                HttpCookie cookie = new HttpCookie("AdminUserLogin");
                cookie.Values.Add("Id", userData.Item.ID.ToString());
                cookie.Values.Add("LoginUserImg", userData.Item.AvtarURL);
                cookie.Values.Add("LoginUserEmail", userData.Item.Email);
                cookie.Values.Add("LoginUserPassword", userData.Item.Password);
                cookie.Expires = DateTime.Now.AddHours(1);
                HttpCookie remcookie = new HttpCookie("remAdminUserLogin");
                if (remember == "on")
                {
                    remcookie.Values.Add("LoginUserEmail", userData.Item.Email);
                    remcookie.Values.Add("LoginUserPassword", userData.Item.Password);
                    remcookie.Values.Add("remember", "on");
                }
                Response.Cookies.Add(cookie);
                Response.Cookies.Add(remcookie);

                return RedirectToAction(Actions.Index, Pages.Controllers.Home, new { Area = "" });

            }
            else
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Please Enter valid UserName or Password.");
            }
            return RedirectToAction(Actions.Index, Pages.Controllers.Home, new { Area = "" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.CustomerSignUp)]
        public ActionResult CustomerSignUp(string cName, string cEmail, string cPassword)
        {
            AbstractCustomer customer = new Customer()
            {
                FirstName = cName,
                Email = cEmail,
                Password = cPassword
            };

            SuccessResult<AbstractCustomer> userData = _customerService.InsertUpdate(customer);
            if (userData != null && userData.Code == 200)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Sign up successfully.");
            }
            else
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Failed to sign up.");
            }
            return RedirectToAction(Actions.Index, Pages.Controllers.Home, new { Area = "" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.LogInAdminOrTrader)]
        public ActionResult LogInAdminOrTrader(int role, string email, string password, string remember)
        {
            // Admin
            if (role == 1)
            {
                SuccessResult<AbstractAdmin> userData = _adminService.Login(email, password);
                if (userData != null && userData.Code == 200 && userData.Item != null)
                {
                    Session.Clear();
                    ProjectSession.LoginUserID = userData.Item.Id;
                    ProjectSession.LoginUserName = userData.Item.Name;

#warning Change code to dynamic (RoleID)
                    ProjectSession.LoginRoleID = 3; // Admin

                    HttpCookie cookie = new HttpCookie("AdminUserLogin");
                    cookie.Values.Add("Id", userData.Item.Id.ToString());
                    cookie.Values.Add("LoginUserImg", userData.Item.AvtarURL);
                    cookie.Expires = DateTime.Now.AddHours(1);
                    HttpCookie adcookie = new HttpCookie("adAdminUserLogin");

                    if (remember == "on")
                    {
                        adcookie.Values.Add("LoginUserEmail", userData.Item.Email);
                        adcookie.Values.Add("LoginUserPassword", userData.Item.Password);
                        adcookie.Values.Add("remember", "on");
                    }

                    Response.Cookies.Add(cookie);
                    Response.Cookies.Add(adcookie);

                    return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Please Enter valid UserName or Password.");
                }

                return RedirectToAction(Actions.Index, Pages.Controllers.AdminPanel, new { Area = "" });
            }
            // Trader
            else
            {
                SuccessResult<AbstractTraders> userData = _traderService.Login(email, password);
                if (userData != null && userData.Code == 200 && userData.Item != null)
                {
                    Session.Clear();
                    ProjectSession.LoginUserID = userData.Item.ID;
                    ProjectSession.LoginUserName = userData.Item.FirstName + " " + userData.Item.LastName;

#warning Change code to dynamic (RoleID)
                    ProjectSession.LoginRoleID = 2; // Trader
                    HttpCookie cookie = new HttpCookie("AdminUserLogin");
                    cookie.Values.Add("Id", userData.Item.ID.ToString());
                    cookie.Values.Add("LoginUserImg", userData.Item.AvtarURL);
                    cookie.Expires = DateTime.Now.AddHours(1);

                    HttpCookie trcookie = new HttpCookie("trAdminUserLogin");
                    if (remember == "on")
                    {
                        trcookie.Values.Add("LoginUserEmail", userData.Item.EmailID);
                        trcookie.Values.Add("LoginUserPassword", userData.Item.Password);
                        trcookie.Values.Add("remember", "on");
                    }

                    Response.Cookies.Add(cookie);
                    Response.Cookies.Add(trcookie);

                    return RedirectToAction(Actions.Index, Pages.Controllers.TDashboard, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Please Enter valid UserName or Password.");
                }

                return RedirectToAction(Actions.Index, Pages.Controllers.AdminPanel, new { Area = "" });
            }
        }

        [AllowAnonymous]
        [ActionName(Actions.Logout)]
        public ActionResult Logout()
        {
            long Role = ProjectSession.LoginRoleID;

            Session.Clear();
            Session.Abandon();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            if (Role == 3 || Role == 2) // Admin or Trader
            {
                return RedirectToAction(Actions.Index, Pages.Controllers.AdminPanel, new { Area = "" });
            }

            // Customer
            return RedirectToAction(Actions.Index, Pages.Controllers.Home, new { Area = "" });
        }

        [HttpGet]
        [ActionName(Actions.ForgotPassword)]
        public ActionResult ForgotPassword()
        {
            ViewBag.DDProductCategory = ProductCategoryDropdown();
            return View();
        }

        [HttpPost]
        [ActionName(Actions.ForgotPassword)]
        public ActionResult ForgotPassword(string Email)
        {
            return View();
        }

        public ActionResult successForgot()
        {
            return View();
        }

        [HttpGet]
        [ActionName(Actions.IsEmailIdExist)]
        public JsonResult IsEmailIdExist(string Email)
        {
            var result = _customerService.IsEmailIdExist(Email);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.SendResetLink)]
        public JsonResult SendResetLink(string Email)
        {
            string token = Guid.NewGuid().ToString();

            string body = "<p>Hello,</p><p>Please Find the below link to reset password.</p><a href='"+Configurations.ClientURL+"Account/ResetPassword?token=" + token + "' target='_blank'>Forgot Password Link</a>";
            //var isEmailSend = EmailHelper.Send(Email, "", "", "Forgot-Password", body, null, null);
            var isEmailSend = EmailHelper.SendMail(Email, body, "Forgot-Password");

            var setToken = _customerService.Updatetoken(Email, token);
            return Json(setToken);
        }

        [HttpGet]
        public ActionResult ResetPassword(string token)
        {
            Customer customer = new Customer();
            customer.token = token;
            return View(customer);
        }

        [HttpPost]
        public ActionResult ResetPassword(string token, string confirmPassword)
        {
            Customer customer = new Customer();
            if (!string.IsNullOrEmpty(token) && !string.IsNullOrEmpty(confirmPassword))
            {
                _customerService.ResetPassword(token, confirmPassword);
                ViewBag.IsReset = true;
                return View("~/Views/Account/successResetPassword.cshtml");
            }
            return View(customer);
        }

        private List<SelectListItem> ProductCategoryDropdown()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var category = _productCategoryService.SelectAll(pageParam, "");

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category1 in category.Values)
            {
                items.Add(new SelectListItem() { Text = category1.Name.ToString(), Value = category1.ID.ToString() });
            }

            return items;
        }
    }
}