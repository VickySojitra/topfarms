﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TopFarm.Controllers
{
    public class TNotificationController : Controller
    {
        // GET: Notification
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            return View();
        }
    }
}