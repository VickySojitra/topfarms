﻿using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Entities.Contract;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers
{
    public class TDashboardController : AdminBaseController
    {

        #region Fields
      
        private readonly AbstractCommonService _commonService;

        #endregion

        #region Ctor

        public TDashboardController(AbstractCommonService commonService)
        {
            _commonService = commonService;
        }

        #endregion


        // GET: Dashboard
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }
            AbstractCommonCount item = _commonService.GettDashboardCountList(ProjectSession.LoginUserID, ConvertTo.Integer(ProjectSession.LoginRoleID));
            Session["trNotificationCount"] = item.trNotificationCount;
            Session["TotalSalesCount"] = item.TotalSalesCount;
            Session["MemberSince"] = item.MemberSince;


            return View();
        }
    }
}