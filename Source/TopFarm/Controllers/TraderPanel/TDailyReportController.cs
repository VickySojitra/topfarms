﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Pages;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers
{
    public class TDailyReportController : AdminBaseController
    {
        private readonly AbstractDailyReportService _dailyReportService;

        public TDailyReportController(AbstractDailyReportService dailyReportService)
        {
            _dailyReportService = dailyReportService;
        }

        // GET: DailyReport
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            return View();
        }

        [ActionName(Actions.AddDailyReport)]
        public ActionResult AddDailyReport(int dailyReportID = 0)
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            ViewBag.DDVisitPurpose = BindVisitPurpose();

            AbstractDailyReport dailyReport = new DailyReport();
            if(dailyReportID > 0)
            {
                dailyReport = _dailyReportService.Select(dailyReportID).Item;
            }

            dailyReport.Visit = new DailyVisit();
            return View(dailyReport);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.BindDailyReport)]
        public JsonResult BindDailyReport([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                var model = _dailyReportService.SelectAll(pageParam, null, ProjectSession.LoginUserID);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<DailyReport>(), 0, 0), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.SaveDailyReport)]
        public ActionResult SaveDailyReport(DailyReport report)
        {
            try
            {
                // Add
                report.Date = new DateTime(report.Date.Year, report.Date.Month, report.Date.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                report.TraderId = report.CreatedBy = report.UpdatedBy = ProjectSession.LoginUserID;
                SuccessResult<AbstractDailyReport> result = _dailyReportService.InsertUpdate(report);
                
                if (result.Item != null)
                {
                    if(result.Item.IsSubmitted == false)
                    {
                        TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Daily visit saved successfully.");
                        return RedirectToAction(Actions.AddDailyReport, Pages.Controllers.TDailyReport, new { Area = "", dailyReportID = result.Item.ID });
                    }

                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Daily report submitted successfully.");
                    return RedirectToAction(Actions.Index, Pages.Controllers.TDailyReport, new { Area = ""});
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Failed to add Daily report.");
                    return RedirectToAction(Actions.AddDailyReport, Pages.Controllers.TDailyReport, new { Area = "", dailyReportID = report.ID});
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "An error occurred on the system. Please contact the administrator.");
                return RedirectToAction(Actions.AddDailyReport, Pages.Controllers.TDailyReport, new { Area = "", dailyReportID = report.ID });
            }
        }

        public List<SelectListItem> BindVisitPurpose()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = int.MaxValue;

                var models = _dailyReportService.VisitPurposeSelectAll(pageParam);

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.Name, Value = master.ID.ToString() });
                }
            }
            catch (Exception ex)
            {
            }

            return items;
        }
    }
}