﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Pages;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers
{
    public class TraderProfileController : AdminBaseController
    {
        #region Fields
        private readonly AbstractAdminService _adminService;
        private readonly AbstractCountryService _abstractCountryService;
        private readonly AbstractAddressService _AbstractAddressService;

        #endregion

        #region Ctor
        public TraderProfileController(AbstractAdminService adminService, AbstractCountryService abstractCountryService, AbstractAddressService abstractAddressService)
        {
            this._abstractCountryService = abstractCountryService;
            this._adminService = adminService;
            this._AbstractAddressService = abstractAddressService;
        }
        #endregion

        // GET: TraderProfile
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }
            ViewBag.Country = ProductCountryDropdownList();

            var model = _adminService.TSelect((int)ProjectSession.LoginUserID).Item;
            if (!string.IsNullOrEmpty(model.AvtarURL))
            {
                ViewBag.IsImage = true;
            }
            model.CustomerId = (int)ProjectSession.LoginUserID;
            return View(model);
        }

        [HttpPost]
        public ActionResult UpdateProfile(Admin admin, HttpPostedFileBase FileURL)
        {
            try
            {
                admin.CreatedBy = admin.UpdatedBy = ProjectSession.LoginUserID;
                SuccessResult<AbstractAdmin> result = _adminService.InsertUpdate(admin);

                if (result?.Item != null)
                {
                    // Add product images
                    string profilePath = Server.MapPath("~/" + Path.Combine("Storage", "Admin", result.Item.Id.ToString()));

                    if (Directory.Exists(profilePath) == false)
                    {
                        Directory.CreateDirectory(profilePath);
                    }

                    string URLPath = string.Empty;
                    if (FileURL != null && FileURL.ContentLength > 0)
                    {
                        string fileName = Path.Combine(profilePath, "profile" + Path.GetExtension(FileURL.FileName));
                        FileURL.SaveAs(fileName);

                        result.Item.AvtarURL = Path.Combine("Storage", "Admin", result.Item.Id.ToString(), "profile" + Path.GetExtension(FileURL.FileName));

                        result = _adminService.InsertUpdate(result.Item);

                        var cookie = Request.Cookies["AdminUserLogin"];
                        if (cookie != null)
                        {
                            cookie.Values["LoginUserImg"] = result.Item.AvtarURL;
                            Response.Cookies.Add(cookie);
                        }
                    }
                }

                if (result.Item != null)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Profile changes saved successfully.");
                    return RedirectToAction(Actions.Index, Pages.Controllers.AdminProfile);
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Failed to update profile.");
                    return RedirectToAction(Actions.Index, Pages.Controllers.AdminProfile);
                }
            }
            catch (Exception)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "An error occurred on the system. Please contact the administrator.");
                return RedirectToAction(Actions.Index, Pages.Controllers.AdminProfile);
            }
        }

        [HttpPost]
        [ActionName(Actions.UpdateTProfile)]
        public ActionResult UpdateTProfile(Customer traders, HttpPostedFileBase FileURL)
        {
            try
            {
                #region Trader Address

                AbstractAddress abstractTraders = new Address()
                {
                    CustomerId = traders.CustomerId,
                    Address1 = traders.Address1,
                    Address2 = traders.Address2,
                    Pincode = traders.Pincode,
                    City = traders.City,
                    CountryID = traders.CountryID,
                    ID = traders.Address,
                    Area = traders.Area,
                    State = traders.State,
                    DelFlg = false,
                    UpdatedAt = System.DateTime.Now,
                    UpdatedBy = traders.CustomerId
                };
                SuccessResult<AbstractAddress> addressResult = _AbstractAddressService.Upsert(abstractTraders);

                #endregion

                traders.CreatedBy = traders.UpdatedBy = ProjectSession.LoginUserID;
                traders.Address = Convert.ToInt32(addressResult.Item.ID);
                SuccessResult<AbstractCustomer> result = _adminService.TInsertUpdate(traders);
                result.Item.Email = traders.Email;
                if (result?.Item != null)
                {
                    ProjectSession.LoginUserName = result.Item.FirstName + " " + result.Item.LastName;
                    if (FileURL != null)
                    {
                        // Add product images
                        string profilePath = Server.MapPath("~/" + Path.Combine("Storage", "Admin", result.Item.ID.ToString()));

                        if (Directory.Exists(profilePath) == false)
                        {
                            Directory.CreateDirectory(profilePath);
                        }

                        string URLPath = string.Empty;
                        if (FileURL != null && FileURL.ContentLength > 0)
                        {
                            string fileName = Path.Combine(profilePath, "profile" + Path.GetExtension(FileURL.FileName));
                            FileURL.SaveAs(fileName);

                            result.Item.AvtarURL = Path.Combine("Storage", "Admin", result.Item.ID.ToString(), "profile" + Path.GetExtension(FileURL.FileName));

                            result = _adminService.TInsertUpdate(result.Item);

                            var cookie = Request.Cookies["AdminUserLogin"];
                            if (cookie != null)
                            {
                                cookie.Values["LoginUserImg"] = result.Item.AvtarURL;
                                Response.Cookies.Add(cookie);
                            }
                        }
                    }
                }

                if (result.Item != null)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Profile changes saved successfully.");
                    return RedirectToAction(Actions.Index, Pages.Controllers.TraderProfile);
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Failed to update profile.");
                    return RedirectToAction(Actions.Index, Pages.Controllers.TraderProfile);
                }
            }
            catch (Exception)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "An error occurred on the system. Please contact the administrator.");
                return RedirectToAction(Actions.Index, Pages.Controllers.TraderProfile);
            }
        }

        private List<SelectListItem> ProductCountryDropdownList()
        {
            try
            {
                PageParam pageParam = new PageParam()
                {
                    Limit = int.MaxValue,
                    Offset = 0
                };

                var CategoryList = _abstractCountryService.SelectAll(pageParam);

                List<SelectListItem> items = new List<SelectListItem>();
                foreach (var category in CategoryList.Values)
                {
                    items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.ID.ToString() });
                }

                return items;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}