﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.V1;
using TopFarm.Pages;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers
{
    public class TOrderController : AdminBaseController
    {
        #region Fields
        private readonly AbstractOrdersService _orderService;
        private readonly AbstractCustomerService _customerService;
        private readonly AbstractTradersService _traderService;

        #endregion

        #region Ctor
        public TOrderController(AbstractOrdersService orderService, 
                               AbstractCustomerService customerService,
                               AbstractTradersService traderService)
        {
            _orderService = orderService;
            _customerService = customerService;
            _traderService = traderService;

        }
        #endregion

        // GET: Order
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.BindOrders)]
        public JsonResult BindOrders([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                var model = _orderService.SelectAll(pageParam, traderId: ProjectSession.LoginUserID);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<Orders>(), 0, 0), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [ActionName(Actions.GetOrderMetaData)]
        public JsonResult GetOrderMetaData()
        {
            try
            {
                var model = _orderService.GetOrderMetadata();
                return Json(model.Item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.UpdateOrderStatus)]
        public JsonResult UpdateOrderStatus(int orderId, int statusId, string comment)
        {
            try
            {
                var model = _orderService.Select(orderId);
                if(model.Item != null)
                {
                    model.Item.Status = (byte)statusId;
                    _orderService.InsertUpdate(model.Item);
                }

                if(string.IsNullOrWhiteSpace(comment) == false)
                {
                    var orderComment = new OrderComment()
                    {
                        OrderID = orderId,
                        Message = comment,
                        UserID = ProjectSession.LoginUserID,
                        RoleID = ProjectSession.LoginRoleID,
                        CreatedBy = ProjectSession.LoginUserID
                    };

                    _orderService.InsertUpdateOrderComment(orderComment);
                }

                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddOrderComment)]
        public JsonResult AddOrderComment(int orderId, string comment)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(comment) == false)
                {
                    var orderComment = new OrderComment()
                    {
                        OrderID = orderId,
                        Message = comment,
                        UserID = ProjectSession.LoginUserID,
                        RoleID = ProjectSession.LoginRoleID,
                        CreatedBy = ProjectSession.LoginUserID
                    };

                    _orderService.InsertUpdateOrderComment(orderComment);
                }

                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AssignOrder)]
        public JsonResult AssignOrder(int orderId, int traderId)
        {
            try
            {
                var model = _orderService.Select(orderId);
                if (model.Item != null)
                {
                    model.Item.AssignTo = traderId;
                    model.Item.Status = (byte)OrderStatus.Assigned;
                    _orderService.InsertUpdate(model.Item);
                }

                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.GetOrderDetail)]
        public JsonResult GetOrderDetail(int Id)
        {
            try
            {
                var model = _orderService.Select(Id);
                return Json(model.Item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }        

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.GetTraderDetail)]
        public JsonResult GetTraderDetail(int traderId)
        {
            try
            {
                var model = _traderService.SelectByID(traderId);
                return Json(model.Item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.GetOrderComments)]
        public JsonResult GetOrderComments(int OrderId)
        {
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = int.MaxValue;

                var model = _orderService.SelectAllOrderComment(pageParam, OrderId);
                return Json(model.Values, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.BindTrader)]
        public JsonResult BindTrader()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = int.MaxValue;

                var models = _traderService.SelectAll(pageParam, "");

                foreach (var master in models.Values)
                {
                    items.Add(new SelectListItem() { Text = master.FirstName +" "+ master.LastName, Value = Convert.ToString(master.ID) });
                }
            }
            catch (Exception ex)
            {
            }

            return Json(items);
        }
    }
}