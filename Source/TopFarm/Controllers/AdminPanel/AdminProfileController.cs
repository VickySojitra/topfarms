﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Pages;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers.AdminPanel
{
    public class AdminProfileController : AdminBaseController
    {
        #region Fields
        private readonly AbstractAdminService _adminService;        
        #endregion

        #region Ctor
        public AdminProfileController(AbstractAdminService adminService)
        {
            this._adminService = adminService;
        }
        #endregion

        // GET: AdminProfile
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            var model = _adminService.Select((int)ProjectSession.LoginUserID).Item;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateProfile(Admin admin, HttpPostedFileBase FileURL)
        {
            try
            {
                admin.CreatedBy = admin.UpdatedBy = ProjectSession.LoginUserID;                
                SuccessResult<AbstractAdmin> result = _adminService.InsertUpdate(admin);

                if (result?.Item != null)
                {
                    ProjectSession.LoginUserName = result.Item.Name;

                    // Add product images
                    string profilePath = Server.MapPath("~/" + Path.Combine("Storage", "Admin", result.Item.Id.ToString()));

                    if (Directory.Exists(profilePath) == false)
                    {
                        Directory.CreateDirectory(profilePath);
                    }

                    string URLPath = string.Empty;
                    if (FileURL != null && FileURL.ContentLength > 0)
                    {
                        string fileName = Path.Combine(profilePath, "profile" + Path.GetExtension(FileURL.FileName));
                        FileURL.SaveAs(fileName);

                        result.Item.AvtarURL = Path.Combine("/Storage", "Admin", result.Item.Id.ToString(), "profile" + Path.GetExtension(FileURL.FileName));

                        result = _adminService.InsertUpdate(result.Item);

                        var cookie = Request.Cookies["AdminUserLogin"];
                        if (cookie != null)
                        {
                            cookie.Values["LoginUserImg"] = result.Item.AvtarURL;
                            Response.Cookies.Add(cookie);
                        }
                    }
                }

                if (result.Item != null)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Profile changes saved successfully.");
                    return RedirectToAction(Actions.Index, Pages.Controllers.AdminProfile);
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Failed to update profile.");
                    return RedirectToAction(Actions.Index, Pages.Controllers.AdminProfile);
                }
            }
            catch (Exception)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "An error occurred on the system. Please contact the administrator.");
                return RedirectToAction(Actions.Index, Pages.Controllers.AdminProfile);
            }
        }
    }
}