﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Pages;
using TopFarm.Resource;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers
{
    public class TradersController : AdminBaseController
    {
        #region Fields
        private readonly AbstractTradersService _AbstractTradersService;
        private readonly AbstractCountryService _AbstractCountryService;
        private readonly AbstractAddressService _AbstractAddressService;
        private readonly AbstractCommonService _commonService;

        #endregion

        #region Ctor
        public TradersController(AbstractTradersService abstractTradersService, AbstractCommonService commonService, AbstractCountryService AbstractCountryService, AbstractAddressService abstractAddressService)
        {
            this._AbstractTradersService = abstractTradersService;
            _AbstractCountryService = AbstractCountryService;
            _AbstractAddressService = abstractAddressService;
            _commonService = commonService;
        }
        #endregion

        // GET: Traders
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            return View();
        }

        [HttpGet]
        [ActionName(Actions.GetTraderMetaData)]
        public JsonResult GetTraderMetaData()
        {
            try
            {
                var model = _AbstractTradersService.GetMetaData();
                return Json(model.Item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult FormTraders(long ID = 0)
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            ViewBag.Country = ProductCountryDropdownList();
            AbstractTraders traders = new Traders();
            ViewBag.Edit = "0";
            if (ID > 0)
            {
                traders = _AbstractTradersService.SelectByID(ID).Item;
                ViewBag.Edit = "1" ;
            }

            
            return View(traders);
        }

        [HttpPost]
        public JsonResult BindTraders([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            string response = string.Empty;
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = requestModel.Search.Value;
                var model = _AbstractTradersService.SelectAll(pageParam, search);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult GetTradersDetails(long ID)
        {
            string response = string.Empty;
            try
            {
                var ShippingCategory = this._AbstractTradersService.SelectByID(ID);
                ViewBag.TraderDetails = ShippingCategory;
                return PartialView("TradersDetailsDlg");
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateStatus(long ID, bool Status)
        {
            string response = string.Empty;
            try
            {
                response = $"{{\"Result\" : \"0\", \"Message\" : \"{"Trader status changed."}\"}}";
                _AbstractTradersService.UpdateStatus(ID, Status);
                return Json(response, JsonRequestBehavior.DenyGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{"Something went wrong!Please try again."}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteTrader(long ID)
        {
            string response = string.Empty;
            try
            {
                response = $"{{\"Result\" : \"0\", \"Message\" : \"{"Trader deleted successfully."}\"}}";
                _AbstractTradersService.DeleteByID(ID);
                return Json(response, JsonRequestBehavior.DenyGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{"Something went wrong!Please try again."}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult AddTraders(Traders traders, HttpPostedFileBase AvtarURL)
        {
            try
            {
                AbstractAddress abstractTraders = new Address()
                {
                    Address1 = traders.Address1,
                    Address2 = traders.Address2,
                    Pincode = traders.Pincode,
                    City = traders.City,
                    CountryID = traders.CountryID,
                    ID = traders.Address,
                };
                SuccessResult<AbstractAddress> addressResult = _AbstractAddressService.Upsert(abstractTraders);
                if (addressResult.Item == null)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Something went wrong.Please try again");
                    return RedirectToAction(Actions.ManageProduct, Pages.Controllers.Traders, new { Area = "", prodId = "0" });
                }

                traders.CreatedBy = traders.UpdatedBy = ProjectSession.LoginUserID;
                traders.Password = traders.Password;
                traders.Address = addressResult.Item.ID;
                SuccessResult<AbstractTraders> result = _AbstractTradersService.Upsert(traders);

                if (result?.Item != null)
                {
                    if (IsImage(AvtarURL) == false)
                    {
                        TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Profile image format is wrong.");
                        return RedirectToAction(Actions.ManageProduct, Pages.Controllers.Traders, new { Area = "", prodId = result.Item.ID });
                    }

                    // Add product images
                    string profilePath = Server.MapPath("~/" + Path.Combine("Storage", "Traders", result.Item.ID.ToString()));

                    if (Directory.Exists(profilePath) == false)
                    {
                        Directory.CreateDirectory(profilePath);
                    }

                    string URLPath=string.Empty;
                    if (AvtarURL != null && AvtarURL.ContentLength > 0)
                    {
                        string fileName = Path.Combine(profilePath, "profile" + Path.GetExtension(AvtarURL.FileName));
                        AvtarURL.SaveAs(fileName);

                        result.Item.AvtarURL = Path.Combine("Storage", "Traders", result.Item.ID.ToString(), "profile" + Path.GetExtension(AvtarURL.FileName));

                        _AbstractTradersService.Upsert(result.Item);
                    }
                }

                if (result.Item != null)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Traders data saved successfully.");
                    return RedirectToAction(Actions.Index, Pages.Controllers.Traders);
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Traders is already exist.");
                    return RedirectToAction(Actions.AddTraders, Pages.Controllers.Traders, new { Area = "", prodId = result.Item.ID });
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "An error occurred on the system. Please contact the administrator.");
                return RedirectToAction(Actions.Index, Pages.Controllers.Traders);
            }
        }

        private List<SelectListItem> ProductCountryDropdownList()
        {
            try
            {
                PageParam pageParam = new PageParam()
                {
                    Limit = int.MaxValue,
                    Offset = 0
                };

                var CategoryList = _AbstractCountryService.SelectAll(pageParam);

                List<SelectListItem> items = new List<SelectListItem>();
                foreach (var category in CategoryList.Values)
                {
                    items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.ID.ToString() });
                }

                return items;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool IsImage(HttpPostedFileBase postedFile)
        {
            int ImageMinimumBytes = 512;

            //-------------------------------------------
            //  Check the image mime types
            //-------------------------------------------
            if (!string.Equals(postedFile.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(postedFile.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(postedFile.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(postedFile.ContentType, "image/gif", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(postedFile.ContentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(postedFile.ContentType, "image/png", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            //-------------------------------------------
            //  Check the image extension
            //-------------------------------------------
            var postedFileExtension = Path.GetExtension(postedFile.FileName);
            if (!string.Equals(postedFileExtension, ".jpg", StringComparison.OrdinalIgnoreCase)
                && !string.Equals(postedFileExtension, ".png", StringComparison.OrdinalIgnoreCase)
                && !string.Equals(postedFileExtension, ".gif", StringComparison.OrdinalIgnoreCase)
                && !string.Equals(postedFileExtension, ".jpeg", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            //-------------------------------------------
            //  Attempt to read the file and check the first bytes
            //-------------------------------------------
            try
            {
                if (!postedFile.InputStream.CanRead)
                {
                    return false;
                }
                //------------------------------------------
                //   Check whether the image size exceeding the limit or not
                //------------------------------------------ 
                if (postedFile.ContentLength < ImageMinimumBytes)
                {
                    return false;
                }

                byte[] buffer = new byte[ImageMinimumBytes];
                postedFile.InputStream.Read(buffer, 0, ImageMinimumBytes);
                string content = System.Text.Encoding.UTF8.GetString(buffer);
                if (Regex.IsMatch(content, @"<script|<html|<head|<title|<body|<pre|<table|<a\s+href|<img|<plaintext|<cross\-domain\-policy",
                    RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Multiline))
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }

            //-------------------------------------------
            //  Try to instantiate new Bitmap, if .NET will throw exception
            //  we can assume that it's not a valid image
            //-------------------------------------------

            try
            {
                using (var bitmap = new System.Drawing.Bitmap(postedFile.InputStream))
                {
                }
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                postedFile.InputStream.Position = 0;
            }

            return true;
        }
    }
}