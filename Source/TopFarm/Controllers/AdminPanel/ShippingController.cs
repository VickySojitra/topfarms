﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TopFarm.Common.Paging;
using TopFarm.Entities.V1;
using TopFarm.Resource;
using TopFarm.Services.Contract;
using TopFarm.Services.Contract.Shipping;

namespace TopFarm.Controllers
{
    public class ShippingController : AdminBaseController
    {
		#region Fields
		private readonly AbstractProductCategoryService _AbstractCategoryService;
		private readonly AbstractShippingService _AbstractShippingService;
		#endregion

		#region Ctor
		public ShippingController(AbstractProductCategoryService abstractCategoryService, AbstractShippingService abstractShippingService)
		{
			this._AbstractCategoryService = abstractCategoryService;

			this._AbstractShippingService = abstractShippingService;
		}
		#endregion

		// GET: Shipping
		public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }
            return View();
        }

		public ActionResult ShowShippingRate()
        {
			string response = string.Empty;

			try
            {
				ShippingRate shippingRate = new ShippingRate();
				ViewBag.ProductCategory = ProductCategoryDropdownList();

				return PartialView("ShippingDlg", shippingRate);
			}
            catch (Exception)
            {
				response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
				return Json(response, JsonRequestBehavior.DenyGet);
			}
		}

		[HttpPost]
		public JsonResult AddShippingRate(double FreeShipping, double FreeShippingAbove, int CategoryName, int ID = 0)
		{
			string response = string.Empty;
			try
			{

				ShippingRate shippingRate = new ShippingRate()
				{
					ID = ID,
					FreeShipping = FreeShipping.ToString(),
					FreeShippingAbove = FreeShippingAbove.ToString(),
					CategoryID = CategoryName
				};
				_AbstractShippingService.Upsert(shippingRate);
				response = $"{{\"Result\" : \"0\", \"Message\" : \"{ResourceCommon.MsgShippingRate}\"}}";
				return Json(response, JsonRequestBehavior.DenyGet);
			}
			catch (Exception ex)
			{
				response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
				return Json(response, JsonRequestBehavior.DenyGet);
			}
		}

		[HttpPost]
		public JsonResult BindShipping([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
		{
			string response = string.Empty;
			try
			{
				int totalRecord = 0;
				int filteredRecord = 0;

				PageParam pageParam = new PageParam();
				pageParam.Offset = requestModel.Start;
				pageParam.Limit = requestModel.Length;

				string search = requestModel.Search.Value;
				var model = _AbstractShippingService.SelectAll(pageParam, search);

				totalRecord = (int)model.TotalRecords;
				filteredRecord = (int)model.TotalRecords;

				return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
			}
			catch (Exception)
			{
				response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
				return Json(response, JsonRequestBehavior.DenyGet);
			}
		}

		[HttpPost]
		public ActionResult GetShippingRate(long ID)
		{
			string response = string.Empty;
			try
			{
				var ShippingCategory = this._AbstractShippingService.SelectByID(ID);
				ViewBag.ProductCategory = ProductCategoryDropdownList();
				return PartialView("ShippingDlg", ShippingCategory);
			}
			catch (Exception)
			{
				response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
				return Json(response, JsonRequestBehavior.DenyGet);
			}
		}

		private List<SelectListItem> ProductCategoryDropdownList()
		{
            try
            {
				PageParam pageParam = new PageParam()
				{
					Limit = int.MaxValue,
					Offset = 0
				};

				var CategoryList = _AbstractCategoryService.SelectAll(pageParam);

				List<SelectListItem> items = new List<SelectListItem>();
				foreach (var category in CategoryList.Values)
				{
					items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.ID.ToString() });
				}

				return items;
			}
            catch (Exception)
            {
				throw;
            }
		}
	}
}