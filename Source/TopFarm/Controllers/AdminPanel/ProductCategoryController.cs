﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Resource;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers
{
    public class ProductCategoryController : AdminBaseController
    {
        #region Fields
        private readonly AbstractProductCategoryService _AbstractCategoryService;
        #endregion

        #region Ctor
        public ProductCategoryController(AbstractProductCategoryService abstractCategoryService)
        {
            this._AbstractCategoryService = abstractCategoryService;

        }
        #endregion

        // GET: ProductCategory
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }
            return View();
        }

        [HttpPost]
        public JsonResult AddProductCategory(string ProductCategoryName,double ProductTax)
        {
            string response = string.Empty;
            try
            {
                ProductCategory productCategory = new ProductCategory
                {
                    Name = ProductCategoryName,
                    Tax= ProductTax,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                };

                _AbstractCategoryService.Insert(productCategory);
                response = $"{{\"Result\" : \"0\", \"Message\" : \"{ResourceCommon.MsgInsertData}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public JsonResult BindCategory([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            string response = string.Empty;
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = requestModel.Search.Value;
                var model = _AbstractCategoryService.SelectAll(pageParam, search);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public JsonResult EditProductCategory(long ID, string ProductCategoryName, bool Status,double ProductTax)
        {
            string response = string.Empty;
            try
            {
                ProductCategory productCategory = new ProductCategory
                {
                    Name = ProductCategoryName,
                    Tax = ProductTax,
                    UpdatedAt = DateTime.Now,
                    Status = Status,
                    ID = ID,
                };

                _AbstractCategoryService.Update(productCategory);
               response = $"{{\"Result\" : \"0\", \"Message\" : \"{ResourceCommon.MsgUpdateData}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public JsonResult GetProductCategory(long ID)
        {
            string response = string.Empty;
            try
            {
                AbstractProductCategory ProductCategory = _AbstractCategoryService.SelectByID(ID);
                return Json(ProductCategory, JsonRequestBehavior.DenyGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        private List<SelectListItem> ProductCategoryDropdownList()
        {
            try
            {
                PageParam pageParam = new PageParam()
                {
                    Limit = int.MaxValue,
                    Offset = 0
                };

                var CategoryList = _AbstractCategoryService.SelectAll(pageParam);

                List<SelectListItem> items = new List<SelectListItem>();
                foreach (var category in CategoryList.Values)
                {
                    items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.ID.ToString() });
                }

                return items;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}