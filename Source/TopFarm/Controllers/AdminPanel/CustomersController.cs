﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.V1;
using TopFarm.Pages;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers
{
    public class CustomersController : AdminBaseController
    {
        #region Fields        
        private readonly AbstractCustomerService _customerService;
        #endregion

        #region Ctor
        public CustomersController(AbstractCustomerService customerService)
        {
            _customerService = customerService;
        }
        #endregion

        // GET: Customers
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.BindCustomers)]
        public JsonResult BindCustomers([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;               

                var model = _customerService.SelectAll(pageParam, requestModel.Search.Value);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<Customer>(), 0, 0), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.GetCustomerDetail)]
        public JsonResult GetCustomerDetail(int customerId)
        {
            try
            {
                var model = _customerService.Select(customerId);
                return Json(model.Item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.UpdateCustomerStatus)]
        public JsonResult UpdateCustomerStatus(int customerId, int statusId)
        {
            try
            {
                var model = _customerService.Select(customerId);
                if (model.Item != null)
                {
                    model.Item.Status = (byte)statusId;
                    model.Item.UpdatedBy = ProjectSession.LoginUserID;
                    _customerService.InsertUpdate(model.Item);
                }

                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
    }
}