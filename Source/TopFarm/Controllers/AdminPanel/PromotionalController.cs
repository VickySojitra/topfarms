﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TopFarm.Common.Paging;
using TopFarm.Entities.V1;
using TopFarm.Resource;
using TopFarm.Services.Contract;
using System.Web;
using System.IO;
using TopFarm.Common;
using TopFarm.Entities.Contract;
using TopFarm.Pages;
using System.Text.RegularExpressions;

namespace TopFarm.Controllers
{
    public class PromotionalController : AdminBaseController
    {

        #region Fields
        private readonly AbstractPromotionalService _AbstractPromotionalService;
        private readonly AbstractProductService _AbstractProductService;
        private readonly AbstractPromotionTypeService _AbstractPromotionTypeService;
        #endregion

        #region Ctor
        public PromotionalController(AbstractPromotionalService abstractCategoryService, AbstractProductService productCategoryService, AbstractPromotionTypeService abstractPromotionTypeService)
        {
            this._AbstractPromotionalService = abstractCategoryService;
            _AbstractProductService = productCategoryService;
            _AbstractPromotionTypeService = abstractPromotionTypeService;
        }
        #endregion

        // GET: Promotional
        public ActionResult Index()
        {
            AbstractPromotional model;
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }
           
            return View();
        }

        public ActionResult ShowPromotionDialog()
        {
            string response = string.Empty;

            try
            {
                Promotional promotional = new Promotional();
                ViewBag.DDProductNameDropdown = ProductNameDropdown();
                ViewBag.DDPromotionType = PromotionTypeNameDropdown();
                ViewBag.DDStatus = new List<SelectListItem>() { new SelectListItem() { Text = "Active", Value = "0" },
                                                            new SelectListItem() { Text = "In-Active", Value = "1"  } };

                return PartialView("PromotionalDlg", promotional);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult AddPromotional(Promotional promotional, HttpPostedFileBase ImageURL)
        {
            try
            {
                SuccessResult<AbstractPromotional> result = _AbstractPromotionalService.Upsert(promotional);

                if (result?.Item != null)
                {
                    //if (IsImage(ImageURL) == false)
                    //{
                    //    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Profile image format is wrong.");
                    //    return RedirectToAction(Actions.Index, Pages.Controllers.Promotional, new { Area = "", prodId = result.Item.ID });
                    //}


                    string profilePath = Server.MapPath("~/" + Path.Combine("Storage", "Promotional", result.Item.ID.ToString()));

                    if (Directory.Exists(profilePath) == false)
                    {
                        Directory.CreateDirectory(profilePath);
                    }

                    string URLPath = string.Empty;
                    if (ImageURL != null && ImageURL.ContentLength > 0)
                    {
                        string fileName = Path.Combine(profilePath, "profile" + Path.GetExtension(ImageURL.FileName));
                        ImageURL.SaveAs(fileName);

                        result.Item.ImageURL = Path.Combine("/Storage", "Promotional", result.Item.ID.ToString(), "profile" + Path.GetExtension(ImageURL.FileName));

                        result = _AbstractPromotionalService.Upsert(result.Item);

                        var cookie = Request.Cookies["AdminUserLogin"];
                        if (cookie != null)
                        {
                            cookie.Values["LoginUserImg"] = result.Item.ImageURL;
                            Response.Cookies.Add(cookie);
                        }
                    }
                }
                if (result.Item != null)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Promotional data add/edit successfully.");
                    return RedirectToAction(Actions.Index, Pages.Controllers.Promotional);
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Promotional data is already exist.");
                    return RedirectToAction(Actions.Index, Pages.Controllers.Promotional, new { Area = "", prodId = result.Item.ID });
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "An error occurred on the system. Please contact the administrator.");
                return RedirectToAction(Actions.Index, Pages.Controllers.Promotional);
            }
        }

        //private bool IsImage(HttpPostedFileBase postedFile)
        //{
        //    int ImageMinimumBytes = 512;

        //    //-------------------------------------------
        //    //  Check the image mime types
        //    //-------------------------------------------
        //    if (!string.Equals(postedFile.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
        //        !string.Equals(postedFile.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
        //        !string.Equals(postedFile.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
        //        !string.Equals(postedFile.ContentType, "image/gif", StringComparison.OrdinalIgnoreCase) &&
        //        !string.Equals(postedFile.ContentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
        //        !string.Equals(postedFile.ContentType, "image/png", StringComparison.OrdinalIgnoreCase))
        //    {
        //        return false;
        //    }

        //    //-------------------------------------------
        //    //  Check the image extension
        //    //-------------------------------------------
        //    var postedFileExtension = Path.GetExtension(postedFile.FileName);
        //    if (!string.Equals(postedFileExtension, ".jpg", StringComparison.OrdinalIgnoreCase)
        //        && !string.Equals(postedFileExtension, ".png", StringComparison.OrdinalIgnoreCase)
        //        && !string.Equals(postedFileExtension, ".gif", StringComparison.OrdinalIgnoreCase)
        //        && !string.Equals(postedFileExtension, ".jpeg", StringComparison.OrdinalIgnoreCase))
        //    {
        //        return false;
        //    }

        //    //-------------------------------------------
        //    //  Attempt to read the file and check the first bytes
        //    //-------------------------------------------
        //    try
        //    {
        //        if (!postedFile.InputStream.CanRead)
        //        {
        //            return false;
        //        }
        //        //------------------------------------------
        //        //   Check whether the image size exceeding the limit or not
        //        //------------------------------------------ 
        //        if (postedFile.ContentLength < ImageMinimumBytes)
        //        {
        //            return false;
        //        }

        //        byte[] buffer = new byte[ImageMinimumBytes];
        //        postedFile.InputStream.Read(buffer, 0, ImageMinimumBytes);
        //        string content = System.Text.Encoding.UTF8.GetString(buffer);
        //        if (Regex.IsMatch(content, @"<script|<html|<head|<title|<body|<pre|<table|<a\s+href|<img|<plaintext|<cross\-domain\-policy",
        //            RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Multiline))
        //        {
        //            return false;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }

        //    //-------------------------------------------
        //    //  Try to instantiate new Bitmap, if .NET will throw exception
        //    //  we can assume that it's not a valid image
        //    //-------------------------------------------

        //    try
        //    {
        //        using (var bitmap = new System.Drawing.Bitmap(postedFile.InputStream))
        //        {
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //    finally
        //    {
        //        postedFile.InputStream.Position = 0;
        //    }

        //    return true;
        //}
        [HttpPost]
        public ActionResult GetPromotional(long ID)
        {
            string response = string.Empty;
            try
            {
                var ShippingCategory = this._AbstractPromotionalService.SelectByID(ID);
                ViewBag.DDProductNameDropdown = ProductNameDropdown();
                ViewBag.DDPromotionType = PromotionTypeNameDropdown();
                ViewBag.DDStatus = new List<SelectListItem>() { new SelectListItem() { Text = "Active", Value = "true" },
                                                            new SelectListItem() { Text = "In-Active", Value = "false"  } };
                if (!string.IsNullOrEmpty(ShippingCategory.ImageURL))
                {
                    ViewBag.IsImage = true;
                }
                return PartialView("PromotionalDlg", ShippingCategory);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public JsonResult BindPromotional([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            string response = string.Empty;
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = requestModel.Search.Value;
                var model = _AbstractPromotionalService.SelectAll(pageParam, search);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        private List<SelectListItem> ProductNameDropdown()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var category = _AbstractProductService.SelectAll(pageParam, "");

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category1 in category.Values)
            {
                items.Add(new SelectListItem() { Text = category1.Name.ToString(), Value = category1.ID.ToString() });
            }

            return items;
        }

        private List<SelectListItem> PromotionTypeNameDropdown()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var category = _AbstractPromotionTypeService.SelectAll(pageParam, "");

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category1 in category.Values)
            {
                items.Add(new SelectListItem() { Text = category1.Name.ToString(), Value = category1.ID.ToString() });
            }

            return items;
        }

    }
}