﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.V1;
using TopFarm.Pages;
using TopFarm.Resource;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers
{
    public class SupportTicketController : AdminBaseController
    {
        #region Fields
        private readonly AbstractSupportTicketService _abstractSupportTicketService;
        #endregion

        #region Ctor
        public SupportTicketController(AbstractSupportTicketService abstractSupportTicketService)
        {
            this._abstractSupportTicketService = abstractSupportTicketService;
        }
        #endregion

        // GET: SupportTicket
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }
            
            return View();
        }

        [HttpPost]
        public JsonResult BindSupportTicket([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            string response = string.Empty;
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = requestModel.Search.Value;
                var model = _abstractSupportTicketService.SelectAll(pageParam, search);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpGet]
        [ActionName(Actions.GetSupportTicketMetaData)]
        public JsonResult GetSupportTicketMetaData()
        {
            try
            {
                int[] Tickets = _abstractSupportTicketService.TicketCount();
                return Json(Tickets, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                int[] Tickets = new int[] { 0, 0, 0 };
                return Json(Tickets, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateTicketStatus(long ID, bool Status)
        {
            string response = string.Empty;
            try
            {
                response = $"{{\"Result\" : \"0\", \"Message\" : \"{ResourceCommon.MsgChangeStatusTicket}\"}}";
                _abstractSupportTicketService.UpdateStatus(ID, Status);
                return Json(response, JsonRequestBehavior.DenyGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddSupportTicketorComment)]
        public JsonResult AddSupportTicketorComment(string ticketDescription, int ticketId = 0, bool status = true)
        {
            string response = string.Empty;
            try
            {
                SupportTicket ticket = new SupportTicket()
                {
                    ID = ticketId,
                    CustomerID = ProjectSession.LoginUserID,
                    Status = status,
                    DelFlg = false,
                    CreatedBy = ProjectSession.LoginUserID,
                    UpdatedBy = ProjectSession.LoginUserID
                };

                var result = _abstractSupportTicketService.InsertUpdate(ticket);

                if (result.IsSuccessStatusCode == false)
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }

                SupportComment comment = new SupportComment()
                {
                    SupportTicketID = result.Item.ID,
                    RoleID = (int)ProjectSession.LoginRoleID,
                    UserID = ProjectSession.LoginUserID,
                    Comment = ticketDescription,
                    DelFlg = false,
                    CreatedBy = ProjectSession.LoginUserID,
                    UpdatedBy = ProjectSession.LoginUserID
                };

                var result1 = _abstractSupportTicketService.InsertUpdateComment(comment);

                if (result1.IsSuccessStatusCode == true)
                {
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.Current.Error(ex);
            }

            return Json(0, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [ActionName(Actions.GetTicketComments)]
        public JsonResult GetTicketComments(long ticketId)
        {
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = int.MaxValue;

                var model = _abstractSupportTicketService.SelectCommentsByTicketId(pageParam, ticketId);
                return Json(model.Values, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
    }
}