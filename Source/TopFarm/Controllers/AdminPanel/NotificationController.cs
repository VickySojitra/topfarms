﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Resource;
using TopFarm.Services.Contract;
using TopFarm.Pages;

namespace TopFarm.Controllers
{
    public class NotificationController : Controller
    {
        #region Fields
        private readonly AbstractNotificationService _AbstractNotificationService;
        private readonly AbstractCommonService _commonService;
        private readonly AbstractOrdersService _orderService;
        #endregion

        #region Ctor
        public NotificationController(AbstractNotificationService abstractNotificationService, AbstractCommonService commonService, AbstractOrdersService orderService)
        {
            this._AbstractNotificationService = abstractNotificationService;
            _commonService = commonService;
            _orderService = orderService;
        }
        #endregion
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.GetAdminOrderDetails)]
        public JsonResult GetAdminOrderDetails(int Id)
        {
            try
            {
                var model = _orderService.Select(Id);
                return Json(model.Item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult BindNotification()
        {
            string response = string.Empty;
            try
            {
                
                var model = _AbstractNotificationService.Notifications_ByRoleID("",3,0);


                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }
    }
}