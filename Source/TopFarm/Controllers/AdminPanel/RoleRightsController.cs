﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TopFarm.Controllers.AdminPanel
{
    public class RoleRightsController : Controller
    {
        // GET: RoleRights
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }
            return View();
        }
    }
}