﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Pages;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers
{
    public class ProductController : AdminBaseController
    {
        #region Fields
        private readonly AbstractProductService _productService;
        private readonly AbstractProductCategoryService _productCategoryService;
        private readonly AbstractProductReviewService _productReviewService;
        #endregion

        public ProductController(AbstractProductService productService, 
                                 AbstractProductCategoryService productCategoryService,
                                 AbstractProductReviewService productReviewService)
        {
            _productService = productService;
            _productCategoryService = productCategoryService;
            _productReviewService = productReviewService;
        }

        // GET: Product
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            return View();
        }

        [HttpGet]
        [ActionName(Actions.GetProductMetaData)]
        public JsonResult GetProductMetaData()
        {
            try
            {
                var ProdMetaData = _productService.GetMetaData();
                if (ProdMetaData.IsSuccessStatusCode && ProdMetaData.Item != null)
                {
                    return Json(ProdMetaData.Item, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
            }

            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.ManageProduct)]
        public ActionResult ManageProduct(long prodId = 0)
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            ViewBag.DDProductCategory = ProductCategoryDropdown();

            AbstractProduct product = null;
            if (prodId > 0)
            {
                product = _productService.Select(prodId).Item;
                product.ProductImages = _productService.ProductImagesByProductId(product.ID).Values;
            }

            if (product == null)
            {
                product = new Product();
                product.ProductImages = new List<AbstractProductImages>();
            }
            ViewBag.catId = product.CategoryID;
            return View(product);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.BindProducts)]
        public JsonResult BindProducts([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                var model = _productService.SelectAll(pageParam, requestModel.Search.Value);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<Product>(), 0, 0), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddProduct)]
        public ActionResult AddProduct(Product product, HttpPostedFileBase productimg1,
                                        HttpPostedFileBase productimg2,
                                        HttpPostedFileBase productimg3,
                                        HttpPostedFileBase productimg4,
                                        HttpPostedFileBase productimg5,
                                        HttpPostedFileBase productimg6)
        {
            try
            {
                // Add product
                product.CreatedBy = product.UpdatedBy = ProjectSession.LoginUserID;
                SuccessResult<AbstractProduct> result = _productService.InsertUpdate(product);

                if (result?.Item != null)
                {
                    // Add product images
                    string profilePath = Server.MapPath("~/" + Path.Combine("Storage", "Product", result.Item.ID.ToString()));

                    if (Directory.Exists(profilePath) == false)
                    {
                        Directory.CreateDirectory(profilePath);
                    }

                    ProductImages productImages = new ProductImages();
                    productImages.ProductId = result.Item.ID;
                    productImages.IsActive = true;
                    productImages.CreatedBy = productImages.UpdatedBy = ProjectSession.LoginUserID;

                    if (productimg1 != null && productimg1.ContentLength > 0)
                    {
                        string fileName = Path.Combine(profilePath, DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + "productimg1" + Path.GetExtension(productimg1.FileName));

                        productimg1.SaveAs(fileName);

                        productImages.URL = Path.Combine("Storage", "Product", result.Item.ID.ToString(), DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + "productimg1" + Path.GetExtension(productimg1.FileName));

                        if (product.ProductImages != null && product.ProductImages.Count > 0 && product.ProductImages[0] != null)
                        {
                            productImages.ID = product.ProductImages[0].ID;
                        }

                        productImages.IsImage = IsImage(productimg1);

                        _productService.InsertUpdateProductImage(productImages);
                    }

                    if (productimg2 != null && productimg2.ContentLength > 0)
                    {
                        string fileName = Path.Combine(profilePath, DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + "productimg2" + Path.GetExtension(productimg2.FileName));

                        //string fileName = Path.Combine(profilePath, "productimg2" + Path.GetExtension(productimg2.FileName));
                        productimg2.SaveAs(fileName);

                        productImages.URL = Path.Combine("Storage", "Product", result.Item.ID.ToString(), DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + "productimg2" + Path.GetExtension(productimg2.FileName));

                        if (product.ProductImages != null && product.ProductImages.Count > 1 && product.ProductImages[1] != null)
                        {
                            productImages.ID = product.ProductImages[1].ID;
                        }

                        productImages.IsImage = IsImage(productimg2);

                        _productService.InsertUpdateProductImage(productImages);
                    }

                    if (productimg3 != null && productimg3.ContentLength > 0)
                    {
                        string fileName = Path.Combine(profilePath, DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + "productimg3" + Path.GetExtension(productimg3.FileName));

                        //string fileName = Path.Combine(profilePath, "productimg3" + Path.GetExtension(productimg3.FileName));
                        productimg3.SaveAs(fileName);

                        productImages.URL = Path.Combine("Storage", "Product", result.Item.ID.ToString(), DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + "productimg3" + Path.GetExtension(productimg3.FileName));

                        if (product.ProductImages != null && product.ProductImages.Count > 2 && product.ProductImages[2] != null)
                        {
                            productImages.ID = product.ProductImages[2].ID;
                        }
                        productImages.IsImage = IsImage(productimg3);
                        _productService.InsertUpdateProductImage(productImages);
                    }

                    if (productimg4 != null && productimg4.ContentLength > 0)
                    {
                        string fileName = Path.Combine(profilePath, DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + "productimg4" + Path.GetExtension(productimg4.FileName));

                        //string fileName = Path.Combine(profilePath, "productimg4" + Path.GetExtension(productimg4.FileName));
                        productimg4.SaveAs(fileName);

                        productImages.URL = Path.Combine("Storage", "Product", result.Item.ID.ToString(), DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + "productimg4" + Path.GetExtension(productimg4.FileName));

                        if (product.ProductImages != null && product.ProductImages.Count > 3 && product.ProductImages[3] != null)
                        {
                            productImages.ID = product.ProductImages[3].ID;
                        }
                        productImages.IsImage = IsImage(productimg4);
                        _productService.InsertUpdateProductImage(productImages);
                    }

                    if (productimg5 != null && productimg5.ContentLength > 0)
                    {
                        string fileName = Path.Combine(profilePath, DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + "productimg5" + Path.GetExtension(productimg5.FileName));

                        //string fileName = Path.Combine(profilePath, "productimg5" + Path.GetExtension(productimg5.FileName));
                        productimg5.SaveAs(fileName);

                        productImages.URL = Path.Combine("Storage", "Product", result.Item.ID.ToString(), DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + "productimg5" + Path.GetExtension(productimg5.FileName));

                        if (product.ProductImages != null && product.ProductImages.Count > 4 && product.ProductImages[4] != null)
                        {
                            productImages.ID = product.ProductImages[4].ID;
                        }
                        productImages.IsImage = IsImage(productimg5);
                        _productService.InsertUpdateProductImage(productImages);
                    }

                    if (productimg6 != null && productimg6.ContentLength > 0)
                    {
                        string fileName = Path.Combine(profilePath, DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + "productimg6" + Path.GetExtension(productimg6.FileName));
                        //string fileName = Path.Combine(profilePath, "productimg6" + Path.GetExtension(productimg6.FileName));
                        productimg6.SaveAs(fileName);

                        productImages.URL = Path.Combine("Storage", "Product", result.Item.ID.ToString(), DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + "productimg6" + Path.GetExtension(productimg6.FileName));

                        if (product.ProductImages != null && product.ProductImages.Count > 5 && product.ProductImages[5] != null)
                        {
                            productImages.ID = product.ProductImages[5].ID;
                        }
                        productImages.IsImage = IsImage(productimg6);
                        _productService.InsertUpdateProductImage(productImages);
                    }
                }

                if (result.Item != null)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Product saved successfully.");
                    return RedirectToAction(Actions.Index, Pages.Controllers.Product, new { Area = "" });
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Product is already exist.");
                    return RedirectToAction(Actions.ManageProduct, Pages.Controllers.Product, new { Area = "", prodId = product.ID });
                }
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "An error occurred on the system. Please contact the administrator.");
                return RedirectToAction(Actions.ManageProduct, Pages.Controllers.Product, new { Area = "", prodId = product.ID });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.ChangeStatus)]
        public ActionResult ChangeStatus(long prodId = 0)
        {
            try
            {
                if (prodId > 0)
                {
                    var product = _productService.Select(prodId).Item;
                    if (product != null)
                    {
                        product.IsActive = !product.IsActive;
                        _productService.InsertUpdate(product);
                    } 
                }

                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddProductReview)]
        public ActionResult AddProductReview(string Review, int Rating, int ProductId)
        {
            string response = string.Empty;
            try
            {
                if (ProductId == 0)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Failed to save review. Please contact the administrator.");
                    return RedirectToAction(Actions.ShopSingle, Pages.Controllers.Home, new { ProductID = ProductId });
                }

                ProductReview review = new ProductReview
                {
                    Review = Review,
                    ProductID = ProductId,
                    Rating = Rating,
                    CreatedBy = ProjectSession.LoginUserID,
                    CreatedRole = ProjectSession.LoginRoleID
                };

                var result = _productReviewService.InsertUpdate(review);
                if (result.IsSuccessStatusCode)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Product review saved successfully.");
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Failed to save product review. please try again after sometime.");
                }
            }
            catch (Exception)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "An error occurred on the system. Please contact the administrator.");
            }

            return RedirectToAction(Actions.ShopSingle, Pages.Controllers.Home, new { ProductID = ProductId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.LikeDisLikeProductReview)]
        public JsonResult LikeDisLikeProductReview(int ProductReviewId, bool IsLike)
        {
            string response = string.Empty;
            try
            {
                if (ProductReviewId == 0)
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }

                ProductReviewLike review = new ProductReviewLike
                {
                    ProdReviewId = ProductReviewId,
                    Like = IsLike,
                    Dislike = !IsLike,
                    CreatedBy = ProjectSession.LoginUserID,
                    CreatedRole = ProjectSession.LoginRoleID,
                    UpdatedBy = ProjectSession.LoginUserID,
                    UpdatedRole = ProjectSession.LoginRoleID
                };

                var result = _productReviewService.ProdLikeInsertUpdate(review);

                if(result.IsSuccessStatusCode ==true)
                {
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                
            }

            return Json(0, JsonRequestBehavior.AllowGet);
        }
        #region Private Methods
        private List<SelectListItem> ProductCategoryDropdown()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var category = _productCategoryService.SelectAll(pageParam, "");

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category1 in category.Values)
            {
                items.Add(new SelectListItem() { Text = category1.Name.ToString(), Value = category1.ID.ToString() });
            }

            return items;
        }

        private bool IsImage(HttpPostedFileBase postedFile)
        {
            int ImageMinimumBytes = 512;

            //-------------------------------------------
            //  Check the image mime types
            //-------------------------------------------
            if (!string.Equals(postedFile.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(postedFile.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(postedFile.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(postedFile.ContentType, "image/gif", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(postedFile.ContentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(postedFile.ContentType, "image/png", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            //-------------------------------------------
            //  Check the image extension
            //-------------------------------------------
            var postedFileExtension = Path.GetExtension(postedFile.FileName);
            if (!string.Equals(postedFileExtension, ".jpg", StringComparison.OrdinalIgnoreCase)
                && !string.Equals(postedFileExtension, ".png", StringComparison.OrdinalIgnoreCase)
                && !string.Equals(postedFileExtension, ".gif", StringComparison.OrdinalIgnoreCase)
                && !string.Equals(postedFileExtension, ".jpeg", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            //-------------------------------------------
            //  Attempt to read the file and check the first bytes
            //-------------------------------------------
            try
            {
                if (!postedFile.InputStream.CanRead)
                {
                    return false;
                }
                //------------------------------------------
                //   Check whether the image size exceeding the limit or not
                //------------------------------------------ 
                if (postedFile.ContentLength < ImageMinimumBytes)
                {
                    return false;
                }

                byte[] buffer = new byte[ImageMinimumBytes];
                postedFile.InputStream.Read(buffer, 0, ImageMinimumBytes);
                string content = System.Text.Encoding.UTF8.GetString(buffer);
                if (Regex.IsMatch(content, @"<script|<html|<head|<title|<body|<pre|<table|<a\s+href|<img|<plaintext|<cross\-domain\-policy",
                    RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Multiline))
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }

            //-------------------------------------------
            //  Try to instantiate new Bitmap, if .NET will throw exception
            //  we can assume that it's not a valid image
            //-------------------------------------------

            try
            {
                using (var bitmap = new System.Drawing.Bitmap(postedFile.InputStream))
                {
                }
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                postedFile.InputStream.Position = 0;
            }

            return true;
        }
        #endregion
    }
}