﻿using System;
using System.Web.Mvc;
using TopFarm.Common;

namespace TopFarm.Controllers
{
    public class AdminBaseController : Controller
    {
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }

        /// <summary>
        /// Called by the ASP.NET MVC framework before the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                if (ProjectSession.LoginUserID == 0 || ProjectSession.LoginRoleID == 1) // If Customer or not logged in
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.warning.ToString(), "Please sign in...");
                    filterContext.Result = new RedirectResult("~/AdminPanel/Index");
                    return;
                }
                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}