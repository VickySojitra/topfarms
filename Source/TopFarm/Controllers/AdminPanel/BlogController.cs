﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Resource;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers
{
    public class BlogController : AdminBaseController
    {
        #region Fields
        private readonly AbstractBlogCategoryService _AbstractBlogCategoryService;
        private readonly AbstractBlogService _AbstractBlogService;
        #endregion

        #region Ctor
        public BlogController(AbstractBlogCategoryService abstractCategoryService, AbstractBlogService abstractBlogService)
        {
            this._AbstractBlogCategoryService = abstractCategoryService;
            this._AbstractBlogService = abstractBlogService;
        }
        #endregion

        // GET: Blog
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            return View();
        }

        [HttpPost]
        public ActionResult AddBlogCategory(string Name)
        {
            string response = string.Empty;
            try
            {
                BlogCategory blogCategory = new BlogCategory
                {
                    Name = Name,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                };

                _AbstractBlogCategoryService.Upsert(blogCategory);
                response = $"{{\"Result\" : \"0\", \"Message\" : \"{ResourceCommon.MsgInsertData}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }



        [HttpPost]
        public ActionResult AddBlog(long ID, long BlogCategoryID, string ArticleName, string Articles, bool Status)
        {
            string response = string.Empty;
            try
            {
                Blog blogCategory = new Blog
                {
                    ID = ID,
                    BlogCategoryID = BlogCategoryID,
                    ArticleName = ArticleName,
                    Articles = Articles,
                    Status = Status,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                };

                _AbstractBlogService.Upsert(blogCategory);
                response = $"{{\"Result\" : \"0\", \"Message\" : \"{ResourceCommon.MsgAddEditBlog}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult GetBlog(long ID = 0)
        {
            string response = string.Empty;
            try
            {
                AbstractBlog blogCategory = _AbstractBlogService.SelectByID(ID);
                return Json(blogCategory, JsonRequestBehavior.DenyGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateStatus(long ID, bool Status)
        {
            string response = string.Empty;
            try
            {
                response = $"{{\"Result\" : \"0\", \"Message\" : \"{ResourceCommon.MsgChangeStatus}\"}}";
                _AbstractBlogService.UpdateStatus(ID, Status);
                return Json(response, JsonRequestBehavior.DenyGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteBlog(long ID)
        {
            string response = string.Empty;
            try
            {
                response = $"{{\"Result\" : \"0\", \"Message\" : \"Blog deleted successfully.\"}}";
                _AbstractBlogService.DeleteByID(ID);
                return Json(response, JsonRequestBehavior.DenyGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public JsonResult BindBlogArticle([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            string response = string.Empty;
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                string search = requestModel.Search.Value;
                var model = _AbstractBlogService.SelectAll(pageParam, search);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpGet]
        public JsonResult BlogCategoryDropdownList()
        {
            try
            {
                PageParam pageParam = new PageParam()
                {
                    Limit = int.MaxValue,
                    Offset = 0
                };

                var CategoryList = _AbstractBlogCategoryService.SelectAll();

                List<SelectListItem> items = new List<SelectListItem>();
                foreach (var category in CategoryList.Values)
                {
                    items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.ID.ToString() });
                }

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}