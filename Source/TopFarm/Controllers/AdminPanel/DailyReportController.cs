﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TopFarm.Common.Paging;
using TopFarm.Entities.V1;
using TopFarm.Pages;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers
{
    public class DailyReportController : AdminBaseController
    {
        private readonly AbstractDailyReportService _dailyReportService;

        public DailyReportController(AbstractDailyReportService dailyReportService)
        {
            _dailyReportService = dailyReportService;
        }

        // GET: DailyReport
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.BindDailyReport)]
        public JsonResult BindDailyReport([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, DateTime? date = null)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                var model = _dailyReportService.SelectAll(pageParam, date);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<DailyReport>(), 0, 0), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.GetDailyReport)]
        public JsonResult GetDailyReport(int reportId)
        {
            try
            {
                var model = _dailyReportService.Select(reportId);
                return Json(model.Item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
    }
}