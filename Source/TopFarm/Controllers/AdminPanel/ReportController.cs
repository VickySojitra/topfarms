﻿using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers.AdminPanel
{
    public class ReportController : Controller
    {
        #region Fields
        private readonly AbstractTradersService _AbstractTradersService;
        private readonly AbstractCustomerService _customerService;
        private readonly AbstractOrdersService _orderService;

        #endregion

        #region Ctor
        public ReportController(AbstractTradersService abstractTradersService, AbstractCustomerService customerService, AbstractOrdersService orderService)
        {
            this._AbstractTradersService = abstractTradersService;
            this._customerService = customerService;
            this._orderService = orderService;

        }
        #endregion

        // GET: Report
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }
            return View();
        }

        public bool DownloadReport(int type, int period)
        {
            #region Get StartDate & EndDate
            DateTime startDate;
            DateTime endDate;
            DateTime now = DateTime.Now;
            if (period == 1)
            {
                startDate = new DateTime(now.Year, now.Month, 1);
                endDate = startDate.AddMonths(1).AddDays(-1);
            }
            else if (period == 2)
            {
                var today = DateTime.Today;
                var month = new DateTime(today.Year, today.Month, 1);
                startDate = month.AddMonths(-1);
                endDate = today;
            }
            else if (period == 3)
            {
                startDate = new DateTime(now.AddYears(-3).Year, DateTime.Now.AddMonths(-3).Month,1);
                endDate = now.AddDays(-1);
            }
            else
            {
                startDate = new DateTime(now.AddYears(-1).Year, DateTime.Now.Month, 1);
                endDate = now.AddDays(-1);
            }

            #endregion

            List<AbstractTraders> traderPdfData=new List<AbstractTraders>();
            List<AbstractCustomer> customersPdfData = new List<AbstractCustomer>();
            List<AbstractOrders> salesPdfData = new List<AbstractOrders>();

            if (type == 1)
            {
                var sales = _orderService.SalesReportSelectAll(startDate, endDate);
                DownloadSalesReport(sales.Values);
            }
            else if (type == 2)
            {
               var trader = _AbstractTradersService.TraderReportSelectAll(Convert.ToDateTime(startDate), Convert.ToDateTime(endDate));
                DownloadTraderReport(trader.Values);
            }
            else
            {
                var customer = _customerService.CustromerReportSelectAll(startDate,endDate);
                DownloadCustomerReport(customer.Values);
            }

            return true;
        }

        public FileStreamResult DownloadTraderReport(List<AbstractTraders> traderPdfData)
        {
            MemoryStream workStream = new MemoryStream();

            Document doc = new Document();
            PdfPTable tableLayout = new PdfPTable(5);


            Document document = new Document();
            PdfWriter.GetInstance(document, workStream).CloseStream = false;
            StringBuilder sb = new StringBuilder();
            #region html
            sb.Append("<table width='100%' cellspacing='0' cellpadding='2'>");
            sb.Append("<tr><td align='center' colspan = '2'><b>Trader Report</b></td></tr>");

            sb.Append("</td><td align = 'right'><b>Date: </b>");
            sb.Append(System.DateTime.Now.ToString("MM/dd/yyyy"));
            sb.Append(" </td></tr>");

            sb.Append("</table>");
            int count = 1;
            sb.Append("<table width='100%' cellspacing='0' cellpadding='2' border='1'>");
            sb.Append("<tr><td align='center' width = '10%' colspan = '2'><b>No.</b></td><td align='center' colspan = '2'><b>Name</b></td><td align='center' style='background-color: #18B5F0' colspan = '2'><b>EmailId</b></td><td align='center' style='background-color: #18B5F0' colspan = '2'><b>MobileNo</b></td></tr>");

            sb.Append("</table>");
            sb.Append("<table width='100%' cellspacing='0' cellpadding='2' border='1'>");
            foreach (var d in traderPdfData)
            {
                sb.Append("<tr><td align='center' style='background-color: #18B5F0' colspan = '2'><b> " + count + "</b></td>" +
                    //"<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + d.ProductName + "</b></td>" +
                    "<td align='center' style='border-left: 1px solid black; background-color: #18B5F0;' colspan = '2'><b>" + d.FirstName + " " + d.LastName + "</b></td>" +
                    "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + d.EmailID + "</b></td>" +
                    "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + d.MobileNo + "</b></td>" +

                    "</tr>");
            }
            sb.Append("</table>");

            sb.Append("<br />");
            #endregion

            StyleSheet styles = new StyleSheet();
            styles.LoadStyle("#headerdiv", HtmlTags.COLOR, "Blue");

            //styles.LoadTagStyle(HtmlTags.TD, HtmlTags.COLOR, "#ff0000");
            using (Document document1 = new Document())
            {
                PdfWriter.GetInstance(document1, Response.OutputStream);
                document1.Open();
                List<IElement> objects = HTMLWorker.ParseToList(
                  new StringReader(sb.ToString()), styles
                );
                foreach (IElement element in objects)
                {
                    document1.Add(element);
                }
                document1.Close();
            }
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=TraderReport.pdf");
            Response.Flush();
            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;
            TempData.Keep();
            return new FileStreamResult(workStream, "application/pdf");
        }

        public FileStreamResult DownloadCustomerReport(List<AbstractCustomer> customerPdfData)
        {
            MemoryStream workStream = new MemoryStream();

            Document doc = new Document();
            PdfPTable tableLayout = new PdfPTable(5);


            Document document = new Document();
            PdfWriter.GetInstance(document, workStream).CloseStream = false;
            StringBuilder sb = new StringBuilder();
            #region html
            sb.Append("<table width='100%' cellspacing='0' cellpadding='2'>");
            sb.Append("<tr><td align='center' colspan = '2'><b>Customer Report</b></td></tr>");

            sb.Append("</td><td align = 'right'><b>Date: </b>");
            sb.Append(System.DateTime.Now.ToString("MM/dd/yyyy"));
            sb.Append(" </td></tr>");

            sb.Append("</table>");
            int count = 1;
            sb.Append("<table width='100%' cellspacing='0' cellpadding='2' border='1'>");
            sb.Append("<tr><td align='center' width = '10%' colspan = '2'><b>No.</b></td><td align='center' colspan = '2'><b>Name</b></td><td align='center' style='background-color: #18B5F0' colspan = '2'><b>EmailId</b></td><td align='center' style='background-color: #18B5F0' colspan = '2'><b>MobileNo</b></td></tr>");

            sb.Append("</table>");
            sb.Append("<table width='100%' cellspacing='0' cellpadding='2' border='1'>");
            foreach (var d in customerPdfData)
            {
                sb.Append("<tr><td align='center' style='background-color: #18B5F0' colspan = '2'><b> " + count + "</b></td>" +
                    "<td align='center' style='border-left: 1px solid black; background-color: #18B5F0;' colspan = '2'><b>" + d.FirstName + " " + d.LastName + "</b></td>" +
                    "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + d.Email + "</b></td>" +
                    "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + d.Phone + "</b></td>" +

                    "</tr>");
            }
            sb.Append("</table>");

            sb.Append("<br />");
            #endregion

            StyleSheet styles = new StyleSheet();
            styles.LoadStyle("#headerdiv", HtmlTags.COLOR, "Blue");

            //styles.LoadTagStyle(HtmlTags.TD, HtmlTags.COLOR, "#ff0000");
            using (Document document1 = new Document())
            {
                PdfWriter.GetInstance(document1, Response.OutputStream);
                document1.Open();
                List<IElement> objects = HTMLWorker.ParseToList(
                  new StringReader(sb.ToString()), styles
                );
                foreach (IElement element in objects)
                {
                    document1.Add(element);
                }
                document1.Close();
            }
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=CustomerReport.pdf");
            Response.Flush();
            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;
            TempData.Keep();
            return new FileStreamResult(workStream, "application/pdf");
        }

        public FileStreamResult DownloadSalesReport(List<AbstractOrders> orderPdfData)
        {
            MemoryStream workStream = new MemoryStream();

            Document doc = new Document();
            PdfPTable tableLayout = new PdfPTable(5);


            Document document = new Document();
            PdfWriter.GetInstance(document, workStream).CloseStream = false;
            StringBuilder sb = new StringBuilder();
            #region html
            sb.Append("<table width='100%' cellspacing='0' cellpadding='2'>");
            sb.Append("<tr><td align='center' colspan = '2'><b>Sales Report</b></td></tr>");

            sb.Append("</td><td align = 'right'><b>Date: </b>");
            sb.Append(System.DateTime.Now.ToString("MM/dd/yyyy"));
            sb.Append(" </td></tr>");

            sb.Append("</table>");
            int count = 1;
            sb.Append("<table width='100%' cellspacing='0' cellpadding='2' border='1'>");
            sb.Append("<tr><td align='center' width = '10%' colspan = '2'><b>No.</b></td><td align='center' colspan = '2'><b>OrderId</b></td><td align='center' style='background-color: #18B5F0' colspan = '2'><b>EmailId</b></td><td align='center' style='background-color: #18B5F0' colspan = '2'><b>Amount</b></td></tr>");

            sb.Append("</table>");
            sb.Append("<table width='100%' cellspacing='0' cellpadding='2' border='1'>");
            foreach (var d in orderPdfData)
            {
                sb.Append("<tr><td align='center' style='background-color: #18B5F0' colspan = '2'><b> " + count + "</b></td>" +
                    "<td align='center' style='border-left: 1px solid black; background-color: #18B5F0;' colspan = '2'><b>" + d.OrderId + "</b></td>" +
                    "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + d.CustomerName + "</b></td>" +
                    "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + d.FinalAmount + "</b></td>" +

                    "</tr>");
                count++;
            }
            sb.Append("</table>");

            sb.Append("<br />");
            #endregion

            StyleSheet styles = new StyleSheet();
            styles.LoadStyle("#headerdiv", HtmlTags.COLOR, "Blue");

            //styles.LoadTagStyle(HtmlTags.TD, HtmlTags.COLOR, "#ff0000");
            using (Document document1 = new Document())
            {
                PdfWriter.GetInstance(document1, Response.OutputStream);
                document1.Open();
                List<IElement> objects = HTMLWorker.ParseToList(
                  new StringReader(sb.ToString()), styles
                );
                foreach (IElement element in objects)
                {
                    document1.Add(element);
                }
                document1.Close();
            }
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=SalesReport.pdf");
            Response.Flush();
            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;
            TempData.Keep();
            return new FileStreamResult(workStream, "application/pdf");
        }
    }
}