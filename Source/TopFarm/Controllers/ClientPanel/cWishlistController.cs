﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Pages;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers
{
    public class cWishlistController : ClientBaseController
    {
        #region Fields
        private readonly AbstractWishlistService _wishlistService;
        private readonly AbstractCommonService _commonService;
        private readonly AbstractProductCategoryService _productCategoryService;

        #endregion

        public cWishlistController(AbstractWishlistService wishlistService, AbstractCommonService commonService, AbstractProductCategoryService productCategoryService)
        {
            _wishlistService = wishlistService;
            _commonService = commonService;
            _productCategoryService = productCategoryService;
        }

        // GET: cWishlist
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }
            if (ProjectSession.LoginUserID > 0)
            {
                AbstractCommonCount cData = GetcDashboardCountList(ProjectSession.LoginUserID);
                if (cData != null)
                {
                    Session["Order"] = cData.OrdersCount;
                    Session["wishlist"] = cData.WishlistCount;
                    Session["supportticket"] = cData.SupportTicketsCount;
                    Session["cart"] = cData.CartCount;
                }
            }
            ViewBag.DDProductCategory = ProductCategoryDropdownList();
            return View();
        }

        [HttpGet]
        [ActionName(Actions.GetCustomerWishlist)]
        public JsonResult GetCustomerWishlist()
        {
            List<AbstractWishlist> items = new List<AbstractWishlist>();
            try
            {
                items = _wishlistService.SelectAllByCustomerID(ProjectSession.LoginUserID)?.Values;
            }
            catch (Exception ex)
            {
                GlobalLogger.Current.Error(ex);
            }

            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.DeleteWishList)]
        public ActionResult DeleteWishList(long ID)
        {
            string response = string.Empty;
            try
            {
                response = $"{{\"Result\" : \"0\", \"Message\" : \"{"Wishlist deleted successfully."}\"}}";
                _wishlistService.DeleteByID(ID);
                return Json(response, JsonRequestBehavior.DenyGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{"Something went wrong!Please try again."}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }
        #region GetDashboardCount

        public AbstractCommonCount GetcDashboardCountList(long ID)
        {
            try
            {
                //List<AbstractCommonCount> items = new List<AbstractCommonCount>();

                AbstractCommonCount item = _commonService.GetcDashboardCountList(ID);
                Session["Order"] = item.OrdersCount;
                Session["Wishlist"] = item.WishlistCount;
                Session["SupportTicket"] = item.SupportTicketsCount;
                Session["Notification"] = item.NotificationCount;
                Session["cart"] = item.CartCount;


                return item;
            }
            catch (Exception ex)
            {
                GlobalLogger.Current.Error(ex);
            }
            return null;
        }

        #endregion

        private List<SelectListItem> ProductCategoryDropdownList()
        {
            try
            {
                PageParam pageParam = new PageParam()
                {
                    Limit = int.MaxValue,
                    Offset = 0
                };

                var CategoryList = _productCategoryService.SelectAll(pageParam);

                List<SelectListItem> items = new List<SelectListItem>();
                foreach (var category in CategoryList.Values)
                {
                    items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.ID.ToString() });
                }

                return items;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}