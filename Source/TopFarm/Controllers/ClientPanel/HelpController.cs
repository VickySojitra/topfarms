﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Resource;
using TopFarm.Services.Contract;
using TopFarm.Common;

namespace TopFarm.Controllers
{
    public class HelpController : AdminBaseController
    {
        #region Fields
        private readonly AbstractBlogCategoryService _AbstractBlogCategoryService;
        private readonly AbstractBlogService _AbstractBlogService;
        #endregion

        #region Ctor
        public HelpController(AbstractBlogCategoryService abstractCategoryService, AbstractBlogService abstractBlogService)
        {
            this._AbstractBlogCategoryService = abstractCategoryService;
            this._AbstractBlogService = abstractBlogService;
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult BlogCategoryList()
        {
            string response = string.Empty;
            try
            {
                var model = _AbstractBlogCategoryService.SelectAll("");

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }
        public ActionResult HelpDetails()
        {
            return View();
        }
        //[HttpPost]
        //public JsonResult BlogCategoryDetails(long BlogCategoryId = 0)
        //{
        //    SuccessResult<AbstractBlog> result = _AbstractBlogService.Blog_BlogCategoryId(BlogCategoryId);
        //    result.Item = null;
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}
        public JsonResult BlogCategoryDetails(long BlogCategoryId = 0)
        {
            string response = string.Empty;
            try
            {
                var model = _AbstractBlogService.Blog_BlogCategoryId(BlogCategoryId);

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }


    }
}