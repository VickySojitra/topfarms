﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Pages;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers.ClientPanel
{
    public class CustomerAddressController : Controller
    {
        #region Fields        
        private readonly AbstractAddressService _customerAddressService;
        private readonly AbstractAddressService _addressService;
        private readonly AbstractCountryService _abstractCountryService;
        private readonly AbstractCustomerService _customerService;
        private readonly AbstractProductCategoryService _productCategoryService;


        #endregion

        #region Ctor
        public CustomerAddressController(AbstractAddressService customerAddressService, AbstractProductCategoryService productCategoryService, AbstractCustomerService customerService, AbstractAddressService addressService, AbstractCountryService abstractCountryService)
        {
            _customerAddressService = customerAddressService;
            _addressService = addressService;
            _abstractCountryService = abstractCountryService;
            _customerService = customerService;
            _productCategoryService = productCategoryService;
        }
        #endregion

        // GET: CustomerAddress
        //[ValidateAntiForgeryToken]
        [ActionName(Actions.Index)]
        public ActionResult Index()
        {
            //TempData["Update"] = false;
            //TempData["Add"] = false;
            Customer customer = new Customer();
            customer.CustomerId = ProjectSession.LoginUserID;
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }
            ViewBag.Country = ProductCountryDropdownList();
            ViewBag.DDProductCategory = ProductCategoryDropdownList();
            return View(customer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.BindCustomerAddress)]
        public JsonResult BindCustomerAddress([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                var model = _customerAddressService.SelectAll(pageParam, requestModel.Search.Value);
                //Session["TEST"] = "test";

                //foreach (Address v in model.Values)
                //{
                //    if (v.IsPrimaryAddress == true)
                //    {
                //        Session["IsAllowPrimary"] = true;
                //        break;
                //    }
                //    else {
                //        Session["IsAllowPrimary"] = false;
                //    }
                //}

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;
                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<Address>(), 0, 0), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult UpdateCustomerAddress(Customer customer)
        {
            try
            {
                #region Add Address
                // Add product images
                Address address = new Address()
                {
                    ID = customer.ID,
                    CustomerId = ProjectSession.LoginUserID,
                    Address1 = customer.Address1,
                    Address2 = customer.Address2,
                    Pincode = customer.Pincode,
                    Area = customer.Area,
                    City = customer.City,
                    State = customer.State,
                    CountryID = customer.CountryID,
                    CreatedBy = ProjectSession.LoginUserID,
                    CreatedAt = System.DateTime.Now,
                    IsPrimaryAddress = customer.IsPrimaryAddress,
                    UpdatedBy = ProjectSession.LoginUserID,
                    UpdatedAt = System.DateTime.Now
                };
                SuccessResult<AbstractAddress> adrsresult = _addressService.Upsert(address);
                #endregion
                if (address.ID > 0)
                {
                    TempData["openPopup"] = "Customer address updated successfully";

                }
                else
                {
                    TempData["openPopup"] = "Customer address added successfully";
                }
                ViewBag.Country = ProductCountryDropdownList();

                //return RedirectToAction("Index", "CustomerAddress");
                return Json(adrsresult, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private List<SelectListItem> ProductCountryDropdownList()
        {
            try
            {
                PageParam pageParam = new PageParam()
                {
                    Limit = int.MaxValue,
                    Offset = 0
                };

                var CategoryList = _abstractCountryService.SelectAll(pageParam);

                List<SelectListItem> items = new List<SelectListItem>();
                foreach (var category in CategoryList.Values)
                {
                    items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.ID.ToString() });
                }

                return items;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public JsonResult GetAddressDatabyId(int id)
        {
            try
            {
                if (id > 0)
                {
                    var addressData = _addressService.AddressByID(id);
                    int cid = Convert.ToInt32(addressData.CustomerId);
                    var customerData = _customerService.GetCustomerById(cid).Item;
                    return Json(new { data = addressData, cdata = customerData }, JsonRequestBehavior.AllowGet);
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public JsonResult DeleteAddressDatabyId(int id)
        {
            try
            {
                var model = _customerAddressService.Delete(id);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        private List<SelectListItem> ProductCategoryDropdownList()
        {
            try
            {
                PageParam pageParam = new PageParam()
                {
                    Limit = int.MaxValue,
                    Offset = 0
                };

                var CategoryList = _productCategoryService.SelectAll(pageParam);

                List<SelectListItem> items = new List<SelectListItem>();
                foreach (var category in CategoryList.Values)
                {
                    items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.ID.ToString() });
                }

                return items;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}