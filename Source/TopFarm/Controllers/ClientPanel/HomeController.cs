﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Pages;
using TopFarm.Services.Contract;
using TopFarm.Resource;


namespace TopFarm.Controllers
{
    /// <summary>
    /// Home (Main page)
    /// </summary>
    /// <remarks>
    /// Do not add any method which performs insert / update operations
    /// Or requires values from sessions
    /// </remarks>
    public class HomeController : Controller
    {
        #region Fields
        private readonly AbstractProductService _productService;
        private readonly AbstractProductCategoryService _productCategoryService;
        private readonly AbstractProductReviewService _productReviewService;
        private readonly AbstractWishlistService _wishlistService;
        private readonly AbstractCartService _cartService;
        private readonly AbstractCommonService _commonService;
        private readonly AbstractOrdersService _orderService;
        private readonly AbstractNotificationService _AbstractNotificationService;

        #endregion

        #region Ctor

        public HomeController(AbstractProductService productService,
                              AbstractProductCategoryService productCategoryService,
                              AbstractProductReviewService productReviewService,
                              AbstractWishlistService wishlistService,
                              AbstractCartService cartService,
                              AbstractCommonService commonService,
                              AbstractOrdersService orderService,
                              AbstractNotificationService abstractNotificationService)
        {
            _productService = productService;
            _productCategoryService = productCategoryService;
            _productReviewService = productReviewService;
            _wishlistService = wishlistService;
            _cartService = cartService;
            _commonService = commonService;
            _orderService = orderService;
            this._AbstractNotificationService = abstractNotificationService;
        }

        #endregion

        #region Index / Home page Actions

        // GET: Home
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            try
            {
                if (ProjectSession.LoginUserID > 0)
                {
                    AbstractCommonCount cData = GetcDashboardCountList(ProjectSession.LoginUserID);
                    if (cData != null)
                    {
                        Session["Order"] = cData.OrdersCount;
                        Session["wishlist"] = cData.WishlistCount;
                        Session["supportticket"] = cData.SupportTicketsCount;
                        Session["cart"] = cData.CartCount;
                    }
                }
                ViewBag.DDProductCategory = ProductCategoryDropdown();
            }
            catch (Exception ex)
            {
                GlobalLogger.Current.Error(ex);
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "An error occurred on the system. Please contact the administrator.");
            }

            return View();
        }

        #endregion

        #region Shop Single Page Actions

        [HttpGet]
        //[ValidateAntiForgeryToken]
        [ActionName(Actions.ShopSingle)]
        public ActionResult ShopSingle(int ProductID)
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            AbstractProduct product = null;

            try
            {
                if (ProductID > 0)
                {
                    product = _productService.Select(ProductID).Item;
                    product.ProductImages = _productService.ProductImagesByProductId(product.ID).Values;

                    if (product.ProductImages == null || product.ProductImages.Count == 0)
                    {
                        product.ProductImages = new List<AbstractProductImages>()
                        {
                            new ProductImages()
                            {
                                IsImage = true,
                                URL = "/img/No-prod-images.png"
                            }
                        };
                    }

                    try
                    {
                        if (ProjectSession.LoginUserID != 0 && product != null)
                        {
                            var wishlist = _wishlistService.SelectAllByCustomerID(ProjectSession.LoginUserID)?.Values;

                            if (wishlist?.Count > 0)
                            {
                                var arr = wishlist.Select(a => a.ProductId).ToList();
                                product.IsWishlisted = arr.Contains(product.ID);
                            }

                            var cart = _cartService.SelectAllByCustomerID(ProjectSession.LoginUserID)?.Values;

                            if (cart?.Count > 0)
                            {
                                var arr = cart.Select(a => a.ProductId).ToList();
                                product.IsInCart = arr.Contains(product.ID);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        GlobalLogger.Current.Error(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "An error occurred on the system. Please contact the administrator.");
                GlobalLogger.Current.Error(ex);
            }

            if (product == null)
            {
                product = new Product();
                product.ProductImages = new List<AbstractProductImages>();
            }
            ViewBag.DDProductCategory = ProductCategoryDropdown();

            return View(product);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.GetProductReview)]
        public JsonResult GetProductReview(int ProductID, int offset = 0, int limit = -1, int SortBy = 0)
        {
            List<AbstractProductReview> items = new List<AbstractProductReview>();
            try
            {
                // Get product reviews
                PageParam pageParam = new PageParam();
                pageParam.Offset = offset;
                pageParam.Limit = limit == -1 ? int.MaxValue : limit;

                items = _productReviewService.SelectAll(pageParam, "", ProductID, SortBy)?.Values;
            }
            catch (Exception ex)
            {
            }

            return Json(items);
        }

        #endregion

        #region Shop Grid page Actions
        [ActionName(Actions.ShopGrid)]
        public ActionResult ShopGrid(long categoryId = 0)
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            ViewBag.FilterCategoryId = categoryId;

            try
            {
                long MaxProductPrice = 1000;

                var ProdMetaData = _productService.GetMetaData();
                if (ProdMetaData.IsSuccessStatusCode && ProdMetaData.Item != null)
                {
                    MaxProductPrice = (long)Math.Ceiling(ProdMetaData.Item.MaxProductPrice);
                }

                ViewBag.ProductCategory = ProductCategoryDropdown();
                ViewBag.MaxProductPrice = MaxProductPrice;
                if (ProjectSession.LoginUserID > 0)
                {
                    AbstractCommonCount cData = GetcDashboardCountList(ProjectSession.LoginUserID);
                    if (cData != null)
                    {
                        Session["Order"] = cData.OrdersCount;
                        Session["wishlist"] = cData.WishlistCount;
                        Session["supportticket"] = cData.SupportTicketsCount;
                        Session["cart"] = cData.CartCount;
                    }
                }
                ViewBag.DDProductCategory = ProductCategoryDropdown();
            }
            catch (Exception ex)
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "An error occurred on the system. Please contact the administrator.");
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.GetProducts)]
        public JsonResult GetProducts(int offset = 0, int limit = 9, int SortBy = 0, long categoryId = 0, decimal minPrice = 0, decimal maxPrice = 0)
        {
            List<AbstractProduct> items = new List<AbstractProduct>();
            try
            {
                // Get product reviews
                PageParam pageParam = new PageParam();
                pageParam.Offset = offset;
                pageParam.Limit = limit;

                items = _productService.GetProductsByFilter(pageParam, "", categoryId, (ProductSortTypes)SortBy, minPrice, maxPrice)?.Values;

                try
                {
                    if (ProjectSession.LoginUserID != 0 &&
                        (items != null && items.Count > 0))
                    {
                        var wishlist = _wishlistService.SelectAllByCustomerID(ProjectSession.LoginUserID)?.Values;

                        if (wishlist?.Count > 0)
                        {
                            var arr = wishlist.Select(a => a.ProductId).ToList();
                            if (items?.Count > 0)
                            {
                                items.ForEach(a => { if (arr.Contains(a.ID)) { a.IsWishlisted = true; } });
                            }
                        }

                        var cart = _cartService.SelectAllByCustomerID(ProjectSession.LoginUserID)?.Values;

                        if (cart?.Count > 0)
                        {
                            var arr = cart.Select(a => a.ProductId).ToList();
                            if (items?.Count > 0)
                            {
                                items.ForEach(a => { if (arr.Contains(a.ID)) { a.IsInCart = true; } });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    GlobalLogger.Current.Error(ex);
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.Current.Error(ex);
            }

            return Json(items);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.GetpriceProducts)]
        public JsonResult GetpriceProducts(Product priceSearch, int offset = 0, int limit = 9, int SortBy = 0, long categoryId = 0, decimal minPrice = 0, decimal maxPrice = 0)
        {
            List<AbstractProduct> items = new List<AbstractProduct>();
            long TotalRecords = 0;
            try
            {
                // Get product reviews
                PageParam pageParam = new PageParam();
                pageParam.Offset = offset;
                pageParam.Limit = limit;

                var res = _productService.GetProductpriceByFilter(pageParam, priceSearch, categoryId, (ProductSortTypes)SortBy, minPrice, maxPrice);

                items = res?.Values;
                TotalRecords = (long)res?.TotalRecords;

                try
                {
                    if (ProjectSession.LoginUserID != 0 &&
                        (items != null && items.Count > 0))
                    {
                        var wishlist = _wishlistService.SelectAllByCustomerID(ProjectSession.LoginUserID)?.Values;

                        if (wishlist?.Count > 0)
                        {
                            var arr = wishlist.Select(a => a.ProductId).ToList();
                            if (items?.Count > 0)
                            {
                                items.ForEach(a => { if (arr.Contains(a.ID)) { a.IsWishlisted = true; } });
                            }
                        }

                        var cart = _cartService.SelectAllByCustomerID(ProjectSession.LoginUserID)?.Values;

                        if (cart?.Count > 0)
                        {
                            var arr = cart.Select(a => a.ProductId).ToList();
                            if (items?.Count > 0)
                            {
                                items.ForEach(a => { if (arr.Contains(a.ID)) { a.IsInCart = true; } });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    GlobalLogger.Current.Error(ex);
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.Current.Error(ex);
            }

            return Json(new { items, TotalRecords });
        }

        #endregion

        #region Private methods
        private List<SelectListItem> ProductCategoryDropdown()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var category = _productCategoryService.SelectAll(pageParam, "");

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category1 in category.Values)
            {
                items.Add(new SelectListItem() { Text = category1.Name.ToString(), Value = category1.ID.ToString() });
            }

            return items;
        }
        #endregion

        #region GetDashboardCount

        public AbstractCommonCount GetcDashboardCountList(long ID)
        {
            try
            {
                //List<AbstractCommonCount> items = new List<AbstractCommonCount>();

                AbstractCommonCount item = _commonService.GetcDashboardCountList(ID);
                Session["Order"] = item.OrdersCount;
                Session["Wishlist"] = item.WishlistCount;
                Session["SupportTicket"] = item.SupportTicketsCount;
                Session["Notification"] = item.NotificationCount;
                Session["cart"] = item.CartCount;


                return item;
            }
            catch (Exception ex)
            {
                GlobalLogger.Current.Error(ex);
            }
            return null;
        }

        #endregion

        #region GetProductByCategory

        [HttpGet]
        [ActionName(Actions.GetProductByCategory)]
        public JsonResult GetProductByCategory(int id)
        {
            try
            {
                var prodList = _productService.GetProductByCategory(id);
                return Json(prodList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpGet]
        [ActionName(Actions.GetProductPriceList)]
        public JsonResult GetProductPriceList()
        {
            try
            {
                var prodList = _productService.GetProductPriceList().Values;
                return Json(prodList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region Notificatio0n

        public ActionResult Notification()
        {

            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            try
            {
                if (ProjectSession.LoginUserID > 0)
                {
                    AbstractCommonCount cData = GetcDashboardCountList(ProjectSession.LoginUserID);
                    if (cData != null)
                    {
                        Session["Order"] = cData.OrdersCount;
                        Session["wishlist"] = cData.WishlistCount;
                        Session["supportticket"] = cData.SupportTicketsCount;
                        Session["cart"] = cData.CartCount;
                    }
                }
                ViewBag.DDProductCategory = ProductCategoryDropdown();
            }
            catch (Exception ex)
            {
                GlobalLogger.Current.Error(ex);
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "An error occurred on the system. Please contact the administrator.");
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.GetCustomerOrderDetail)]
        public JsonResult GetCustomerOrderDetail(int Id)
        {
            try
            {
                var model = _orderService.Select(Id);
                return Json(model.Item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult BindNotification()
        {
            string response = string.Empty;
            try
            {

                var model = _AbstractNotificationService.Notifications_ByRoleID("", 1, 0);


                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        public JsonResult OrderIdListById()
        {
            try
            {
                var data = _orderService.OrderIdListById(Convert.ToInt32(ProjectSession.LoginUserID)).Values;
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}