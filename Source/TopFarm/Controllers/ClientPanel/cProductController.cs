﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.V1;
using TopFarm.Pages;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers
{
    public class cProductController : ClientBaseController
    {
        #region Fields
        private readonly AbstractProductService _productService;
        private readonly AbstractProductCategoryService _productCategoryService;
        private readonly AbstractProductReviewService _productReviewService;
        private readonly AbstractCartService _cartService;
        private readonly AbstractWishlistService _wishlistService;
        #endregion

        public cProductController(AbstractProductService productService, 
                                 AbstractProductCategoryService productCategoryService,
                                 AbstractProductReviewService productReviewService,
                                 AbstractCartService cartService,
                                 AbstractWishlistService wishlistService)
        {
            _productService = productService;
            _productCategoryService = productCategoryService;
            _productReviewService = productReviewService;
            _cartService = cartService;
            _wishlistService = wishlistService;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddProductReview)]
        public ActionResult AddProductReview(string Review, int Rating, int ProductId)
        {
            string response = string.Empty;
            try
            {
                if (ProductId == 0)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Failed to save review. Please contact the administrator.");
                    return RedirectToAction(Actions.ShopSingle, Pages.Controllers.Home, new { ProductID = ProductId });
                }

                ProductReview review = new ProductReview
                {
                    Review = Review,
                    ProductID = ProductId,
                    Rating = Rating,
                    CreatedBy = ProjectSession.LoginUserID,
                    CreatedRole = ProjectSession.LoginRoleID
                };

                var result = _productReviewService.InsertUpdate(review);
                if (result.IsSuccessStatusCode)
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), "Product review saved successfully.");
                }
                else
                {
                    TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "Failed to save product review. please try again after sometime.");
                }
            }
            catch (Exception)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "An error occurred on the system. Please contact the administrator.");
            }

            return RedirectToAction(Actions.ShopSingle, Pages.Controllers.Home, new { ProductID = ProductId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.LikeDisLikeProductReview)]
        public JsonResult LikeDisLikeProductReview(int ProductReviewId, bool IsLike)
        {
            string response = string.Empty;
            try
            {
                if (ProductReviewId == 0)
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }

                ProductReviewLike review = new ProductReviewLike
                {
                    ProdReviewId = ProductReviewId,
                    Like = IsLike,
                    Dislike = !IsLike,
                    CreatedBy = ProjectSession.LoginUserID,
                    CreatedRole = ProjectSession.LoginRoleID,
                    UpdatedBy = ProjectSession.LoginUserID,
                    UpdatedRole = ProjectSession.LoginRoleID
                };

                var result = _productReviewService.ProdLikeInsertUpdate(review);

                if(result.IsSuccessStatusCode ==true)
                {
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                
            }

            return Json(0, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddToCart)]
        public JsonResult AddToCart(int ProductId, int Quantity)
        {
            string response = string.Empty;
            try
            {
                if (ProductId == 0)
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }

                Cart cart = new Cart
                {
                    ProductId = ProductId,
                    Quantity = Quantity,
                    CustomerId = ProjectSession.LoginUserID
                };

                var result = _cartService.Upsert(cart);

                if (result.IsSuccessStatusCode == true)
                {
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
            }

            return Json(0, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddToWishlist)]
        public JsonResult AddToWishlist(int ProductId)
        {
            string response = string.Empty;
            try
            {
                if (ProductId == 0)
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }

                Wishlist cart = new Wishlist
                {
                    ProductId = ProductId,
                    CustomerId = ProjectSession.LoginUserID
                };

                var result = _wishlistService.Upsert(cart);

                if (result.IsSuccessStatusCode == true)
                {
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
            }

            return Json(0, JsonRequestBehavior.AllowGet);
        }

        #region Private Methods
        private List<SelectListItem> ProductCategoryDropdown()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var category = _productCategoryService.SelectAll(pageParam, "");

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category1 in category.Values)
            {
                items.Add(new SelectListItem() { Text = category1.Name.ToString(), Value = category1.ID.ToString() });
            }

            return items;
        }

        private bool IsImage(HttpPostedFileBase postedFile)
        {
            int ImageMinimumBytes = 512;

            //-------------------------------------------
            //  Check the image mime types
            //-------------------------------------------
            if (!string.Equals(postedFile.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(postedFile.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(postedFile.ContentType, "image/pjpeg", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(postedFile.ContentType, "image/gif", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(postedFile.ContentType, "image/x-png", StringComparison.OrdinalIgnoreCase) &&
                !string.Equals(postedFile.ContentType, "image/png", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            //-------------------------------------------
            //  Check the image extension
            //-------------------------------------------
            var postedFileExtension = Path.GetExtension(postedFile.FileName);
            if (!string.Equals(postedFileExtension, ".jpg", StringComparison.OrdinalIgnoreCase)
                && !string.Equals(postedFileExtension, ".png", StringComparison.OrdinalIgnoreCase)
                && !string.Equals(postedFileExtension, ".gif", StringComparison.OrdinalIgnoreCase)
                && !string.Equals(postedFileExtension, ".jpeg", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            //-------------------------------------------
            //  Attempt to read the file and check the first bytes
            //-------------------------------------------
            try
            {
                if (!postedFile.InputStream.CanRead)
                {
                    return false;
                }
                //------------------------------------------
                //   Check whether the image size exceeding the limit or not
                //------------------------------------------ 
                if (postedFile.ContentLength < ImageMinimumBytes)
                {
                    return false;
                }

                byte[] buffer = new byte[ImageMinimumBytes];
                postedFile.InputStream.Read(buffer, 0, ImageMinimumBytes);
                string content = System.Text.Encoding.UTF8.GetString(buffer);
                if (Regex.IsMatch(content, @"<script|<html|<head|<title|<body|<pre|<table|<a\s+href|<img|<plaintext|<cross\-domain\-policy",
                    RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Multiline))
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }

            //-------------------------------------------
            //  Try to instantiate new Bitmap, if .NET will throw exception
            //  we can assume that it's not a valid image
            //-------------------------------------------

            try
            {
                using (var bitmap = new System.Drawing.Bitmap(postedFile.InputStream))
                {
                }
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                postedFile.InputStream.Position = 0;
            }

            return true;
        }
        #endregion
    }
}