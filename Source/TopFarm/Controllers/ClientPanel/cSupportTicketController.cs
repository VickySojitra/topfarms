﻿using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Pages;
using TopFarm.Resource;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers
{
    public class cSupportTicketController : ClientBaseController
    {
        #region Fields
        private readonly AbstractSupportTicketService _supportTicketService;
        private readonly AbstractProductCategoryService _productCategoryService;

        #endregion

        #region Ctor
        public cSupportTicketController(AbstractSupportTicketService abstractSupportTicketService, AbstractProductCategoryService productCategoryService)
        {
            this._supportTicketService = abstractSupportTicketService;
            _productCategoryService = productCategoryService;

        }
        #endregion

        // GET: cSupportTicket
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }
            ViewBag.DDProductCategory = ProductCategoryDropdownList();
            return View();
        }

        [HttpGet]
        [ActionName(Actions.TicketDetail)]
        public ActionResult TicketDetail(int ticketId)
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            AbstractSupportTicket ticket = null;

            try
            {
                ticket = _supportTicketService.SelectbyID(ticketId)?.Item;

                if(ticket == null)
                {
                    ticket = new SupportTicket();
                }
            }
            catch(Exception ex)
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), "An error occurred on the system. Please contact the administrator.");
            }

            return View(ticket);
        }

        [HttpPost]
        [ActionName(Actions.BindSupportTicket)]
        public JsonResult BindSupportTicket([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int status = -1)
        {
            string response = string.Empty;
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                
                var model = _supportTicketService.SelectbyCustomerID(pageParam, ProjectSession.LoginUserID, status);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.AddSupportTicketorComment)]
        public JsonResult AddSupportTicketorComment(string ticketDescription, int ticketId= 0, bool status = true)
        {
            string response = string.Empty;
            try
            {
                SupportTicket ticket = new SupportTicket()
                {
                    ID = ticketId,
                    CustomerID = ProjectSession.LoginUserID,
                    Status= status,
                    DelFlg = false,
                    CreatedBy = ProjectSession.LoginUserID,
                    UpdatedBy = ProjectSession.LoginUserID
                };

                var result = _supportTicketService.InsertUpdate(ticket);

                if (result.IsSuccessStatusCode == false)
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }

                SupportComment comment = new SupportComment()
                {
                    SupportTicketID = result.Item.ID,
                    RoleID = (int)ProjectSession.LoginRoleID,
                    UserID = ProjectSession.LoginUserID,
                    Comment = ticketDescription,
                    DelFlg = false,
                    CreatedBy = ProjectSession.LoginUserID,
                    UpdatedBy = ProjectSession.LoginUserID
                };

                var result1 = _supportTicketService.InsertUpdateComment(comment);

                if (result1.IsSuccessStatusCode == true)
                {
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                GlobalLogger.Current.Error(ex);
            }

            return Json(0, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.GetTicketComments)]
        public JsonResult GetTicketComments(long ticketId)
        {
            try
            {
                PageParam pageParam = new PageParam();
                pageParam.Offset = 0;
                pageParam.Limit = int.MaxValue;

                var model = _supportTicketService.SelectCommentsByTicketId(pageParam, ticketId);
                return Json(model.Values, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }


        private List<SelectListItem> ProductCategoryDropdownList()
        {
            try
            {
                PageParam pageParam = new PageParam()
                {
                    Limit = int.MaxValue,
                    Offset = 0
                };

                var CategoryList = _productCategoryService.SelectAll(pageParam);

                List<SelectListItem> items = new List<SelectListItem>();
                foreach (var category in CategoryList.Values)
                {
                    items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.ID.ToString() });
                }

                return items;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}