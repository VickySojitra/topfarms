﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Data.Contract;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Common;
using TopFarm.Pages;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers
{
    public class cCartController : ClientBaseController
    {
        #region Fields
        private readonly AbstractCartService _cartService;
        private readonly AbstractCommonService _commonService;
        private readonly AbstractProductCategoryService _productCategoryService;
        private readonly AbstractAddressService _addressService;
        private readonly AbstractCountryService _abstractCountryService;
        private readonly AbstractCustomerService _customerService;
        private readonly AbstractPromotionalService _AbstractPromotionalService;
        private readonly AbstractOrdersService _orderService;
        private readonly AbstractProductService _productService;
        private readonly AbstractNotificationDao _abstractNotificationDao;

        #endregion

        public cCartController(AbstractCartService cartService,
                                AbstractCommonService commonService,
                                AbstractProductCategoryService productCategoryService,
                                AbstractAddressService abstractAddressService,
                                AbstractCountryService abstractCountryService,
                                AbstractCustomerService customerService,
                                 AbstractPromotionalService AbstractPromotionalService,
                                 AbstractNotificationDao abstractNotificationDao,
                                 AbstractOrdersService orderService,
                                 AbstractProductService productService)
        {
            _cartService = cartService;
            _commonService = commonService;
            _productCategoryService = productCategoryService;
            _addressService = abstractAddressService;
            _abstractCountryService = abstractCountryService;
            _customerService = customerService;
            _AbstractPromotionalService = AbstractPromotionalService;
            _abstractNotificationDao = abstractNotificationDao;
            _orderService = orderService;
            _productService = productService;
        }

        // GET: cCart
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }
            AbstractCommonCount cData = GetcDashboardCountList(ProjectSession.LoginUserID);
            if (cData != null)
            {
                Session["Order"] = cData.OrdersCount;
                Session["wishlist"] = cData.WishlistCount;
                Session["supportticket"] = cData.SupportTicketsCount;
                Session["cart"] = cData.CartCount;
            }
            ViewBag.DDProductCategory = ProductCategoryDropdownList();

            return View();
        }

        [HttpGet]
        [ActionName(Actions.GetCustomerCart)]
        public JsonResult GetCustomerCart()
        {
            List<AbstractCart> items = new List<AbstractCart>();

            try
            {
                items = _cartService.SelectAllByCustomerID(ProjectSession.LoginUserID)?.Values;
                decimal Tax = 0;
                decimal shipping = 0;


                foreach (var t in items)
                {
                    Tax += t.Tax;
                    if (t.Total < t.FreeShippingAbove)
                    {
                        shipping += t.FreeShipping;
                    }
                }
                items[0].TotalShipping = shipping;
                //ViewBag.shippingAmt = shipping;
                ProjectSession.TotalTax = Convert.ToDouble(Tax);
            }
            catch (Exception ex)
            {
                GlobalLogger.Current.Error(ex);
            }

            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName(Actions.GetShippingAmount)]
        public JsonResult GetShippingAmount()
        {
            List<AbstractCart> items = new List<AbstractCart>();
            try
            {
                items = _cartService.GroupCategorybyId(ProjectSession.LoginUserID)?.Values;
            }
            catch (Exception ex)
            {
                GlobalLogger.Current.Error(ex);
            }

            return Json(items, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ActionName(Actions.DeleteCart)]
        public ActionResult DeleteCart(long ID)
        {
            string response = string.Empty;
            try
            {
                response = $"{{\"Result\" : \"0\", \"Message\" : \"{"Item removed successfully."}\"}}";
                _cartService.DeleteByID(ID);
                return Json(response, JsonRequestBehavior.DenyGet);
            }
            catch (Exception)
            {
                response = $"{{\"Result\" : \"-1\", \"Message\" : \"{"Something went wrong!Please try again."}\"}}";
                return Json(response, JsonRequestBehavior.DenyGet);
            }
        }

        public AbstractCommonCount GetcDashboardCountList(long ID)
        {
            try
            {
                //List<AbstractCommonCount> items = new List<AbstractCommonCount>();

                AbstractCommonCount item = _commonService.GetcDashboardCountList(ID);
                Session["Order"] = item.OrdersCount;
                Session["Wishlist"] = item.WishlistCount;
                Session["SupportTicket"] = item.SupportTicketsCount;
                Session["Notification"] = item.NotificationCount;
                Session["cart"] = item.CartCount;


                return item;
            }
            catch (Exception ex)
            {
                GlobalLogger.Current.Error(ex);
            }
            return null;
        }

        private List<SelectListItem> ProductCategoryDropdownList()
        {
            try
            {
                PageParam pageParam = new PageParam()
                {
                    Limit = int.MaxValue,
                    Offset = 0
                };

                var CategoryList = _productCategoryService.SelectAll(pageParam);

                List<SelectListItem> items = new List<SelectListItem>();
                foreach (var category in CategoryList.Values)
                {
                    items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.ID.ToString() });
                }

                return items;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private List<SelectListItem> ProductCountryDropdownList()
        {
            try
            {
                PageParam pageParam = new PageParam()
                {
                    Limit = int.MaxValue,
                    Offset = 0
                };

                var CategoryList = _abstractCountryService.SelectAll(pageParam);

                List<SelectListItem> items = new List<SelectListItem>();
                foreach (var category in CategoryList.Values)
                {
                    items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.ID.ToString() });
                }

                return items;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Checkout


        [HttpGet]
        [ActionName(Actions.CheckoutDetails)]
        public ActionResult CheckoutDetails()
        {
            try
            {
                if (TempData["openPopup"] != null)
                {
                    ViewBag.openPopup = TempData["openPopup"];
                }

                ViewBag.Country = ProductCountryDropdownList();
                ViewBag.DDProductCategory = ProductCategoryDropdownList();
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = "Something went wrong! Please try again after sometime...";
                return RedirectToAction(Actions.Index, TopFarm.Pages.Controllers.cCart);
            }
            return View();
        }

        [HttpPost]
        [ActionName(Actions.CheckoutDetails)]
        public ActionResult CheckoutDetails(string orderComments = "", int ShippingAddress = 0, int BillingAddress = 0, string PromoCode = "", int ShippingMethod = 0, int PaymentMethod = 0)
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }

            ViewBag.Country = ProductCountryDropdownList();
            ViewBag.DDProductCategory = ProductCategoryDropdownList();

            try
            {
                // Delete old checkout details
                _cartService.DeleteCheckoutByCustID(ProjectSession.LoginUserID);
                //var checkoutData = _cartService.SelectCheckoutByCustID(ProjectSession.LoginUserID).Item;

                var checkout = new Checkout()
                {
                    CustomerID = ProjectSession.LoginUserID,
                    Note = orderComments
                };

                //var checkout = new Checkout()
                //{
                //    ID = checkoutData.ID,
                //    CustomerID = ProjectSession.LoginUserID,
                //    Note = orderComments,
                //    ShippingAddress = ShippingAddress == 0 ? checkoutData.ShippingAddress : ShippingAddress,
                //    BillingAddress = BillingAddress == 0 ? checkoutData.BillingAddress : BillingAddress,
                //    PromoCode = !string.IsNullOrEmpty(PromoCode) ? PromoCode : checkoutData.PromoCode,
                //    ShippingMethod = ShippingMethod == 0 ? checkoutData.ShippingMethod : ShippingMethod,
                //    PaymentMethod = PaymentMethod == 0 ? checkoutData.PaymentMethod : PaymentMethod,
                //};

                _cartService.UpsertCheckOut(checkout);
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = "Something went wrong! Please try again after sometime...";
                return RedirectToAction(Actions.Index, TopFarm.Pages.Controllers.cCart);
            }

            return View();
        }

        [HttpPost]
        [ActionName(Actions.AddCheckoutAddress)]
        public ActionResult AddCheckoutAddress(double ShipAmount = 0, int ShippingAddress = 0, int BillingAddress = 0, string PromoCode = "", int ShippingMethod = 0, int PaymentMethod = 0)
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }
            if (ShipAmount > 0)
            {
                ProjectSession.ShipAmount = ShipAmount;
            }

            if (PaymentMethod > 0)
            {
                ProjectSession.PaymentMethod = PaymentMethod;
            }
            ViewBag.Country = ProductCountryDropdownList();
            ViewBag.DDProductCategory = ProductCategoryDropdownList();

            try
            {
                // Delete old checkout details
                var checkoutData = _cartService.SelectCheckoutByCustID(ProjectSession.LoginUserID).Item;
                Session["shippingId"] = checkoutData.ShippingAddress;
                var checkout = new Checkout()
                {
                    ID = checkoutData.ID,
                    CustomerID = ProjectSession.LoginUserID,
                    Note = checkoutData.Note,
                    ShippingAddress = ShippingAddress == 0 ? checkoutData.ShippingAddress : ShippingAddress,
                    BillingAddress = BillingAddress == 0 ? checkoutData.BillingAddress : BillingAddress,
                    PromoCode = !string.IsNullOrEmpty(PromoCode) ? PromoCode : checkoutData.PromoCode,
                    ShippingMethod = ShippingMethod == 0 ? checkoutData.ShippingMethod : ShippingMethod,
                    PaymentMethod = PaymentMethod == 0 ? checkoutData.PaymentMethod : PaymentMethod,
                    ShipAmount = ShipAmount == 0 ? checkoutData.ShipAmount : ShipAmount
                };

                _cartService.UpsertCheckOut(checkout);
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = "Something went wrong! Please try again after sometime...";
                return RedirectToAction(Actions.Index, TopFarm.Pages.Controllers.cCart);
            }

            return RedirectToAction("CheckoutShipping", "cCart");
        }


        [HttpGet]
        public ActionResult CheckoutShipping()
        {
            try
            {
                ViewBag.Country = ProductCountryDropdownList();
                ViewBag.DDProductCategory = ProductCategoryDropdownList();
                var checkoutData = _cartService.SelectCheckoutByCustID(ProjectSession.LoginUserID).Item;

            }
            catch (Exception ex)
            {
                TempData["openPopup"] = "Something went wrong! Please try again after sometime...";
                return RedirectToAction(Actions.Index, TopFarm.Pages.Controllers.cCart);
            }
            return View();
        }

        [HttpGet]
        public ActionResult CheckoutPayment()
        {
            try
            {
                ViewBag.Country = ProductCountryDropdownList();
                ViewBag.DDProductCategory = ProductCategoryDropdownList();
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = "Something went wrong! Please try again after sometime...";
                return RedirectToAction(Actions.Index, TopFarm.Pages.Controllers.cCart);
            }
            return View();
        }

        [HttpGet]
        public ActionResult CheckoutReview()
        {
            if (Session["shippingId"].ToString() != null)
            {
                int id = Convert.ToInt32(Session["shippingId"]);
                var addressData = _addressService.AddressByID(id);
                var customerData = _customerService.Select((int)ProjectSession.LoginUserID).Item;
                ViewBag.Name = customerData.FirstName + "   " + customerData.LastName;
                ViewBag.Address = addressData.Address1;
                ViewBag.Phone = customerData.Phone;
            }
            ViewBag.PaymentMethod = ProjectSession.PaymentMethod == 1 ? true : false;
            ViewBag.OrderId = ProjectSession.strOrderId;

            try
            {
                ViewBag.Country = ProductCountryDropdownList();
                ViewBag.DDProductCategory = ProductCategoryDropdownList();
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = "Something went wrong! Please try again after sometime...";
                return RedirectToAction(Actions.Index, TopFarm.Pages.Controllers.cCart);
            }
            return View();
        }

        [HttpGet]
        public JsonResult SelectCheckoutByCustID()
        {
            try
            {
                var checkoutData = _cartService.SelectCheckoutByCustID(ProjectSession.LoginUserID).Item;
                return Json(checkoutData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult DiscountByPromocode(string promoCode)
        {
            string response = string.Empty;
            try
            {
                var promoCodeData = _AbstractPromotionalService.DiscountByPromocode(promoCode);
                if (promoCodeData.ID > 0 && !string.IsNullOrEmpty(promoCodeData.Discount))
                {
                    return Json(promoCodeData, JsonRequestBehavior.AllowGet);
                }
                return Json("", JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
                //response = $"{{\"Result\" : \"-1\", \"Message\" : \"{ResourceCommon.ErrInsertCategory}\"}}";
                //return Json(response, JsonRequestBehavior.DenyGet);
            }
        }
        #endregion

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.UpdateOrderStatus)]
        public JsonResult UpdateOrderStatus(decimal Amount, int _promoCodeId = 0)
        {
            try
            {
                var checkoutData = _cartService.SelectCheckoutByCustID(ProjectSession.LoginUserID).Item;
                Orders orders = new Orders()
                {
                    PaymentTypes = Convert.ToByte(checkoutData.PaymentMethod),
                    ShipmentAddress = checkoutData.ShippingAddress,
                    CustomerID = ProjectSession.LoginUserID,
                    AssignTo = ProjectSession.LoginUserID,
                    AssignDate = System.DateTime.Now,
                    OrderId = 0,
                    DelFlg = false,
                    CreatedBy = ProjectSession.LoginUserID,
                    Status = 1,                   // ?
                    PromoCodeId = _promoCodeId
                };
                var oData = _orderService.InsertUpdate(orders).Item;
                ProjectSession.OrderId = oData.ID;
                ProjectSession.strOrderId = oData.OrderId.ToString();

                var cartitems = _cartService.SelectAllByCustomerID(ProjectSession.LoginUserID)?.Values;
                foreach (var d in cartitems)
                {
                    var product = _productService.Select(d.ProductId).Item;
                    OrderDetails oDetails = new OrderDetails()
                    {
                        OrderId = oData.ID,
                        ProductId = d.ProductId,
                        Quantity = d.Quantity,
                        Rate = product.OfferPrice > 0 ? product.OfferPrice : product.StandardPrice,
                        //Amount = Amount,
                        CreatedAt = System.DateTime.Now,
                        CreatedBy = ProjectSession.LoginUserID,
                        CreatedRole = ProjectSession.LoginRoleID
                    };
                    var odetailData = _orderService.InsertUpdateOrderDetail(oDetails).Item; // add data in order details table
                    var deleteCartData = _cartService.DeleteByID(d.ID);   // delete from cart table
                    _cartService.DeleteCheckoutByCustID(ProjectSession.LoginUserID);   // delete from checkout table
                    AbstractCommonCount cData = GetcDashboardCountList(ProjectSession.LoginUserID);
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        //[ValidateAntiForgeryToken]
        [ActionName(Actions.CheckoutComplete)]
        public ActionResult CheckoutComplete()
        {
            try
            {
                if (TempData["openPopup"] != null)
                {
                    ViewBag.openPopup = TempData["openPopup"];
                }

                ViewBag.Country = ProductCountryDropdownList();
                ViewBag.DDProductCategory = ProductCategoryDropdownList();
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = "Something went wrong! Please try again after sometime...";
                return RedirectToAction(Actions.Index, TopFarm.Pages.Controllers.cCart);
            }
            return View();
        }

        [HttpGet]
        [ActionName(Actions.OrderTracking)]
        public ActionResult OrderTracking(int id = 0)
        {
            Checkout checkout = new Checkout();
            try
            {
                if (TempData["openPopup"] != null)
                {
                    ViewBag.openPopup = TempData["openPopup"];
                }
                checkout.TaxAmount = Convert.ToDouble(ProjectSession.TotalTax);
                checkout.ShipAmount = Convert.ToDouble(ProjectSession.ShipAmount);

                ViewBag.OrderId = id == 0 ? ProjectSession.OrderId : id;
                ViewBag.Country = ProductCountryDropdownList();
                ViewBag.DDProductCategory = ProductCategoryDropdownList();
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = "Something went wrong! Please try again after sometime...";
                return RedirectToAction(Actions.Index, TopFarm.Pages.Controllers.cCart);
            }
            return View(checkout);
        }


        public JsonResult UpdateOrderNotify()
        {
            try
            {
                AbstractNotification abstractNotification = new Notification();
                abstractNotification.UserID = ProjectSession.LoginUserID;
                abstractNotification.RoleID = ProjectSession.LoginRoleID;
                abstractNotification.Message = "<a onclick=showOrderDetails(" + ProjectSession.OrderId + ")>Your Order Details For " + ProjectSession.strOrderId + " </a>";
                abstractNotification.CreatedBy = ProjectSession.LoginUserID;

                _abstractNotificationDao.InsertUpdate(abstractNotification);
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = "Something went wrong! Please try again after sometime...";
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public JsonResult TrackOrderDetail(int id)
        {
            try
            {
                var orderData = _orderService.TrackOrderDetail(id);

                ShippingMethod u = (ShippingMethod)orderData.ShipMethod;
                OrderStatus o = (OrderStatus)orderData.intStatus;
                //       string strShipMethod = u.ToIntegerString();
                orderData.strStatus = o.ToString();
                orderData.strShipMethod = u.ToString();
                return Json(orderData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = "Something went wrong! Please try again after sometime...";
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }
    }
    //public static class ShippingMethodExtension
    //{
    //    public static string ToIntegerString(this ShippingMethod u)
    //    {
    //        return ((int)u).ToString();
    //    }
    //}
}