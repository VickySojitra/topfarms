﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.Contract;
using TopFarm.Entities.V1;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers.ClientPanel
{
    public class CustomerProfileController : ClientBaseController
    {

        #region Fields
        private readonly AbstractCustomerService _customerService;
        private readonly AbstractAddressService _addressService;
        private readonly AbstractCountryService _abstractCountryService;
        private readonly AbstractProductCategoryService _productCategoryService;

        #endregion

        #region Ctor
        public CustomerProfileController(AbstractCustomerService customerService, AbstractProductCategoryService productCategoryService, AbstractAddressService addressService, AbstractCountryService abstractCountryService)
        {
            this._customerService = customerService;
            this._addressService = addressService;
            this._abstractCountryService = abstractCountryService;
            this._productCategoryService=productCategoryService;
        }
        #endregion

        // GET: CustomerProfile
        public ActionResult Index()
        {
            AbstractCustomer model;
            ViewBag.Country = ProductCountryDropdownList();
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }
            model = _customerService.Select((int)ProjectSession.LoginUserID).Item;
            if (!string.IsNullOrEmpty(model.AvtarURL))
            {
                ViewBag.IsImage = true;
            }
            var adrs = _addressService.SelectByID((int)ProjectSession.LoginUserID);
            ViewBag.DDProductCategory = ProductCategoryDropdown();

            if (adrs != null)
            {
                model.AddressId = adrs.ID;
                model.CustomerId = adrs.CustomerId;
                model.Address1 = adrs.Address1;
                model.Address2 = adrs.Address2;
                model.City = adrs.City;
                model.State = adrs.State;
                model.CountryID = adrs.CountryID;
                model.Pincode = adrs.Pincode;
                model.Area = adrs.Area;
            }
            return View(model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult UpdateProfile(Customer customer, HttpPostedFileBase FileURL)
        {
            try
            {
                customer.CreatedBy = customer.UpdatedBy = ProjectSession.LoginUserID;
                SuccessResult<AbstractCustomer> result = _customerService.InsertUpdate(customer);

                #region Add Address
                // Add product images
                Address address = new Address()
                {
                    ID = customer.AddressId,
                    CustomerId = ProjectSession.LoginUserID,
                    Address1 = customer.Address1,
                    Address2 = customer.Address2,
                    Pincode = customer.Pincode,
                    Area = customer.Area,
                    City = customer.City,
                    State = customer.State,
                    CountryID = customer.CountryID,
                    IsPrimaryAddress = true
                };
                SuccessResult<AbstractAddress> adrsresult = _addressService.Upsert(address);
                #endregion

                if (result?.Item != null)
                {
                    ProjectSession.LoginUserName = result.Item.FirstName + " " + result.Item.LastName;
                    if (FileURL != null)
                    {
                        string profilePath = Server.MapPath("~/" + Path.Combine("Storage", "Customer", result.Item.ID.ToString()));

                        if (Directory.Exists(profilePath) == false)
                        {
                            Directory.CreateDirectory(profilePath);
                        }

                        string URLPath = string.Empty;
                        if (FileURL != null && FileURL.ContentLength > 0)
                        {
                            string fileName = Path.Combine(profilePath, "profile" + Path.GetExtension(FileURL.FileName));
                            FileURL.SaveAs(fileName);

                            result.Item.AvtarURL = Path.Combine("/Storage", "Customer", result.Item.ID.ToString(), "profile" + Path.GetExtension(FileURL.FileName));

                            result = _customerService.InsertUpdate(result.Item);

                            var cookie = Request.Cookies["AdminUserLogin"];
                            if (cookie != null)
                            {
                                cookie.Values["LoginUserImg"] = result.Item.AvtarURL;
                                Response.Cookies.Add(cookie);
                            }
                        }
                    }
                }
                TempData["openPopup"] = "Customer address updated successfully";

                return RedirectToAction("Index", "CustomerProfile");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<SelectListItem> ProductCountryDropdownList()
        {
            try
            {
                PageParam pageParam = new PageParam()
                {
                    Limit = int.MaxValue,
                    Offset = 0
                };

                var CategoryList = _abstractCountryService.SelectAll(pageParam);

                List<SelectListItem> items = new List<SelectListItem>();
                foreach (var category in CategoryList.Values)
                {
                    items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.ID.ToString() });
                }

                return items;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Private methods
        private List<SelectListItem> ProductCategoryDropdown()
        {
            PageParam pageParam = new PageParam()
            {
                Limit = int.MaxValue,
                Offset = 0
            };

            var category = _productCategoryService.SelectAll(pageParam, "");

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var category1 in category.Values)
            {
                items.Add(new SelectListItem() { Text = category1.Name.ToString(), Value = category1.ID.ToString() });
            }

            return items;
        }
        #endregion

        private List<SelectListItem> ProductCategoryDropdownList()
        {
            try
            {
                PageParam pageParam = new PageParam()
                {
                    Limit = int.MaxValue,
                    Offset = 0
                };

                var CategoryList = _productCategoryService.SelectAll(pageParam);

                List<SelectListItem> items = new List<SelectListItem>();
                foreach (var category in CategoryList.Values)
                {
                    items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.ID.ToString() });
                }

                return items;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}