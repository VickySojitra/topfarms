﻿using DataTables.Mvc;
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TopFarm.Common;
using TopFarm.Common.Paging;
using TopFarm.Entities.V1;
using TopFarm.Pages;
using TopFarm.Services.Contract;

namespace TopFarm.Controllers.ClientPanel
{
    public class cOrderController : ClientBaseController
    {
        #region Fields
        private readonly AbstractOrdersService _orderService;
        private readonly AbstractProductCategoryService _productCategoryService;

        #endregion

        #region Ctor
        public cOrderController(AbstractOrdersService orderService, AbstractProductCategoryService productCategoryService)
        {
            this._orderService = orderService;
            this._productCategoryService = productCategoryService;
        }
        #endregion

        // GET: cOrder
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
            {
                ViewBag.openPopup = TempData["openPopup"];
            }
            ViewBag.DDProductCategory = ProductCategoryDropdownList();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.BindOrders)]
        public JsonResult BindOrders([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, int Status = 0)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                var model = _orderService.SelectAll(pageParam, CustomerId: ProjectSession.LoginUserID, Status: Status);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new DataTablesResponse(requestModel.Draw, new List<Orders>(), 0, 0), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult OrderDetailsById(int id)
        {
            try
            {
                var orderdata = _orderService.OrderDetailsById(id);
                TempData["Id"] = orderdata.ID;
                TempData.Keep();
                return Json(orderdata, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
     
        public FileStreamResult GenerateOrder1()
        {
            int orderid = Convert.ToInt32(TempData["Id"]);
            MemoryStream workStream = new MemoryStream();

            //if (orderid > 0)
            //{
                var orderdata = _orderService.OrderDetailsById(orderid);
                Document doc = new Document();
                PdfPTable tableLayout = new PdfPTable(5);


                Document document = new Document();
                PdfWriter.GetInstance(document, workStream).CloseStream = false;
                StringBuilder sb = new StringBuilder();
                #region html
                sb.Append("<table width='100%' cellspacing='0' cellpadding='2'>");
                sb.Append("<tr><td align='center' style='background-color: #18B5F0' colspan = '2'><b>Invoice</b></td></tr>");
                sb.Append("<tr><td colspan = '2'></td></tr>");
                sb.Append("<tr><td><b>Order No: </b>");
                sb.Append(orderdata.ID);
                sb.Append("</td><td align = 'right'><b>Date: </b>");
                sb.Append(orderdata.OrderDate);
                sb.Append(" </td></tr>");
                sb.Append("<tr><td colspan = '2'><b>Shipping address : </b>");
                sb.Append(orderdata.Address1);
                sb.Append("</td></tr>");
                sb.Append("</table>");
                int count = 1;
                sb.Append("<table width='100%' cellspacing='0' cellpadding='2' border='1'>");
            sb.Append("<tr><td align='center' style='background-color: #18B5F0; width: 10px;' colspan = '2'><b>No.</b></td><td align='center' style='background-color: red; width: 90px;' colspan = '2'><b>Title</b></td></tr>");
                    //+
                    //     "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>Title</b></td>" +
                    //     "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>Rate</b></td>" +
                    //     "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>Quantity</b></td>" +
                    //     "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>Amount</b></td>" +

                //     "</tr>");
                sb.Append("</table>");
                sb.Append("<table width='100%' cellspacing='0' cellpadding='2' border='1'>");
                foreach (var d in orderdata.listProduct)
                {
                    sb.Append("<tr><td align='center' style='background-color: #18B5F0' colspan = '2'><b> " + count + "</b></td>" +
                        //"<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + d.ProductName + "</b></td>" +
                        "<td align='center' style='border-left: 1px solid black; background-color: #18B5F0;' colspan = '2'><b>" + d.ProductName + "</b></td>" +
                        "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + d.Rate + "</b></td>" +
                        "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + d.Quantity + "</b></td>" +
                        "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + d.Amount + "</b></td>" +

                        "</tr>");
                }
                sb.Append("<tr><td align='center' style='background-color: #18B5F0' colspan = '2'><b>Total Amount</b></td>" +
                        "<td align='center' style='background-color: #18B5F0' colspan = '2'><b></b></td>" +
                        "<td align='center' style='background-color: #18B5F0' colspan = '2'><b></b></td>" +
                        "<td align='center' style='background-color: #18B5F0' colspan = '2'><b></b></td>" +
                        "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + orderdata.TotalAmount + "</b></td>" +

                        "</tr>");
                sb.Append("</table>");

                sb.Append("<br />");
                #endregion
                StringReader sr = new StringReader(sb.ToString());
                HTMLWorker htmlparser = new HTMLWorker(document);
                PdfWriter writer = PdfWriter.GetInstance(document, Response.OutputStream);
                document.Open();


                htmlparser.Parse(sr);

                document.Close();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=Invoice_" + 1 + ".pdf");
                Response.Flush();
                byte[] byteInfo = workStream.ToArray();
                workStream.Write(byteInfo, 0, byteInfo.Length);
                workStream.Position = 0;
            TempData.Keep();
                return new FileStreamResult(workStream, "application/pdf");
          
        }

        public FileStreamResult GenerateOrder()
        {
            int orderid = Convert.ToInt32(TempData["Id"]);
            MemoryStream workStream = new MemoryStream();

            //if (orderid > 0)
            //{
            var orderdata = _orderService.OrderDetailsById(orderid);
            Document doc = new Document();
            PdfPTable tableLayout = new PdfPTable(5);


            Document document = new Document();
            PdfWriter.GetInstance(document, workStream).CloseStream = false;
            StringBuilder sb = new StringBuilder();
            #region html
            sb.Append("<table width='100%' cellspacing='0' cellpadding='2'>");
            sb.Append("<tr><td align='center' colspan = '2'><b>Invoice</b></td></tr>");
            sb.Append("<tr><td colspan = '2'></td></tr>");
            sb.Append("<tr><td><b>Order No: </b>");
            sb.Append(orderdata.ID);
            //sb.Append("<td>Shipping address");
            //sb.Append(orderdata.Address1);
            //sb.Append("</td></tr>");

            sb.Append("</td><td align = 'right'><b>Date: </b>");
            sb.Append(orderdata.OrderDate);
            sb.Append(" </td></tr>");
            sb.Append("<tr><td colspan = '2'><b>Name : </b>");
            sb.Append(orderdata.FirstName);
            sb.Append("</td></tr>");
            sb.Append("<tr><td colspan = '2'><b>Email : </b>");
            sb.Append(orderdata.Email);
            sb.Append("</td></tr>");
            sb.Append("<tr><td colspan = '2'><b>Shipping address : </b>");
            sb.Append(orderdata.Address1);
            sb.Append("</td></tr>");
            sb.Append("</table>");
            int count = 1;
            sb.Append("<table width='100%' cellspacing='0' cellpadding='2' border='1'>");
            sb.Append("<tr><td align='center' width = '10%' colspan = '2'><b>No.</b></td><td align='center' colspan = '2'><b>Title</b></td><td align='center' style='background-color: #18B5F0' colspan = '2'><b>Rate</b></td><td align='center' style='background-color: #18B5F0' colspan = '2'><b>Quantity</b></td><td align='center' style='background-color: #18B5F0' colspan = '2'><b>Amount</b></td></tr>");
            //+
            //     "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>Title</b></td>" +
            //     "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>Rate</b></td>" +
            //     "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>Quantity</b></td>" +
            //     "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>Amount</b></td>" +

            //     "</tr>");
            sb.Append("</table>");
            sb.Append("<table width='100%' cellspacing='0' cellpadding='2' border='1'>");
            foreach (var d in orderdata.listProduct)
            {
                sb.Append("<tr><td align='center' style='background-color: #18B5F0' colspan = '2'><b> " + count + "</b></td>" +
                    //"<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + d.ProductName + "</b></td>" +
                    "<td align='center' style='border-left: 1px solid black; background-color: #18B5F0;' colspan = '2'><b>" + d.ProductName + "</b></td>" +
                    "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + d.Rate + "</b></td>" +
                    "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + d.Quantity + "</b></td>" +
                    "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + d.Amount + "</b></td>" +

                    "</tr>");
            }
            sb.Append("<tr><td align='center' style='background-color: #18B5F0' colspan = '2'><b>Total Amount</b></td>" +
                    "<td align='center' style='background-color: #18B5F0' colspan = '2'><b></b></td>" +
                    "<td align='center' style='background-color: #18B5F0' colspan = '2'><b></b></td>" +
                    "<td align='center' style='background-color: #18B5F0' colspan = '2'><b></b></td>" +
                    "<td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + orderdata.TotalAmount + "</b></td>" +

                    "</tr>");
            sb.Append("</table>");

            sb.Append("<br />");
            #endregion

            StyleSheet styles = new StyleSheet();
            styles.LoadStyle("#headerdiv", HtmlTags.COLOR, "Blue");

            //styles.LoadTagStyle(HtmlTags.TD, HtmlTags.COLOR, "#ff0000");
            using (Document document1 = new Document())
            {
                PdfWriter.GetInstance(document1, Response.OutputStream);
                document1.Open();
                List<IElement> objects = HTMLWorker.ParseToList(
                  new StringReader(sb.ToString()), styles
                );
                foreach (IElement element in objects)
                {
                    document1.Add(element);
                }
                document1.Close();
            }

            //StringReader sr = new StringReader(sb.ToString());
            //HTMLWorker htmlparser = new HTMLWorker(document);
            //PdfWriter writer = PdfWriter.GetInstance(document, Response.OutputStream);
            //document.Open();


            //htmlparser.Parse(sr);

            //document.Close();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Invoice_" + 12 + ".pdf");
            Response.Flush();
            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;
            TempData.Keep();
            return new FileStreamResult(workStream, "application/pdf");

        }
        private List<SelectListItem> ProductCategoryDropdownList()
        {
            try
            {
                PageParam pageParam = new PageParam()
                {
                    Limit = int.MaxValue,
                    Offset = 0
                };

                var CategoryList = _productCategoryService.SelectAll(pageParam);

                List<SelectListItem> items = new List<SelectListItem>();
                foreach (var category in CategoryList.Values)
                {
                    items.Add(new SelectListItem() { Text = category.Name.ToString(), Value = category.ID.ToString() });
                }

                return items;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}