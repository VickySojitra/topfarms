﻿namespace TopFarm.Pages
{
    public class Controllers
    {
        #region Admin
        public const string Customers = "Customers";
        public const string Traders = "Traders";
        public const string Product = "Product";
        public const string ProductCategory = "ProductCategory";
        public const string Shipping = "Shipping";
        public const string Order = "Order";
        public const string Blog = "Blog";
        public const string DailyReport = "DailyReport";
        public const string SupportTicket = "SupportTicket";
        public const string Promotional = "Promotional";
        public const string Account = "Account";
        public const string AdminPanel = "AdminPanel";
        public const string Dashboard = "Dashboard";
        public const string Notification = "Notification";
        public const string Transaction = "Transaction";
        public const string Report = "Report";
        public const string RoleRights = "RoleRights";
        public const string AdminProfile = "AdminProfile";
        public const string CustomerProfile = "CustomerProfile";
        public const string CustomerAddress = "CustomerAddress";
        public const string TraderProfile = "TraderProfile";

        
        #endregion

        #region Client
        public const string Home = "Home";
        public const string cProduct = "cProduct";
        public const string cWishlist = "cWishlist";
        public const string cCart = "cCart";
        public const string cSupportTicket = "cSupportTicket";
        public const string cOrder = "cOrder";
        public const string Help = "Help";
        #endregion

        #region Traders
        public const string TDashboard = "TDashboard";
        public const string TOrder = "TOrder";
        public const string TNotification = "TNotification";
        public const string TDailyReport = "TDailyReport";
        #endregion
    }
}